﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace PROJECT.Application.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException() : base("One or more validation failures have occurred.")
        {
            Errors = new List<ValidationFailure>();
        }

        public ValidationException(IEnumerable<ValidationFailure> failures)
            : this()
        {
            Errors = failures.ToList();
        }

        public List<ValidationFailure> Errors { get; }
    }
}