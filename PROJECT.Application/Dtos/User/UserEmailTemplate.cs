﻿namespace PROJECT.Application.Dtos.User
{
    public class UserEmailTemplate
    {
        public string Email { get; set; }
    }
}