﻿namespace PROJECT.Application.Dtos.Drive
{
    public class DriveCreateFolderResult
    {
        public string Path { get; set; }

        public string Name { get; set; }
    }
}