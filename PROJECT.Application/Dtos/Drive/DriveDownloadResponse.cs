﻿using System.IO;

namespace PROJECT.Application.Dtos.Drive
{
    public class DriveDownloadResponse
    {
        public string FileName { get; set; }

        public Stream Content { get; set; }
    }
}