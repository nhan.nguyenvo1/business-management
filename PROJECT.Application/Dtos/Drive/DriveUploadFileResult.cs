﻿namespace PROJECT.Application.Dtos.Drive
{
    public class DriveUploadFileResult
    {
        public string FileName { get; set; }

        public string Path { get; set; }
        public ulong FileSize { get; set; }
    }
}