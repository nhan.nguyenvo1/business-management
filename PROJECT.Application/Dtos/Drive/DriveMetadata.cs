﻿namespace PROJECT.Application.Dtos.Drive
{
    public class DriveMetadata
    {
        public bool IsFile { get; set; }

        public DriveFileMetadata AsFile { get; set; }

        public bool IsFolder { get; set; }

        public DriveFolderMetadata AsFolder { get; set; }

        public bool IsDeleted { get; set; }

        public DriveDeletedMetadata AsDeleted { get; set; }

        public string Name { get; set; }

        public string Path { get; set; }
    }
}