﻿using PROJECT.Domain.Enums;

namespace PROJECT.Application.Dtos.Company
{
    public class CompanyStatusVm
    {
        public int Id { get; set; }


        public CompanyStatus Status { get; set; }

        public string Reason { get; set; }
    }
}