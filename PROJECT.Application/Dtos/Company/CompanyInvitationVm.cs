﻿using System;
using PROJECT.Application.Features.Employees.Commands.CreateEmployee;

namespace PROJECT.Application.Dtos.Company
{
    public class CompanyInvitationVm
    {
        public string Email { get; set; }

        public int CompanyId { get; set; }

        public DateTime? Expired { get; set; }

        public CreateEmployeeCommand More { get; set; }
    }
}