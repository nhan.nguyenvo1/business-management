﻿using System.ComponentModel.DataAnnotations;

namespace PROJECT.Application.Dtos.Account
{
    public class VerifyEmailRequest
    {
        [Required] [EmailAddress] public string Email { get; set; }

        [Required] public string Token { get; set; }
    }
}