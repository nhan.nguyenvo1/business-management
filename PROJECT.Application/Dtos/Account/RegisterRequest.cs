﻿using System;
using System.ComponentModel.DataAnnotations;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Dtos.Account
{
    public class RegisterRequest
    {
        [Required] public string UserName { get; set; }

        [Required] public string Email { get; set; }

        [Required] public string PhoneNumber { get; set; }

        [Required] public string FirstName { get; set; }

        [Required] public string LastName { get; set; }

        public UserGender Gender { get; set; }

        public DateTime Dob { get; set; }

        [Required] public string Password { get; set; }

        [Required] [Compare("Password")] public string ConfirmPassword { get; set; }
    }
}