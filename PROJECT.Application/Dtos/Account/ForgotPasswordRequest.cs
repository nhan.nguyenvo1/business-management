﻿using System.ComponentModel.DataAnnotations;

namespace PROJECT.Application.Dtos.Account
{
    public class ForgotPasswordRequest
    {
        [Required] [EmailAddress] public string Email { get; set; }
    }
}