﻿using System.ComponentModel.DataAnnotations;

namespace PROJECT.Application.Dtos.Account
{
    public class RevokeTokenRequest
    {
        [Required] public string AccessToken { get; set; }

        [Required] public string RefreshToken { get; set; }
    }
}