﻿namespace PROJECT.Application.Dtos.Account
{
    public class AuthResponse
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}