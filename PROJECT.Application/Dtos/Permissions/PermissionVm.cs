﻿namespace PROJECT.Application.Dtos.Permissions
{
    public class PermissionVm
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
