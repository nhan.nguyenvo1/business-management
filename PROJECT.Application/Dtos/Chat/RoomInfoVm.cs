﻿using System.Collections.Generic;

namespace PROJECT.Application.Dtos.Chat
{
    public class RoomInfoVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string AvatarUrl { get; set; }

        public int Page { get; set; }

        public int Limit { get; set; }

        public int Total { get; set; }

        public ICollection<MessageVm> Messages { get; set; }
    }
}