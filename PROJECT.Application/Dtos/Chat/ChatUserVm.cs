﻿namespace PROJECT.Application.Dtos.Chat
{
    public class ChatUserVm
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string AvatarUrl { get; set; }
    }
}