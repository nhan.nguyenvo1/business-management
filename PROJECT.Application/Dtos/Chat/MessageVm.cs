﻿using System;

namespace PROJECT.Application.Dtos.Chat
{
    public class MessageVm
    {
        public int Id { get; set; }

        public int RoomId { get; set; }

        public string Content { get; set; }

        public ChatUserVm Sender { get; set; }

        public DateTime When { get; set; }

        public string Attachment { get; set; }

        public string MessageType { get; set; }

        public bool Me { get; set; }
    }
}