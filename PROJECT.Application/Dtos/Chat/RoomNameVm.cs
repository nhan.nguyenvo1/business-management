﻿namespace PROJECT.Application.Dtos.Chat
{
    public class RoomNameVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}