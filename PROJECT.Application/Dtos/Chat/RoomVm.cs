﻿using PROJECT.Domain.Enums;

namespace PROJECT.Application.Dtos.Chat
{
    public class RoomVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string AvatarUrl { get; set; }

        public bool IsRead { get; set; }

        public RoomType RoomType { get; set; }
    }
}