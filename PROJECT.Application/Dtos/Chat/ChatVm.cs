﻿using System.Collections.Generic;

namespace PROJECT.Application.Dtos.Chat
{
    public class ChatVm
    {
        public int Page { get; set; }

        public int Limit { get; set; }

        public int Total { get; set; }

        public ICollection<MessageVm> Messages { get; set; }
    }
}