﻿namespace PROJECT.Application.Dtos.Cloudinary
{
    public class CloudinaryResponse
    {
        public CloudinaryResponse(string publicId, string url)
        {
            PublicId = publicId;
            Url = url;
        }

        public string PublicId { get; }

        public string Url { get; }
    }
}