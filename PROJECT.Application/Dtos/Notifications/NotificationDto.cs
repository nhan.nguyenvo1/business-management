﻿using System;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Dtos.Notifications
{
    public class NotificationDto
    {
        public int Id { get; set; }

        public NotificationCaterogy Category { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public bool Read { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}