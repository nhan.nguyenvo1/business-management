﻿namespace PROJECT.Application.EmailTemplate
{
    public class RemoveCompanyTemplate
    {
        public const string Body =
            "<table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\""
            + "style=\"max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);\">"
            + "<tr>"
            + "<td style =\"height:40px;\">&nbsp;</td>"
            + "</tr>"
            + "<tr>"
            + "<td style =\"padding:0 35px;\">"
            + "<h1 style =\"color:#424252; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;\">"
            + "Welcome to easy business"
            + "</h1>"
            + "<span style =\"display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;\"></span>"
            + "<p style =\"color:#455056; font-size:15px;line-height:24px; margin:0;\">"
            + "Hey {0}!"
            + "</p>"
            + "<p style =\"color:#455056; font-size:15px;line-height:24px; margin:0;\">"
            + "You have just been removed from the {1} by {2}."
            + "</p>"
            + "<a href =\"{3}\""
            + "style=\"background:#20e277;text-decoration:none !important; font-weight:500; margin-top:35px; color:#fff;text-transform:uppercase; font-size:14px;padding:10px 24px;display:inline-block;border-radius:50px;\">"
            + "Back To Web Site"
            + "</a>"
            + "</td>"
            + "</tr>"
            + "<tr>"
            + "<td style =\"height:40px;\">&nbsp;</td>"
            + "</tr>"
            + "</table>";
    }
}