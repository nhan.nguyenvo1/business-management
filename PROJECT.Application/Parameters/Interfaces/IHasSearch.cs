﻿namespace PROJECT.Application.Parameters.Interfaces
{
    public interface IHasSearch
    {
        string Search { get; set; }
    }
}