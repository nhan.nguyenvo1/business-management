﻿using System.Collections.Generic;

namespace PROJECT.Application.Parameters.Interfaces
{
    public interface IHasFilters
    {
        Dictionary<string, string> Filters { get; set; }
    }
}