﻿namespace PROJECT.Application.Constants
{
    public static class ErrorMessageConstant
    {
        public const string InvalidEmail = "Incorrect email format";

        public const string InvalidPhoneNumber = "Incorrect phone number format";

        public const string NotFound = "{0} with {1} = {2} does not exists";

        public const string Duplicated = "{0} with {1} = {2} already exists";

        public const string Required = "{0} can not be empty";

        public const string MaxLength = "{0} must not exceed {1} characters";

        public const string InvalidFile = "{0} must be {1}";

        public const string LengthRequired = "The request must have at least {0} {1}";

        public const string OverDate = "{0} must be more than the current date";

        public const string OrRequired = "{0} or {1} must be non-null in pairs";

        public const string Ambiguous = "This request is ambigous";

        public const string TimeError = "You already {0} today";

        public const string CheckInError = "You are not check in";
    }
}