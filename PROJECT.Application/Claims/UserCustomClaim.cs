﻿using PROJECT.Application.BCL;
using System.Security.Claims;

namespace PROJECT.Application.Claims
{
    /// <summary>
    /// Authorization claim for giving access by user id.
    /// </summary>
    public class UserCustomClaim
    {
        private readonly string userId;

        /// <summary>
        /// Create a new instance of <see cref="UserCustomClaim"/>
        /// </summary>
        /// <param name="userId"></param>
        public UserCustomClaim(string userId)
        {
            this.userId = userId;
        }

        /// <summary>
        /// Claim
        /// </summary>
        /// <param name="claim"></param>
        public static implicit operator Claim(UserCustomClaim claim)
        {
            Preconditions.NotNull(claim, nameof(claim));

            return new Claim(nameof(UserCustomClaim), $"{claim.userId}");
        }
    }
}
