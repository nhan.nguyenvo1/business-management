﻿using PROJECT.Application.BCL;
using System.Security.Claims;

namespace PROJECT.Application.Claims
{
    /// <summary>
    /// Authorization claim for giving access by user id.
    /// </summary>
    public class ClientCustomClaim
    {
        private readonly int companyId;

        /// <summary>
        /// Create a new instance of <see cref="ClientCustomClaim"/>
        /// </summary>
        /// <param name="companyId"></param>
        public ClientCustomClaim(int companyId)
        {
            this.companyId = companyId;
        }

        /// <summary>
        /// Claim
        /// </summary>
        /// <param name="claim"></param>
        public static implicit operator Claim(ClientCustomClaim claim)
        {
            Preconditions.NotNull(claim, nameof(claim));

            return new Claim(nameof(ClientCustomClaim), $"{claim.companyId}");
        }
    }
}
