﻿using System.Collections.Generic;

namespace PROJECT.Application.Wrappers
{
    public class PagedResponse<T> : Response<ICollection<T>>
    {
        public PagedResponse(ICollection<T> data, int page, int limit, int total)
        {
            Total = total;
            Page = page;
            Limit = limit;
            Data = data;
            Message = null;
            Succeeded = true;
            Errors = null;
        }

        public int Total { get; set; }

        public int Page { get; set; }

        public int Limit { get; set; }
    }
}