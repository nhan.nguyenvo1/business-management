﻿using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace PROJECT.Application.ValidatorExtensions
{
    public class IFormFileValidator : AbstractValidator<IFormFile>
    {
        public IFormFileValidator()
        {
            RuleFor(p => p.Length)
                .GreaterThan(0);

            RuleFor(p => p).NotNull();
        }
    }
}