﻿using System.IO;
using System.Threading.Tasks;
using PROJECT.Application.Dtos.Cloudinary;

namespace PROJECT.Application.Interfaces
{
    public interface ICloudinaryService
    {
        Task<CloudinaryResponse> UploadAsync(string name, Stream stream, bool useFileName, string folder = "");

        Task RemoveAsync(string path);

        string GetImage(string path, string extension = "jpg");
    }
}