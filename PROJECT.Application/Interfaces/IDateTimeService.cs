﻿using System;

namespace PROJECT.Application.Interfaces
{
    public interface IDateTimeService
    {
        DateTime UtcNow { get; }
    }
}