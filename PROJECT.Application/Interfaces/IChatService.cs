﻿using System.Threading.Tasks;

namespace PROJECT.Application.Interfaces
{
    public interface IChatService
    {
        Task GetAvailableRooms(int page, int limit, string search);

        Task GetOwnRooms(int page, int limit, string search);

        Task CreateRoom(string roomName, string password = "");

        Task JoinRoom(int roomId, string password = "");

        Task InviteUserToRoom(string userId, int roomId);

        Task GetRoomInfo(int roomId, int page, int limit);

        Task PostNotification(int roomId, string imageUrl, string source, string content);

        Task Typing(int roomId, bool flag);

        Task LeaveRoom(int roomId);

        Task KickUser(string targetId, int roomId, string callingId, string reason);

        Task BroadcastMessage(string userId, string message);

        Task SendMessage(string message, int roomId);
    }
}