﻿using System.Threading.Tasks;

namespace PROJECT.Application.Interfaces
{
    public interface ICryptographyService
    {
        Task<string> Encrypt<T>(T o);

        Task<T> Decrypt<T>(string cipherText);
    }
}