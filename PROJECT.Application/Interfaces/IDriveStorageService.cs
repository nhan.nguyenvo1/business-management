﻿using System.IO;
using System.Threading.Tasks;
using PROJECT.Application.Dtos.Drive;

namespace PROJECT.Application.Interfaces
{
    public interface IDriveStorageService
    {
        Task<DriveCreateFolderResult> CreateFolderAsync(string path);

        Task<DriveUploadFileResult> UploadFileAsync(string path, Stream body, bool replaced = false);

        Task<bool> PathExists(string path);

        Task<DriveDownloadResponse> DownloadAsync(string path);

        Task<DriveDownloadResponse> DownloadZipAsync(string path);

        Task<DriveMetadata> CopyAsync(string src, string des);

        Task<DriveMetadata> MoveAsync(string src, string des);

        Task ChangeName(string path, string newName);

        Task DeleteAsync(string path);
    }
}