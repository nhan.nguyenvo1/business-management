﻿namespace PROJECT.Application.Interfaces
{
    public interface IHubService
    {
        /// <summary>
        /// Hub connection id
        /// </summary>
        string ConnectionId { get; }
    }
}
