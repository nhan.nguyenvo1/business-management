﻿namespace PROJECT.Application.Interfaces
{
    public interface ICompanyService
    {
        int? CompanyId { get; }
    }
}