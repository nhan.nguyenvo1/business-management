﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IUserRepository : IGenericRepository<AppUser, string>
    {
        Task<AppUser> UpdateAvatarAsync(string userId, IFormFile avatar);

        Task<AppUser> AssignRoleAsync(AppUser appUser, ICollection<string> roleNames);

        Task<bool> IsInCompanyAsync(int companyId);

        Task<AppUser> CancleRolesAsync(AppUser appUser, ICollection<string> roleNames);
    }
}