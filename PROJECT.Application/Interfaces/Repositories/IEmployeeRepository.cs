﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IEmployeeRepository : IGenericRepository<AppUser, string>
    {
        Task<bool> ExistAsync(string usernameOrEmail);

        Task<IReadOnlyList<AppUser>> GetPagedResponseAsync(int page, int limit, string search, string sort,
            string order, Dictionary<string, string> filters);

        Task<int> CountAsync(string search, Dictionary<string, string> filters);

        Task<IReadOnlyList<AppUser>> GetByDepartmentIdAsync(Department department, int page,
            int limit, string search, string sort, string order,
            Dictionary<string, string> filters);

        Task<int> CountByDepartmentIdAsync(Department department, string search,
            Dictionary<string, string> filters);
    }
}