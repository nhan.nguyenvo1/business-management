﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface ITaskAttachmentRepository : IGenericRepository<TaskAttachment, int>
    {
    }
}