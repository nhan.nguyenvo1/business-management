﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IStorageRepository : IGenericRepository<AppStorage, int>
    {
    }
}