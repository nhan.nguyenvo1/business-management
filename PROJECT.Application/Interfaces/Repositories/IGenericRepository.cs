﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IGenericRepository<T, TKey> where T : class
    {
        Task<IReadOnlyList<TViewModel>> GetPagedResponseAsync<TViewModel>(int page, int limit);

        Task<IReadOnlyList<TViewModel>> GetAllAsync<TViewModel>(bool ignoreQueryFilters = false);

        Task<T> GetByIdAsync(TKey id);

        Task<TViewModel> GetByIdAsync<TViewModel>(TKey id, bool ignoreQueryFilters = false);

        Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate, bool ignoreQueryFilters = false);

        Task<int> CountAsync(Expression<Func<T, bool>> predicate);

        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate, bool ignoreQueryFilters = false);

        Task<TViewModel> GetSingleAsync<TViewModel>(Expression<Func<T, bool>> predicate);

        Task<IReadOnlyList<T>> GetAllAsync();

        Task<IReadOnlyList<T>> GetPagedResponseAsync(int page, int limit);

        Task<T> AddAsync(T entity);

        Task<T> FindSingle(Expression<Func<T, bool>> predicate);

        Task AddRangeAsync(ICollection<T> entities);

        Task<T> UpdateAsync(T entity);

        Task DeleteAsync(T entity);

        Task DeleteAsync(TKey id);

        Task DeleteByConditionAsync(Expression<Func<T, bool>> predicate);


        Task<IReadOnlyList<TViewModel>> GetByConditionAsync<TViewModel>(Expression<Func<T, bool>> predicate);
    }
}