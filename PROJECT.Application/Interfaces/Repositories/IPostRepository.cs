﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IPostRepository : IGenericRepository<Post, int>
    {
        Task<ICollection<TViewModel>> GetPagedResponseAsync<TViewModel>(int page, int limit, string search, string sort,
            string order, Dictionary<string, string> filters, bool isRaw = true, bool personal = false);

        Task<int> CountAsync(string search, Dictionary<string, string> filters, bool isRaw = true,
            bool personal = false);
    }
}