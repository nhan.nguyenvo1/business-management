﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IAppFileRepository : IGenericRepository<AppFile, int>
    {
        Task AddChildrenAsync(int desId, int srcId);

        Task<bool> AlreadyShared(int driveId, string userId);

        Task AddToSharedAsync(AppFile drive, string userId, bool canAdd, bool canRead, bool canWrite, bool root = true);

        Task<IReadOnlyList<TViewModel>> GetSharedAsync<TViewModel>(int? folderId, string userId);

        Task<TViewModel> GetSharedByIdAsync<TViewModel>(int driveId, string userId);

        Task<IReadOnlyList<TViewModel>> GetDeletedAsync<TViewModel>(string userId);

        Task<AppFile> GetDeletedByIdAsync(int driveId, string userId);

        Task<IReadOnlyCollection<T>> GetRecursive<T>(string userId);
    }
}