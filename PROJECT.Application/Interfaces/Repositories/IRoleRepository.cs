﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IRoleRepository : IGenericRepository<AppRole, string>
    {
        Task<IReadOnlyList<AppRole>> GetPagedResponseAsync(int page, int limit, string search, string sort,
            string order, Dictionary<string, string> filters);

        Task<int> CountAsync(string search, Dictionary<string, string> filters);

        Task<bool> AssignPermissions(AppRole appRole, IEnumerable<string> requestPermissions);

        Task<bool> CancelPermissions(AppRole appRole, ICollection<string> requestPermissions);

        Task<ICollection<string>> GetByUserIdAsync(string userId);
    }
}