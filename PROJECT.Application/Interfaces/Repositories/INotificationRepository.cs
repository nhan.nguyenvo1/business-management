﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface INotificationRepository : IGenericRepository<Notification, int>
    {
        Task<IReadOnlyList<T>> GetByUserIdAsync<T>(string userId);

        Task MarkAsReadAsync(string userId, int notificationId);

        Task MarkAllAsync(string userId, int companyId, bool read);
    }
}