﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IPackageRequestRepository : IGenericRepository<PackageRequest, int>
    {
    }
}