﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface ICompanyRepository : IGenericRepository<Company, int>
    {
        Task<ICollection<TViewModel>> GetPagedResponseAsync<TViewModel>(int page, int limit, string search);

        Task AddUserAsync(int companyId, int departmentId, string userId);
    }
}