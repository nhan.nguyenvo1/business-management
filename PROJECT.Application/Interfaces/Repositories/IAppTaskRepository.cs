﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IAppTaskRepository : IGenericRepository<AppTask, int>
    {
        Task<IReadOnlyList<TViewModel>> GetByUserIdAsync<TViewModel>(string userId);

        Task<ICollection<TViewModel>> GetPagedResponseAsync<TViewModel>(int page, int limit, string search, string sort,
            string order,
            Dictionary<string, string> filters);

        Task<int> CountAsync(string search, Dictionary<string, string> filters);

        Task<ICollection<TViewModel>> GetPersonalPagedResponseAsync<TViewModel>(string userId, int page, int limit,
            string search,
            string sort,
            string order,
            Dictionary<string, string> filters);

        Task<int> CountPersonalAsync(string userId, string search, Dictionary<string, string> filters);
    }
}