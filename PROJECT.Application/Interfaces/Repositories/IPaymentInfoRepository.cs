﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IPaymentInfoRepository : IGenericRepository<PaymentInfo, int>
    {
        
    }
}
