﻿using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface ICompanyDriveRepository : IGenericRepository<CompanyDrive, int>
    {
        Task AddChildrenAsync(int desId, int srcId);
    }
}