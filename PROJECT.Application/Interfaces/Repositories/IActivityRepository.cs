﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IActivityRepository : IGenericRepository<Activity, int>
    {
        
    }
}
