﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IDepartmentRepository : IGenericRepository<Department, int>
    {
        Task<List<Department>> GetAsync();
        new Task DeleteAsync(int id);

        Task<bool> ExistAsync(int departmentId);

        Task<int> CountAsync(string search, Dictionary<string, string> filters);
    }
}