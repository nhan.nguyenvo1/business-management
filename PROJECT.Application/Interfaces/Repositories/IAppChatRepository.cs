﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IAppChatRepository : IGenericRepository<AppChat, int>
    {
        Task<ICollection<TViewModel>> GetChatListAsync<TViewModel>(string userId, int page, int limit, string search);

        Task<ICollection<TViewModel>> GetByRoomId<TViewModel>(int roomId, int page, int limit);
    }
}