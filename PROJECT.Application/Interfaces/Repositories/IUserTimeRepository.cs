﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IUserTimeRepository : IGenericRepository<UserTime, int>
    {
        Task<ICollection<TViewModel>> GetPagingAsync<TViewModel>(int page, int limit, string search);

        Task<int> CountAsync(string search);
    }
}