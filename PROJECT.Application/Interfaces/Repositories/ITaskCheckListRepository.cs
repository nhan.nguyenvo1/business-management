﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface ITaskCheckListRepository : IGenericRepository<TaskCheckList, int>
    {
    }
}