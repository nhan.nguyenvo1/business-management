﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IContactRepository : IGenericRepository<Contact, int>
    {
        
    }
}
