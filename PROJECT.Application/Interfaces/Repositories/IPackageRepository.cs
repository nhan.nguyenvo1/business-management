﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IPackageRepository : IGenericRepository<Package, int>
    {
    }
}