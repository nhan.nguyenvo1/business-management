﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IPostAttachmentRepository : IGenericRepository<PostAttachment, int>
    {
    }
}