﻿using System.Linq;
using System.Threading.Tasks;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IAppChatRoomRepository : IGenericRepository<AppRoom, int>
    {
        Task<IQueryable<AppRoom>> GetAvailableByUserId(string userId);

        Task<IQueryable<AppRoom>> GetOwnByUserId(string userId, int page, int limit, string search);
    }
}