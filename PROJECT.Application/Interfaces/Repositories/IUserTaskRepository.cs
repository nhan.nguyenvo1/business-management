﻿using PROJECT.Domain.Entities;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IUserTaskRepository : IGenericRepository<UserTask, int>
    {
        
    }
}
