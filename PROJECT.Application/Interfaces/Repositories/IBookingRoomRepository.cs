﻿using PROJECT.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PROJECT.Application.Interfaces.Repositories
{
    public interface IBookingRoomRepository : IGenericRepository<BookingRoom, int>
    {
        Task AddUserToRoomAsync(int roomId, ICollection<string> userIds);
    }
}