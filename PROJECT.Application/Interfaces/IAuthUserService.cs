﻿namespace PROJECT.Application.Interfaces
{
    public interface IAuthUserService
    {
        string UserId { get; }
    }
}