﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace PROJECT.Application.Interfaces
{
    public interface IUnitOfWork
    {
        Task CommitAsync();
        Task<IDbContextTransaction> BeginTransactionAsync();
    }
}