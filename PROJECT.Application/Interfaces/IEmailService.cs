﻿using System.Threading.Tasks;
using PROJECT.Application.Dtos.Email;

namespace PROJECT.Application.Interfaces
{
    public interface IEmailService
    {
        Task<bool> SendAsync(EmailRequest request);
    }
}