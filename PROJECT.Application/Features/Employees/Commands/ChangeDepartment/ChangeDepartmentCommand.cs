﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Employees.Commands.ChangeDepartment
{
    public class ChangeDepartmentCommand : IRequest<Response<string>>
    {
        public string UserId { get; set; }

        public int DepartmentId { get; set; }

        public class ChangeDepartmentCommandHandler : IRequestHandler<ChangeDepartmentCommand, Response<string>>
        {
            private readonly ICompanyService _companyService;
            private readonly IDepartmentRepository _departmentRepository;
            private readonly IEmployeeRepository _employeeRepository;
            private readonly IUnitOfWork _unitOfWork;

            public ChangeDepartmentCommandHandler(IUnitOfWork unitOfWork, IEmployeeRepository employeeRepository,
                IDepartmentRepository departmentRepository, ICompanyService companyService)
            {
                _unitOfWork = unitOfWork;
                _employeeRepository = employeeRepository;
                _departmentRepository = departmentRepository;
                _companyService = companyService;
            }

            public async Task<Response<string>> Handle(ChangeDepartmentCommand request,
                CancellationToken cancellationToken)
            {
                var user = await _employeeRepository.GetByIdAsync(request.UserId);

                if (user == null)
                    throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "User", "id",
                        request.UserId));

                if (!await _departmentRepository.AnyAsync(department => department.Id == request.DepartmentId))
                    throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "Department", "id",
                        request.DepartmentId));
                user.Departments.Remove(user.Departments.FirstOrDefault(d => d.CompanyId == _companyService.CompanyId));
                user.Departments.Add(await _departmentRepository.GetByIdAsync(request.DepartmentId));

                user = await _employeeRepository.UpdateAsync(user);
                await _unitOfWork.CommitAsync();

                return new Response<string>(data: user.Id);
            }
        }
    }
}