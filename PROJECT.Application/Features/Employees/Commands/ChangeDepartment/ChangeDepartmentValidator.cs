﻿using FluentValidation;

namespace PROJECT.Application.Features.Employees.Commands.ChangeDepartment
{
    public class ChangeDepartmentValidator : AbstractValidator<ChangeDepartmentCommand>
    {
        public ChangeDepartmentValidator()
        {
            RuleFor(p => p.UserId)
                .NotNull()
                .NotEmpty();

            RuleFor(p => p.DepartmentId)
                .NotEmpty();
        }
    }
}