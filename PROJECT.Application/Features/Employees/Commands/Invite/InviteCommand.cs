﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using MediatR;
using Microsoft.Extensions.Options;
using PROJECT.Application.Dtos.Company;
using PROJECT.Application.Dtos.Email;
using PROJECT.Application.EmailTemplate;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Features.Employees.Commands.CreateEmployee;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Settings;

namespace PROJECT.Application.Features.Employees.Commands.Invite
{
    public class InviteCommand : IRequest<Response<string>>
    {
        public string Email { get; set; }

        public bool NoExpired { get; set; } = false;

        public class InviteCommandHandler : IRequestHandler<InviteCommand, Response<string>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly ClientSetting _clientSetting;
            private readonly ICompanyService _companyService;
            private readonly ICryptographyService _cryptographyService;
            private readonly IDateTimeService _dateTimeService;
            private readonly IEmailService _emailService;
            private readonly IUserRepository _userRepository;

            public InviteCommandHandler(ICryptographyService cryptographyService,
                ICompanyService companyService,
                IEmailService emailService,
                IUserRepository userRepository,
                IOptions<ClientSetting> clientSetting,
                IAuthUserService authUserService,
                IDateTimeService dateTimeService)
            {
                _cryptographyService = cryptographyService;
                _companyService = companyService;
                _emailService = emailService;
                _userRepository = userRepository;
                _clientSetting = clientSetting.Value;
                _authUserService = authUserService;
                _dateTimeService = dateTimeService;
            }

            public async Task<Response<string>> Handle(InviteCommand request, CancellationToken cancellationToken)
            {
                if (await _userRepository.AnyAsync(u =>
                    u.Email == request.Email && u.Companies.Any(c => c.Id == _companyService.CompanyId.Value)))
                    throw new ApiException($"User {request.Email} already taken");

                var user = await _userRepository.GetByIdAsync(_authUserService.UserId);

                var token = await _cryptographyService.Encrypt(new CompanyInvitationVm
                {
                    CompanyId = _companyService.CompanyId.Value,
                    Email = request.Email,
                    Expired = !request.NoExpired ? _dateTimeService.UtcNow.AddDays(2) : null,
                    More = new CreateEmployeeCommand
                    {
                        Email = request.Email
                    }
                });

                var url = $"{_clientSetting.Url}/invitation";

                var uriBuilder = new UriBuilder(url);
                var query = HttpUtility.ParseQueryString(uriBuilder.Query);
                query["token"] = token;
                uriBuilder.Query = query.ToString();
                url = uriBuilder.ToString();

                var emailRequest = new EmailRequest
                {
                    To = request.Email,
                    Subject = "Easy business",
                    Body = string.Format(InvitationTemplate.Body, user.FullName, url)
                };
                if (await _emailService.SendAsync(emailRequest))
                    return new Response<string>("Send invitation successfully");

                throw new ApiException("Server mail error");
            }
        }
    }
}