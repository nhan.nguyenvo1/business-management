﻿using FluentValidation;

namespace PROJECT.Application.Features.Employees.Commands.Invite
{
    public class InviteValidtor : AbstractValidator<InviteCommand>
    {
        public InviteValidtor()
        {
            RuleFor(p => p.Email)
                .NotEmpty()
                .NotNull()
                .EmailAddress();
        }
    }
}