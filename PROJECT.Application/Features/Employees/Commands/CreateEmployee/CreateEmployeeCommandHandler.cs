﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using PROJECT.Application.Dtos.Company;
using PROJECT.Application.Dtos.Email;
using PROJECT.Application.EmailTemplate;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Settings;

namespace PROJECT.Application.Features.Employees.Commands.CreateEmployee
{
    public class CreateEmployeeCommandHandler : IRequestHandler<CreateEmployeeCommand, Response<string>>
    {
        private readonly IAuthUserService _authUserService;
        private readonly ClientSetting _clientSetting;
        private readonly ICompanyService _companyService;
        private readonly ICryptographyService _cryptographyService;
        private readonly IDateTimeService _dateTimeService;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IEmailService _emailService;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly UserManager<AppUser> _userManager;

        public CreateEmployeeCommandHandler(IEmployeeRepository employeeRepository,
            IDepartmentRepository departmentRepository,
            ICompanyService companyService,
            IDateTimeService dateTimeService,
            ICryptographyService cryptographyService,
            IOptions<ClientSetting> clientSetting,
            IAuthUserService authUserService,
            IEmailService emailService, UserManager<AppUser> userManager)
        {
            _employeeRepository = employeeRepository;
            _departmentRepository = departmentRepository;
            _companyService = companyService;
            _dateTimeService = dateTimeService;
            _cryptographyService = cryptographyService;
            _clientSetting = clientSetting.Value;
            _authUserService = authUserService;
            _emailService = emailService;
            _userManager = userManager;
        }


        public async Task<Response<string>> Handle(CreateEmployeeCommand request, CancellationToken cancellationToken)
        {
            if (await _employeeRepository.AnyAsync(e => e.NormalizedEmail == _userManager.NormalizeEmail(request.Email)
                                                        || e.NormalizedUserName ==
                                                        _userManager.NormalizeName(request.UserName), true))
                throw new ApiException(
                    $"A user with email {request.Email} or user name {request.UserName} already exists.");

            if (!await _departmentRepository.AnyAsync(d => d.Id == request.DepartmentId))
                throw new ApiException("Invalid department");

            var user = await _employeeRepository.GetByIdAsync(_authUserService.UserId);

            var invitationVm = new CompanyInvitationVm
            {
                Email = request.Email,
                CompanyId = _companyService.CompanyId.Value,
                Expired = _dateTimeService.UtcNow.AddDays(2),
                More = request
            };

            var token = await _cryptographyService.Encrypt(invitationVm);

            var url = $"{_clientSetting.Url}/invitation";

            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["token"] = token;
            uriBuilder.Query = query.ToString();
            url = uriBuilder.ToString();

            var emailRequest = new EmailRequest
            {
                To = request.Email,
                Subject = "Easy business",
                Body = string.Format(InvitationTemplate.Body, user.FullName, url)
            };
            if (await _emailService.SendAsync(emailRequest))
                return new Response<string>("Send invitation successfully");

            throw new ApiException("Server mail error");
        }
    }
}