﻿using System;
using MediatR;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Employees.Commands.CreateEmployee
{
    public class CreateEmployeeCommand : IRequest<Response<string>>
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserGender Gender { get; set; }

        public DateTime Dob { get; set; }

        public int DepartmentId { get; set; }
    }
}