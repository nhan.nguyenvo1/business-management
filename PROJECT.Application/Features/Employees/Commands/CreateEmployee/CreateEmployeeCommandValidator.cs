﻿using FluentValidation;
using PROJECT.Application.Constants;
using PROJECT.Application.Interfaces.Repositories;

namespace PROJECT.Application.Features.Employees.Commands.CreateEmployee
{
    public class CreateEmployeeCommandValidator : AbstractValidator<CreateEmployeeCommand>
    {
        public CreateEmployeeCommandValidator(IDepartmentRepository departmentRepository,
            IEmployeeRepository employeeRepository)
        {
            RuleFor(p => p.UserName)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "User name"))
                .MaximumLength(200)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "User name", 200))
                .MustAsync(async (username, cancellation) => !await employeeRepository.ExistAsync(username))
                .WithMessage((command, userName) =>
                    string.Format(ErrorMessageConstant.Duplicated, "User", "user name", userName));

            RuleFor(p => p.Email)
                .EmailAddress()
                .WithMessage(ErrorMessageConstant.InvalidEmail)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Email"))
                .MaximumLength(320)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "Email", 320))
                .MustAsync(async (email, cancellation) => !await employeeRepository.ExistAsync(email))
                .When(command => !string.IsNullOrEmpty(command.Email))
                .WithMessage((command, email) =>
                    string.Format(ErrorMessageConstant.Duplicated, "User", "email", email));

            RuleFor(p => p.PhoneNumber)
                .NotNull()
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Phone number"))
                .Matches("^[0-9]*$")
                .MaximumLength(16)
                .MinimumLength(4)
                .WithMessage(ErrorMessageConstant.InvalidPhoneNumber);

            RuleFor(p => p.FirstName)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "First name"))
                .MaximumLength(100)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "First name", 100));

            RuleFor(p => p.LastName)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Last name"))
                .MaximumLength(100)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "Last name", 100));

            RuleFor(p => p.Gender)
                .IsInEnum()
                .WithMessage("Invalid Gender")
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Gender"));

            RuleFor(p => p.DepartmentId)
                .NotNull()
                .Must(departmentId => departmentId > 0)
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Department"))
                .MustAsync(async (departmentId, cancellation) => await departmentRepository.ExistAsync(departmentId))
                .When(command => command.DepartmentId > 0)
                .WithMessage((command, departmentId) =>
                    string.Format(ErrorMessageConstant.NotFound, "Department", "id", departmentId));
        }
    }
}