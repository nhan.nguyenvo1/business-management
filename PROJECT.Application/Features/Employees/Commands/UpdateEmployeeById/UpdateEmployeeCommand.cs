﻿using System;
using MediatR;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Employees.Commands.UpdateEmployeeById
{
    public class UpdateEmployeeCommand : IRequest<Response<int>>
    {
        public string Id { get; set; }

        public string PhoneNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserGender Gender { get; set; }

        public DateTime Dob { get; set; }
    }
}