﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Options;
using PROJECT.Application.Constants;
using PROJECT.Application.Dtos.Email;
using PROJECT.Application.EmailTemplate;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Settings;

namespace PROJECT.Application.Features.Employees.Commands.DeleteEmployeeById
{
    public class DeleteEmployeeCommandHandler : IRequestHandler<DeleteEmployeeCommand, Response<string>>
    {
        private readonly IAuthUserService _authUserService;
        private readonly ClientSetting _clientSetting;
        private readonly ICompanyRepository _companyRepository;
        private readonly ICompanyService _companyService;
        private readonly IEmailService _emailService;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IUserRepository _userRepository;

        public DeleteEmployeeCommandHandler(IEmployeeRepository employeeRepository,
            IEmailService emailService,
            IOptions<ClientSetting> clientSetting,
            ICompanyRepository companyRepository,
            IUserRepository userRepository,
            ICompanyService companyService,
            IAuthUserService authUserService)
        {
            _employeeRepository = employeeRepository;
            _emailService = emailService;
            _clientSetting = clientSetting.Value;
            _companyRepository = companyRepository;
            _userRepository = userRepository;
            _companyService = companyService;
            _authUserService = authUserService;
        }

        public async Task<Response<string>> Handle(DeleteEmployeeCommand request, CancellationToken cancellationToken)
        {
            var employee = await _employeeRepository.GetByIdAsync(request.Id);

            if (employee == null)
                throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Employee", "id", request.Id));

            await _employeeRepository.DeleteAsync(employee);
            var company = await _companyRepository.GetByIdAsync(_companyService.CompanyId.Value);
            var user = await _userRepository.GetByIdAsync(_authUserService.UserId);
            var url = $"{_clientSetting.Url}";

            var body = string.Format(RemoveCompanyTemplate.Body, employee.FullName, company.Name, user.FullName, url);

            var emailRequest = new EmailRequest
            {
                To = employee.Email,
                Subject = "Easy Business",
                Body = body
            };

            if (!await _emailService.SendAsync(emailRequest)) throw new ApiException("Server mail error");

            return new Response<string>(request.Id);
        }
    }
}