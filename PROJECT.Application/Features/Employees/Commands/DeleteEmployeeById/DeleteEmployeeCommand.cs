﻿using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Employees.Commands.DeleteEmployeeById
{
    public class DeleteEmployeeCommand : IRequest<Response<string>>
    {
        public DeleteEmployeeCommand(string id)
        {
            Id = id;
        }

        public string Id { get; }
    }
}