﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Employees.Queries.GetAllEmployees
{
    public class GetAllEmployeesQueryHandler : IRequestHandler<GetAllEmployeesQuery, PagedResponse<EmployeeVm>>
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IMapper _mapper;


        public GetAllEmployeesQueryHandler(IEmployeeRepository employeeRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<EmployeeVm>> Handle(GetAllEmployeesQuery request,
            CancellationToken cancellationToken)
        {
            var employees =
                await _employeeRepository.GetPagedResponseAsync(request.Page, request.Limit, request.Search,
                    request.Sort,
                    request.Order, request.Filters);
            var total = await _employeeRepository.CountAsync(request.Search, request.Filters);


            return new PagedResponse<EmployeeVm>(
                _mapper.Map<ICollection<EmployeeVm>>(employees),
                request.Page,
                request.Limit,
                total);
        }
    }
}