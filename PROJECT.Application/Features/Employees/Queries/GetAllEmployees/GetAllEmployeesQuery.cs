﻿using System.Collections.Generic;
using MediatR;
using PROJECT.Application.Parameters;
using PROJECT.Application.Parameters.Interfaces;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Employees.Queries.GetAllEmployees
{
    public class GetAllEmployeesQuery : RequestParameter, IRequest<PagedResponse<EmployeeVm>>, IHasSort, IHasSearch,
        IHasFilters
    {
        public Dictionary<string, string> Filters { get; set; }
        public string Search { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }
    }
}