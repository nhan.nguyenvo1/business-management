﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Employees.Queries.GetEmployeesByDepartmentId
{
    public class
        GetEmployeesByDepartmentIdQueryHandler : IRequestHandler<GetEmployeesByDepartmentIdQuery,
            PagedResponse<EmployeeVm>>
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IMapper _mapper;

        public GetEmployeesByDepartmentIdQueryHandler(IEmployeeRepository employeeRepository, IMapper mapper,
            IDepartmentRepository departmentRepository)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
            _departmentRepository = departmentRepository;
        }

        public async Task<PagedResponse<EmployeeVm>> Handle(GetEmployeesByDepartmentIdQuery request,
            CancellationToken cancellationToken)
        {
            var department = await _departmentRepository.GetByIdAsync(request.DepartmentId);

            if (department == null) throw new ApiException($"Cannot find department with id = {request.DepartmentId}");

            var employees = await _employeeRepository.GetByDepartmentIdAsync(department,
                request.Parameter.Page, request.Parameter.Limit, request.Parameter.Search, request.Parameter.Sort,
                request.Parameter.Order, request.Parameter.Filters);

            var total = await _employeeRepository.CountByDepartmentIdAsync(department,
                request.Parameter.Search, request.Parameter.Filters);

            return new PagedResponse<EmployeeVm>(_mapper.Map<ICollection<EmployeeVm>>(employees),
                request.Parameter.Page, request.Parameter.Limit, total);
        }
    }
}