﻿using MediatR;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Employees.Queries.GetEmployeesByDepartmentId
{
    public class GetEmployeesByDepartmentIdQuery : IRequest<PagedResponse<EmployeeVm>>
    {
        public int DepartmentId { get; set; }

        public GetEmployeesByDepartmentIdParameter Parameter { get; set; }
    }
}