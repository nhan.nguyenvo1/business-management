﻿using System.Collections.Generic;
using PROJECT.Application.Parameters;
using PROJECT.Application.Parameters.Interfaces;

namespace PROJECT.Application.Features.Employees.Queries.GetEmployeesByDepartmentId
{
    public class GetEmployeesByDepartmentIdParameter : RequestParameter, IHasSort, IHasSearch, IHasFilters
    {
        public Dictionary<string, string> Filters { get; set; }
        public string Search { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }
    }
}