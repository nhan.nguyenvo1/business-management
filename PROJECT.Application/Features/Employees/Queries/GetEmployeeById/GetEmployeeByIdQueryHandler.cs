﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Employees.Queries.GetEmployeeById
{
    public class GetEmployeeByIdQueryHandler : IRequestHandler<GetEmployeeByIdQuery, Response<EmployeeVm>>
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IMapper _mapper;

        public GetEmployeeByIdQueryHandler(IMapper mapper, IEmployeeRepository employeeRepository)
        {
            _mapper = mapper;
            _employeeRepository = employeeRepository;
        }

        public async Task<Response<EmployeeVm>> Handle(GetEmployeeByIdQuery request,
            CancellationToken cancellationToken)
        {
            var user = await _employeeRepository.GetByIdAsync(request.Id);
            if (user == null) throw new ApiException($"Employee with id = {request.Id} doesn't exists");

            return new Response<EmployeeVm>(_mapper.Map<EmployeeVm>(user));
        }
    }
}