﻿using MediatR;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Employees.Queries.GetEmployeeById
{
    public class GetEmployeeByIdQuery : IRequest<Response<EmployeeVm>>
    {
        public GetEmployeeByIdQuery(string id)
        {
            Id = id;
        }

        public string Id { get; }
    }
}