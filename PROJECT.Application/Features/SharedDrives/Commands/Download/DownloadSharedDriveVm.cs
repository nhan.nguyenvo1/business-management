﻿using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.SharedDrives.Commands.Download
{
    public class DownloadSharedDriveVm
    {
        public string Path { get; set; }

        public bool CanRead { get; set; }

        public FileType FileType { get; set; }
    }
}