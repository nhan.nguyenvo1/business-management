﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Dtos.Drive;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.SharedDrives.Commands.Download
{
    public class DownloadSharedDriveCommand : IRequest<Response<DriveDownloadResponse>>
    {
        public int DriveId { get; set; }

        public class
            DownloadSharedDriveCommandHandler : IRequestHandler<DownloadSharedDriveCommand,
                Response<DriveDownloadResponse>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IDriveStorageService _driveStorageService;

            public DownloadSharedDriveCommandHandler(IAppFileRepository appFileRepository,
                IDriveStorageService driveStorageService, IAuthUserService authUserService)
            {
                _appFileRepository = appFileRepository;
                _driveStorageService = driveStorageService;
                _authUserService = authUserService;
            }

            public async Task<Response<DriveDownloadResponse>> Handle(DownloadSharedDriveCommand request,
                CancellationToken cancellationToken)
            {
                var file = await _appFileRepository.GetSharedByIdAsync<DownloadSharedDriveVm>(request.DriveId,
                    _authUserService.UserId);

                if (file == null) throw new KeyNotFoundException("File does not exists");

                if (!file.CanRead)
                    throw new UnauthorizedAccessException("You do not have permission to download this file/folder.");

                if (file.FileType == FileType.Folder)
                    return new Response<DriveDownloadResponse>(await _driveStorageService.DownloadZipAsync(file.Path));

                return new Response<DriveDownloadResponse>(await _driveStorageService.DownloadAsync(file.Path));
            }
        }
    }
}