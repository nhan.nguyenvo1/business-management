﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.SharedDrives.Queries.GetFolders
{
    public class GetSharedFoldersQuery : IRequest<Response<IReadOnlyList<GetSharedFoldersVm>>>
    {
        public int? FolderId { get; set; }

        public class
            GetSharedFoldersQueryHandler : IRequestHandler<GetSharedFoldersQuery,
                Response<IReadOnlyList<GetSharedFoldersVm>>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;

            public GetSharedFoldersQueryHandler(IAppFileRepository appFileRepository, IAuthUserService authUserService)
            {
                _appFileRepository = appFileRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<IReadOnlyList<GetSharedFoldersVm>>> Handle(GetSharedFoldersQuery request,
                CancellationToken cancellationToken)
            {
                var drives =
                    await _appFileRepository.GetSharedAsync<GetSharedFoldersVm>(request.FolderId,
                        _authUserService.UserId);

                return new Response<IReadOnlyList<GetSharedFoldersVm>>(drives);
            }
        }
    }
}