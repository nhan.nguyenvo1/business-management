﻿namespace PROJECT.Application.Features.SharedDrives.Queries.GetFolders
{
    public class SharedOwnerVm
    {
        public string Id { get; set; }

        public string AvatarUrl { get; set; }

        public string FullName { get; set; }
    }
}