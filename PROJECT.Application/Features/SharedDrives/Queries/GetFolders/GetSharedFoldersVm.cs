﻿namespace PROJECT.Application.Features.SharedDrives.Queries.GetFolders
{
    public class GetSharedFoldersVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string FileType { get; set; }

        public ulong? FileSize { get; set; }

        public SharedOwnerVm Created { get; set; }

        public SharedOwnerVm Updated { get; set; }

        public string CreatedDate { get; set; }

        public string UpdatedDate { get; set; }
    }
}