﻿using System;

namespace PROJECT.Application.Features.Calendars.Queries.GetHomeCalendars
{
    public class GetHomeCalendarsViewModel
    {
        public int Id { get; set; }

        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public string Color { get; set; }

        public string Name { get; set; }

        public bool Timed { get; set; } = false;

        public CalendarType Type {  get; set; }
    }
}