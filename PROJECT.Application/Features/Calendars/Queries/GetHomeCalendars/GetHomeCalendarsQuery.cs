﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Calendars.Queries.GetHomeCalendars
{
    public class GetHomeCalendarsQuery : IRequest<Response<IReadOnlyList<GetHomeCalendarsViewModel>>>
    {
        public class GetHomeCalendarsQueryHandler :
            IRequestHandler<GetHomeCalendarsQuery, Response<IReadOnlyList<GetHomeCalendarsViewModel>>>
        {
            private readonly IAppTaskRepository _appTaskRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IBookingRoomRepository _bookingRoomRepository;

            public GetHomeCalendarsQueryHandler(IAppTaskRepository appTaskRepository, IAuthUserService authUserService, IBookingRoomRepository bookingRoomRepository)
            {
                _appTaskRepository = appTaskRepository;
                _authUserService = authUserService;
                _bookingRoomRepository = bookingRoomRepository;
            }

            public async Task<Response<IReadOnlyList<GetHomeCalendarsViewModel>>> Handle(GetHomeCalendarsQuery request,
                CancellationToken cancellationToken)
            {
                var taskCalandars = await _appTaskRepository.GetByUserIdAsync<GetHomeCalendarsViewModel>(_authUserService.UserId);
                var bookingRoomsCalandars = await _bookingRoomRepository.GetByConditionAsync<GetHomeCalendarsViewModel>(e => e.CreatedBy == _authUserService.UserId);
                var calandars = new List<GetHomeCalendarsViewModel>();
                calandars.AddRange(taskCalandars.Select(e => { e.Type = CalendarType.Task; return e; }));
                calandars.AddRange(bookingRoomsCalandars.Select(e => { e.Type = CalendarType.BookingRoom; return e; }));
                return new(calandars);
            }
        }
    }
}