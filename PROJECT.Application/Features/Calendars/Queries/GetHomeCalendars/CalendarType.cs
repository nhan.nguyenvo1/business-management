﻿using System;

namespace PROJECT.Application.Features.Calendars.Queries.GetHomeCalendars
{
    [Flags]
    public enum CalendarType
    {
        Task = 1,
        BookingRoom = 2,
    }
}
