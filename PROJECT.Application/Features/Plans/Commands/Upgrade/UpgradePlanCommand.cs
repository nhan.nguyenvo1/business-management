﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using MediatR;
using Microsoft.Extensions.Options;
using PROJECT.Application.Dtos.Email;
using PROJECT.Application.EmailTemplate;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Settings;

namespace PROJECT.Application.Features.Plans.Commands.Upgrade
{
    public class UpgradePlanCommand : IRequest<Response<bool>>
    {
        public int PackageId { get; set; }

        public class UpgradePlanCommandHandler : IRequestHandler<UpgradePlanCommand, Response<bool>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IEmailService _emailService;
            private readonly IPackageRequestRepository _packageRequestRepository;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserRepository _userRepository;
            private readonly ClientSetting _clientSetting;
            private readonly IPackageRepository _packageRepository;

            public UpgradePlanCommandHandler(IPackageRequestRepository packageRequestRepository,
                                             IEmailService emailService,
                                             IAuthUserService authUserService,
                                             IUserRepository userRepository,
                                             IUnitOfWork unitOfWork,
                                             IOptions<ClientSetting> clientSetting, 
                                             IPackageRepository packageRepository)
            {
                _packageRequestRepository = packageRequestRepository;
                _emailService = emailService;
                _authUserService = authUserService;
                _userRepository = userRepository;
                _unitOfWork = unitOfWork;
                _clientSetting = clientSetting.Value;
                _packageRepository = packageRepository;
            }

            public async Task<Response<bool>> Handle(UpgradePlanCommand request, CancellationToken cancellationToken)
            {
                var trans = await _unitOfWork.BeginTransactionAsync();
                var user = await _userRepository.GetByIdAsync(_authUserService.UserId);
                var plan = await _packageRepository.GetSingleAsync(p => p.Id == request.PackageId && !p.Deleted, true);

                if (plan == null)
                {
                    throw new KeyNotFoundException();
                }

                await _packageRequestRepository.AddAsync(new PackageRequest
                {
                    UserId = _authUserService.UserId,
                    PackageId = request.PackageId,
                    Cost = plan.Cost
                });

                try
                {
                    await _unitOfWork.CommitAsync();
                    await trans.CommitAsync(cancellationToken);
                    var url = $"{_clientSetting.Url}";

                    await _emailService.SendAsync(new EmailRequest
                    {
                        Body = string.Format(OrderTemplate.Body,
                                             user.FullName,
                                             plan.Name,
                                             plan.Cost,
                                             plan.Sale,
                                             Math.Floor(plan.Cost - (plan.Cost * plan.Sale / 100)),
                                             url),
                        Subject = "You have a request to upgrade your plan",
                        To = user.Email
                    });
                }
                catch (Exception)
                {
                    await trans.RollbackAsync(cancellationToken);
                    throw new ApiException("Your request still waiting for approve.");
                }

                return new Response<bool>(true);
            }
        }
    }
}