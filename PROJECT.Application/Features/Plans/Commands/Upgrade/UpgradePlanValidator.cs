﻿using FluentValidation;

namespace PROJECT.Application.Features.Plans.Commands.Upgrade
{
    public class UpgradePlanValidator : AbstractValidator<UpgradePlanCommand>
    {
        public UpgradePlanValidator()
        {
            RuleFor(p => p.PackageId)
                .NotEmpty();
        }
    }
}