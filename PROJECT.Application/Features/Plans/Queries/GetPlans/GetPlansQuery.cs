﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Plans.Queries.GetPlans
{
    public class GetPlansQuery : IRequest<Response<IReadOnlyList<GetPlansVm>>>
    {
        public class GetPlansQueryHandler : IRequestHandler<GetPlansQuery, Response<IReadOnlyList<GetPlansVm>>>
        {
            private readonly IPackageRepository _packageRepository;

            public GetPlansQueryHandler(IPackageRepository packageRepository)
            {
                _packageRepository = packageRepository;
            }

            public async Task<Response<IReadOnlyList<GetPlansVm>>> Handle(GetPlansQuery request,
                CancellationToken cancellationToken)
            {
                return new(await _packageRepository.GetAllAsync<GetPlansVm>());
            }
        }
    }
}