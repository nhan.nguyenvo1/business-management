﻿namespace PROJECT.Application.Features.Plans.Queries.GetPlans
{
    public class GetPlansVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int NumOfEmployee { get; set; }

        public int SizeOfDrive { get; set; }

        public float Cost { get; set; }

        public int NumOfUser { get; set; }
    }
}