﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.TrashBins.Queries.GetDeletedDrives
{
    public class GetDeletedDrivesQuery : IRequest<Response<IReadOnlyList<GetDeletedDrivesVm>>>
    {
        public class
            GetDeletedDrivesQueryHandler : IRequestHandler<GetDeletedDrivesQuery,
                Response<IReadOnlyList<GetDeletedDrivesVm>>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;

            public GetDeletedDrivesQueryHandler(IAppFileRepository appFileRepository, IAuthUserService authUserService)
            {
                _appFileRepository = appFileRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<IReadOnlyList<GetDeletedDrivesVm>>> Handle(GetDeletedDrivesQuery request,
                CancellationToken cancellationToken)
            {
                var deletedDrives =
                    await _appFileRepository.GetDeletedAsync<GetDeletedDrivesVm>(_authUserService.UserId);

                return new Response<IReadOnlyList<GetDeletedDrivesVm>>(deletedDrives);
            }
        }
    }
}