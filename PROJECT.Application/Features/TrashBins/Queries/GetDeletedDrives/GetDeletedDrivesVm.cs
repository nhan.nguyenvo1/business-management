﻿namespace PROJECT.Application.Features.TrashBins.Queries.GetDeletedDrives
{
    public class GetDeletedDrivesVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string FileType { get; set; }

        public ulong? FileSize { get; set; }

        public DeletedOwnerVm Created { get; set; }

        public DeletedOwnerVm Updated { get; set; }

        public string CreatedDate { get; set; }

        public string UpdatedDate { get; set; }
    }
}