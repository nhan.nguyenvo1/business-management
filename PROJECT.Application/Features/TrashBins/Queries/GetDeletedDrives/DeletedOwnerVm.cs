﻿namespace PROJECT.Application.Features.TrashBins.Queries.GetDeletedDrives
{
    public class DeletedOwnerVm
    {
        public string Id { get; set; }

        public string AvatarUrl { get; set; }

        public string FullName { get; set; }
    }
}