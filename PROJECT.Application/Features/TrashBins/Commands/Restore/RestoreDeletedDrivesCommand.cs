﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.TrashBins.Commands.Restore
{
    public class RestoreDeletedDrivesCommand : IRequest<Response<bool>>
    {
        public ICollection<int> DriveIds { get; set; }

        public class RestoreDeletedDrivesCommandHandler : IRequestHandler<RestoreDeletedDrivesCommand, Response<bool>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IUnitOfWork _unitOfWork;

            public RestoreDeletedDrivesCommandHandler(IAuthUserService authUserService,
                IAppFileRepository appFileRepository, IUnitOfWork unitOfWork)
            {
                _authUserService = authUserService;
                _appFileRepository = appFileRepository;
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<bool>> Handle(RestoreDeletedDrivesCommand request,
                CancellationToken cancellationToken)
            {
                var errorMessage = new StringBuilder();

                foreach (var driveId in request.DriveIds)
                {
                    var drive = await _appFileRepository.GetDeletedByIdAsync(driveId, _authUserService.UserId);

                    if (drive == null)
                    {
                        errorMessage.AppendLine($"File or folder with id = {driveId} does not exists");
                        continue;
                    }

                    drive.Deleted = false;
                    await _appFileRepository.UpdateAsync(drive);
                }

                await _unitOfWork.CommitAsync();

                if (errorMessage.Length > 0) return new Response<bool>(false, errorMessage.ToString());

                return new Response<bool>(true);
            }
        }
    }
}