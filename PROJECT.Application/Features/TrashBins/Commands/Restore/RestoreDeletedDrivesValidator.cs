﻿using FluentValidation;

namespace PROJECT.Application.Features.TrashBins.Commands.Restore
{
    public class RestoreDeletedDrivesValidator : AbstractValidator<RestoreDeletedDrivesCommand>
    {
        public RestoreDeletedDrivesValidator()
        {
            RuleFor(p => p.DriveIds)
                .NotEmpty()
                .NotNull();
        }
    }
}