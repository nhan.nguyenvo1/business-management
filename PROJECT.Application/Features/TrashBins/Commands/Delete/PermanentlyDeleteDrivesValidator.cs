﻿using FluentValidation;

namespace PROJECT.Application.Features.TrashBins.Commands.Delete
{
    public class PermanentlyDeleteDrivesValidator : AbstractValidator<PermanentlyDeleteDrivesCommand>
    {
        public PermanentlyDeleteDrivesValidator()
        {
            RuleFor(p => p.DriveIds)
                .NotEmpty()
                .NotNull();
        }
    }
}