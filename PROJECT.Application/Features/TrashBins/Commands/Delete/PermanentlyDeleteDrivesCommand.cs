﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.TrashBins.Commands.Delete
{
    public class PermanentlyDeleteDrivesCommand : IRequest<Response<bool>>
    {
        public ICollection<int> DriveIds { get; set; }

        public class
            PermanentlyDeleteDrivesCommandHandler : IRequestHandler<PermanentlyDeleteDrivesCommand, Response<bool>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IUnitOfWork _unitOfWork;

            public PermanentlyDeleteDrivesCommandHandler(IAppFileRepository appFileRepository,
                IUnitOfWork unitOfWork,
                IDriveStorageService driveStorageService,
                IAuthUserService authUserService)
            {
                _appFileRepository = appFileRepository;
                _unitOfWork = unitOfWork;
                _driveStorageService = driveStorageService;
                _authUserService = authUserService;
            }

            public async Task<Response<bool>> Handle(PermanentlyDeleteDrivesCommand request,
                CancellationToken cancellationToken)
            {
                var errorMessage = new StringBuilder();
                foreach (var driveId in request.DriveIds)
                {
                    var drive = await _appFileRepository.GetDeletedByIdAsync(driveId, _authUserService.UserId);

                    if (drive == null)
                    {
                        errorMessage.AppendLine($"File or folder with id = {driveId} does not exists");
                        continue;
                    }

                    await _appFileRepository.DeleteAsync(drive);

                    try
                    {
                        await _driveStorageService.DeleteAsync(drive.Path);
                    }
                    catch (Exception)
                    {
                    }
                }

                await _unitOfWork.CommitAsync();

                if (errorMessage.Length > 0) return new Response<bool>(false, errorMessage.ToString());

                return new Response<bool>(true);
            }
        }
    }
}