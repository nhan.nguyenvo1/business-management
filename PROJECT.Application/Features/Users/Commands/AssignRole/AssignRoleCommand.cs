﻿using System.Collections.Generic;
using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Users.Commands.AssignRole
{
    public class AssignRoleCommand : IRequest<Response<string>>
    {
        public string UserId { get; set; }

        public ICollection<string> RoleNames { get; set; }
    }
}