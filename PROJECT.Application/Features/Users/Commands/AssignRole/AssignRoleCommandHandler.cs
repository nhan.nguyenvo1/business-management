﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Users.Commands.AssignRole
{
    public class AssignRoleCommandHandler : IRequestHandler<AssignRoleCommand, Response<string>>
    {
        private readonly IUserRepository _userRepository;

        public AssignRoleCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Response<string>> Handle(AssignRoleCommand request, CancellationToken cancellationToken)
        {
            var appUser = await _userRepository.GetByIdAsync(request.UserId);
            if (appUser == null) return new Response<string>($"User with id = {request.UserId} does not exists");

            appUser = await _userRepository.AssignRoleAsync(appUser, request.RoleNames);

            return new Response<string>(data: appUser.Id);
        }
    }
}