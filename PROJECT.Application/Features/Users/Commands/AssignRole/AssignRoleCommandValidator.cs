﻿using FluentValidation;

namespace PROJECT.Application.Features.Users.Commands.AssignRole
{
    public class AssignRoleCommandValidator : AbstractValidator<AssignRoleCommand>
    {
        public AssignRoleCommandValidator()
        {
            RuleFor(p => p.UserId)
                .NotEmpty()
                .NotNull()
                .WithMessage("User id can not be empty");

            RuleFor(p => p.RoleNames)
                .NotNull()
                .WithMessage("Role names cannot be empty")
                .NotEmpty()
                .WithMessage("The request must have at least 1 role name");
        }
    }
}