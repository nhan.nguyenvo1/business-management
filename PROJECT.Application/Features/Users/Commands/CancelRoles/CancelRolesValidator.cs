﻿using FluentValidation;

namespace PROJECT.Application.Features.Users.Commands.CancelRoles
{
    public class CancelRolesValidator : AbstractValidator<CancelRolesCommand>
    {
        public CancelRolesValidator()
        {
            RuleFor(p => p.UserId)
                .NotEmpty()
                .NotNull()
                .WithMessage("User id can not be empty");

            RuleFor(p => p.RoleNames)
                .NotNull()
                .WithMessage("Role names cannot be empty")
                .NotEmpty()
                .WithMessage("The request must have at least 1 role name");
        }
    }
}