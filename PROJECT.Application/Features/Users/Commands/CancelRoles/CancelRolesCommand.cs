﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Users.Commands.CancelRoles
{
    public class CancelRolesCommand : IRequest<Response<string>>
    {
        public string UserId { get; set; }

        public ICollection<string> RoleNames { get; set; }

        public class CancelRolesCommandHandler : IRequestHandler<CancelRolesCommand, Response<string>>
        {
            private readonly IUserRepository _userRepository;

            public CancelRolesCommandHandler(IUserRepository userRepository)
            {
                _userRepository = userRepository;
            }

            public async Task<Response<string>> Handle(CancelRolesCommand request, CancellationToken cancellationToken)
            {
                var user = await _userRepository.GetByIdAsync(request.UserId);

                if (user == null) throw new KeyNotFoundException("User does not exists");

                user = await _userRepository.CancleRolesAsync(user, request.RoleNames);

                return new Response<string>(data: user.Id);
            }
        }
    }
}