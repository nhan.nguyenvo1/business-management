﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Users.Commands.UpdateProfile
{
    public class UpdateProfileCommand : IRequest<Response<string>>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public UserGender Gender { get; set; }

        public string Phone { get; set; }

        public class UpdateProfileCommandHandler : IRequestHandler<UpdateProfileCommand, Response<string>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserRepository _userRepository;

            public UpdateProfileCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IUserRepository userRepository,
                IAuthUserService authUserService)
            {
                _unitOfWork = unitOfWork;
                _mapper = mapper;
                _userRepository = userRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<string>> Handle(UpdateProfileCommand request,
                CancellationToken cancellationToken)
            {
                var user = await _userRepository.GetByIdAsync(_authUserService.UserId);

                if (user == null) throw new NotImplementedException("Ambiguous User");

                user = _mapper.Map(request, user);
                user = await _userRepository.UpdateAsync(user);
                await _unitOfWork.CommitAsync();

                return new Response<string>(data: user.Id);
            }
        }
    }
}