﻿using FluentValidation;
using PROJECT.Application.Constants;

namespace PROJECT.Application.Features.Users.Commands.UpdateProfile
{
    public class UpdateProfileValidator : AbstractValidator<UpdateProfileCommand>
    {
        public UpdateProfileValidator()
        {
            RuleFor(p => p.Phone)
                .NotNull()
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Phone number"))
                .Matches("^[0-9]*$")
                .MaximumLength(16)
                .MinimumLength(4)
                .WithMessage(ErrorMessageConstant.InvalidPhoneNumber);

            RuleFor(p => p.FirstName)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "First name"))
                .MaximumLength(100)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "First name", 100));

            RuleFor(p => p.LastName)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Last name"))
                .MaximumLength(100)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "Last name", 100));

            RuleFor(p => p.Gender)
                .IsInEnum()
                .WithMessage("Invalid Gender")
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Gender"));
        }
    }
}