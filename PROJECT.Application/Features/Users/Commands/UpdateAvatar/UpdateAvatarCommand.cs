﻿using MediatR;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Users.Commands.UpdateAvatar
{
    public class UpdateAvatarCommand : IRequest<Response<string>>
    {
        public IFormFile Avatar { get; set; }
    }
}