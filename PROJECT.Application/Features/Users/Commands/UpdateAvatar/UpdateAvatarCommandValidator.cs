﻿using System.Threading.Tasks;
using FluentValidation;
using PROJECT.Application.Constants;

namespace PROJECT.Application.Features.Users.Commands.UpdateAvatar
{
    public class UpdateAvatarCommandValidator : AbstractValidator<UpdateAvatarCommand>
    {
        public UpdateAvatarCommandValidator()
        {
            RuleFor(p => p.Avatar)
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Avatar"))
                .MustAsync((avatar, cancellation) => Task.FromResult(avatar.ContentType.StartsWith("image/")))
                .When(command => command.Avatar != null)
                .WithMessage(string.Format(ErrorMessageConstant.InvalidFile, "Avatar", "image"));
        }
    }
}