﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Users.Commands.UpdateAvatar
{
    public class UpdateAvatarCommandHandler : IRequestHandler<UpdateAvatarCommand, Response<string>>
    {
        private readonly IAuthUserService _authUserService;
        private readonly IUserRepository _userRepository;

        public UpdateAvatarCommandHandler(IUserRepository userRepository, IAuthUserService authUserService)
        {
            _userRepository = userRepository;
            _authUserService = authUserService;
        }

        public async Task<Response<string>> Handle(UpdateAvatarCommand request, CancellationToken cancellationToken)
        {
            var appUser = await _userRepository.UpdateAvatarAsync(_authUserService.UserId, request.Avatar);

            return new Response<string>(appUser.Id);
        }
    }
}