﻿using FluentValidation;

namespace PROJECT.Application.Features.DesktopDrives.Commands.RenameDrive
{
    public class RenameDriveValidator : AbstractValidator<RenameDriveCommand>
    {
        public RenameDriveValidator()
        {
            RuleFor(p => p.DestinationPath).NotEmpty();

            RuleFor(p => p.SourcePath).NotEmpty();
        }
    }
}
