﻿using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Commands.RenameDrive
{
    public class RenameDriveCommand : IRequest<Response<string>>
    {
        public string SourcePath { get; set; }

        public string DestinationPath { get; set; }

        public class RenameDriveCommandHandler : IRequestHandler<RenameDriveCommand, Response<string>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IUnitOfWork _unitOfWork;

            public RenameDriveCommandHandler(IUnitOfWork unitOfWork,
                                             IAppFileRepository appFileRepository,
                                             IAuthUserService authUserService,
                                             IDriveStorageService driveStorageService)
            {
                _unitOfWork = unitOfWork;
                _appFileRepository = appFileRepository;
                _authUserService = authUserService;
                _driveStorageService = driveStorageService;
            }

            public async Task<Response<string>> Handle(RenameDriveCommand request, CancellationToken cancellationToken)
            {
                var src = await _appFileRepository.GetSingleAsync(d => d.Path.EndsWith(request.SourcePath) && _authUserService.UserId.Equals(d.AppStorage.UserId));

                if (src == null)
                {
                    throw new KeyNotFoundException();
                }

                var result = await _driveStorageService.MoveAsync(src.Path, src.Path.Replace(request.SourcePath, request.DestinationPath));

                src.Path = result.Path;
                src.Name = result.Name;

                await _appFileRepository.UpdateAsync(src);

                await _unitOfWork.CommitAsync();

                return new Response<string>(result.Name);
            }
        }
    }
}
