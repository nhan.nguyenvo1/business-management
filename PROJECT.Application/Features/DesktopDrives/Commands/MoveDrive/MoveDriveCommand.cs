﻿using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Commands.MoveDrive
{
    public class MoveDriveCommand : IRequest<Response<object>>
    {
        public string SourcePath { get; set; }

        public string DestinationPath { get; set; }

        public class MoveDriveCommandHandler : IRequestHandler<MoveDriveCommand, Response<object>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly ICompanyService _companyService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IUnitOfWork _unitOfWork;

            public MoveDriveCommandHandler(IAppFileRepository appFileRepository,
                IUnitOfWork unitOfWork,
                IDriveStorageService driveStorageService,
                ICompanyService companyService,
                IAuthUserService authUserService)
            {
                _appFileRepository = appFileRepository;
                _unitOfWork = unitOfWork;
                _driveStorageService = driveStorageService;
                _companyService = companyService;
                _authUserService = authUserService;
            }

            public async Task<Response<object>> Handle(MoveDriveCommand request, CancellationToken cancellationToken)
            {
                var src = await _appFileRepository.GetSingleAsync(file => file.Path.EndsWith(request.SourcePath));

                if (src == null) throw new KeyNotFoundException($"File {request.SourcePath} does not exists");

                var desPathBuilder = new StringBuilder();
                int? desPathId = null;
                if (!string.IsNullOrEmpty(request.DestinationPath))
                {
                    var des = await _appFileRepository.GetSingleAsync(file =>
                        file.Path.EndsWith(request.DestinationPath) && file.FileType == FileType.Folder);

                    if (des == null) throw new KeyNotFoundException("Destination folder does not exists");

                    desPathBuilder.Append(des.Path);
                    desPathId = des.Id;
                }
                else
                {
                    desPathBuilder.Append($"/{_companyService.CompanyId}/{_authUserService.UserId}");
                }

                desPathBuilder.Append('/');
                desPathBuilder.Append(src.Name);

                var desPath = desPathBuilder.ToString();

                var result = await _driveStorageService.MoveAsync(src.Path, desPath);

                src.Path = result.Path;
                src.Name = result.Name;
                src.FileId = desPathId;

                var fileResult = await _appFileRepository.UpdateAsync(src);

                await _unitOfWork.CommitAsync();

                return new Response<object>(result);
            }
        }
    }
}
