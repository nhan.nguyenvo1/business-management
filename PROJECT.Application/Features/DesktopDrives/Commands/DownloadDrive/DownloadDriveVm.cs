﻿namespace PROJECT.Application.Features.DesktopDrives.Commands.DownloadDrive
{
    public class DownloadDriveVm
    {
        public string Path { get; set; }
    }
}