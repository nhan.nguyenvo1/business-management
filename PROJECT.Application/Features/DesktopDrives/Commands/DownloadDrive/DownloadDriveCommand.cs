﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.DesktopDrives.Commands.DownloadDrive
{
    public class DownloadDriveCommand : IRequest<Stream>
    {
        public int FileId { get; set; }

        public class DownloadFileCommandHandler : IRequestHandler<DownloadDriveCommand, Stream>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IUnitOfWork _unitOfWork;

            public DownloadFileCommandHandler(IAppFileRepository appFileRepository,
                IDriveStorageService driveStorageService, IUnitOfWork unitOfWork)
            {
                _appFileRepository = appFileRepository;
                _driveStorageService = driveStorageService;
                _unitOfWork = unitOfWork;
            }

            public async Task<Stream> Handle(DownloadDriveCommand request,
                CancellationToken cancellationToken)
            {
                var file = await _appFileRepository.GetSingleAsync<DownloadDriveVm>(file =>
                    file.Id == request.FileId && file.FileType != FileType.Folder);

                if (file == null) throw new KeyNotFoundException("File does not exists");

                try
                {
                    var response = await _driveStorageService.DownloadAsync(file.Path);

                    return response.Content;
                }
                catch (Exception)
                {
                    await _appFileRepository.DeleteAsync(await _appFileRepository.GetByIdAsync(request.FileId));
                    await _unitOfWork.CommitAsync();
                    throw new KeyNotFoundException("File does not exists");
                }
            }
        }
    }
}