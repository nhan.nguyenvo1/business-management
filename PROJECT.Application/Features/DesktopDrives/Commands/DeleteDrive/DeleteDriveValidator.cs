﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Commands.DeleteDrive
{
    public class DeleteDriveValidator : AbstractValidator<DeleteDriveCommand>
    {
        public DeleteDriveValidator()
        {
            RuleFor(p => p.Path)
                .NotEmpty();
        }
    }
}
