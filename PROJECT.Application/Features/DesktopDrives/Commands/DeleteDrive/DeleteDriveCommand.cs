﻿using MediatR;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Commands.DeleteDrive
{
    public class DeleteDriveCommand : IRequest<Response<object>>
    {
        public string Path { get; set; }

        public class DeleteDriveCommandHandler : IRequestHandler<DeleteDriveCommand, Response<object>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IAuthUserService _authUserService;
            private readonly ICompanyService _companyService;

            public DeleteDriveCommandHandler(IAppFileRepository appFileRepository,
                IUnitOfWork unitOfWork,
                IDriveStorageService driveStorageService, IAuthUserService authUserService, ICompanyService companyService)
            {
                _appFileRepository = appFileRepository;
                _unitOfWork = unitOfWork;
                _driveStorageService = driveStorageService;
                _authUserService = authUserService;
                _companyService = companyService;
            }

            public async Task<Response<object>> Handle(DeleteDriveCommand request, CancellationToken cancellationToken)
            {
                var drive = await _appFileRepository.GetSingleAsync(d => d.Path.EndsWith(request.Path) && _authUserService.UserId.Equals(d.AppStorage.UserId));

                if (drive == null)
                {
                    throw new ApiException($"File or folder {request.Path} does not exists");
                }

                await _appFileRepository.DeleteAsync(drive);

                try
                {
                    await _driveStorageService.DeleteAsync(drive.Path);
                }
                catch (Exception)
                {
                    throw;
                }

                await _unitOfWork.CommitAsync();

                return new Response<object>(drive);
            }
        }
    }
}
