﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Commands.CreateDrive
{
    public class CreateDriveValidator : AbstractValidator<CreateDriveCommand>
    {
        public CreateDriveValidator()
        {
            RuleFor(p => p.File)
                .NotEmpty();
        }
    }
}
