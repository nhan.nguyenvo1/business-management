﻿using MediatR;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.Dtos.Drive;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Commands.CreateDrive
{
    public class CreateDriveCommand : IRequest<Response<object>>
    {
        public string FolderPath { get; set; }

        public IFormFile File { get; set; }

        public class CreateDriveCommandHandler : IRequestHandler<CreateDriveCommand, Response<object>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IStorageRepository _storageRepository;
            private readonly IUnitOfWork _unitOfWork;

            public CreateDriveCommandHandler(IUnitOfWork unitOfWork, IAppFileRepository appFileRepository,
                IDriveStorageService driveStorageService, IStorageRepository storageRepository,
                IAuthUserService authUserService)
            {
                _unitOfWork = unitOfWork;
                _appFileRepository = appFileRepository;
                _driveStorageService = driveStorageService;
                _storageRepository = storageRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<object>> Handle(CreateDriveCommand request, CancellationToken cancellationToken)
            {
                var storage = await _storageRepository.GetSingleAsync(p => p.UserId == _authUserService.UserId);
                var folder = string.IsNullOrEmpty(request.FolderPath) 
                    ? null 
                    : await _appFileRepository.GetSingleAsync(drive => drive.FileType == FileType.Folder && drive.Path.EndsWith(request.FolderPath));

                DriveUploadFileResult drive;
                if (folder != null)
                {
                    var path = $"{folder.Path}/{request.File.FileName}";
                    var stream = request.File.OpenReadStream();
                    drive = await _driveStorageService.UploadFileAsync(path, stream);
                    await _appFileRepository.AddAsync(new AppFile
                    {
                        Path = drive.Path,
                        Name = drive.FileName,
                        FileType = FileType.Other,
                        FileSize = drive.FileSize,
                        StorageId = storage.Id,
                        FileId = folder.Id
                    });
                }
                else
                {
                    var path = $"{storage.Path}/{request.File.FileName}";
                    var stream = request.File.OpenReadStream();
                    drive = await _driveStorageService.UploadFileAsync(path, stream);
                    await _appFileRepository.AddAsync(new AppFile
                    {
                        Path = drive.Path,
                        Name = drive.FileName,
                        FileType = FileType.Other,
                        FileSize = drive.FileSize,
                        StorageId = storage.Id
                    }).ConfigureAwait(false);
                }
                await _unitOfWork.CommitAsync();
                return new Response<object>(drive);
            }
        }
    }
}
