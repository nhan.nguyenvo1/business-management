﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Commands.CreateFolder
{
    public class CreateFolderValidator : AbstractValidator<CreateDesktopFolderCommand>
    {
        public CreateFolderValidator()
        {
            RuleFor(p => p.FolderName)
                .NotEmpty();
        }
    }
}
