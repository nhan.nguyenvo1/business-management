﻿using MediatR;
using PROJECT.Application.Dtos.Drive;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Commands.CreateFolder
{
    public class CreateDesktopFolderCommand : IRequest<Response<object>>
    {
        public string FolderName { get; set; }

        public string FolderPath { get; set; }

        public class CreateDesktopFolderCommandHandler : IRequestHandler<CreateDesktopFolderCommand, Response<object>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IStorageRepository _storageRepository;
            private readonly IUnitOfWork _unitOfWork;

            public CreateDesktopFolderCommandHandler(IUnitOfWork unitOfWork,
                IAppFileRepository appFileRepository,
                IDriveStorageService driveStorageService,
                IStorageRepository storageRepository,
                IAuthUserService authUserService)
            {
                _unitOfWork = unitOfWork;
                _appFileRepository = appFileRepository;
                _driveStorageService = driveStorageService;
                _storageRepository = storageRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<object>> Handle(CreateDesktopFolderCommand request, CancellationToken cancellationToken)
            {
                // find folder
                var parent = !string.IsNullOrEmpty(request.FolderPath) 
                    ? await _appFileRepository.GetSingleAsync(drive => drive.FileType == FileType.Folder && drive.Path.EndsWith(request.FolderPath))
                    : null;
                var storage =
                    await _storageRepository.GetSingleAsync(storage => storage.UserId == _authUserService.UserId);

                var appFile = new AppFile { StorageId = storage.Id };
                DriveCreateFolderResult result;
                if (parent != null)
                {
                    // create folder
                    result = await _driveStorageService.CreateFolderAsync($"{parent.Path}/{request.FolderName}");
                    appFile.Path = result.Path;
                    appFile.Name = result.Name;
                    appFile.FileId = parent.Id;
                }
                else
                {
                    result = await _driveStorageService.CreateFolderAsync($"{storage.Path}/{request.FolderName}");
                    appFile.Path = result.Path;
                    appFile.Name = result.Name;
                }

                appFile = await _appFileRepository.AddAsync(appFile);
                try
                {
                    await _unitOfWork.CommitAsync();
                }
                catch (Exception)
                {
                    throw new ApiException($"Dupplicate folder name '{request.FolderName}'");
                }

                return new Response<object>(result);
            }
        }
    }
}
