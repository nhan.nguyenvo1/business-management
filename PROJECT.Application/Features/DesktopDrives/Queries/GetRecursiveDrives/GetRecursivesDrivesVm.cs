﻿using PROJECT.Domain.Enums;
using System;

namespace PROJECT.Application.Features.DesktopDrives.Queries.GetRecursiveDrives
{
    public class GetRecursivesDrivesVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public FileType FileType { get; set; }

        public ulong? FileSize { get; set; }

        public int? FileId { get; set; }

        public string Path { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int CompanyId { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }
    }
}
