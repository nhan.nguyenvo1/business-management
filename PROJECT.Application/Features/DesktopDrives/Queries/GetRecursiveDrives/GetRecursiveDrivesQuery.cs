﻿using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.DesktopDrives.Queries.GetRecursiveDrives
{
    public class GetRecursiveDrivesQuery : IRequest<Response<IReadOnlyCollection<GetRecursivesDrivesVm>>>
    {
        public class GetRecursiveDrivesQueryHandler : IRequestHandler<GetRecursiveDrivesQuery, Response<IReadOnlyCollection<GetRecursivesDrivesVm>>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;

            public GetRecursiveDrivesQueryHandler(IAuthUserService authUserService,
                                                  IAppFileRepository appFileRepository)
            {
                _authUserService = authUserService;
                _appFileRepository = appFileRepository;
            }

            public async Task<Response<IReadOnlyCollection<GetRecursivesDrivesVm>>> Handle(GetRecursiveDrivesQuery request, CancellationToken cancellationToken)
            {
                return new Response<IReadOnlyCollection<GetRecursivesDrivesVm>>(await _appFileRepository.GetRecursive<GetRecursivesDrivesVm>(_authUserService.UserId));
            }
        }
    }
}
