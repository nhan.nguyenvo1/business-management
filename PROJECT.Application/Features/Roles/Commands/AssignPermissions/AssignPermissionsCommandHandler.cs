﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Commands.AssignPermissions
{
    public class AssignPermissionsCommandHandler : IRequestHandler<AssignPermissionsCommand, Response<string>>
    {
        private readonly IRoleRepository _roleRepository;

        public AssignPermissionsCommandHandler(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<Response<string>> Handle(AssignPermissionsCommand request,
            CancellationToken cancellationToken)
        {
            var appRole = await _roleRepository.GetByIdAsync(request.RoleId);

            if (appRole == null)
                throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Role", "id", request.RoleId));

            var isSucceed = await _roleRepository.AssignPermissions(appRole, request.Permissions);

            if (!isSucceed) throw new ApiException("Assign permissions fail");

            return new Response<string>(appRole.Id);
        }
    }
}