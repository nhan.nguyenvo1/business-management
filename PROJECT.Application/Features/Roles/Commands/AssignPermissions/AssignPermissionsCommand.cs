﻿using System.Collections.Generic;
using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Commands.AssignPermissions
{
    public class AssignPermissionsCommand : IRequest<Response<string>>
    {
        public string RoleId { get; set; }

        public ICollection<string> Permissions { get; set; }
    }
}