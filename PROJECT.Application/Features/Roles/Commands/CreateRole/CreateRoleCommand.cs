﻿using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Commands.CreateRole
{
    public class CreateRoleCommand : IRequest<Response<string>>
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}