﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.Roles.Commands.CreateRole
{
    public class CreateRoleCommandHandler : IRequestHandler<CreateRoleCommand, Response<string>>
    {
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;
        private readonly IRoleRepository _roleRepository;

        public CreateRoleCommandHandler(IRoleRepository roleRepository, IMapper mapper, ICompanyService companyService)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
            _companyService = companyService;
        }

        public async Task<Response<string>> Handle(CreateRoleCommand request, CancellationToken cancellationToken)
        {
            request.Name = $"{_companyService.CompanyId}-{request.Name}";
            var role = _mapper.Map<AppRole>(request);

            role = await _roleRepository.AddAsync(role);
            return new Response<string>(data: role.Id);
        }
    }
}