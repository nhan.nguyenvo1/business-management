﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Commands.CancelPermissions
{
    public class CancelPermissionsCommandHandler : IRequestHandler<CancelPermissionsCommand, Response<string>>
    {
        private readonly IRoleRepository _roleRepository;

        public CancelPermissionsCommandHandler(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<Response<string>> Handle(CancelPermissionsCommand request,
            CancellationToken cancellationToken)
        {
            var appRole = await _roleRepository.GetByIdAsync(request.RoleId);

            if (appRole == null)
                throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Role", "id", request.RoleId));

            var isSucceed = await _roleRepository.CancelPermissions(appRole, request.Permissions);

            if (!isSucceed) throw new ApiException("Cancel permissions fail");

            return new Response<string>(appRole.Id);
        }
    }
}