﻿using System.Linq;
using FluentValidation;
using PROJECT.Application.Constants;

namespace PROJECT.Application.Features.Roles.Commands.CancelPermissions
{
    public class CancelPermissionsCommandValidator : AbstractValidator<CancelPermissionsCommand>
    {
        public CancelPermissionsCommandValidator()
        {
            RuleFor(p => p.RoleId)
                .NotNull()
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Role"));

            RuleFor(p => p.Permissions)
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Permissions"))
                .Must((command, collection) => collection.Any())
                .WithMessage(string.Format(ErrorMessageConstant.LengthRequired, "1", "permission"));
        }
    }
}