﻿using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Commands.UpdateRoleById
{
    public class UpdateRoleByIdCommand : IRequest<Response<string>>
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}