﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Extensions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Commands.UpdateRoleById
{
    public class UpdateRoleByIdCommandHandler : IRequestHandler<UpdateRoleByIdCommand, Response<string>>
    {
        private readonly ICompanyService _companyService;
        private readonly IMapper _mapper;
        private readonly IRoleRepository _roleRepository;

        public UpdateRoleByIdCommandHandler(IRoleRepository roleRepository, IMapper mapper,
            ICompanyService companyService)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
            _companyService = companyService;
        }

        public async Task<Response<string>> Handle(UpdateRoleByIdCommand request, CancellationToken cancellationToken)
        {
            var role = await _roleRepository.GetByIdAsync(request.Id);

            if (role == null) throw new ApiException($"Cannot find any role with id = {request.Id}");

            request.Name = $"{_companyService.CompanyId}-{request.Name.Replace($"{ _companyService.CompanyId }-", "", 1)}";

            role = _mapper.Map(request, role);

            role = await _roleRepository.UpdateAsync(role);

            return new Response<string>(role.Id);
        }
    }
}