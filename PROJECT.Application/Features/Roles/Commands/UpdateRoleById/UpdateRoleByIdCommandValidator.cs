﻿using FluentValidation;
using PROJECT.Application.Constants;

namespace PROJECT.Application.Features.Roles.Commands.UpdateRoleById
{
    public class UpdateRoleByIdCommandValidator : AbstractValidator<UpdateRoleByIdCommand>
    {
        public UpdateRoleByIdCommandValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Role id"));

            RuleFor(p => p.Name)
                .NotNull()
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Role name"))
                .MaximumLength(50)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "Role name", 50));

            RuleFor(p => p.Description)
                .NotNull()
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Role description"))
                .MaximumLength(250)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "Role description", 250));
        }
    }
}