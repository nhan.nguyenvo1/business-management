﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Commands.DeleteRoleById
{
    public class DeleteRoleByIdCommandHandler : IRequestHandler<DeleteRoleByIdCommand, Response<string>>
    {
        private readonly IRoleRepository _roleRepository;

        public DeleteRoleByIdCommandHandler(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<Response<string>> Handle(DeleteRoleByIdCommand request, CancellationToken cancellationToken)
        {
            var role = await _roleRepository.GetByIdAsync(request.Id);

            if (role == null)
                throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Role", "id", request.Id));

            await _roleRepository.DeleteAsync(role);

            return new Response<string>(request.Id);
        }
    }
}