﻿using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Commands.DeleteRoleById
{
    public class DeleteRoleByIdCommand : IRequest<Response<string>>
    {
        public DeleteRoleByIdCommand(string id)
        {
            Id = id;
        }

        public string Id { get; }
    }
}