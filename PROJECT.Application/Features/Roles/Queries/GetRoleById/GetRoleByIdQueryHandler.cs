﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.Roles.Queries.GetRoleById
{
    public class GetRoleByIdQueryHandler : IRequestHandler<GetRoleByIdQuery, Response<GetRoleByIdViewModel>>
    {
        private readonly IMapper _mapper;
        private readonly IRoleRepository _roleRepository;

        public GetRoleByIdQueryHandler(IRoleRepository roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public async Task<Response<GetRoleByIdViewModel>> Handle(GetRoleByIdQuery request,
            CancellationToken cancellationToken)
        {
            var role = await _roleRepository.GetByIdAsync(request.Id);

            if (role == null)
                throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Role", "id", request.Id));

            return new Response<GetRoleByIdViewModel>(_mapper.Map<AppRole, GetRoleByIdViewModel>(role));
        }
    }
}