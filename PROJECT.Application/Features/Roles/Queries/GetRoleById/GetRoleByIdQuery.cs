﻿using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Queries.GetRoleById
{
    public class GetRoleByIdQuery : IRequest<Response<GetRoleByIdViewModel>>
    {
        public GetRoleByIdQuery(string id)
        {
            Id = id;
        }

        public string Id { get; }
    }
}