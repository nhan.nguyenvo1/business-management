﻿namespace PROJECT.Application.Features.Roles.Queries.GetRoleById
{
    public class GetRoleByIdViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}