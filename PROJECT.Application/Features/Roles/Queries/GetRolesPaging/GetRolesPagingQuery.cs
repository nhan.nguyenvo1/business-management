﻿using System.Collections.Generic;
using MediatR;
using PROJECT.Application.Parameters;
using PROJECT.Application.Parameters.Interfaces;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Queries.GetRolesPaging
{
    public class GetRolesPagingQuery : RequestParameter, IRequest<PagedResponse<GetRolesPagingViewModel>>, IHasSort,
        IHasSearch, IHasFilters
    {
        public Dictionary<string, string> Filters { get; set; }
        public string Search { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }
    }
}