﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Queries.GetRolesPaging
{
    public class
        GetRolesPagingQueryHandler : IRequestHandler<GetRolesPagingQuery, PagedResponse<GetRolesPagingViewModel>>
    {
        private readonly IMapper _mapper;
        private readonly IRoleRepository _roleRepository;

        public GetRolesPagingQueryHandler(IMapper mapper, IRoleRepository roleRepository)
        {
            _mapper = mapper;
            _roleRepository = roleRepository;
        }

        public async Task<PagedResponse<GetRolesPagingViewModel>> Handle(GetRolesPagingQuery request,
            CancellationToken cancellationToken)
        {
            var roles = await _roleRepository.GetPagedResponseAsync(request.Page, request.Limit, request.Search,
                request.Sort, request.Order, request.Filters);

            var total = await _roleRepository.CountAsync(request.Search, request.Filters);

            return new PagedResponse<GetRolesPagingViewModel>(
                _mapper.Map<ICollection<GetRolesPagingViewModel>>(roles),
                request.Page,
                request.Limit,
                total);
        }
    }
}