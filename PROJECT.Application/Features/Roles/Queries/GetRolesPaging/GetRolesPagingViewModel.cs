﻿namespace PROJECT.Application.Features.Roles.Queries.GetRolesPaging
{
    public class GetRolesPagingViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}