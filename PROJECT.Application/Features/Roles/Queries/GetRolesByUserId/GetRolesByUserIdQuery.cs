﻿using System.Collections.Generic;
using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Queries.GetRolesByUserId
{
    public class GetRolesByUserIdQuery : IRequest<Response<ICollection<string>>>
    {
        public string UserId { get; set; }
    }
}