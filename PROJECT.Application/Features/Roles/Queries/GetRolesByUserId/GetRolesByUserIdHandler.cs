﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Roles.Queries.GetRolesByUserId
{
    public class
        GetRolesByUserIdHandler : IRequestHandler<GetRolesByUserIdQuery,
            Response<ICollection<string>>>
    {
        private readonly IRoleRepository _roleRepository;

        public GetRolesByUserIdHandler(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<Response<ICollection<string>>> Handle(GetRolesByUserIdQuery request,
            CancellationToken cancellationToken)
        {
            return new Response<ICollection<string>>(await _roleRepository.GetByUserIdAsync(request.UserId));
        }
    }
}