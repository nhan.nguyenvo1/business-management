﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Dtos.Notifications;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Notifications.Queries.GetNotifications
{
    public class GetNotificationsQuery : IRequest<Response<IReadOnlyList<NotificationDto>>>
    {
        public class
            GetNotificationsQueryHandler : IRequestHandler<GetNotificationsQuery,
                Response<IReadOnlyList<NotificationDto>>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly INotificationRepository _notificationRepository;
            private readonly ICompanyService _companyService;

            public GetNotificationsQueryHandler(INotificationRepository notificationRepository,
                IAuthUserService authUserService, ICompanyService companyService)
            {
                _notificationRepository = notificationRepository;
                _authUserService = authUserService;
                _companyService = companyService;
            }

            public async Task<Response<IReadOnlyList<NotificationDto>>> Handle(GetNotificationsQuery request,
                CancellationToken cancellationToken)
            {
                var temp = _companyService.CompanyId;
                var notifications =
                    await _notificationRepository.GetByUserIdAsync<NotificationDto>(_authUserService.UserId);

                return new Response<IReadOnlyList<NotificationDto>>(notifications);
            }
        }
    }
}