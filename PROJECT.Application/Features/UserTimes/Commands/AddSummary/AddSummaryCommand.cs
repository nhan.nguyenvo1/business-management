﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.UserTimes.Commands.AddSummary
{
    public class AddSummaryCommand : IRequest<Response<int>>
    {
        public string Summary { get; set; }

        public class AddSummaryCommandHandler : IRequestHandler<AddSummaryCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IDateTimeService _dateTimeService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserTimeRepository _userTimeRepository;

            public AddSummaryCommandHandler(IUserTimeRepository userTimeRepository, IUnitOfWork unitOfWork,
                IDateTimeService dateTimeService, IAuthUserService authUserService)
            {
                _userTimeRepository = userTimeRepository;
                _unitOfWork = unitOfWork;
                _dateTimeService = dateTimeService;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(AddSummaryCommand request, CancellationToken cancellationToken)
            {
                var now = _dateTimeService.UtcNow;
                var userTime = await _userTimeRepository.FindSingle(usertime => usertime.Date.Date == now.Date);

                if (userTime == null)
                {
                    userTime = new UserTime
                    {
                        Summary = request.Summary,
                        Date = now,
                        UserId = _authUserService.UserId
                    };

                    userTime = await _userTimeRepository.AddAsync(userTime);
                    await _unitOfWork.CommitAsync();

                    return new Response<int>(userTime.Id);
                }

                userTime.Summary = request.Summary;

                userTime = await _userTimeRepository.UpdateAsync(userTime);
                await _unitOfWork.CommitAsync();

                return new Response<int>(userTime.Id);
            }
        }
    }
}