﻿using FluentValidation;
using PROJECT.Application.Constants;

namespace PROJECT.Application.Features.UserTimes.Commands.AddSummary
{
    public class AddSummaryCommandValidator : AbstractValidator<AddSummaryCommand>
    {
        public AddSummaryCommandValidator()
        {
            RuleFor(p => p.Summary)
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Summary"))
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Summary"));
        }
    }
}