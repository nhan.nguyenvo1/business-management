﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.UserTimes.Commands.CheckIn
{
    public class CheckInCommand : IRequest<Response<int>>
    {
        public class CheckInCommandHandler : IRequestHandler<CheckInCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IDateTimeService _dateTimeService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserTimeRepository _userTimeRepository;

            public CheckInCommandHandler(IUserTimeRepository userTimeRepository, IUnitOfWork unitOfWork,
                IAuthUserService authUserService, IDateTimeService dateTimeService)
            {
                _userTimeRepository = userTimeRepository;
                _unitOfWork = unitOfWork;
                _authUserService = authUserService;
                _dateTimeService = dateTimeService;
            }

            public async Task<Response<int>> Handle(CheckInCommand request, CancellationToken cancellationToken)
            {
                var now = _dateTimeService.UtcNow;

                if (await _userTimeRepository.AnyAsync(usertime => usertime.UserId == _authUserService.UserId
                                                                   && usertime.Date.Date == now.Date))
                    throw new ApiException(string.Format(ErrorMessageConstant.TimeError, "checked in"));

                var userTime = new UserTime
                {
                    CheckIn = now.TimeOfDay,
                    UserId = _authUserService.UserId,
                    CheckOut = null,
                    Date = now.Date
                };

                userTime = await _userTimeRepository.AddAsync(userTime);
                await _unitOfWork.CommitAsync();

                return new Response<int>(userTime.Id);
            }
        }
    }
}