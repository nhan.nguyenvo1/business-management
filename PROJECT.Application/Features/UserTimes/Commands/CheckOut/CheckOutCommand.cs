﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.UserTimes.Commands.CheckOut
{
    public class CheckOutCommand : IRequest<Response<int>>
    {
        public class CheckOutCommandHandler : IRequestHandler<CheckOutCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IDateTimeService _dateTimeService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserTimeRepository _userTimeRepository;

            public CheckOutCommandHandler(IUserTimeRepository userTimeRepository, IUnitOfWork unitOfWork,
                IAuthUserService authUserService, IDateTimeService dateTimeService)
            {
                _userTimeRepository = userTimeRepository;
                _unitOfWork = unitOfWork;
                _authUserService = authUserService;
                _dateTimeService = dateTimeService;
            }

            public async Task<Response<int>> Handle(CheckOutCommand request, CancellationToken cancellationToken)
            {
                var now = _dateTimeService.UtcNow;

                var userTime = await _userTimeRepository.FindSingle(usertime => usertime.UserId == _authUserService.UserId && usertime.Date.Date == now.Date);

                if (userTime == null) throw new ApiException(ErrorMessageConstant.CheckInError);

                userTime.CheckOut = now.TimeOfDay;

                userTime = await _userTimeRepository.UpdateAsync(userTime);

                await _unitOfWork.CommitAsync();

                return new Response<int>(userTime.Id);
            }
        }
    }
}