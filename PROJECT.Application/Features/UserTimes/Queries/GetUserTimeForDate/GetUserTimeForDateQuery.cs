﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.UserTimes.Queries.GetUserTimeForDate
{
    public class GetUserTimeForDateQuery : IRequest<Response<GetUserTimeForDateVm>>
    {
        public class
            GetUserTimeForDateQueryHandler : IRequestHandler<GetUserTimeForDateQuery, Response<GetUserTimeForDateVm>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IDateTimeService _dateTimeService;
            private readonly IUserTimeRepository _userTimeRepository;

            public GetUserTimeForDateQueryHandler(IUserTimeRepository userTimeRepository,
                IDateTimeService dateTimeService, IAuthUserService authUserService)
            {
                _userTimeRepository = userTimeRepository;
                _dateTimeService = dateTimeService;
                _authUserService = authUserService;
            }

            public async Task<Response<GetUserTimeForDateVm>> Handle(GetUserTimeForDateQuery request,
                CancellationToken cancellationToken)
            {
                var now = _dateTimeService.UtcNow;
                var userId = _authUserService.UserId;
                var userTime = await _userTimeRepository
                    .GetSingleAsync<GetUserTimeForDateVm>(
                        usertime => usertime.Date.Date == now.Date &&
                                    usertime.UserId == userId);

                if (userTime == null) return new Response<GetUserTimeForDateVm>(new GetUserTimeForDateVm());

                return new Response<GetUserTimeForDateVm>(userTime);
            }
        }
    }
}