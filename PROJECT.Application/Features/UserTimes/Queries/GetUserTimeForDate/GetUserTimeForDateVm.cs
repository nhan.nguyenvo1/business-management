﻿namespace PROJECT.Application.Features.UserTimes.Queries.GetUserTimeForDate
{
    public class GetUserTimeForDateVm
    {
        public string Date { get; set; } = string.Empty;

        public string CheckIn { get; set; } = string.Empty;

        public string CheckOut { get; set; } = string.Empty;

        public string Summary { get; set; } = string.Empty;
    }
}