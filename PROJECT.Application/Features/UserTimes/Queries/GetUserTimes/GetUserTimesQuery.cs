﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Parameters;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.UserTimes.Queries.GetUserTimes
{
    public class GetUserTimesQuery : RequestParameter, IRequest<PagedResponse<GetUserTimesVm>>
    {
        public string Search { get; set; }

        public class GetUserTimesQueryHandler : IRequestHandler<GetUserTimesQuery, PagedResponse<GetUserTimesVm>>
        {
            private readonly IUserTimeRepository _userTimeRepository;

            public GetUserTimesQueryHandler(IUserTimeRepository userTimeRepository)
            {
                _userTimeRepository = userTimeRepository;
            }

            public async Task<PagedResponse<GetUserTimesVm>> Handle(GetUserTimesQuery request,
                CancellationToken cancellationToken)
            {
                return new(
                    await _userTimeRepository.GetPagingAsync<GetUserTimesVm>(request.Page, request.Limit,
                        request.Search),
                    request.Page,
                    request.Limit,
                    await _userTimeRepository.CountAsync(request.Search));
            }
        }
    }
}