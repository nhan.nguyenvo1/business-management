﻿namespace PROJECT.Application.Features.UserTimes.Queries.GetUserTimes
{
    public class WorkTimesVm
    {
        public string CheckIn { get; set; }

        public string CheckOut { get; set; }

        public string Date { get; set; }

        public string Summary { get; set; }
    }
}