﻿using System.Collections.Generic;

namespace PROJECT.Application.Features.UserTimes.Queries.GetUserTimes
{
    public class GetUserTimesVm
    {
        public string UserId { get; set; }


        public string FullName { get; set; }

        public string Avatar { get; set; }


        public ICollection<WorkTimesVm> Worktimes { get; set; }
    }
}