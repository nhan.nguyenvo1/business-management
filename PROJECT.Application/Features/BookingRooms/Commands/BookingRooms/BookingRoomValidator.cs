﻿using System;
using FluentValidation;

namespace PROJECT.Application.Features.BookingRooms.Commands.BookingRooms
{
    public class BookingRoomValidator : AbstractValidator<BookingRoomCommand>
    {
        public BookingRoomValidator()
        {
            RuleFor(p => p.Title)
                .NotEmpty()
                .NotNull()
                .MaximumLength(200);

            RuleFor(p => p.Date)
                .NotNull()
                .Must(p => p.Date >= DateTime.Now.Date);

            RuleFor(p => p.From)
                .NotNull();

            RuleFor(p => p.From)
                .Must((request, from) => from > DateTime.Now.TimeOfDay)
                .When(request => request.Date.Date == DateTime.Now.Date);

            RuleFor(p => p.To)
                .Must((request, to) => request.From < request.To.Value)
                .When(request => request.To.HasValue);

            RuleFor(p => p.UserIds)
                .NotEmpty()
                .NotNull();
        }
    }
}