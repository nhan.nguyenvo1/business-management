﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.BookingRooms.Commands.BookingRooms
{
    public class BookingRoomCommand : IRequest<Response<int>>
    {
        public BookingType Type { get; set; } = BookingType.PeerToPeer;

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan From { get; set; }

        public TimeSpan? To { get; set; }

        public ICollection<string> UserIds { get; set; }

        public class BookingRoomCommandHandler : IRequestHandler<BookingRoomCommand, Response<int>>
        {
            private readonly IBookingRoomRepository _bookingRoomRepository;
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IAuthUserService _authUserService;

            public BookingRoomCommandHandler(IUnitOfWork unitOfWork,
                                             IBookingRoomRepository bookingRoomRepository,
                                             IMapper mapper, 
                                             IAuthUserService authUserService)
            {
                _unitOfWork = unitOfWork;
                _bookingRoomRepository = bookingRoomRepository;
                _mapper = mapper;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(BookingRoomCommand request, CancellationToken cancellationToken)
            {
                var bookingRoom = _mapper.Map<BookingRoom>(request);
                var transaction = await _unitOfWork.BeginTransactionAsync();

                try
                {
                    bookingRoom = await _bookingRoomRepository.AddAsync(bookingRoom);
                    await _unitOfWork.CommitAsync();

                    await _bookingRoomRepository.AddUserToRoomAsync(bookingRoom.Id, request.UserIds);
                    await _unitOfWork.CommitAsync();

                    await transaction.CommitAsync(cancellationToken);
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync(cancellationToken);
                    throw;
                }

                return new Response<int>(bookingRoom.Id);
            }
        }
    }
}