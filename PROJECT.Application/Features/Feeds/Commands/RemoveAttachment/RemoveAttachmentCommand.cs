﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Feeds.Commands.RemoveAttachment
{
    public class RemoveAttachmentCommand : IRequest<Response<int>>
    {
        public int PostId { get; set; }

        public int AttachmentId { get; set; }

        public class RemoveAttachmentCommandHandler : IRequestHandler<RemoveAttachmentCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly ICloudinaryService _cloudinaryService;
            private readonly IPostRepository _postRepository;
            private readonly IUnitOfWork _unitOfWork;

            public RemoveAttachmentCommandHandler(IUnitOfWork unitOfWork, IAuthUserService authUserService,
                IPostRepository postRepository, ICloudinaryService cloudinaryService)
            {
                _unitOfWork = unitOfWork;
                _authUserService = authUserService;
                _postRepository = postRepository;
                _cloudinaryService = cloudinaryService;
            }

            public async Task<Response<int>> Handle(RemoveAttachmentCommand request,
                CancellationToken cancellationToken)
            {
                var userId = _authUserService.UserId;
                var feed = await _postRepository.GetSingleAsync(feed =>
                    feed.Id == request.PostId && feed.CreatedBy.Equals(userId));

                if (feed == null)
                    throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "Feed", "id",
                        request.PostId));

                if (feed.Status == PostStatus.Approved) throw new ApiException("Can not edit approved posts");

                var attachment =
                    feed.PostAttachments.FirstOrDefault(attachment => attachment.Id == request.AttachmentId);

                await _cloudinaryService.RemoveAsync(attachment.PublicId);

                feed.PostAttachments.Remove(attachment);

                await _unitOfWork.CommitAsync();

                return new Response<int>(feed.Id);
            }
        }
    }
}