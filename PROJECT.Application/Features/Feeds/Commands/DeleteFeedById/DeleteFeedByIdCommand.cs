﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Feeds.Commands.DeleteFeedById
{
    public class DeleteFeedByIdCommand : IRequest<Response<int>>
    {
        public DeleteFeedByIdCommand(int id)
        {
            Id = id;
        }

        public int Id { get; }

        public class DeleteFeedByIdCommandHandler : IRequestHandler<DeleteFeedByIdCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IMapper _mapper;
            private readonly IPostRepository _postRepository;
            private readonly IUnitOfWork _unitOfWork;

            public DeleteFeedByIdCommandHandler(IPostRepository postRepository, IMapper mapper, IUnitOfWork unitOfWork,
                IAuthUserService authUserService)
            {
                _postRepository = postRepository;
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(DeleteFeedByIdCommand request, CancellationToken cancellationToken)
            {
                var userId = _authUserService.UserId;
                var feed = await _postRepository.GetSingleAsync(feed =>
                    feed.Id == request.Id && feed.CreatedBy == userId);

                if (feed == null)
                    throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "Feed", "id",
                        request.Id));

                await _postRepository.DeleteAsync(feed);
                await _unitOfWork.CommitAsync();

                return new Response<int>(request.Id);
            }
        }
    }
}