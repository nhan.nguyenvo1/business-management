﻿using FluentValidation;
using PROJECT.Application.Constants;
using PROJECT.Application.ValidatorExtensions;

namespace PROJECT.Application.Features.Feeds.Commands.CreatePost
{
    public class CreatePostCommandValidator : AbstractValidator<CreatePostCommand>
    {
        public CreatePostCommandValidator()
        {
            RuleFor(p => p.Title)
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Title"))
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Title"));

            RuleFor(p => p.Description)
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Description"))
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Description"));

            RuleFor(p => p.BriefDescription)
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Description"))
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Description"));

            RuleForEach(p => p.Attachments)
                .SetValidator(new IFormFileValidator());
        }
    }
}