﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.Feeds.Commands.CreatePost
{
    public class CreatePostCommandHandler : IRequestHandler<CreatePostCommand, Response<int>>
    {
        private readonly ICloudinaryService _cloudinaryService;
        private readonly IMapper _mapper;
        private readonly IPostAttachmentRepository _postAttachmentRepository;
        private readonly IPostRepository _postRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreatePostCommandHandler(IMapper mapper, IPostRepository postRepository, IUnitOfWork unitOfWork,
            ICloudinaryService cloudinaryService, IPostAttachmentRepository postAttachmentRepository)
        {
            _mapper = mapper;
            _postRepository = postRepository;
            _unitOfWork = unitOfWork;
            _cloudinaryService = cloudinaryService;
            _postAttachmentRepository = postAttachmentRepository;
        }


        public async Task<Response<int>> Handle(CreatePostCommand request, CancellationToken cancellationToken)
        {
            // Save post
            var post = _mapper.Map<Post>(request);

            post = await _postRepository.AddAsync(post);
            await _unitOfWork.CommitAsync();
            // Save files
            if (!request.Attachments.Any()) return new Response<int>(post.Id);

            foreach (var attachment in request.Attachments)
            {
                await using var stream = attachment.OpenReadStream();
                var cloudinaryResponse =
                    await _cloudinaryService.UploadAsync(attachment.FileName, stream, false, $"Posts/{post.Id}");
                var postAttachment = new PostAttachment
                {
                    FileName = attachment.FileName,
                    Url = cloudinaryResponse.Url,
                    PublicId = cloudinaryResponse.PublicId,
                    PostId = post.Id
                };
                await _postAttachmentRepository.AddAsync(postAttachment);
            }

            await _unitOfWork.CommitAsync();

            return new Response<int>(post.Id);
        }
    }
}