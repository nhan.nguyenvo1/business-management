﻿using System.Collections.Generic;
using MediatR;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Feeds.Commands.CreatePost
{
    public class CreatePostCommand : IRequest<Response<int>>
    {
        public string Title { get; set; }

        public string BriefDescription { get; set; }

        public string Description { get; set; }

        public ICollection<IFormFile> Attachments { get; set; } = new List<IFormFile>();
    }
}