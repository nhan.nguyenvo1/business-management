﻿using FluentValidation;
using PROJECT.Application.Constants;

namespace PROJECT.Application.Features.Feeds.Commands.ApprovePost
{
    public class ApprovePostCommandValidator : AbstractValidator<ApprovePostCommand>
    {
        public ApprovePostCommandValidator()
        {
            RuleFor(p => p.PostId)
                .NotEmpty()
                .Must(p => p > 0)
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Post"));
        }
    }
}