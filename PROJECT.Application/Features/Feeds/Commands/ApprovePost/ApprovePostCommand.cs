﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Feeds.Commands.ApprovePost
{
    public class ApprovePostCommand : IRequest<Response<int>>
    {
        public int PostId { get; set; }

        public class ApprovePostCommandHandler : IRequestHandler<ApprovePostCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IPostRepository _postRepository;
            private readonly IUnitOfWork _unitOfWork;

            public ApprovePostCommandHandler(IPostRepository postRepository, IUnitOfWork unitOfWork,
                IAuthUserService authUserService)
            {
                _postRepository = postRepository;
                _unitOfWork = unitOfWork;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(ApprovePostCommand request, CancellationToken cancellationToken)
            {
                var post = await _postRepository.GetByIdAsync(request.PostId);

                if (post == null)
                    throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Post", "id", request.PostId));

                if (post.Status == PostStatus.Approved)
                    throw new ApiException($"Post with id = {post.Id} already approve");

                post.Status = PostStatus.Approved;
                post.ApprovedBy = _authUserService.UserId;
                post = await _postRepository.UpdateAsync(post);
                await _unitOfWork.CommitAsync();

                return new Response<int>(post.Id, post.CreatedBy);
            }
        }
    }
}