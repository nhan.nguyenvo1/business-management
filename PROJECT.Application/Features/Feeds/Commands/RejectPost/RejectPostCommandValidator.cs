﻿using FluentValidation;
using PROJECT.Application.Constants;
using PROJECT.Application.Features.Feeds.Commands.ApprovePost;

namespace PROJECT.Application.Features.Feeds.Commands.RejectPost
{
    public class RejectPostCommandValidator : AbstractValidator<ApprovePostCommand>
    {
        public RejectPostCommandValidator()
        {
            RuleFor(p => p.PostId)
                .NotEmpty()
                .Must(p => p > 0)
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Post"));
        }
    }
}