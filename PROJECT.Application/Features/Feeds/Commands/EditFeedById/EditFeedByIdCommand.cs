﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Feeds.Commands.EditFeedById
{
    public class EditFeedByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string BriefDescription { get; set; }

        public string Description { get; set; }

        public class EditFeedByIdCommandHandler : IRequestHandler<EditFeedByIdCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly IMapper _mapper;
            private readonly IPostRepository _postRepository;
            private readonly IUnitOfWork _unitOfWork;

            public EditFeedByIdCommandHandler(IPostRepository postRepository,
                IMapper mapper,
                IUnitOfWork unitOfWork,
                IAuthUserService authUserService)
            {
                _postRepository = postRepository;
                _mapper = mapper;
                _unitOfWork = unitOfWork;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(EditFeedByIdCommand request, CancellationToken cancellationToken)
            {
                var userId = _authUserService.UserId;
                var feed = await _postRepository.GetSingleAsync(feed =>
                    feed.Id == request.Id && feed.CreatedBy.Equals(userId));

                if (feed == null)
                    throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "Feed", "id",
                        request.Id));

                if (feed.Status == PostStatus.Approved) throw new ApiException("Can not edit approved posts");

                feed = _mapper.Map(request, feed);

                feed = await _postRepository.UpdateAsync(feed);

                await _unitOfWork.CommitAsync();

                return new Response<int>(feed.Id);
            }
        }
    }
}