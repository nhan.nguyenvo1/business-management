﻿using FluentValidation;
using PROJECT.Application.Constants;
using PROJECT.Application.Interfaces.Repositories;

namespace PROJECT.Application.Features.Feeds.Commands.EditFeedById
{
    public class EditFeedByIdValidator : AbstractValidator<EditFeedByIdCommand>
    {
        public EditFeedByIdValidator(IPostRepository postRepository)
        {
            RuleFor(p => p.Title)
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Title"))
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Title"));

            RuleFor(p => p.Description)
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Description"))
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Description"));

            RuleFor(p => p.BriefDescription)
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Brief Description"))
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Brief Description"));
        }
    }
}