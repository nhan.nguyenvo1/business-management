﻿namespace PROJECT.Application.Features.Feeds.Commands.EditFeedById
{
    public class EditFeedByIdRequest
    {
        public string Title { get; set; }

        public string BriefDescription { get; set; }

        public string Description { get; set; }
    }
}