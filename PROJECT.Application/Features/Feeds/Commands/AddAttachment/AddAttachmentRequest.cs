﻿using Microsoft.AspNetCore.Http;

namespace PROJECT.Application.Features.Feeds.Commands.AddAttachment
{
    public class AddAttachmentRequest
    {
        public IFormFile Attachment { get; set; }
    }
}