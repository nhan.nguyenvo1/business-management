﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Feeds.Commands.AddAttachment
{
    public class AddAttachmentCommand : IRequest<Response<int>>
    {
        public int PostId { get; set; }

        public IFormFile Attachment { get; set; }

        public class AddAttachmentCommandHandler : IRequestHandler<AddAttachmentCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly ICloudinaryService _cloudinaryService;
            private readonly IPostRepository _postRepository;
            private readonly IUnitOfWork _unitOfWork;

            public AddAttachmentCommandHandler(IUnitOfWork unitOfWork, IPostRepository postRepository,
                ICloudinaryService cloudinaryService, IAuthUserService authUserService)
            {
                _unitOfWork = unitOfWork;
                _postRepository = postRepository;
                _cloudinaryService = cloudinaryService;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(AddAttachmentCommand request, CancellationToken cancellationToken)
            {
                var userId = _authUserService.UserId;
                var feed = await _postRepository.GetSingleAsync(feed =>
                    feed.Id == request.PostId && feed.CreatedBy.Equals(userId));

                if (feed == null)
                    throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "Feed", "id",
                        request.PostId));

                if (feed.Status == PostStatus.Approved) throw new ApiException("Can not edit approved posts");

                var cloudinaryResponse = await _cloudinaryService.UploadAsync(request.Attachment.FileName,
                    request.Attachment.OpenReadStream(), false,
                    $"Posts/{feed.Id}");

                feed.PostAttachments.Add(new PostAttachment
                {
                    Url = cloudinaryResponse.Url,
                    FileName = request.Attachment.FileName,
                    PublicId = cloudinaryResponse.PublicId
                });

                await _unitOfWork.CommitAsync();

                return new Response<int>(feed.Id);
            }
        }
    }
}