﻿using FluentValidation;
using PROJECT.Application.Constants;
using PROJECT.Application.ValidatorExtensions;

namespace PROJECT.Application.Features.Feeds.Commands.AddAttachment
{
    public class AddAttachmentValidator : AbstractValidator<AddAttachmentRequest>
    {
        public AddAttachmentValidator()
        {
            RuleFor(p => p.Attachment)
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Attachment"));

            RuleFor(p => p.Attachment)
                .SetValidator(new IFormFileValidator());
        }
    }
}