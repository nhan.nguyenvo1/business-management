﻿namespace PROJECT.Application.Features.Feeds.Queries.GetApprovedPostsPaging
{
    public class GetApprovedPostsPagingViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string BriefDescription { get; set; }

        public string Status { get; set; }

        public string CreatedDate { get; set; }

        public ApprovedPostUserViewModel Author { get; set; }

        public ApprovedPostUserViewModel ApprovedUser { get; set; }
    }
}