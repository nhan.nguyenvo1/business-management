﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Parameters;
using PROJECT.Application.Parameters.Interfaces;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Feeds.Queries.GetApprovedPostsPaging
{
    public class GetApprovedPostsPagingQuery : RequestParameter,
        IRequest<PagedResponse<GetApprovedPostsPagingViewModel>>, IHasSort, IHasSearch, IHasFilters
    {
        public Dictionary<string, string> Filters { get; set; }
        public string Search { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }

        public class
            GetApprovedPostsPagingQueryHandler : IRequestHandler<GetApprovedPostsPagingQuery,
                PagedResponse<GetApprovedPostsPagingViewModel>>
        {
            private readonly IMapper _mapper;
            private readonly IPostRepository _postRepository;

            public GetApprovedPostsPagingQueryHandler(IPostRepository postRepository, IMapper mapper)
            {
                _postRepository = postRepository;
                _mapper = mapper;
            }

            public async Task<PagedResponse<GetApprovedPostsPagingViewModel>> Handle(
                GetApprovedPostsPagingQuery request, CancellationToken cancellationToken)
            {
                var posts = await _postRepository.GetPagedResponseAsync<GetApprovedPostsPagingViewModel>(request.Page,
                    request.Limit, request.Search,
                    request.Sort, request.Order, request.Filters, isRaw: false, personal: false);
                var total = await _postRepository.CountAsync(request.Search, request.Filters, false);

                return new PagedResponse<GetApprovedPostsPagingViewModel>(posts, request.Page, request.Limit, total);
            }
        }
    }
}