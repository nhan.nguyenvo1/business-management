﻿namespace PROJECT.Application.Features.Feeds.Queries.GetApprovedPostsPaging
{
    public class ApprovedPostUserViewModel
    {
        public string Id { get; set; }

        public string AvatarUrl { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}