﻿namespace PROJECT.Application.Features.Feeds.Queries.GetPostById
{
    public class PostAttachmentViewModel
    {
        public string Url { get; set; }

        public int Id { get; set; }

        public string FileName { get; set; }

        public int PostId { get; set; }
    }
}