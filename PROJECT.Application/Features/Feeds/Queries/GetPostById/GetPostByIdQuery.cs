﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Feeds.Queries.GetPostById
{
    public class GetPostByIdQuery : IRequest<Response<GetPostByIdQueryViewModel>>
    {
        public GetPostByIdQuery(int id, bool isRaw)
        {
            Id = id;
            IsRaw = isRaw;
        }

        public int Id { get; }

        public bool IsRaw { get; }

        public class GetPostByIdQueryHandler : IRequestHandler<GetPostByIdQuery, Response<GetPostByIdQueryViewModel>>
        {
            private readonly IMapper _mapper;
            private readonly IPostRepository _postRepository;

            public GetPostByIdQueryHandler(IPostRepository postRepository, IMapper mapper)
            {
                _postRepository = postRepository;
                _mapper = mapper;
            }

            public async Task<Response<GetPostByIdQueryViewModel>> Handle(GetPostByIdQuery request,
                CancellationToken cancellationToken)
            {
                var post = await _postRepository.GetByIdAsync(request.Id);

                if (post == null)
                    throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Post", "id", request.Id));

                if (!request.IsRaw && post.Status != PostStatus.Approved)
                    throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Post", "id", request.Id));

                return new Response<GetPostByIdQueryViewModel>(_mapper.Map<GetPostByIdQueryViewModel>(post));
            }
        }
    }
}