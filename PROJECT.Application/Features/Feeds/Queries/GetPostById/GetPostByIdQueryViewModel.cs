﻿using System.Collections.Generic;

namespace PROJECT.Application.Features.Feeds.Queries.GetPostById
{
    public class GetPostByIdQueryViewModel
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string BriefDescription { get; set; }

        public string Description { get; set; }

        public string CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string Status { get; set; }

        public ICollection<PostAttachmentViewModel> PostAttachments { get; set; }

        public PostUserViewModel Author { get; set; }

        public PostUserViewModel ApprovedUser { get; set; }
    }
}