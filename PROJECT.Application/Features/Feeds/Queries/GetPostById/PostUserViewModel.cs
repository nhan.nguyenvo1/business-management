﻿namespace PROJECT.Application.Features.Feeds.Queries.GetPostById
{
    public class PostUserViewModel
    {
        public string Id { get; set; }

        public string AvatarUrl { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}