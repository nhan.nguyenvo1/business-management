﻿namespace PROJECT.Application.Features.Feeds.Queries.GetPostsPaging
{
    public class GetPostsPagingViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Status { get; set; }

        public string CreatedDate { get; set; }

        public PostAuthorViewModel Author { get; set; }

        public PostAuthorViewModel ApprovedUser { get; set; }
    }
}