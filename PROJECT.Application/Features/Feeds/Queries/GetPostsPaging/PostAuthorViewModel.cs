﻿namespace PROJECT.Application.Features.Feeds.Queries.GetPostsPaging
{
    public class PostAuthorViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}