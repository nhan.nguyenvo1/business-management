﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Parameters;
using PROJECT.Application.Parameters.Interfaces;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Feeds.Queries.GetPostsPaging
{
    public class GetPostsPagingQuery : RequestParameter, IRequest<PagedResponse<GetPostsPagingViewModel>>, IHasSort,
        IHasSearch, IHasFilters
    {
        public Dictionary<string, string> Filters { get; set; }
        public string Search { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }

        public class
            GetPostsPagingQueryHandler : IRequestHandler<GetPostsPagingQuery, PagedResponse<GetPostsPagingViewModel>>
        {
            private readonly IMapper _mapper;
            private readonly IPostRepository _postRepository;

            public GetPostsPagingQueryHandler(IPostRepository postRepository, IMapper mapper)
            {
                _postRepository = postRepository;
                _mapper = mapper;
            }

            public async Task<PagedResponse<GetPostsPagingViewModel>> Handle(GetPostsPagingQuery request,
                CancellationToken cancellationToken)
            {
                var posts = await _postRepository.GetPagedResponseAsync<GetPostsPagingViewModel>(request.Page,
                    request.Limit, request.Search,
                    request.Sort, request.Order, request.Filters, isRaw: true, personal: false);
                var total = await _postRepository.CountAsync(request.Search, request.Filters);

                return new PagedResponse<GetPostsPagingViewModel>(posts, request.Page, request.Limit, total);
            }
        }
    }
}