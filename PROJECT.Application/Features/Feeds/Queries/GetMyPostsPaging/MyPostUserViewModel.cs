﻿namespace PROJECT.Application.Features.Feeds.Queries.GetMyPostsPaging
{
    public class MyPostUserViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}