﻿namespace PROJECT.Application.Features.Feeds.Queries.GetMyPostsPaging
{
    public class GetMyPostsPagingViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Status { get; set; }

        public string CreatedDate { get; set; }

        public MyPostUserViewModel ApprovedUser { get; set; }
    }
}