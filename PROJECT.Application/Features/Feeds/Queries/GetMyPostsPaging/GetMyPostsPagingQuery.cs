﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Parameters;
using PROJECT.Application.Parameters.Interfaces;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Feeds.Queries.GetMyPostsPaging
{
    public class GetMyPostsPagingQuery : RequestParameter, IRequest<PagedResponse<GetMyPostsPagingViewModel>>, IHasSort,
        IHasSearch, IHasFilters
    {
        public Dictionary<string, string> Filters { get; set; }
        public string Search { get; set; }

        public string Order { get; set; }

        public string Sort { get; set; }

        public class
            GetMyPostsPagingQueryHandler : IRequestHandler<GetMyPostsPagingQuery,
                PagedResponse<GetMyPostsPagingViewModel>>
        {
            private readonly IMapper _mapper;
            private readonly IPostRepository _postRepository;

            public GetMyPostsPagingQueryHandler(IPostRepository postRepository, IMapper mapper)
            {
                _postRepository = postRepository;
                _mapper = mapper;
            }


            public async Task<PagedResponse<GetMyPostsPagingViewModel>> Handle(GetMyPostsPagingQuery request,
                CancellationToken cancellationToken)
            {
                var posts = await _postRepository.GetPagedResponseAsync<GetMyPostsPagingViewModel>(request.Page,
                    request.Limit, request.Search,
                    request.Sort, request.Order, request.Filters, isRaw: true, personal: true);
                var total = await _postRepository.CountAsync(request.Search, request.Filters, true, true);

                return new PagedResponse<GetMyPostsPagingViewModel>(posts, request.Page, request.Limit, total);
            }
        }
    }
}