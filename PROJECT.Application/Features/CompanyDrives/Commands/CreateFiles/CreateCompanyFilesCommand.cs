﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.CompanyDrives.Commands.CreateFiles
{
    public class CreateCompanyFilesCommand : IRequest<Response<bool>>
    {
        public int? FolderId { get; set; }

        public ICollection<IFormFile> Files { get; set; }


        public class CreateCompanyFilesCommandHandler : IRequestHandler<CreateCompanyFilesCommand, Response<bool>>
        {
            private readonly ICompanyDriveRepository _companyDriveRepository;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IStorageRepository _storageRepository;
            private readonly IUnitOfWork _unitOfWork;

            public CreateCompanyFilesCommandHandler(IUnitOfWork unitOfWork,
                ICompanyDriveRepository appFileRepository,
                IDriveStorageService driveStorageService,
                IStorageRepository storageRepository)
            {
                _unitOfWork = unitOfWork;
                _companyDriveRepository = appFileRepository;
                _driveStorageService = driveStorageService;
                _storageRepository = storageRepository;
            }

            public async Task<Response<bool>> Handle(CreateCompanyFilesCommand request,
                CancellationToken cancellationToken)
            {
                var storage = await _storageRepository.GetSingleAsync(p => string.IsNullOrEmpty(p.UserId));
                var folder = await _companyDriveRepository.GetByIdAsync(request.FolderId ?? 0);

                if (folder != null)
                    foreach (var file in request.Files)
                    {
                        var path = $"{folder.Path}/{file.FileName}";
                        var stream = file.OpenReadStream();
                        var result = await _driveStorageService.UploadFileAsync(path, stream);
                        await _companyDriveRepository.AddAsync(new CompanyDrive
                        {
                            Path = result.Path,
                            Name = result.FileName,
                            FileType = FileType.Other,
                            FileSize = result.FileSize,
                            StorageId = storage.Id,
                            FileId = folder.Id
                        }).ConfigureAwait(false);
                    }
                else
                    foreach (var file in request.Files)
                    {
                        var path = $"{storage.Path}/{file.FileName}";
                        var stream = file.OpenReadStream();
                        var result = await _driveStorageService.UploadFileAsync(path, stream);
                        await _companyDriveRepository.AddAsync(new CompanyDrive
                        {
                            Path = result.Path,
                            Name = result.FileName,
                            FileType = FileType.Other,
                            FileSize = result.FileSize,
                            StorageId = storage.Id
                        }).ConfigureAwait(false);
                    }

                await _unitOfWork.CommitAsync();

                return new Response<bool>(true);
            }
        }
    }
}