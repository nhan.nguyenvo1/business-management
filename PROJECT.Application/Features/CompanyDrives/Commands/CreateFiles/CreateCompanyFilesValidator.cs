using FluentValidation;
using PROJECT.Application.ValidatorExtensions;

namespace PROJECT.Application.Features.CompanyDrives.Commands.CreateFiles
{
    public class CreateCompanyFilesValidator : AbstractValidator<CreateCompanyFilesCommand>
    {
        public CreateCompanyFilesValidator()
        {
            RuleFor(p => p.Files)
                .NotEmpty();

            RuleForEach(p => p.Files)
                .SetValidator(new IFormFileValidator());
        }
    }
}