﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.CompanyDrives.Commands.CreateFolder
{
    public class CreateCompanyFolderCommand : IRequest<Response<int>>
    {
        public string FolderName { get; set; }

        public int? ParentId { get; set; }

        public class CreateCompanyFolderCommandHandler : IRequestHandler<CreateCompanyFolderCommand, Response<int>>
        {
            private readonly ICompanyDriveRepository _companyDriveRepository;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IStorageRepository _storageRepository;
            private readonly IUnitOfWork _unitOfWork;

            public CreateCompanyFolderCommandHandler(IUnitOfWork unitOfWork,
                ICompanyDriveRepository appFileRepository,
                IDriveStorageService driveStorageService,
                IStorageRepository storageRepository)
            {
                _unitOfWork = unitOfWork;
                _companyDriveRepository = appFileRepository;
                _driveStorageService = driveStorageService;
                _storageRepository = storageRepository;
            }

            public async Task<Response<int>> Handle(CreateCompanyFolderCommand request,
                CancellationToken cancellationToken)
            {
                // find folder
                var parent = await _companyDriveRepository.GetByIdAsync(request.ParentId ?? 0);
                var storage = await _storageRepository.GetSingleAsync(storage => string.IsNullOrEmpty(storage.UserId));

                var appFile = new CompanyDrive {StorageId = storage.Id};
                if (parent != null)
                {
                    // create folder
                    var result = await _driveStorageService.CreateFolderAsync($"{parent.Path}/{request.FolderName}");
                    appFile.Path = result.Path;
                    appFile.Name = result.Name;
                    appFile.FileId = parent.Id;
                }
                else
                {
                    var result = await _driveStorageService.CreateFolderAsync($"{storage.Path}/{request.FolderName}");
                    appFile.Path = result.Path;
                    appFile.Name = result.Name;
                }

                appFile = await _companyDriveRepository.AddAsync(appFile);
                try
                {
                    await _unitOfWork.CommitAsync();
                }
                catch (Exception)
                {
                    throw new ApiException($"Dupplicate folder name '{request.FolderName}'");
                }

                return new Response<int>(appFile.Id);
            }
        }
    }
}