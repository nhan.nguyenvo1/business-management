﻿using FluentValidation;

namespace PROJECT.Application.Features.CompanyDrives.Commands.CreateFolder
{
    public class CreateCompanyFolderValidator : AbstractValidator<CreateCompanyFolderCommand>
    {
        public CreateCompanyFolderValidator()
        {
            RuleFor(p => p.FolderName)
                .NotEmpty()
                .NotNull();
        }
    }
}