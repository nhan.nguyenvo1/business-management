﻿using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.CompanyDrives.Commands.Copy
{
    public class CompanyDriveCopyVm
    {
        public int Id { get; set; }

        public string Path { get; set; }

        public string Name { get; set; }

        public int StorageId { get; set; }

        public FileType FileType { get; set; }

        public ulong? FileSize { get; set; }
    }
}