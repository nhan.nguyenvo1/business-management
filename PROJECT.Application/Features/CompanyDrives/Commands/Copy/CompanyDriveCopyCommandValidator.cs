﻿using FluentValidation;

namespace PROJECT.Application.Features.CompanyDrives.Commands.Copy
{
    public class CompanyDriveCopyCommandValidator : AbstractValidator<CompanyDriveCopyCommand>
    {
        public CompanyDriveCopyCommandValidator()
        {
            RuleFor(p => p.DriveId)
                .NotEmpty();
        }
    }
}