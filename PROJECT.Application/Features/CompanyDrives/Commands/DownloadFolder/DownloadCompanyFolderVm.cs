﻿namespace PROJECT.Application.Features.CompanyDrives.Commands.DownloadFolder
{
    public class DownloadCompanyFolderVm
    {
        public string Path { get; set; }
    }
}