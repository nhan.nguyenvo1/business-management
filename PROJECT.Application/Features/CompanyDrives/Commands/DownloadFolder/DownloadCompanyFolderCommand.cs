﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Dtos.Drive;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.CompanyDrives.Commands.DownloadFolder
{
    public class DownloadCompanyFolderCommand : IRequest<Response<DriveDownloadResponse>>
    {
        public int FolderId { get; set; }

        public class
            DownloadCompanyFolderCommandHandler : IRequestHandler<DownloadCompanyFolderCommand,
                Response<DriveDownloadResponse>>
        {
            private readonly ICompanyDriveRepository _companyDriveRepository;
            private readonly IDriveStorageService _driveStorageService;

            public DownloadCompanyFolderCommandHandler(ICompanyDriveRepository appFileRepository,
                IDriveStorageService driveStorageService)
            {
                _companyDriveRepository = appFileRepository;
                _driveStorageService = driveStorageService;
            }

            public async Task<Response<DriveDownloadResponse>> Handle(DownloadCompanyFolderCommand request,
                CancellationToken cancellationToken)
            {
                var folder = await _companyDriveRepository.GetSingleAsync<DownloadCompanyFolderVm>(file =>
                    file.Id == request.FolderId && file.FileType == FileType.Folder);

                if (folder == null) throw new KeyNotFoundException("File does not exists");

                var response = await _driveStorageService.DownloadZipAsync(folder.Path);

                return new Response<DriveDownloadResponse>(response);
            }
        }
    }
}