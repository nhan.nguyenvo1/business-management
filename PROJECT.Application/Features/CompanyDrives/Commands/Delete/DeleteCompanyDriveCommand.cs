﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.CompanyDrives.Commands.Delete
{
    public class DeleteCompanyDriveCommand : IRequest<Response<bool>>
    {
        public int DriveId { get; set; }

        public class DeleteCompanyDriveCommandHandler : IRequestHandler<DeleteCompanyDriveCommand, Response<bool>>
        {
            private readonly ICompanyDriveRepository _companyDriveRepository;
            private readonly IUnitOfWork _unitOfWork;

            public DeleteCompanyDriveCommandHandler(ICompanyDriveRepository appFileRepository,
                IUnitOfWork unitOfWork)
            {
                _companyDriveRepository = appFileRepository;
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<bool>> Handle(DeleteCompanyDriveCommand request,
                CancellationToken cancellationToken)
            {
                var drive = await _companyDriveRepository.GetSingleAsync(d => d.Id == request.DriveId);

                if (drive == null)
                    throw new KeyNotFoundException("File or folder that you want to delete does not exists");

                var path = drive.Path;
                await _companyDriveRepository.DeleteAsync(drive);

                await _unitOfWork.CommitAsync();

                return new Response<bool>(true);
            }
        }
    }
}