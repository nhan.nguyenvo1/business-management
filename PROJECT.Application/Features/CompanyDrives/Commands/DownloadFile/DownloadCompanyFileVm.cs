﻿namespace PROJECT.Application.Features.CompanyDrives.Commands.DownloadFile
{
    public class DownloadCompanyFileVm
    {
        public string Path { get; set; }
    }
}