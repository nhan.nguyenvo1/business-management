﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Dtos.Drive;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.CompanyDrives.Commands.DownloadFile
{
    public class DownloadCompanyFileCommand : IRequest<Response<DriveDownloadResponse>>
    {
        public int FileId { get; set; }

        public class
            DownloadCompanyFileCommandHandler : IRequestHandler<DownloadCompanyFileCommand,
                Response<DriveDownloadResponse>>
        {
            private readonly ICompanyDriveRepository _companyDriveRepository;
            private readonly IDriveStorageService _driveStorageService;

            public DownloadCompanyFileCommandHandler(ICompanyDriveRepository appFileRepository,
                IDriveStorageService driveStorageService)
            {
                _companyDriveRepository = appFileRepository;
                _driveStorageService = driveStorageService;
            }

            public async Task<Response<DriveDownloadResponse>> Handle(DownloadCompanyFileCommand request,
                CancellationToken cancellationToken)
            {
                var file = await _companyDriveRepository.GetSingleAsync<DownloadCompanyFileVm>(file =>
                    file.Id == request.FileId && file.FileType != FileType.Folder);

                if (file == null) throw new KeyNotFoundException("File does not exists");

                var response = await _driveStorageService.DownloadAsync(file.Path);

                return new Response<DriveDownloadResponse>(response);
            }
        }
    }
}