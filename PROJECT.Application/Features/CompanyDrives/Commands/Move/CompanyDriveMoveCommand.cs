﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.CompanyDrives.Commands.Move
{
    public class CompanyDriveMoveCommand : IRequest<Response<int>>
    {
        public int DriveId { get; set; }

        public int? DestinationId { get; set; }

        public class CompanyDriveMoveCommandHandler : IRequestHandler<CompanyDriveMoveCommand, Response<int>>
        {
            private readonly ICompanyDriveRepository _companyDriveRepository;
            private readonly ICompanyService _companyService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IUnitOfWork _unitOfWork;

            public CompanyDriveMoveCommandHandler(ICompanyDriveRepository appFileRepository,
                IUnitOfWork unitOfWork,
                IDriveStorageService driveStorageService,
                ICompanyService companyService)
            {
                _companyDriveRepository = appFileRepository;
                _unitOfWork = unitOfWork;
                _driveStorageService = driveStorageService;
                _companyService = companyService;
            }

            public async Task<Response<int>> Handle(CompanyDriveMoveCommand request,
                CancellationToken cancellationToken)
            {
                var src = await _companyDriveRepository.GetSingleAsync(file => file.Id == request.DriveId);

                if (src == null) throw new KeyNotFoundException("File does not exists");

                var desPathBuilder = new StringBuilder();
                int? desPathId = null;
                if (request.DestinationId.HasValue)
                {
                    var des = await _companyDriveRepository.GetSingleAsync<CompanyDriveMoveVm>(file =>
                        file.Id == request.DestinationId &&
                        file.FileType == FileType.Folder);

                    if (des == null) throw new KeyNotFoundException("Destination folder does not exists");

                    desPathBuilder.Append(des.Path);
                    desPathId = des.Id;
                }
                else
                {
                    desPathBuilder.Append($"/{_companyService.CompanyId}/Shared");
                }

                desPathBuilder.Append('/');
                desPathBuilder.Append(src.Name);

                var desPath = desPathBuilder.ToString();

                var result = await _driveStorageService.MoveAsync(src.Path, desPath);

                src.Path = result.Path;
                src.Name = result.Name;
                src.FileId = desPathId;

                var fileResult = await _companyDriveRepository.UpdateAsync(src);

                await _unitOfWork.CommitAsync();

                return new Response<int>(fileResult.Id);
            }
        }
    }
}