﻿using FluentValidation;

namespace PROJECT.Application.Features.CompanyDrives.Commands.Move
{
    public class CompanyDriveMoveCommandValidator : AbstractValidator<CompanyDriveMoveCommand>
    {
        public CompanyDriveMoveCommandValidator()
        {
            RuleFor(p => p.DriveId)
                .NotEmpty();
        }
    }
}