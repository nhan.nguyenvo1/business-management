﻿using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.CompanyDrives.Commands.Move
{
    public class CompanyDriveMoveVm
    {
        public int Id { get; set; }

        public string Path { get; set; }

        public string Name { get; set; }

        public int StorageId { get; set; }

        public FileType FileType { get; set; }
    }
}