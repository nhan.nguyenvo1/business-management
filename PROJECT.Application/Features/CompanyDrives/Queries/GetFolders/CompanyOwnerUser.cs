﻿namespace PROJECT.Application.Features.CompanyDrives.Queries.GetFolders
{
    public class CompanyOwnerUser
    {
        public string Id { get; set; }

        public string AvatarUrl { get; set; }

        public string FullName { get; set; }
    }
}