﻿namespace PROJECT.Application.Features.CompanyDrives.Queries.GetFolders
{
    public class GetCompanyFoldersVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string FileType { get; set; }

        public ulong? FileSize { get; set; }

        public CompanyOwnerUser Created { get; set; }

        public CompanyOwnerUser Updated { get; set; }

        public string CreatedDate { get; set; }

        public string UpdatedDate { get; set; }
    }
}