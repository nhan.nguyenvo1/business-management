﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.CompanyDrives.Queries.GetFolders
{
    public class GetCompanyFoldersQuery : IRequest<Response<IReadOnlyList<GetCompanyFoldersVm>>>
    {
        public int? FolderId { get; set; }

        public class GetCompanyFoldersQueryHandler : IRequestHandler<GetCompanyFoldersQuery,
            Response<IReadOnlyList<GetCompanyFoldersVm>>>
        {
            private readonly ICompanyDriveRepository _companyDriveRepository;

            public GetCompanyFoldersQueryHandler(ICompanyDriveRepository companyDriveRepository)
            {
                _companyDriveRepository = companyDriveRepository;
            }


            public async Task<Response<IReadOnlyList<GetCompanyFoldersVm>>> Handle(GetCompanyFoldersQuery request,
                CancellationToken cancellationToken)
            {
                var companyDrives =
                    await _companyDriveRepository.GetByConditionAsync<GetCompanyFoldersVm>(drive =>
                        request.FolderId == drive.FileId);

                return new Response<IReadOnlyList<GetCompanyFoldersVm>>(companyDrives);
            }
        }
    }
}