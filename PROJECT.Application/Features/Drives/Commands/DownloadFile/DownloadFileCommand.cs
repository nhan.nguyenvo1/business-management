﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Dtos.Drive;
using PROJECT.Application.Features.Drives.Commands.DownloadFile;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Drives.Commands.DownloadFiles
{
    public class DownloadFileCommand : IRequest<Response<DriveDownloadResponse>>
    {
        public int FileId { get; set; }

        public class DownloadFileCommandHandler : IRequestHandler<DownloadFileCommand, Response<DriveDownloadResponse>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IDriveStorageService _driveStorageService;

            public DownloadFileCommandHandler(IAppFileRepository appFileRepository,
                IDriveStorageService driveStorageService)
            {
                _appFileRepository = appFileRepository;
                _driveStorageService = driveStorageService;
            }

            public async Task<Response<DriveDownloadResponse>> Handle(DownloadFileCommand request,
                CancellationToken cancellationToken)
            {
                var file = await _appFileRepository.GetSingleAsync<DownloadFileVm>(file =>
                    file.Id == request.FileId && file.FileType != FileType.Folder);

                if (file == null) throw new KeyNotFoundException("File does not exists");

                var response = await _driveStorageService.DownloadAsync(file.Path);

                return new Response<DriveDownloadResponse>(response);
            }
        }
    }
}