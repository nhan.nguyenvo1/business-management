﻿namespace PROJECT.Application.Features.Drives.Commands.DownloadFile
{
    public class DownloadFileVm
    {
        public string Path { get; set; }
    }
}