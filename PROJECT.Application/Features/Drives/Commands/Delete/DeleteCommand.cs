﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Drives.Commands.Delete
{
    public class DeleteCommand : IRequest<Response<bool>>
    {
        public int DriveId { get; set; }

        public class DeleteCommandHandler : IRequestHandler<DeleteCommand, Response<bool>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IUnitOfWork _unitOfWork;

            public DeleteCommandHandler(IAppFileRepository appFileRepository, IUnitOfWork unitOfWork)
            {
                _appFileRepository = appFileRepository;
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<bool>> Handle(DeleteCommand request, CancellationToken cancellationToken)
            {
                var drive = await _appFileRepository.GetSingleAsync(d => d.Id == request.DriveId);

                if (drive == null)
                    throw new KeyNotFoundException("File or folder that you want to delete does not exists");

                var path = drive.Path;
                await _appFileRepository.DeleteAsync(drive);

                await _unitOfWork.CommitAsync();

                return new Response<bool>(true);
            }
        }
    }
}