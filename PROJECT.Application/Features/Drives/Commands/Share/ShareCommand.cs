﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Drives.Commands.Share
{
    public class ShareCommand : IRequest<Response<bool>>
    {
        public ICollection<int> DriveIds { get; set; }

        public ICollection<SharedUserRequest> SharedUserRequests { get; set; }

        public class ShareCommandHandler : IRequestHandler<ShareCommand, Response<bool>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserRepository _userRepository;

            public ShareCommandHandler(IAppFileRepository appFileRepository,
                IAuthUserService authUserService,
                IUserRepository userRepository,
                IUnitOfWork unitOfWork)
            {
                _appFileRepository = appFileRepository;
                _authUserService = authUserService;
                _userRepository = userRepository;
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<bool>> Handle(ShareCommand request, CancellationToken cancellationToken)
            {
                var errorMessage = new StringBuilder();
                var userId = _authUserService.UserId;

                foreach (var driveId in request.DriveIds)
                {
                    var drive = await _appFileRepository.GetSingleAsync(d => d.Id == driveId && d.CreatedBy == userId);

                    if (drive == null)
                    {
                        errorMessage.AppendLine($"Drive with id = {driveId} does not exists");
                        continue;
                    }

                    foreach (var sharedUserRequest in request.SharedUserRequests)
                    {
                        if (!await _userRepository.AnyAsync(u => u.Id.Equals(sharedUserRequest.UserId)))
                        {
                            errorMessage.AppendLine($"User with id = {sharedUserRequest.UserId} does not exists");
                            continue;
                        }

                        if (!await _appFileRepository.AlreadyShared(driveId, sharedUserRequest.UserId))
                            await _appFileRepository.AddToSharedAsync(drive,
                                sharedUserRequest.UserId,
                                sharedUserRequest.CanAdd,
                                sharedUserRequest.CanRead,
                                sharedUserRequest.CanWrite,
                                true);
                    }
                }

                await _unitOfWork.CommitAsync();

                if (errorMessage.Length > 0) return new Response<bool>(false, errorMessage.ToString());

                return new Response<bool>(true);
            }
        }
    }
}