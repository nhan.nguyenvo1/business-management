﻿namespace PROJECT.Application.Features.Drives.Commands.Share
{
    public class SharedUserRequest
    {
        public string UserId { get; set; }

        public bool CanRead { get; set; }

        public bool CanWrite { get; set; }

        public bool CanAdd { get; set; }
    }
}