﻿using FluentValidation;

namespace PROJECT.Application.Features.Drives.Commands.Share
{
    public class ShareUserValidator : AbstractValidator<SharedUserRequest>
    {
        public ShareUserValidator()
        {
            RuleFor(p => p.UserId)
                .NotEmpty()
                .NotNull();

            RuleFor(p => p.CanAdd)
                .NotNull();

            RuleFor(p => p.CanRead)
                .NotNull();

            RuleFor(p => p.CanWrite)
                .NotNull();
        }
    }
}