﻿using FluentValidation;

namespace PROJECT.Application.Features.Drives.Commands.Share
{
    public class ShareValidator : AbstractValidator<ShareCommand>
    {
        public ShareValidator()
        {
            RuleFor(p => p.DriveIds)
                .NotEmpty()
                .NotNull();

            RuleFor(p => p.SharedUserRequests)
                .NotNull()
                .NotEmpty();

            RuleForEach(p => p.SharedUserRequests)
                .SetValidator(new ShareUserValidator());
        }
    }
}