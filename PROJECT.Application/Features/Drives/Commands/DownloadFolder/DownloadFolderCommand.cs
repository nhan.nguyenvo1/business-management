﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Dtos.Drive;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Drives.Commands.DownloadFolder
{
    public class DownloadFolderCommand : IRequest<Response<DriveDownloadResponse>>
    {
        public int FolderId { get; set; }

        public class
            DownloadFolderCommandHandler : IRequestHandler<DownloadFolderCommand, Response<DriveDownloadResponse>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IDriveStorageService _driveStorageService;

            public DownloadFolderCommandHandler(IAppFileRepository appFileRepository,
                IDriveStorageService driveStorageService)
            {
                _appFileRepository = appFileRepository;
                _driveStorageService = driveStorageService;
            }

            public async Task<Response<DriveDownloadResponse>> Handle(DownloadFolderCommand request,
                CancellationToken cancellationToken)
            {
                var folder = await _appFileRepository.GetSingleAsync<DownloadFolderVm>(file =>
                    file.Id == request.FolderId && file.FileType == FileType.Folder);

                if (folder == null) throw new KeyNotFoundException("File does not exists");

                var response = await _driveStorageService.DownloadZipAsync(folder.Path);

                return new Response<DriveDownloadResponse>(response);
            }
        }
    }
}