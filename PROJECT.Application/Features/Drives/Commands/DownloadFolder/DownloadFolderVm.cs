﻿namespace PROJECT.Application.Features.Drives.Commands.DownloadFolder
{
    public class DownloadFolderVm
    {
        public string Path { get; set; }
    }
}