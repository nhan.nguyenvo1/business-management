﻿using FluentValidation;

namespace PROJECT.Application.Features.Drives.Commands.Move
{
    public class MoveCommandValidator : AbstractValidator<MoveCommand>
    {
        public MoveCommandValidator()
        {
            RuleFor(p => p.DriveId)
                .NotEmpty();
        }
    }
}