using FluentValidation;
using PROJECT.Application.ValidatorExtensions;

namespace PROJECT.Application.Features.Drives.Commands.CreateFiles
{
    public class CreateFilesValidator : AbstractValidator<CreateFilesCommand>
    {
        public CreateFilesValidator()
        {
            RuleFor(p => p.Files)
                .NotEmpty();

            RuleForEach(p => p.Files)
                .SetValidator(new IFormFileValidator());
        }
    }
}