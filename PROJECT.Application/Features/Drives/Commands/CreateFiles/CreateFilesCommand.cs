﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Drives.Commands.CreateFiles
{
    public class CreateFilesCommand : IRequest<Response<bool>>
    {
        public int? FolderId { get; set; }

        public ICollection<IFormFile> Files { get; set; }


        public class CreateFilesCommandHandler : IRequestHandler<CreateFilesCommand, Response<bool>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IStorageRepository _storageRepository;
            private readonly IUnitOfWork _unitOfWork;

            public CreateFilesCommandHandler(IUnitOfWork unitOfWork, IAppFileRepository appFileRepository,
                IDriveStorageService driveStorageService, IStorageRepository storageRepository,
                IAuthUserService authUserService)
            {
                _unitOfWork = unitOfWork;
                _appFileRepository = appFileRepository;
                _driveStorageService = driveStorageService;
                _storageRepository = storageRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<bool>> Handle(CreateFilesCommand request, CancellationToken cancellationToken)
            {
                var storage = await _storageRepository.GetSingleAsync(p => p.UserId == _authUserService.UserId);
                var folder =
                    await _appFileRepository.GetByIdAsync(request.FolderId.HasValue ? request.FolderId.Value : 0);

                if (folder != null)
                    foreach (var file in request.Files)
                    {
                        var path = $"{folder.Path}/{file.FileName}";
                        var stream = file.OpenReadStream();
                        var result = await _driveStorageService.UploadFileAsync(path, stream);
                        await _appFileRepository.AddAsync(new AppFile
                        {
                            Path = result.Path,
                            Name = result.FileName,
                            FileType = FileType.Other,
                            FileSize = result.FileSize,
                            StorageId = storage.Id,
                            FileId = folder.Id
                        });
                    }
                else
                    foreach (var file in request.Files)
                    {
                        var path = $"{storage.Path}/{file.FileName}";
                        var stream = file.OpenReadStream();
                        var result = await _driveStorageService.UploadFileAsync(path, stream);
                        await _appFileRepository.AddAsync(new AppFile
                        {
                            Path = result.Path,
                            Name = result.FileName,
                            FileType = FileType.Other,
                            FileSize = result.FileSize,
                            StorageId = storage.Id
                        }).ConfigureAwait(false);
                    }

                await _unitOfWork.CommitAsync();

                return new Response<bool>(true);
            }
        }
    }
}