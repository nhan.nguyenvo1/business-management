﻿using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Drives.Commands.Copy
{
    public class CopyCommand : IRequest<Response<int>>
    {
        public int DriveId { get; set; }

        public int? DestinationId { get; set; }

        public class CopyCommandHandler : IRequestHandler<CopyCommand, Response<int>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly ICompanyService _companyService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IUnitOfWork _unitOfWork;

            public CopyCommandHandler(IAppFileRepository appFileRepository,
                IUnitOfWork unitOfWork,
                IDriveStorageService driveStorageService,
                ICompanyService companyService,
                IAuthUserService authUserService)
            {
                _appFileRepository = appFileRepository;
                _unitOfWork = unitOfWork;
                _driveStorageService = driveStorageService;
                _companyService = companyService;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(CopyCommand request, CancellationToken cancellationToken)
            {
                var src = await _appFileRepository.GetSingleAsync(file => file.Id == request.DriveId);

                if (src == null) throw new KeyNotFoundException("File does not exists");

                var desPathBuilder = new StringBuilder();
                int? desPathId = null;
                if (request.DestinationId.HasValue)
                {
                    var des = await _appFileRepository.GetSingleAsync<CopyVm>(file =>
                        file.Id == request.DestinationId && file.FileType == FileType.Folder);

                    if (des == null) throw new KeyNotFoundException("Destination folder does not exists");

                    desPathBuilder.Append(des.Path);
                    desPathId = des.Id;
                }
                else
                {
                    desPathBuilder.Append($"/{_companyService.CompanyId}/{_authUserService.UserId}");
                }

                desPathBuilder.Append('/');
                desPathBuilder.Append(src.Name);

                var desPath = desPathBuilder.ToString();

                var result = await _driveStorageService.CopyAsync(src.Path, desPath);

                src.Id = default;
                src.Path = result.Path;
                src.Name = result.Name;
                src.FileId = desPathId;

                var fileResult = await _appFileRepository.AddAsync(src);

                await _unitOfWork.CommitAsync();

                if (src.FileType == FileType.Folder)
                    await _appFileRepository.AddChildrenAsync(fileResult.Id, request.DriveId);

                await _unitOfWork.CommitAsync();

                return new Response<int>(fileResult.Id);
            }
        }
    }
}