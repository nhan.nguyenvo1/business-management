﻿using FluentValidation;

namespace PROJECT.Application.Features.Drives.Commands.Copy
{
    public class CopyCommandValidator : AbstractValidator<CopyCommand>
    {
        public CopyCommandValidator()
        {
            RuleFor(p => p.DriveId)
                .NotEmpty();
        }
    }
}