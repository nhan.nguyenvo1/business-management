﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.Drives.Commands.CreateFolder
{
    public class CreateFolderCommand : IRequest<Response<int>>
    {
        public string FolderName { get; set; }

        public int? ParentId { get; set; }

        public class CreateFolderCommandHandler : IRequestHandler<CreateFolderCommand, Response<int>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IDriveStorageService _driveStorageService;
            private readonly IStorageRepository _storageRepository;
            private readonly IUnitOfWork _unitOfWork;

            public CreateFolderCommandHandler(IUnitOfWork unitOfWork,
                IAppFileRepository appFileRepository,
                IDriveStorageService driveStorageService,
                IStorageRepository storageRepository,
                IAuthUserService authUserService)
            {
                _unitOfWork = unitOfWork;
                _appFileRepository = appFileRepository;
                _driveStorageService = driveStorageService;
                _storageRepository = storageRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(CreateFolderCommand request, CancellationToken cancellationToken)
            {
                // find folder
                var parent = await _appFileRepository.GetByIdAsync(request.ParentId ?? 0);
                var storage =
                    await _storageRepository.GetSingleAsync(storage => storage.UserId == _authUserService.UserId);

                var appFile = new AppFile {StorageId = storage.Id};
                if (parent != null)
                {
                    // create folder
                    var result = await _driveStorageService.CreateFolderAsync($"{parent.Path}/{request.FolderName}");
                    appFile.Path = result.Path;
                    appFile.Name = result.Name;
                    appFile.FileId = parent.Id;
                }
                else
                {
                    var result = await _driveStorageService.CreateFolderAsync($"{storage.Path}/{request.FolderName}");
                    appFile.Path = result.Path;
                    appFile.Name = result.Name;
                }

                appFile = await _appFileRepository.AddAsync(appFile);
                try
                {
                    await _unitOfWork.CommitAsync();
                }
                catch (Exception)
                {
                    throw new ApiException($"Dupplicate folder name '{request.FolderName}'");
                }

                return new Response<int>(appFile.Id);
            }
        }
    }
}