﻿using FluentValidation;

namespace PROJECT.Application.Features.Drives.Commands.CreateFolder
{
    public class CreateFolderValidator : AbstractValidator<CreateFolderCommand>
    {
        public CreateFolderValidator()
        {
            RuleFor(p => p.FolderName)
                .NotEmpty()
                .NotNull();
        }
    }
}