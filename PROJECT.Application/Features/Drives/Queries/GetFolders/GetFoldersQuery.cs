﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Drives.Queries.GetFolders
{
    public class GetFoldersQuery : IRequest<Response<IReadOnlyList<GetFoldersVm>>>
    {
        public int? FolderId { get; set; } = null;

        public class GetFoldersQueryHandler : IRequestHandler<GetFoldersQuery, Response<IReadOnlyList<GetFoldersVm>>>
        {
            private readonly IAppFileRepository _appFileRepository;
            private readonly IAuthUserService _authUserService;

            public GetFoldersQueryHandler(IAppFileRepository appFileRepository, IAuthUserService authUserService)
            {
                _appFileRepository = appFileRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<IReadOnlyList<GetFoldersVm>>> Handle(GetFoldersQuery request,
                CancellationToken cancellationToken)
            {
                var folders = await _appFileRepository.GetByConditionAsync<GetFoldersVm>(file =>
                    file.FileId == request.FolderId
                    && _authUserService.UserId == file.CreatedBy);

                return new Response<IReadOnlyList<GetFoldersVm>>(folders);
            }
        }
    }
}