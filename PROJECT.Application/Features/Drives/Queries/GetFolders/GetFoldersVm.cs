﻿namespace PROJECT.Application.Features.Drives.Queries.GetFolders
{
    public class GetFoldersVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string FileType { get; set; }

        public ulong? FileSize { get; set; }

        public OwnerUser Created { get; set; }

        public OwnerUser Updated { get; set; }

        public string CreatedDate { get; set; }

        public string UpdatedDate { get; set; }
    }
}