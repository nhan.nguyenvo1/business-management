﻿namespace PROJECT.Application.Features.Drives.Queries.GetFolders
{
    public class OwnerUser
    {
        public string Id { get; set; }

        public string AvatarUrl { get; set; }

        public string FullName { get; set; }
    }
}