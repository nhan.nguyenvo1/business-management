﻿namespace PROJECT.Application.Features.Companies.Queries.GetCompanies
{
    public class GetCompaniesVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}