﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Parameters;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Companies.Queries.GetCompanies
{
    public class GetCompaniesQuery : RequestParameter, IRequest<PagedResponse<GetCompaniesVm>>
    {
        public string Search { get; set; }

        public class GetCompaniesQueryHandler : IRequestHandler<GetCompaniesQuery, PagedResponse<GetCompaniesVm>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly ICompanyRepository _companyRepository;

            public GetCompaniesQueryHandler(IAuthUserService authUserService, ICompanyRepository companyRepository)
            {
                _authUserService = authUserService;
                _companyRepository = companyRepository;
            }

            public async Task<PagedResponse<GetCompaniesVm>> Handle(GetCompaniesQuery request,
                CancellationToken cancellationToken)
            {
                var companies =
                    await _companyRepository.GetPagedResponseAsync<GetCompaniesVm>(request.Page, request.Limit,
                        request.Search);

                var total = await _companyRepository.CountAsync(company =>
                    string.IsNullOrEmpty(request.Search) || company.Name.Contains(request.Search));

                return new PagedResponse<GetCompaniesVm>(companies, request.Page, request.Limit, total);
            }
        }
    }
}