﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Identity;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Helper;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Constants;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Companies.Commands.CreateCompany
{
    public class CreateCompanyCommand : IRequest<Response<int>>
    {
        public string Name { get; set; }

        public class CreateCompanyCommandHandler : IRequestHandler<CreateCompanyCommand, Response<int>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly ICompanyRepository _companyRepository;
            private readonly IDepartmentRepository _departmentRepository;
            private readonly RoleManager<AppRole> _roleManager;
            private readonly IStorageRepository _storageRepository;
            private readonly IUnitOfWork _unitOfWork;
            private readonly UserManager<AppUser> _userManager;
            private readonly IUserRepository _userRepository;

            public CreateCompanyCommandHandler(IUnitOfWork unitOfWork,
                ICompanyRepository companyRepository,
                IAuthUserService authUserService,
                IUserRepository userRepository,
                IDepartmentRepository departmentRepository,
                IStorageRepository storageRepository,
                RoleManager<AppRole> roleManager,
                UserManager<AppUser> userManager)
            {
                _unitOfWork = unitOfWork;
                _companyRepository = companyRepository;
                _authUserService = authUserService;
                _userRepository = userRepository;
                _departmentRepository = departmentRepository;
                _storageRepository = storageRepository;
                _roleManager = roleManager;
                _userManager = userManager;
            }

            public async Task<Response<int>> Handle(CreateCompanyCommand request, CancellationToken cancellationToken)
            {
                var trans = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    var userId = _authUserService.UserId;

                    var company = new Company
                    {
                        Name = request.Name
                    };

                    var user = await _userRepository.GetByIdAsync(userId);

                    // Create company
                    company.Users.Add(user);
                    company = await _companyRepository.AddAsync(company);
                    await _unitOfWork.CommitAsync();

                    // Create Department
                    var department = new Department
                    {
                        Name = SystemConstant.DefaultDepartment,
                        Url = $"/{SystemConstant.DefaultDepartment.GenerateSlug()}",
                        SupervisorId = userId,
                        CompanyId = company.Id
                    };
                    department.Users.Add(user);
                    department = await _departmentRepository.AddAsync(department);
                    await _unitOfWork.CommitAsync();

                    var storages = new List<AppStorage>
                    {
                        new()
                        {
                            UserId = user.Id,
                            Path = $"/{company.Id}/{user.Id}",
                            CompanyId = company.Id
                        },

                        new()
                        {
                            StorageType = StorageType.CompanyStorage,
                            Limit = 2000,
                            Path = $"/{company.Id}/Shared",
                            CompanyId = company.Id
                        }
                    };

                    // Create Storage
                    await _storageRepository.AddRangeAsync(storages);

                    // Create Role
                    var roleName = $"{company.Id}-SupperAdmin";
                    var role = new AppRole(roleName)
                    {
                        CompanyId = company.Id,
                        NormalizedName = _roleManager.NormalizeKey(roleName)
                    };

                    var result = await _roleManager.CreateAsync(role);
                    if (!result.Succeeded)
                    {
                        await trans.RollbackAsync(cancellationToken);
                        throw new ApiException($"Error: {result.Errors.First().Description}");
                    }

                    result = await _userManager.AddToRoleAsync(user, roleName);
                    if (!result.Succeeded)
                    {
                        await trans.RollbackAsync(cancellationToken);
                        throw new ApiException($"Error: {result.Errors.First().Description}");
                    }

                    await _unitOfWork.CommitAsync();
                    await trans.CommitAsync(cancellationToken);
                    return new Response<int>(company.Id);
                }
                catch (Exception)
                {
                    await trans.RollbackAsync(cancellationToken);
                    throw;
                }
            }
        }
    }
}