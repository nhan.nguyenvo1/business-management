﻿using FluentValidation;

namespace PROJECT.Application.Features.Companies.Commands.AcceptInvitation
{
    public class AcceptInvitationValidator : AbstractValidator<AcceptInvitationCommand>
    {
        public AcceptInvitationValidator()
        {
            RuleFor(p => p.Token)
                .NotNull()
                .NotEmpty();
        }
    }
}