﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Dtos.Company;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Features.Employees.Commands.CreateEmployee;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Application.Features.Companies.Commands.AcceptInvitation
{
    public class AcceptInvitationCommand : IRequest<Response<CreateEmployeeCommand>>
    {
        public string Token { get; set; }

        public class
            AcceptInvitationCommandHandler : IRequestHandler<AcceptInvitationCommand, Response<CreateEmployeeCommand>>
        {
            private readonly IAuthUserService _authUserService;
            private readonly ICompanyRepository _companyRepository;
            private readonly ICryptographyService _cryptographyService;
            private readonly IDateTimeService _dateTimeService;
            private readonly IStorageRepository _storageRepository;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserRepository _userRepository;

            public AcceptInvitationCommandHandler(ICompanyRepository companyRepository,
                ICryptographyService cryptographyService,
                IAuthUserService authUserService,
                IUserRepository userRepository,
                IDateTimeService dateTimeService,
                IUnitOfWork unitOfWork,
                IStorageRepository storageRepository)
            {
                _companyRepository = companyRepository;
                _cryptographyService = cryptographyService;
                _authUserService = authUserService;
                _userRepository = userRepository;
                _dateTimeService = dateTimeService;
                _unitOfWork = unitOfWork;
                _storageRepository = storageRepository;
            }

            public async Task<Response<CreateEmployeeCommand>> Handle(AcceptInvitationCommand request,
                CancellationToken cancellationToken)
            {
                var invitationVm = await _cryptographyService.Decrypt<CompanyInvitationVm>(request.Token);

                if (!await _userRepository.AnyAsync(u => u.Email == invitationVm.Email, true))
                    return new Response<CreateEmployeeCommand>(invitationVm.More);

                if (string.IsNullOrEmpty(_authUserService.UserId)) throw new KeyNotFoundException();

                var user = await _userRepository.GetByIdAsync(_authUserService.UserId);

                if (invitationVm.Expired.HasValue && invitationVm.Expired.Value < _dateTimeService.UtcNow)
                    throw new ApiException("Your invitation has expired");

                if (user.Email != invitationVm.Email) throw new ApiException("Invalid invitation token");

                await _companyRepository.AddUserAsync(invitationVm.CompanyId, invitationVm.More.DepartmentId, user.Id);
                // Create User Storage
                var path = $"/{invitationVm.CompanyId}/{user.Id}";

                await _storageRepository.AddAsync(new AppStorage
                {
                    StorageType = StorageType.PersonalStorage,
                    Path = path,
                    UserId = user.Id,
                    Limit = 500,
                    CompanyId = invitationVm.CompanyId
                });
                await _unitOfWork.CommitAsync();

                return new Response<CreateEmployeeCommand>("Accept invitation successfully");
            }
        }
    }
}