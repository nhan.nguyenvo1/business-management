﻿using System.Collections.Generic;
using MediatR;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Departments.Queries.GetAllDepartments
{
    public class GetAllDepartmentsQuery : IRequest<Response<ICollection<DepartmentVm>>>
    {
    }
}