﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Departments.Queries.GetAllDepartments
{
    public class GetAllDepartmentsQueryHandler :
        IRequestHandler<GetAllDepartmentsQuery, Response<ICollection<DepartmentVm>>>
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IMapper _mapper;

        public GetAllDepartmentsQueryHandler(IDepartmentRepository departmentRepository, IMapper mapper)
        {
            _mapper = mapper;
            _departmentRepository = departmentRepository;
        }

        public async Task<Response<ICollection<DepartmentVm>>> Handle(GetAllDepartmentsQuery request,
            CancellationToken cancellationToken)
        {
            var departments = await _departmentRepository.GetAsync();
            return new Response<ICollection<DepartmentVm>>(_mapper.Map<ICollection<DepartmentVm>>(departments));
        }
    }
}