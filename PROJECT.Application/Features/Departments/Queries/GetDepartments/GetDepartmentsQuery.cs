﻿using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Wrappers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.Departments.Queries.GetDepartments
{
    public class GetDepartmentsQuery : IRequest<Response<IReadOnlyCollection<GetDepartmentsVm>>>
    {
        public class GetDepartmentsQueryHandler : IRequestHandler<GetDepartmentsQuery, Response<IReadOnlyCollection<GetDepartmentsVm>>>
        {
            private readonly IQueryExecutorAsync queryExecutorAsync;
            private readonly ICompanyService companyService;

            public GetDepartmentsQueryHandler(ICompanyService companyService, IQueryExecutorAsync queryExecutorAsync)
            {
                this.companyService = companyService;
                this.queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetDepartmentsVm>>> Handle(GetDepartmentsQuery request, CancellationToken cancellationToken)
            {
                const string query = "SELECT Id, Name FROM Department WHERE CompanyId = @CompanyId";
                var response = await queryExecutorAsync.Fetch<GetDepartmentsVm>(query, new { companyService.CompanyId });

                return new Response<IReadOnlyCollection<GetDepartmentsVm>>(response);
            }
        }
    }
}
