﻿namespace PROJECT.Application.Features.Departments.Queries.GetDepartments
{
    public class GetDepartmentsVm
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
