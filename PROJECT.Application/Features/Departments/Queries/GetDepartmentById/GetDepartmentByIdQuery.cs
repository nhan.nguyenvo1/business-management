﻿using MediatR;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Departments.Queries.GetDepartmentById
{
    public class GetDepartmentByIdQuery : IRequest<Response<DepartmentVm>>
    {
        public GetDepartmentByIdQuery(int id)
        {
            Id = id;
        }

        public int Id { get; set; }
    }
}