﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Features.Departments.Queries.GetDepartmentById
{
    public class GetDepartmentByIdQueryHandler : IRequestHandler<GetDepartmentByIdQuery, Response<DepartmentVm>>
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IMapper _mapper;

        public GetDepartmentByIdQueryHandler(IMapper mapper, IDepartmentRepository departmentRepository)
        {
            _mapper = mapper;
            _departmentRepository = departmentRepository;
        }


        public async Task<Response<DepartmentVm>> Handle(GetDepartmentByIdQuery request,
            CancellationToken cancellationToken)
        {
            var department = await _departmentRepository.GetByIdAsync(request.Id);
            if (department == null)
                throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Department", "id", request.Id));

            return new Response<DepartmentVm>(_mapper.Map<DepartmentVm>(department));
        }
    }
}