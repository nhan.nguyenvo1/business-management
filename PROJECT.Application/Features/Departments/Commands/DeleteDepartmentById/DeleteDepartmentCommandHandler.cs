﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Departments.Commands.DeleteDepartmentById
{
    public class DeleteDepartmentCommandHandler : IRequestHandler<DeleteDepartmentCommand, Response<int>>
    {
        private readonly IDepartmentRepository _departmentRepository;

        public DeleteDepartmentCommandHandler(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        public async Task<Response<int>> Handle(DeleteDepartmentCommand request, CancellationToken cancellationToken)
        {
            if (!await _departmentRepository.ExistAsync(request.Id))
                throw new ApiException(string.Format(ErrorMessageConstant.NotFound, "Department", "Id", request.Id));

            await _departmentRepository.DeleteAsync(request.Id);
            return new Response<int>(request.Id);
        }
    }
}