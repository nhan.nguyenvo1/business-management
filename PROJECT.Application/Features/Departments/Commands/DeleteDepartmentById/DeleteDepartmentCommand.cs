﻿using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Departments.Commands.DeleteDepartmentById
{
    public class DeleteDepartmentCommand : IRequest<Response<int>>
    {
        public DeleteDepartmentCommand(int id)
        {
            Id = id;
        }

        public int Id { get; }
    }
}