﻿using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Departments.Commands.UpdateDepartment
{
    public class UpdateDepartmentCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}