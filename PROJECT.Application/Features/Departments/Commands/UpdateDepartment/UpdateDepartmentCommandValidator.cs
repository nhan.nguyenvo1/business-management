﻿using FluentValidation;
using PROJECT.Application.Constants;

namespace PROJECT.Application.Features.Departments.Commands.UpdateDepartment
{
    public class UpdateDepartmentCommandValidator : AbstractValidator<UpdateDepartmentCommand>
    {
        public UpdateDepartmentCommandValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Department name"))
                .MaximumLength(100)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "Department name", 100));

            RuleFor(p => p.Id)
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Department id"));
        }
    }
}