﻿using FluentValidation;
using PROJECT.Application.Constants;
using PROJECT.Application.Interfaces.Repositories;

namespace PROJECT.Application.Features.Departments.Commands.CreateDepartment
{
    public class CreateDepartmentCommandValidator : AbstractValidator<CreateDepartmentCommand>
    {
        public CreateDepartmentCommandValidator(IDepartmentRepository departmentRepository,
            IEmployeeRepository employeeRepository)
        {
            RuleFor(p => p.Name)
                .NotNull().NotEmpty().WithMessage(string.Format(ErrorMessageConstant.Required, "Department name"))
                .MaximumLength(100).WithMessage(string.Format(ErrorMessageConstant.MaxLength, "Department name", 100));

            RuleFor(p => p.DepartmentId)
                .MustAsync(async (id, cancellation) => id != null && await departmentRepository.ExistAsync(id.Value))
                .When(p => p.DepartmentId != null)
                .WithMessage((command, id) => string.Format(ErrorMessageConstant.NotFound, "Department", "Id", id));

            RuleFor(p => p.SupervisorId)
                .MustAsync(async (id, cancellation) => id != null && await employeeRepository.GetByIdAsync(id) != null)
                .When(p => p.SupervisorId != null)
                .WithMessage((command, id) => string.Format(ErrorMessageConstant.NotFound, "Supervisor", "Id", id));
        }
    }
}