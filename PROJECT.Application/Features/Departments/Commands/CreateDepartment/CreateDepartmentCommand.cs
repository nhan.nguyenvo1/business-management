﻿using MediatR;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Departments.Commands.CreateDepartment
{
    public class CreateDepartmentCommand : IRequest<Response<int>>
    {
        public string Name { get; set; }

        public int? DepartmentId { get; set; }

        public string SupervisorId { get; set; }
    }
}