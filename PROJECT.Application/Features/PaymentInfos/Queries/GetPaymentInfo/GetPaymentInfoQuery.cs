﻿using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.PaymentInfos.Queries.GetPaymentInfo
{
    public class GetPaymentInfoQuery : IRequest<Response<PaymentInfo>>
    {
        public class GetPaymentInfoQueryHandler : IRequestHandler<GetPaymentInfoQuery, Response<PaymentInfo>>
        {
            private readonly IPaymentInfoRepository _paymentInfoRepository;

            public GetPaymentInfoQueryHandler(IPaymentInfoRepository contactRepository)
            {
                _paymentInfoRepository = contactRepository;
            }

            public async Task<Response<PaymentInfo>> Handle(GetPaymentInfoQuery request, CancellationToken cancellationToken)
            {
                return new Response<PaymentInfo>(await _paymentInfoRepository.GetSingleAsync(c => c.IsDefault));
            }
        }
    }
}
