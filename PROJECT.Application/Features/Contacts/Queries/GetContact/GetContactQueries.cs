﻿using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.Contacts.Queries.GetContact
{
    public class GetContactQueries : IRequest<Response<Contact>>
    {
        public class GetContactQueriesHandler : IRequestHandler<GetContactQueries, Response<Contact>>
        {
            private readonly IContactRepository _contactRepository;

            public GetContactQueriesHandler(IContactRepository contactRepository)
            {
                _contactRepository = contactRepository;
            }

            public async Task<Response<Contact>> Handle(GetContactQueries request, CancellationToken cancellationToken)
            {
                return new Response<Contact>(await _contactRepository.GetSingleAsync(c => c.IsDefault));
            }
        }
    }
}
