﻿using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.Tasks.Commands.DeleteTask
{
    public class DeleteTaskByIdCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public class DeleteTaskByIdCommandHandler : IRequestHandler<DeleteTaskByIdCommand, Response<int>>
        {
            private readonly IAppTaskRepository _appTaskRepository;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IAuthUserService _authUserService;

            public DeleteTaskByIdCommandHandler(IAppTaskRepository appTaskRepository,
                                                IUnitOfWork unitOfWork,
                                                IAuthUserService authUserService)
            {
                _appTaskRepository = appTaskRepository;
                _unitOfWork = unitOfWork;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(DeleteTaskByIdCommand request, CancellationToken cancellationToken)
            {
                var task = await _appTaskRepository.GetSingleAsync(e => e.Id == request.Id && e.CreatedBy == _authUserService.UserId);

                if (task == null)
                {
                    throw new KeyNotFoundException("Task that you need to delete does exists");
                }

                await _appTaskRepository.DeleteAsync(task);
                await _unitOfWork.CommitAsync();

                return new Response<int>(request.Id);
            }
        }
    }
}
