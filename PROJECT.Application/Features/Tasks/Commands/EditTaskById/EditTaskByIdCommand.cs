﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PROJECT.Application.Constants;
using PROJECT.Application.Features.Tasks.Commands.EditTaskById.Requests;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.Tasks.Commands.EditTaskById
{
    public class EditTaskByIdCommand : EditTaskByIdRequest, IRequest<Response<int>>
    {
        public EditTaskByIdCommand()
        {
        }

        public EditTaskByIdCommand(int id, EditTaskByIdRequest request) : base(request.Title, request.Description,
            request.DueDate, request.ResponsiblePersons, request.CheckLists, request.Attachments, request.Files)
        {
            Id = id;
        }

        public int Id { get; set; }

        public class EditTaskByIdCommandHandler : IRequestHandler<EditTaskByIdCommand, Response<int>>
        {
            private readonly IAppTaskRepository _appTaskRepository;
            private readonly ICloudinaryService _cloudinaryService;
            private readonly IMapper _mapper;
            private readonly ITaskAttachmentRepository _taskAttachmentRepository;
            private readonly ITaskCheckListRepository _taskCheckListRepository;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserRepository _userRepository;
            private readonly IUserTaskRepository _userTaskRepository;
            private readonly ILogger<EditTaskByIdCommandHandler> _logger;
            public EditTaskByIdCommandHandler(IAppTaskRepository appTaskRepository,
                                              ITaskCheckListRepository taskCheckListRepository,
                                              ITaskAttachmentRepository taskAttachmentRepository,
                                              IUnitOfWork unitOfWork,
                                              ICloudinaryService cloudinaryService,
                                              IMapper mapper,
                                              IUserRepository userRepository,
                                              ILogger<EditTaskByIdCommandHandler> logger, 
                                              IUserTaskRepository userTaskRepository)
            {
                _appTaskRepository = appTaskRepository;
                _taskCheckListRepository = taskCheckListRepository;
                _taskAttachmentRepository = taskAttachmentRepository;
                _unitOfWork = unitOfWork;
                _cloudinaryService = cloudinaryService;
                _mapper = mapper;
                _userRepository = userRepository;
                _logger = logger;
                _userTaskRepository = userTaskRepository;
            }

            public async Task<Response<int>> Handle(EditTaskByIdCommand request, CancellationToken cancellationToken)
            {
                var trans = await _unitOfWork.BeginTransactionAsync();
                // Get Task
                var appTask = await _appTaskRepository.GetByIdAsync(request.Id);
                if (appTask == null)
                    throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "Task", "id",
                        request.Id));

                // Change Title and Description
                appTask = _mapper.Map(request, appTask);
                try
                {
                    // Handle Attachment Files
                    if (request.Attachments != null)
                        await HandleAttachmentFilesAsync(appTask, request.Attachments, request.Files);

                    // Handle Responsible Persons
                    if (request.ResponsiblePersons != null)
                        foreach (var responsiblePerson in request.ResponsiblePersons)
                            // Handle delete
                            if (responsiblePerson.DeletedFlag)
                            {
                                await _userTaskRepository.DeleteByConditionAsync(e => e.UserId == responsiblePerson.UserId && e.TaskId == appTask.Id);
                            }
                            // Handle add
                            else
                            {
                                await _userTaskRepository.AddAsync(new UserTask
                                {
                                    UserId = responsiblePerson.UserId,
                                    TaskId = appTask.Id
                                });
                            }

                    // Handle Check Lists
                    if (request.CheckLists != null) await HandleCheckListsAsync(appTask, request.CheckLists);

                    appTask = await _appTaskRepository.UpdateAsync(appTask);
                    // Save
                    await _unitOfWork.CommitAsync();
                    await trans.CommitAsync(cancellationToken);
                }
                catch (Exception)
                {
                    await trans.RollbackAsync(cancellationToken);
                    throw;
                }

                return new Response<int>(request.Id);
            }

            private async Task HandleCheckListsAsync(AppTask appTask, ICollection<CheckListRequest> checkLists)
            {
                foreach (var checkList in checkLists)
                    // Handle delete
                    if (checkList.DeletedFlag && checkList.Id.HasValue)
                    {
                        await _taskCheckListRepository.DeleteAsync(checkList.Id.Value);
                    }
                    // Handle edit
                    else if (checkList.Id.HasValue)
                    {
                        // Get check list by id
                        var taskCheckList = await _taskCheckListRepository.GetByIdAsync(checkList.Id.Value);

                        if (taskCheckList == null)
                            throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "Check list",
                                "id", checkList.Id.Value));

                        taskCheckList.Title = checkList.Title;
                        await _taskCheckListRepository.UpdateAsync(taskCheckList);
                    }
                    // Handle add
                    else
                    {
                        await _taskCheckListRepository.AddAsync(new TaskCheckList
                        {
                            TaskId = appTask.Id,
                            Title = checkList.Title
                        });
                    }
            }

            private async Task HandleAttachmentFilesAsync(AppTask appTask, ICollection<AttachmentRequest> attachments,
                ICollection<IFormFile> files)
            {
                foreach (var attachment in attachments)
                    // Handle delete
                    if (attachment.Id.HasValue)
                    {
                        // Get attachment
                        var taskAttachment = await _taskAttachmentRepository.GetByIdAsync(attachment.Id.Value);
                        if (taskAttachment == null)
                            throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound,
                                "Task attachment", "id", attachment.Id));
                        // Remove database
                        await _taskAttachmentRepository.DeleteAsync(taskAttachment);
                        // Remove from cloudinary
                        try
                        {
                            await _cloudinaryService.RemoveAsync(taskAttachment.PublicId);
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                    // Handle Add
                    else
                    {
                        var fileAttachment = files.First(file => file.FileName.Equals(attachment.FileName));
                        // Add to cloudinary
                        var cloudinaryResponse = await _cloudinaryService.UploadAsync(fileAttachment.FileName,
                            fileAttachment.OpenReadStream(),
                            true, $"Tasks/{appTask.Id}");
                        // Add to database
                        await _taskAttachmentRepository.AddAsync(new TaskAttachment
                        {
                            FileName = fileAttachment.FileName,
                            PublicId = cloudinaryResponse.PublicId,
                            Url = cloudinaryResponse.Url,
                            TaskId = appTask.Id
                        });

                        files.Remove(fileAttachment);
                    }
            }
        }
    }
}