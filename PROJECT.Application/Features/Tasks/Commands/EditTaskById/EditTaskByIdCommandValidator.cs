﻿using System;
using FluentValidation;
using PROJECT.Application.Constants;
using PROJECT.Application.Features.Tasks.Commands.EditTaskById.Requests;

namespace PROJECT.Application.Features.Tasks.Commands.EditTaskById
{
    public class EditTaskByIdCommandValidator : AbstractValidator<EditTaskByIdRequest>
    {
        public EditTaskByIdCommandValidator()
        {
            RuleFor(p => p.Title)
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Title"))
                .NotEmpty()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Title"))
                .MaximumLength(250)
                .WithMessage(string.Format(ErrorMessageConstant.MaxLength, "Title", 250));

            RuleFor(p => p.DueDate)
                .Must(dueDate => dueDate > DateTime.Now)
                .When(command => command.DueDate.HasValue)
                .WithMessage(string.Format(ErrorMessageConstant.OverDate, "Due date"));

            RuleForEach(p => p.Attachments)
                .Must(p => p.Id.HasValue || !string.IsNullOrEmpty(p.FileName))
                .When(command => command.Attachments != null)
                .WithMessage(string.Format(ErrorMessageConstant.OrRequired, "Id", "attachment"));

            RuleForEach(p => p.ResponsiblePersons)
                .Must(p => !string.IsNullOrEmpty(p.UserId))
                .When(command => command.ResponsiblePersons != null)
                .WithMessage(ErrorMessageConstant.Ambiguous);

            RuleForEach(p => p.CheckLists)
                .Must(checkList => checkList.DeletedFlag && checkList.Id.HasValue || !checkList.DeletedFlag &&
                    (checkList.Id.HasValue || !string.IsNullOrEmpty(checkList.Title)))
                .When((command, context) => command.CheckLists != null)
                .WithMessage(ErrorMessageConstant.Ambiguous);
        }
    }
}