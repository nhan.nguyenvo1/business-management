﻿namespace PROJECT.Application.Features.Tasks.Commands.EditTaskById.Requests
{
    public class CheckListRequest
    {
        public int? Id { get; set; }

        public string Title { get; set; }

        public bool DeletedFlag { get; set; }
    }
}