﻿namespace PROJECT.Application.Features.Tasks.Commands.EditTaskById.Requests
{
    public class ResponsiblePersonRequest
    {
        public string UserId { get; set; }

        public bool DeletedFlag { get; set; }
    }
}