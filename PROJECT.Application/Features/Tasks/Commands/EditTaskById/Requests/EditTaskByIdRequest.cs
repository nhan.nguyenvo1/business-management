﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace PROJECT.Application.Features.Tasks.Commands.EditTaskById.Requests
{
    public class EditTaskByIdRequest
    {
        public EditTaskByIdRequest()
        {
        }

        public EditTaskByIdRequest(string title, string description, DateTime? dueDate,
            ICollection<ResponsiblePersonRequest> responsiblePersons, ICollection<CheckListRequest> checkLists,
            ICollection<AttachmentRequest> attachments, ICollection<IFormFile> files)
        {
            Title = title;
            Description = description;
            DueDate = dueDate;
            ResponsiblePersons = responsiblePersons;
            CheckLists = checkLists;
            Attachments = attachments;
            Files = files;
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime? DueDate { get; set; }

        public ICollection<ResponsiblePersonRequest> ResponsiblePersons { get; set; }

        public ICollection<CheckListRequest> CheckLists { get; set; }

        public ICollection<AttachmentRequest> Attachments { get; set; }

        public ICollection<IFormFile> Files { get; set; }
    }
}