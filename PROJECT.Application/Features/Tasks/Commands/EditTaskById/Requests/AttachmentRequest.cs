﻿namespace PROJECT.Application.Features.Tasks.Commands.EditTaskById.Requests
{
    public class AttachmentRequest
    {
        public int? Id { get; set; }

        public string FileName { get; set; }
    }
}