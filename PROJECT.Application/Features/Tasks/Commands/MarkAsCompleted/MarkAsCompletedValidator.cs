﻿using FluentValidation;

namespace PROJECT.Application.Features.Tasks.Commands.MarkAsCompleted
{
    public class MarkAsCompletedValidator : AbstractValidator<MarkAsCompletedCommand>
    {
        public MarkAsCompletedValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty();
        }
    }
}
