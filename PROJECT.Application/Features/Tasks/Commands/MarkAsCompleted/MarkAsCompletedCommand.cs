﻿using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.Tasks.Commands.MarkAsCompleted
{
    public class MarkAsCompletedCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public bool Checked { get; set; } = true;

        public class MarkAsCompletedCommandHandler : IRequestHandler<MarkAsCompletedCommand, Response<int>>
        {
            private readonly IAppTaskRepository _appTaskRepository;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IAuthUserService _authUserService;

            public MarkAsCompletedCommandHandler(IAuthUserService authUserService,
                                                 IAppTaskRepository appTaskRepository,
                                                 IUnitOfWork unitOfWork)
            {
                _authUserService = authUserService;
                _appTaskRepository = appTaskRepository;
                _unitOfWork = unitOfWork;
            }

            public async Task<Response<int>> Handle(MarkAsCompletedCommand request, CancellationToken cancellationToken)
            {
                var task = await _appTaskRepository.GetSingleAsync(e => e.Id == request.Id && e.CreatedBy == _authUserService.UserId);

                if (task == null)
                {
                    throw new KeyNotFoundException();
                }

                task.Completed = request.Checked;

                await _appTaskRepository.UpdateAsync(task);
                await _unitOfWork.CommitAsync();

                return new Response<int>(request.Id);
            }
        }
    }
}
