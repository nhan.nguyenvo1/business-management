﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;

namespace PROJECT.Application.Features.Tasks.Commands.AssignTask
{
    public class AssignTaskCommand : IRequest<Response<int>>
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime? DueDate { get; set; }

        public ICollection<IFormFile> Attachments { get; set; }

        public ICollection<string> CheckLists { get; set; }

        public ICollection<string> ResponsibleIds { get; set; }

        public class AssignTaskCommandHandler : IRequestHandler<AssignTaskCommand, Response<int>>
        {
            private readonly IAppTaskRepository _appTaskRepository;
            private readonly ICloudinaryService _cloudinaryService;
            private readonly IMapper _mapper;
            private readonly ITaskAttachmentRepository _taskAttachmentRepository;
            private readonly ITaskCheckListRepository _taskCheckListRepository;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IUserRepository _userRepository;

            public AssignTaskCommandHandler(IAppTaskRepository appTaskRepository, IUnitOfWork unitOfWork,
                IMapper mapper,
                ITaskAttachmentRepository taskAttachmentRepository, ITaskCheckListRepository taskCheckListRepository,
                ICloudinaryService cloudinaryService, IUserRepository userRepository)
            {
                _appTaskRepository = appTaskRepository;
                _unitOfWork = unitOfWork;
                _mapper = mapper;
                _taskAttachmentRepository = taskAttachmentRepository;
                _taskCheckListRepository = taskCheckListRepository;
                _cloudinaryService = cloudinaryService;
                _userRepository = userRepository;
            }

            public async Task<Response<int>> Handle(AssignTaskCommand request, CancellationToken cancellationToken)
            {
                var task = _mapper.Map<AppTask>(request);
                var appTask = await _appTaskRepository.AddAsync(task);
                var taskId = appTask.Id;
                if (request.ResponsibleIds.Any())
                    foreach (var responsibleId in request.ResponsibleIds)
                    {
                        var user = await _userRepository.GetByIdAsync(responsibleId);
                        if (user == null)
                            throw new KeyNotFoundException("One or more responsible person does not exists");

                        appTask.TaskUsers.Add(user);
                    }

                if (request.Attachments != null && request.Attachments.Any())
                    foreach (var attachment in request.Attachments)
                    {
                        var response = await _cloudinaryService.UploadAsync(attachment.FileName,
                            attachment.OpenReadStream(), true,
                            $"Tasks/{taskId}");

                        var taskAttachment = new TaskAttachment
                        {
                            FileName = attachment.FileName,
                            PublicId = response.PublicId,
                            Url = response.Url
                        };
                        appTask.Attachments.Add(taskAttachment);
                    }

                if (request.CheckLists != null && request.CheckLists.Any())
                    foreach (var checkList in request.CheckLists)
                    {
                        var taskCheckList = new TaskCheckList
                        {
                            Title = checkList,
                            TaskId = taskId
                        };

                        appTask.CheckLists.Add(taskCheckList);
                    }

                await _unitOfWork.CommitAsync();

                return new Response<int>(appTask.Id);
            }
        }
    }
}