﻿using System;
using System.Linq;
using FluentValidation;
using PROJECT.Application.Constants;
using PROJECT.Application.ValidatorExtensions;

namespace PROJECT.Application.Features.Tasks.Commands.AssignTask
{
    public class AssignTaskCommandValidator : AbstractValidator<AssignTaskCommand>
    {
        public AssignTaskCommandValidator()
        {
            RuleFor(p => p.Title)
                .NotEmpty()
                .NotNull()
                .WithMessage(string.Format(ErrorMessageConstant.Required, "Task title"));

            RuleFor(p => p.DueDate)
                .Must(dueDate => dueDate > DateTime.Now)
                .When(command => command.DueDate != null)
                .WithMessage(string.Format(ErrorMessageConstant.OverDate, "Due date"));

            RuleFor(p => p.ResponsibleIds)
                .Must((command, responsibleIds) => responsibleIds != null && responsibleIds.Any())
                .WithMessage(string.Format(ErrorMessageConstant.LengthRequired, "1", "responsible person"));

            RuleForEach(p => p.Attachments)
                .SetValidator(new IFormFileValidator());
        }
    }
}