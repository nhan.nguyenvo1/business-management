﻿using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PROJECT.Application.Features.Tasks.Commands.MarkCheckListCompleted
{
    public class MarkCheckListCompletedCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public bool Checked { get; set; } = true;

        public class MarkCheckListCompletedCommandHandler : IRequestHandler<MarkCheckListCompletedCommand, Response<int>>
        {
            private readonly ITaskCheckListRepository _taskCheckListRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IUnitOfWork _unitOfWork;

            public MarkCheckListCompletedCommandHandler(IUnitOfWork unitOfWork,
                                                        ITaskCheckListRepository taskCheckListRepository,
                                                        IAuthUserService authUserService)
            {
                _unitOfWork = unitOfWork;
                _taskCheckListRepository = taskCheckListRepository;
                _authUserService = authUserService;
            }

            public async Task<Response<int>> Handle(MarkCheckListCompletedCommand request, CancellationToken cancellationToken)
            {
                var taskCheckList = await _taskCheckListRepository.GetSingleAsync(e => e.Id == request.Id && e.Task.CreatedBy == _authUserService.UserId);

                if (taskCheckList == null)
                {
                    throw new KeyNotFoundException();
                }

                taskCheckList.Checked = request.Checked;
                await _taskCheckListRepository.UpdateAsync(taskCheckList);
                await _unitOfWork.CommitAsync();

                return new Response<int>(request.Id);
            }
        }
    }
}
