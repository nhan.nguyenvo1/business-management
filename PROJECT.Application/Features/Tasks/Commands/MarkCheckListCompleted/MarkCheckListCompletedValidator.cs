﻿using FluentValidation;

namespace PROJECT.Application.Features.Tasks.Commands.MarkCheckListCompleted
{
    public class MarkCheckListCompletedValidator : AbstractValidator<MarkCheckListCompletedCommand>
    {
        public MarkCheckListCompletedValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty();
        }
    }
}
