﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Constants;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Tasks.Queries.GetTaskById
{
    public class GetTaskByIdQuery : IRequest<Response<GetTaskByIdViewModel>>
    {
        public GetTaskByIdQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }

        public class GetTaskByIdQueryHandler : IRequestHandler<GetTaskByIdQuery, Response<GetTaskByIdViewModel>>
        {
            private readonly IAppTaskRepository _appTaskRepository;
            private readonly IMapper _mapper;

            public GetTaskByIdQueryHandler(IMapper mapper, IAppTaskRepository appTaskRepository)
            {
                _mapper = mapper;
                _appTaskRepository = appTaskRepository;
            }

            public async Task<Response<GetTaskByIdViewModel>> Handle(GetTaskByIdQuery request,
                CancellationToken cancellationToken)
            {
                var task = await _appTaskRepository.GetByIdAsync<GetTaskByIdViewModel>(request.Id);

                if (task == null)
                    throw new KeyNotFoundException(string.Format(ErrorMessageConstant.NotFound, "Task", "id",
                        request.Id));

                return new Response<GetTaskByIdViewModel>(task);
            }
        }
    }
}