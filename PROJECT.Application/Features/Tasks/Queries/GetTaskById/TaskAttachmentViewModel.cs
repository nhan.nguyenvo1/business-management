﻿namespace PROJECT.Application.Features.Tasks.Queries.GetTaskById
{
    public class TaskAttachmentViewModel
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public string FileName { get; set; }
    }
}