﻿namespace PROJECT.Application.Features.Tasks.Queries.GetTaskById
{
    public class TaskCheckListViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool Checked { get; set; }
    }
}