﻿using System.Collections.Generic;

namespace PROJECT.Application.Features.Tasks.Queries.GetTaskById
{
    public class GetTaskByIdViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string DueDate { get; set; }

        public string Description { get; set; }

        public bool Completed { get; set; }

        public ICollection<TaskResponsiblePersonViewModel> ResponsiblePersons { get; set; }

        public ICollection<TaskCheckListViewModel> CheckLists { get; set; }

        public ICollection<TaskAttachmentViewModel> Attachments { get; set; }
    }
}