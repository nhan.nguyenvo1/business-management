﻿namespace PROJECT.Application.Features.Tasks.Queries.GetTaskById
{
    public class TaskResponsiblePersonViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{LastName} {FirstName}";

        public string AvatarUrl { get; set; }
    }
}