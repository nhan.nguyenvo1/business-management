﻿namespace PROJECT.Application.Features.Tasks.Queries.GetTasksPaging
{
    public class ResponsibleViewModel
    {
        public string Id { get; set; }

        public string Avatar { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}