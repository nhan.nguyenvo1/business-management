﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Parameters;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Tasks.Queries.GetTasksPaging
{
    public class GetTasksPagingQuery : RequestParameter, IRequest<PagedResponse<GetTasksPagingViewModel>>
    {
        public string Search { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }

        public Dictionary<string, string> Filters { get; set; }

        public class
            GetTasksPagingQueryHandler : IRequestHandler<GetTasksPagingQuery, PagedResponse<GetTasksPagingViewModel>>
        {
            private readonly IAppTaskRepository _appTaskRepository;
            private readonly IMapper _mapper;

            public GetTasksPagingQueryHandler(IAppTaskRepository appTaskRepository, IMapper mapper)
            {
                _appTaskRepository = appTaskRepository;
                _mapper = mapper;
            }

            public async Task<PagedResponse<GetTasksPagingViewModel>> Handle(GetTasksPagingQuery request,
                CancellationToken cancellationToken)
            {
                var tasks = await _appTaskRepository.GetPagedResponseAsync<GetTasksPagingViewModel>(request.Page,
                    request.Limit, request.Search,
                    request.Sort, request.Order, request.Filters);

                var total = await _appTaskRepository.CountAsync(request.Search, request.Filters);

                return new PagedResponse<GetTasksPagingViewModel>(
                    tasks,
                    request.Page,
                    request.Limit,
                    total);
            }
        }
    }
}