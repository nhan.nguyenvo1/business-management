﻿using System.Collections.Generic;

namespace PROJECT.Application.Features.Tasks.Queries.GetTasksPaging
{
    public class GetTasksPagingViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string DueDate { get; set; }

        public ResponsibleViewModel Created { get; set; }

        public bool Completed { get; set; }

        public ICollection<ResponsibleViewModel> ResponsiblePersons { get; set; }
    }
}