﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Parameters;
using PROJECT.Application.Wrappers;

namespace PROJECT.Application.Features.Tasks.Queries.GetMyTasksPaging
{
    public class GetMyTasksPagingQuery : RequestParameter, IRequest<PagedResponse<GetMyTaskPagingViewModel>>
    {
        public string Search { get; set; }

        public string Sort { get; set; }

        public string Order { get; set; }

        public Dictionary<string, string> Filters { get; set; }

        public class
            GetMyTasksPagingQueryHandler : IRequestHandler<GetMyTasksPagingQuery,
                PagedResponse<GetMyTaskPagingViewModel>>
        {
            private readonly IAppTaskRepository _appTaskRepository;
            private readonly IAuthUserService _authUserService;
            private readonly IMapper _mapper;

            public GetMyTasksPagingQueryHandler(IAppTaskRepository appTaskRepository, IMapper mapper,
                IAuthUserService authUserService)
            {
                _appTaskRepository = appTaskRepository;
                _mapper = mapper;
                _authUserService = authUserService;
            }

            public async Task<PagedResponse<GetMyTaskPagingViewModel>> Handle(GetMyTasksPagingQuery request,
                CancellationToken cancellationToken)
            {
                var userId = _authUserService.UserId;
                var tasks = await _appTaskRepository.GetPersonalPagedResponseAsync<GetMyTaskPagingViewModel>(userId,
                    request.Page, request.Limit,
                    request.Search,
                    request.Sort, request.Order, request.Filters);

                var total = await _appTaskRepository.CountPersonalAsync(userId, request.Search, request.Filters);

                return new PagedResponse<GetMyTaskPagingViewModel>(
                    tasks,
                    request.Page,
                    request.Limit,
                    total);
            }
        }
    }
}