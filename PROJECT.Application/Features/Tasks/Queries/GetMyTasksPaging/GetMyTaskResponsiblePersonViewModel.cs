﻿namespace PROJECT.Application.Features.Tasks.Queries.GetMyTasksPaging
{
    public class GetMyTaskResponsiblePersonViewModel
    {
        public string Id { get; set; }

        public string Avatar { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{LastName} {FirstName}";
    }
}