﻿using System.Collections.Generic;

namespace PROJECT.Application.Features.Tasks.Queries.GetMyTasksPaging
{
    public class GetMyTaskPagingViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string DueDate { get; set; }

        public GetMyTaskResponsiblePersonViewModel Created { get; set; }

        public ICollection<GetMyTaskResponsiblePersonViewModel> ResponsiblePersons { get; set; }
    }
}