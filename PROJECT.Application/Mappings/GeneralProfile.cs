﻿using System;
using AutoMapper;
using CloudinaryDotNet.Actions;
using PROJECT.Application.Dtos.Account;
using PROJECT.Application.Dtos.Chat;
using PROJECT.Application.Dtos.Cloudinary;
using PROJECT.Application.Dtos.Company;
using PROJECT.Application.Dtos.Notifications;
using PROJECT.Application.Dtos.User;
using PROJECT.Application.Features.BookingRooms.Commands.BookingRooms;
using PROJECT.Application.Features.Calendars.Queries.GetHomeCalendars;
using PROJECT.Application.Features.Companies.Queries.GetCompanies;
using PROJECT.Application.Features.CompanyDrives.Commands.Copy;
using PROJECT.Application.Features.CompanyDrives.Commands.DownloadFile;
using PROJECT.Application.Features.CompanyDrives.Commands.DownloadFolder;
using PROJECT.Application.Features.CompanyDrives.Commands.Move;
using PROJECT.Application.Features.CompanyDrives.Queries.GetFolders;
using PROJECT.Application.Features.Departments.Commands.CreateDepartment;
using PROJECT.Application.Features.Departments.Commands.UpdateDepartment;
using PROJECT.Application.Features.DesktopDrives.Commands.DownloadDrive;
using PROJECT.Application.Features.DesktopDrives.Queries.GetRecursiveDrives;
using PROJECT.Application.Features.Drives.Commands.Copy;
using PROJECT.Application.Features.Drives.Commands.DownloadFile;
using PROJECT.Application.Features.Drives.Commands.DownloadFolder;
using PROJECT.Application.Features.Drives.Commands.Move;
using PROJECT.Application.Features.Drives.Queries.GetFolders;
using PROJECT.Application.Features.Employees.Commands.CreateEmployee;
using PROJECT.Application.Features.Employees.Commands.UpdateEmployeeById;
using PROJECT.Application.Features.Feeds.Commands.CreatePost;
using PROJECT.Application.Features.Feeds.Commands.EditFeedById;
using PROJECT.Application.Features.Feeds.Queries.GetApprovedPostsPaging;
using PROJECT.Application.Features.Feeds.Queries.GetMyPostsPaging;
using PROJECT.Application.Features.Feeds.Queries.GetPostById;
using PROJECT.Application.Features.Feeds.Queries.GetPostsPaging;
using PROJECT.Application.Features.Plans.Queries.GetPlans;
using PROJECT.Application.Features.Roles.Commands.CreateRole;
using PROJECT.Application.Features.Roles.Commands.UpdateRoleById;
using PROJECT.Application.Features.Roles.Queries.GetRoleById;
using PROJECT.Application.Features.Roles.Queries.GetRolesPaging;
using PROJECT.Application.Features.SharedDrives.Commands.Download;
using PROJECT.Application.Features.SharedDrives.Queries.GetFolders;
using PROJECT.Application.Features.Tasks.Commands.AssignTask;
using PROJECT.Application.Features.Tasks.Commands.EditTaskById;
using PROJECT.Application.Features.Tasks.Queries.GetMyTasksPaging;
using PROJECT.Application.Features.Tasks.Queries.GetTaskById;
using PROJECT.Application.Features.Tasks.Queries.GetTasksPaging;
using PROJECT.Application.Features.TrashBins.Queries.GetDeletedDrives;
using PROJECT.Application.Features.Users.Commands.UpdateProfile;
using PROJECT.Application.Features.UserTimes.Queries.GetUserTimeForDate;
using PROJECT.Application.Features.UserTimes.Queries.GetUserTimes;
using PROJECT.Domain.Constants;
using PROJECT.Domain.Entities;
using PROJECT.Domain.ViewModels;

namespace PROJECT.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            #region TimeSpan

            CreateMap<TimeSpan?, string>().ConvertUsing(ts => ts.HasValue ? ts.Value.ToString(@"hh\:mm\:ss") : "");

            #endregion

            #region Date time

            CreateMap<DateTime, string>().ConvertUsing(dt => dt.ToString("yyyy-MM-dd"));

            #endregion

            #region Department

            CreateMap<CreateDepartmentCommand, Department>();
            CreateMap<UpdateDepartmentCommand, Department>()
                .ForMember(des => des.Id, opts => opts.Ignore());
            CreateMap<Department, DepartmentVm>();

            #endregion

            #region Employee

            CreateMap<CreateEmployeeCommand, AppUser>();

            CreateMap<UpdateEmployeeCommand, AppUser>();

            CreateMap<AppUser, EmployeeVm>()
                .ForMember(des => des.Avatar,
                    opts => opts.MapFrom(src => src.AvatarUrl ?? SystemConstant.DefaultAvatar));

            CreateMap<AppUser, PostUserViewModel>();

            CreateMap<AppUser, PostAuthorViewModel>();

            CreateMap<AppUser, ApprovedPostUserViewModel>();

            CreateMap<AppUser, MyPostUserViewModel>();

            #endregion

            #region Auth

            CreateMap<AppUser, AuthVm>()
                .ForMember(des => des.Avatar,
                    opts => opts.MapFrom(src => src.AvatarUrl ?? SystemConstant.DefaultAvatar));

            CreateMap<AppUser, AppUserVm>()
                .ForMember(des => des.Avatar,
                    opts => opts.MapFrom(src => src.AvatarUrl ?? SystemConstant.DefaultAvatar));

            CreateMap<AppUser, ActivityUserVm>()
                .ForMember(des => des.Avatar, opts => opts.MapFrom(src => src.AvatarUrl));

            CreateMap<RegisterRequest, AppUser>();

            #endregion

            #region Role

            CreateMap<AppRole, GetRolesPagingViewModel>();

            CreateMap<AppRole, GetRoleByIdViewModel>();

            CreateMap<CreateRoleCommand, AppRole>();

            CreateMap<UpdateRoleByIdCommand, AppRole>();

            #endregion

            #region Post

            CreateMap<CreatePostCommand, Post>();

            CreateMap<Post, GetPostByIdQueryViewModel>()
                .ForMember(des => des.Author, opts => opts.MapFrom(src => src.Created));

            CreateMap<Post, GetPostsPagingViewModel>()
                .ForMember(des => des.Author, opts => opts.MapFrom(src => src.Created));

            CreateMap<Post, GetApprovedPostsPagingViewModel>()
                .ForMember(des => des.Author, opts => opts.MapFrom(src => src.Created));

            CreateMap<Post, GetMyPostsPagingViewModel>();

            CreateMap<EditFeedByIdCommand, Post>()
                .ForMember(des => des.Id, opts => opts.Ignore());

            #endregion

            #region Post Attachment

            CreateMap<PostAttachment, PostAttachmentViewModel>();

            #endregion

            #region Cloudinary

            CreateMap<RawUploadResult, CloudinaryResponse>()
                .ForMember(des => des.Url, opts => opts.MapFrom(src => src.Url))
                .ForMember(des => des.PublicId, opts => opts.MapFrom(src => src.PublicId))
                .ForAllOtherMembers(opts => opts.Ignore());

            #endregion

            #region Task

            CreateMap<AssignTaskCommand, AppTask>()
                .ForMember(des => des.Attachments, opts => opts.Ignore())
                .ForMember(des => des.CheckLists, opts => opts.Ignore())
                .ForMember(des => des.TaskUsers, opts => opts.Ignore());

            CreateMap<AppUser, ResponsibleViewModel>()
                .ForMember(des => des.Avatar, opts => opts.MapFrom(src => src.AvatarUrl));

            CreateMap<AppTask, GetTasksPagingViewModel>()
                .ForMember(des => des.ResponsiblePersons, opts => opts.MapFrom(src => src.TaskUsers));

            CreateMap<EditTaskByIdCommand, AppTask>()
                .ForMember(des => des.Title, opts => opts.MapFrom(src => src.Title))
                .ForMember(des => des.Description, opts => opts.MapFrom(src => src.Description))
                .ForMember(des => des.DueDate, opts => opts.MapFrom(src => src.DueDate))
                .ForAllOtherMembers(opts => opts.Ignore());

            CreateMap<TaskAttachment, TaskAttachmentViewModel>();

            CreateMap<TaskCheckList, TaskCheckListViewModel>();

            CreateMap<AppUser, TaskResponsiblePersonViewModel>();

            CreateMap<AppTask, GetTaskByIdViewModel>()
                .ForMember(des => des.CheckLists, opts => opts.MapFrom(src => src.CheckLists))
                .ForMember(des => des.Attachments, opts => opts.MapFrom(src => src.Attachments))
                .ForMember(des => des.ResponsiblePersons, opts => opts.MapFrom(src => src.TaskUsers));

            CreateMap<AppUser, GetMyTaskResponsiblePersonViewModel>()
                .ForMember(des => des.Avatar, opts => opts.MapFrom(src => src.AvatarUrl));

            CreateMap<AppTask, GetMyTaskPagingViewModel>()
                .ForMember(des => des.ResponsiblePersons, opts => opts.MapFrom(src => src.TaskUsers));

            #endregion

            #region Calendars

            CreateMap<AppTask, GetHomeCalendarsViewModel>()
                .ForMember(des => des.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(des => des.Name, opts => opts.MapFrom(src => src.Title))
                .ForMember(des => des.Start, opts => opts.MapFrom(src => src.CreatedDate))
                .ForMember(des => des.End, opts => opts.MapFrom(src => src.DueDate))
                .ForMember(des => des.Color, opts =>
                    opts.MapFrom(src => src.Completed ? "success" : src.DueDate > DateTime.Now ? "primary" : "error"));

            CreateMap<BookingRoom, GetHomeCalendarsViewModel>()
                .ForMember(des => des.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(des => des.Name, opts => opts.MapFrom(src => src.Title))
                .ForMember(des => des.Start, opts => opts.MapFrom(src => src.Date + src.From))
                .ForMember(des => des.End, opts => opts.MapFrom(src => src.Date + src.To))
                .ForMember(des => des.Color, opts =>
                    opts.MapFrom(src => (src.Date + src.To) > DateTime.Now ? "info" : "primary"));

            #endregion

            #region UserTimes

            CreateMap<UserTime, WorkTimesVm>();

            CreateMap<AppUser, GetUserTimesVm>()
                .ForMember(des => des.UserId, opts => opts.MapFrom(src => src.Id))
                .ForMember(des => des.FullName, opts => opts.MapFrom(src => src.FullName))
                .ForMember(des => des.Avatar, opts => opts.MapFrom(src => src.AvatarUrl))
                .ForMember(des => des.Worktimes, opts => opts.MapFrom(src => src.UserTimes))
                .ForAllOtherMembers(opts => opts.Ignore());

            CreateMap<UserTime, GetUserTimeForDateVm>()
                .ForMember(des => des.CheckIn, opts => opts.MapFrom(src => src.CheckIn))
                .ForMember(des => des.CheckOut, opts => opts.MapFrom(src => src.CheckOut))
                .ForMember(des => des.Summary, opts => opts.MapFrom(src => src.Summary))
                .ForMember(des => des.Date, opts => opts.MapFrom(src => src.Date))
                .ForAllOtherMembers(opts => opts.Ignore());

            #endregion

            #region Chat

            CreateMap<AppUser, ChatUserVm>();

            CreateMap<AppRoom, RoomVm>();

            CreateMap<AppChat, MessageVm>()
                .ForMember(des => des.Sender, opts => opts.MapFrom(src => src.Sender));

            CreateMap<AppRoom, RoomInfoVm>()
                .ForMember(des => des.Messages, opts => opts.MapFrom(src => src.AppChats));

            CreateMap<AppRoom, RoomNameVm>();

            #endregion

            #region Drive

            CreateMap<AppUser, OwnerUser>()
                .ForMember(des => des.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(des => des.AvatarUrl, opts => opts.MapFrom(src => src.AvatarUrl))
                .ForMember(des => des.FullName, opts => opts.MapFrom(src => src.FullName))
                .ForAllOtherMembers(opts => opts.Ignore());

            CreateMap<AppFile, GetFoldersVm>()
                .ForMember(des => des.Created, opts => opts.MapFrom(src => src.Created))
                .ForMember(des => des.Updated, opts => opts.MapFrom(src => src.Updated));

            CreateMap<AppFile, DownloadFileVm>();

            CreateMap<AppFile, DownloadFolderVm>();

            CreateMap<AppFile, CopyVm>();

            CreateMap<AppFile, MoveVm>();

            #endregion

            #region DesktopDrives

            CreateMap<AppFile, GetRecursivesDrivesVm>();

            CreateMap<AppFile, DownloadDriveVm>();

            #endregion

            #region Company

            CreateMap<Company, GetCompaniesVm>();

            CreateMap<Company, CompanyStatusVm>();

            #endregion

            #region Company Drive

            CreateMap<AppUser, CompanyOwnerUser>()
                .ForMember(des => des.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(des => des.AvatarUrl, opts => opts.MapFrom(src => src.AvatarUrl))
                .ForMember(des => des.FullName, opts => opts.MapFrom(src => src.FullName))
                .ForAllOtherMembers(opts => opts.Ignore());

            CreateMap<CompanyDrive, GetCompanyFoldersVm>()
                .ForMember(des => des.Created, opts => opts.MapFrom(src => src.Created))
                .ForMember(des => des.Updated, opts => opts.MapFrom(src => src.Updated));

            CreateMap<CompanyDrive, DownloadCompanyFolderVm>();

            CreateMap<CompanyDrive, CompanyDriveCopyVm>();

            CreateMap<CompanyDrive, CompanyDriveMoveVm>();

            CreateMap<CompanyDrive, DownloadCompanyFileVm>();

            #endregion

            #region Shared Drive

            CreateMap<AppUser, SharedOwnerVm>()
                .ForMember(des => des.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(des => des.AvatarUrl, opts => opts.MapFrom(src => src.AvatarUrl))
                .ForMember(des => des.FullName, opts => opts.MapFrom(src => src.FullName))
                .ForAllOtherMembers(opts => opts.Ignore());

            CreateMap<AppFile, GetSharedFoldersVm>()
                .ForMember(des => des.Created, opts => opts.MapFrom(src => src.Created))
                .ForMember(des => des.Updated, opts => opts.MapFrom(src => src.Updated));

            CreateMap<SharedFile, DownloadSharedDriveVm>()
                .ForMember(des => des.Path, opts => opts.MapFrom(src => src.File.Path))
                .ForMember(des => des.FileType, opts => opts.MapFrom(src => src.File.FileType))
                .ForMember(des => des.CanRead, opts => opts.MapFrom(src => src.CanRead));

            #endregion

            #region TrashBins

            CreateMap<AppUser, DeletedOwnerVm>()
                .ForMember(des => des.Id, opts => opts.MapFrom(src => src.Id))
                .ForMember(des => des.AvatarUrl, opts => opts.MapFrom(src => src.AvatarUrl))
                .ForMember(des => des.FullName, opts => opts.MapFrom(src => src.FullName))
                .ForAllOtherMembers(opts => opts.Ignore());

            CreateMap<AppFile, GetDeletedDrivesVm>()
                .ForMember(des => des.Created, opts => opts.MapFrom(src => src.Created))
                .ForMember(des => des.Updated, opts => opts.MapFrom(src => src.Updated));

            #endregion

            #region Users

            CreateMap<AppUser, UserEmailTemplate>();

            CreateMap<UpdateProfileCommand, AppUser>()
                .ForMember(src => src.Dob, opts => opts.MapFrom(des => des.DateOfBirth))
                .ForMember(src => src.PhoneNumber, opts => opts.MapFrom(des => des.Phone));

            #endregion

            #region Plans

            CreateMap<Package, GetPlansVm>()
                .ForMember(des => des.NumOfUser, opts => opts.MapFrom(src => src.Users.Count));

            #endregion

            #region Booking Room

            CreateMap<BookingRoomCommand, BookingRoom>();

            #endregion

            #region Notification

            CreateMap<Notification, NotificationDto>()
                .ForMember(des => des.Read, opts => opts.MapFrom(src => false));

            CreateMap<UserNotification, NotificationDto>()
                .ForMember(des => des.Read, opts => opts.MapFrom(src => src.Read))
                .ForMember(des => des.Id, opts => opts.MapFrom(src => src.Notification.Id))
                .ForMember(des => des.Category, opts => opts.MapFrom(src => src.Notification.Category))
                .ForMember(des => des.Title, opts => opts.MapFrom(src => src.Notification.Title))
                .ForMember(des => des.Description, opts => opts.MapFrom(src => src.Notification.Description))
                .ForMember(des => des.CreatedDate, opts => opts.MapFrom(src => src.Notification.CreatedDate))
                .ForMember(des => des.UpdatedDate, opts => opts.MapFrom(src => src.Notification.UpdatedDate))
                .ForMember(des => des.Url, opts => opts.MapFrom(src => src.Notification.Url));

            #endregion
        }
    }
}