﻿using System.Text.RegularExpressions;

namespace PROJECT.Application.Extensions
{
    public static class StringExtension
    {
        public static string Replace(this string original, string oldString, string newString, int time)
        {
            var regex = new Regex(Regex.Escape(oldString));

            return regex.Replace(original, newString, time);
        }
    }
}
