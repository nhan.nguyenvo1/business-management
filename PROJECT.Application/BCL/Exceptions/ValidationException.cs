﻿using System;

namespace PROJECT.Application.BCL.Exceptions
{
    /// <summary>
    ///     Exception for representing that a request or the data it would affect did not meet preconditions.
    /// </summary>
    [Serializable]
    public class ValidationException : BaseException
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message"></param>
        public ValidationException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public ValidationException(string message, Exception ex)
            : base(message, ex)
        {
        }
    }
}