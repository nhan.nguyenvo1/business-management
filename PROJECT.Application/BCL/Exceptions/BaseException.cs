﻿using System;

namespace PROJECT.Application.BCL.Exceptions
{
    /// <summary>
    ///     Exception base class for common properties.
    /// </summary>
    [Serializable]
    public abstract class BaseException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        protected BaseException()
        {
        }

        /// <inheritdoc />
        protected BaseException(string message)
            : base(message)
        {
        }

        /// <inheritdoc />
        protected BaseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}