﻿using JetBrains.Annotations;
using PROJECT.Application.BCL.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PROJECT.Application.BCL
{
    /// <summary>
    ///     Convenience methods to verify that a constructor or method was invoked correctly.
    /// </summary>
    public static class Preconditions
    {
        /// <summary>
        ///     Checks that an argument is not null, and returns it to be set.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="argumentName"></param>
        /// <returns>The source object.</returns>
        [ContractAnnotation("source:null => stop")]
        public static T NotNull<T>([ValidatedNotNull] T source, string argumentName)
            where T : class
        {
            if (source == null) throw new ArgumentNullException(argumentName, $"{argumentName} cannot be null");

            return source;
        }

        /// <summary>
        ///     Checks that an argument is not null or whitespace, and returns it to be set.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="argumentName"></param>
        /// <returns>The source object.</returns>
        [ContractAnnotation("source:null => stop")]
        public static string NotNullOrWhitespace([ValidatedNotNull] string source, string argumentName)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                var value = source == null ? "null" : "whitespace";
                throw new ArgumentNullException(argumentName, $"{argumentName} cannot be {value}:");
            }

            return source;
        }

        /// <summary>
        ///     Checks that an argument is equal to an expected value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="exceptionMessage"></param>
        public static void MustEqual<T>(T source, T target, string exceptionMessage)
        {
            if (!source.Equals(target)) throw new ValidationException(exceptionMessage);
        }

        /// <summary>
        ///     Checks that an argument is not equal to a given value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <param name="exceptionMessage"></param>
        public static T MustNotEqual<T>(T source, T target, string exceptionMessage)
        {
            if (source.Equals(target)) throw new ValidationException(exceptionMessage);

            return source;
        }

        /// <summary>
        ///     Checks that the enumerable is not null or empty.
        /// </summary>
        [ContractAnnotation("source:null => stop")]
        public static void NotNullOrEmpty<T>([ValidatedNotNull] IEnumerable<T> value, string exceptionMessage)
        {
            if (value is null || !value.Any()) throw new ValidationException(exceptionMessage);
        }

        /// <summary>
        ///     Checks that a Guid is not empty.
        /// </summary>
        public static void GuidNotEmpty(Guid value, string argumentName)
        {
            if (value == Guid.Empty) throw new ArgumentException(argumentName, $"{argumentName} cannot be empty");
        }

        public static T NotDefault<T>([ValidatedNotNull] T source, string argumentName) where T : unmanaged
        {
            if (EqualityComparer<T>.Default.Equals(source, default))
                throw new ArgumentException(argumentName, $"{argumentName} cannot be the default value");

            return source;
        }
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public sealed class ValidatedNotNullAttribute : Attribute
    {
    }
}