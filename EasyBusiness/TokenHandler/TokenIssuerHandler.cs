﻿namespace EasyBusiness.TokenHandler
{
    public static class TokenIssuerHandler
    {
        public static string GetAccessToken()
        {
            return Properties.Token.Default.AccessToken;
        }

        public static void SetAccessToken(string accessToken)
        {
            Properties.Token.Default.AccessToken = accessToken;
            Properties.Token.Default.Save();
        }

        public static string GetRefreshToken()
        {
            return Properties.Token.Default.RefreshToken;
        }

        public static void SetRefreshToken(string refreshToken)
        {
            Properties.Token.Default.RefreshToken = refreshToken;
            Properties.Token.Default.Save();
        }
    }
}
