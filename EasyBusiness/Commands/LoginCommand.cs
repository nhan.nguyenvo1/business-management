﻿using EasyBusiness.Services;
using EasyBusiness.States.Authenticators;
using EasyBusiness.States.Navigators;
using EasyBusiness.ViewModels;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace EasyBusiness.Commands
{
    public class LoginCommand : AsyncCommandBase
    {
        private readonly LoginViewModel _loginViewModel;
        private readonly IAuthenticator _authenticator;
        private readonly IRenavigator _renavigator;
        private readonly IToastService _toastService;

        public LoginCommand(LoginViewModel loginViewModel, IAuthenticator authenticator, IRenavigator renavigator, IToastService toastService)
        {
            _loginViewModel = loginViewModel;
            _authenticator = authenticator;
            _renavigator = renavigator;

            _loginViewModel.PropertyChanged += LoginViewModel_PropertyChanged;
            _toastService = toastService;
        }

        public override bool CanExecute(object parameter)
        {
            return _loginViewModel.CanLogin && base.CanExecute(parameter);
        }

        public override async Task ExecuteAsync(object parameter)
        {
            try
            {
                await _authenticator.Login(_loginViewModel.UserNameOrEmail, _loginViewModel.Password);
                _renavigator.Renavigate();
            }
            catch (Exception e)
            {
                _toastService.Toast(ToastType.Error, e.Message);
            }
        }

        private void LoginViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LoginViewModel.CanLogin))
            {
                OnCanExecuteChanged();
            }
        }
    }
}
