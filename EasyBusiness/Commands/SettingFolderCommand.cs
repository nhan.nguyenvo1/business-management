﻿using EasyBusiness.States.SettingStore;
using EasyBusiness.ViewModels;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EasyBusiness.Commands
{
    public class SettingFolderCommand : AsyncCommandBase
    {
        private readonly HomeViewModel _homeViewModel;
        private readonly ISettingStore _settingStore;

        public SettingFolderCommand(HomeViewModel homeViewModel, ISettingStore settingStore)
        {
            _homeViewModel = homeViewModel;
            _settingStore = settingStore;

            _homeViewModel.PropertyChanged += PropertyChanged;
        }

        private void PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_homeViewModel.IsSyncing))
            {
                OnCanExecuteChanged();
            }
        }

        public override bool CanExecute(object parameter)
        {
            return !_homeViewModel.IsSyncing && base.CanExecute(parameter);
        }

        public override Task ExecuteAsync(object parameter)
        {
            var openFolderDialog = new FolderBrowserDialog();

            if (openFolderDialog.ShowDialog() == DialogResult.OK)
            {
                _settingStore.RootFolderPath = openFolderDialog.SelectedPath;
                _homeViewModel.OnSettingFolderChanged(_settingStore.RootFolderPath);
            }

            return Task.CompletedTask;
        }
    }
}
