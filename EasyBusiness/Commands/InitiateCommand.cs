﻿using EasyBusiness.Constants;
using EasyBusiness.Services;
using EasyBusiness.States.SettingStore;
using EasyBusiness.ViewModels;
using PROJECT.Domain.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EasyBusiness.Commands
{
    public class InitiateCommand : AsyncCommandBase
    {
        private readonly HomeViewModel _homeViewModel;
        private readonly IDrivesService _drivesService;
        private const string FilePath = "/Assets/Icons/FileIcon.png";
        private readonly ICollection<FileSystemWatcher> _watchers = new List<FileSystemWatcher>();
        private readonly ISettingStore _settingStore;
        private readonly IToastService _toastService;

        public InitiateCommand(HomeViewModel homeViewModel,
                               IDrivesService drivesService,
                               ISettingStore settingStore, IToastService toastService)
        {
            _homeViewModel = homeViewModel;
            _drivesService = drivesService;
            _settingStore = settingStore;
            _toastService = toastService;

            _homeViewModel.SettingFolderChanged += SettingFolderChanged;
            _homeViewModel.PropertyChanged += PropertyChanged;
        }

        private async void SettingFolderChanged(object sender, EventArgs e)
        {
            await ExecuteAsync(null);
        }

        public override bool CanExecute(object parameter)
        {
            return !_homeViewModel.IsSyncing && base.CanExecute(parameter);
        }


        private void PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_homeViewModel.IsSyncing))
            {
                OnCanExecuteChanged();
            }
        }

        public override async Task ExecuteAsync(object parameter)
        {
            _homeViewModel.Title = "Easy Business System is syncing from server...";
            _homeViewModel.IsSyncing = true;

            if (_watchers.Any())
            {
                ClearWatcher();
            }

            var companies = await _drivesService.GetCompaniesAsync();

            foreach (var company in companies)
            {
                string folder = $"{_settingStore.RootFolderPath}\\{company.Name}-p{company.Id}";
                if (!Directory.Exists(folder))
                {
                    _ = Directory.CreateDirectory(folder);
                }

                await SyncFromServer(company.Id, folder);
                await CreateNewWatcher(folder);
            }

            _homeViewModel.IsSyncing = false;
            _homeViewModel.Title = "EasyBusinessDrive is up to date";
        }

        private async Task SyncFromServer(int companyId, string folder)
        {
            var drives = await _drivesService.GetRecursiveDrive(companyId);

            foreach (var drive in drives)
            {
                var (_, _, path) = RegexService.ExtractPath(drive.Path);
                var actualPath = $"{folder}\\{path}";
                if (drive.FileType == FileType.Folder)
                {
                    if (!Directory.Exists(actualPath))
                    {
                        Directory.CreateDirectory(actualPath);
                        _homeViewModel.Files.Add(new FileTrackingViewModel
                        {
                            Image = FilePath,
                            FileName = drive.Name,
                            Action = "Download From Server",
                            CreatedDate = DateTime.Now.ToString("yyyy-MM-dd"),
                            IsLoading = false,
                            StatusIcon = StatusIconConstant.Success
                        });
                    }
                }
                else
                {
                    if (!File.Exists(actualPath))
                    {
                        try
                        {
                            var fileContent = await _drivesService.DownloadFile(drive.Id, companyId);
                            await WriteFile(fileContent, actualPath);
                            _homeViewModel.Files.Add(new FileTrackingViewModel
                            {
                                Image = FilePath,
                                FileName = drive.Name,
                                Action = "Download From Server",
                                CreatedDate = DateTime.Now.ToString("yyyy-MM-dd"),
                                IsLoading = false,
                                StatusIcon = StatusIconConstant.Success
                            });
                        }
                        catch (HttpRequestException e)
                        {
                            if (e.StatusCode == HttpStatusCode.NotFound)
                            {
                                continue;
                            }
                            _toastService.Toast(ToastType.Error, "Something Broken");
                        }
                    }
                }
            }
        }

        private async Task WriteFile(Stream fileContent, string actualPath)
        {
            using (var fileStream = new FileStream(actualPath, FileMode.Create, FileAccess.Write))
            {
                await fileContent.CopyToAsync(fileStream);
            }
        }

        private Task CreateNewWatcher(string folder)
        {
            var watcher = new FileSystemWatcher(folder)
            {
                NotifyFilter = NotifyFilters.Attributes
                                 | NotifyFilters.CreationTime
                                 | NotifyFilters.DirectoryName
                                 | NotifyFilters.FileName
                                 | NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.Security
                                 | NotifyFilters.Size,
                Filter = "*",
                IncludeSubdirectories = true,
                EnableRaisingEvents = true
            };

            watcher.Created += OnCreated;
            watcher.Deleted += OnDeleted;
            watcher.Renamed += OnRenamed;
            watcher.Error += OnError;

            _watchers.Add(watcher);

            return Task.CompletedTask;
        }

        private void OnError(object sender, ErrorEventArgs e)
        {
            _toastService.Toast(ToastType.Error, $"There was an error. Detail {e.GetException().Message}");
        }

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            var watcher = sender as FileSystemWatcher;
            App.Current.Dispatcher.Invoke(async () =>
            {
                var fileTracking = new FileTrackingViewModel
                {
                    Image = FilePath,
                    FileName = e.Name,
                    Action = "Rename from EasyBusinessDrive",
                    CreatedDate = DateTime.UtcNow.ToString("yyyy-MM-dd"),
                    IsLoading = true
                };

                _homeViewModel.Files.Add(fileTracking);

                try
                {
                    await _drivesService.RenameDrive(e.OldName.Normalize(), e.Name.Normalize(), RegexService.GetCompanyId(watcher.Path));
                    fileTracking.StatusIcon = StatusIconConstant.Success;
                }
                catch (Exception)
                {
                    fileTracking.StatusIcon = StatusIconConstant.Error;
                }

                fileTracking.IsLoading = false;
            });
        }

        private void OnDeleted(object sender, FileSystemEventArgs e)
        {
            var watcher = sender as FileSystemWatcher;
            App.Current.Dispatcher.Invoke(async () =>
            {
                var fileTracking = new FileTrackingViewModel
                {
                    Image = FilePath,
                    FileName = e.Name,
                    Action = "Deleted from EasyBusinessDrive",
                    CreatedDate = DateTime.UtcNow.ToString("yyyy-MM-dd"),
                    IsLoading = true
                };

                _homeViewModel.Files.Add(fileTracking);

                try
                {
                    await _drivesService.DeleteAsync(e.Name.Normalize(), RegexService.GetCompanyId(watcher.Path));
                    fileTracking.StatusIcon = StatusIconConstant.Success;
                }
                catch (Exception)
                {
                    fileTracking.StatusIcon = StatusIconConstant.Error;
                }

                fileTracking.IsLoading = false;
            });
        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            var watcher = sender as FileSystemWatcher;

            App.Current.Dispatcher.Invoke(async () =>
            {
                var fileTracking = new FileTrackingViewModel
                {
                    Image = FilePath,
                    FileName = e.Name,
                    Action = "Upload to EasyBusinessDrive",
                    CreatedDate = DateTime.UtcNow.ToString("yyyy-MM-dd"),
                    IsLoading = true
                };
                _homeViewModel.Files.Add(fileTracking);
                var attribute = File.GetAttributes(e.FullPath);
                try
                {
                    if (attribute.HasFlag(FileAttributes.Directory))
                    {
                        await _drivesService.CreateFolderAsync(watcher.Path.Normalize(), e.Name.Normalize(), RegexService.GetCompanyId(watcher.Path));
                    }
                    else
                    {
                        var file = new FileInfo(e.FullPath);

                        await _drivesService.UploadFileAsync(watcher.Path.Normalize(), file.Name.Normalize(), await File.ReadAllBytesAsync(file.FullName), RegexService.GetCompanyId(watcher.Path));
                    }

                    fileTracking.StatusIcon = StatusIconConstant.Success;
                }
                catch (Exception)
                {
                    fileTracking.StatusIcon = StatusIconConstant.Error;
                }
                fileTracking.IsLoading = false;
            });
        }

        private void ClearWatcher()
        {
            foreach (var watcher in _watchers)
            {
                watcher.Dispose();
            }

            _watchers.Clear();
        }
    }
}
