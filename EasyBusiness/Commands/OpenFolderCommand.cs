﻿using EasyBusiness.States.SettingStore;
using EasyBusiness.ViewModels;
using System.Diagnostics;
using System.Threading.Tasks;

namespace EasyBusiness.Commands
{
    public class OpenFolderCommand : AsyncCommandBase
    {
        private readonly ISettingStore _settingStore;

        public OpenFolderCommand(ISettingStore settingStore)
        {
            _settingStore = settingStore;
        }

        public override Task ExecuteAsync(object parameter)
        {
            _ =Process.Start("explorer.exe", _settingStore.RootFolderPath);
            return Task.CompletedTask;
        }
    }
}
