﻿using EasyBusiness.States.Authenticators;
using EasyBusiness.States.Navigators;
using EasyBusiness.States.SettingStore;
using EasyBusiness.States.TokenStore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EasyBusiness.HotBuilders
{
    public static class AddStoresHostBuilderExtensions
    {
        public static IHostBuilder AddStores(this IHostBuilder host)
        {
            host.ConfigureServices(services =>
            {
                services.AddSingleton<INavigator, Navigator>();
                services.AddSingleton<IAuthenticator, Authenticator>();
                services.AddSingleton<ITokenStore, TokenStore>();
                services.AddSingleton<ISettingStore, SettingStore>();
            });

            return host;
        }
    }
}
