﻿using EasyBusiness.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace EasyBusiness.HotBuilders
{
    public static class AddServicesHostBuilderExtensions
    {
        public static IHostBuilder AddServices(this IHostBuilder host)
        {
            host.ConfigureServices(services =>
            {
                services.AddTransient<IAuthsService, AuthsService>();
                services.AddTransient<IDrivesService, DrivesService>();
                services.AddSingleton<IToastService, ToastService>();
            });

            return host;
        }
    }
}
