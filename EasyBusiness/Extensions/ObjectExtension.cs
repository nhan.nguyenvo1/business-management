﻿using System.Linq;
using System.Web;

namespace EasyBusiness.Extensions
{
    public static class ObjectExtension
    {
        public static string ConvertObjectToQueryString(this object o)
        {
            var properties = from p in o.GetType().GetProperties()
                             where p.GetValue(o, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(o, null).ToString());

            // queryString will be set to "Id=1&State=26&Prefix=f&Index=oo"                  
            return string.Join("&", properties.ToArray()); 
        }
    }
}
