﻿namespace EasyBusiness.Extensions
{
    public static class StringExtension
    {
        public static string NormalizePath(this string s) => s.Replace("\\", "/").ToLower();
    }
}
