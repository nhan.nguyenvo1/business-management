﻿using EasyBusiness.Constants;
using EasyBusiness.TokenHandler;
using Newtonsoft.Json;
using PROJECT.Application.Dtos.Account;
using PROJECT.Application.Wrappers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EasyBusiness.Extensions
{
    public static class HttpClientExtension
    {
        public static async Task<HttpResponseMessage> SendWithRetryAsync(this HttpClient client, HttpRequestMessage request, CancellationToken cancellationToken = default)
        {
            try
            {
                var response = await client.SendAsync(request, cancellationToken);
                response.EnsureSuccessStatusCode();
                return response;
            }
            catch (HttpRequestException e)
            {
                client.CancelPendingRequests();
                if (e.StatusCode == HttpStatusCode.Unauthorized)
                {
                    return await client.RefreshTokenAsync(() => client.SendAsync(request.Clone(), cancellationToken), cancellationToken);
                }

                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static async Task<HttpResponseMessage> RefreshTokenAsync(this HttpClient client, Func<Task<HttpResponseMessage>> act, CancellationToken cancellationToken = default)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(EndPointConstant.Auth.RefreshToken),
                Headers =
                {
                    { HttpRequestHeader.Authorization.ToString(), $"Bearer {TokenIssuerHandler.GetAccessToken()}" },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" }
                },
                Method = HttpMethod.Post,
                Content = new StringContent(JsonConvert.SerializeObject(new
                {
                    RefreshToken = TokenIssuerHandler.GetRefreshToken()
                }), Encoding.UTF8, "application/json")
            };

            try
            {
                var response = await client.SendAsync(request, cancellationToken);
                response.EnsureSuccessStatusCode();
                var auth = await response.Content.ReadAs<Response<AuthResponse>>();

                TokenIssuerHandler.SetAccessToken(auth.Data.AccessToken);
                TokenIssuerHandler.SetRefreshToken(auth.Data.RefreshToken);

                return await act();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private static HttpRequestMessage Clone(this HttpRequestMessage req)
        {
            var clone = new HttpRequestMessage(req.Method, req.RequestUri);

            clone.Content = req.Content;
            clone.Version = req.Version;

            foreach (KeyValuePair<string, object> prop in req.Options)
            {
                clone.Options.TryAdd(prop.Key, prop.Value);
            }

            foreach (KeyValuePair<string, IEnumerable<string>> header in req.Headers)
            {
                clone.Headers.TryAddWithoutValidation(header.Key, header.Value);
            }

            clone.Headers.Authorization = new AuthenticationHeaderValue("Bearer", TokenIssuerHandler.GetAccessToken());

            return clone;
        }
    }
}
