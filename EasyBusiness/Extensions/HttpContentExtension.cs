﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace EasyBusiness.Extensions
{
    public static class HttpContentExtension
    {
        public static async Task<T> ReadAs<T>(this HttpContent httpContent)
        {
            return JsonConvert.DeserializeObject<T>(await httpContent.ReadAsStringAsync());
        }
    }
}
