﻿using EasyBusiness.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasyBusiness.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (DataContext is not null)
            {
                ((dynamic)DataContext).Password = (sender as PasswordBox).Password;
            }
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
        }

        private void EnterHandler(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
            {
                return;
            }

            if (DataContext is not null && ((LoginViewModel)DataContext).LoginCommand.CanExecute(null))
            {
                ((LoginViewModel)DataContext).LoginCommand.Execute(null);
            }
        }
    }
}
