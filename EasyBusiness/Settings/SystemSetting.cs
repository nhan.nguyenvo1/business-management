﻿namespace EasyBusiness.Settings
{
    public static class SystemSetting
    {
        public static string GetRootFolderPath()
        {
            return Properties.System.Default.RootFolderPath;
        }

        public static void SetRootFolderPath(string rootFolderPath)
        {
            Properties.System.Default.RootFolderPath = rootFolderPath;
            Properties.System.Default.Save();
        }
    }
}
