﻿using EasyBusiness.Services;
using System.Windows;
using System.Windows.Threading;

namespace EasyBusiness
{
    public partial class MainWindow : Window
    {
        private readonly IToastService _toastService;
        public MainWindow(object dataContext, IToastService toastService)
        {
            InitializeComponent();

            DataContext = dataContext;
            _toastService = toastService;

            Application.Current.DispatcherUnhandledException += DispatcherUnhandledException;
        }

        private void DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            _toastService.Toast(ToastType.Error, "There was an unhandled error. Reopen application and try it again. \nIf the error still exists, please contact admin for more detail.");
        }
    }
}
