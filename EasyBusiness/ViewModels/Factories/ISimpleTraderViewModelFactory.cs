﻿using EasyBusiness.States.Navigators;

namespace EasyBusiness.ViewModels.Factories
{
    public interface ISimpleTraderViewModelFactory
    {
        ViewModelBase CreateViewModel(ViewType viewType);
    }
}
