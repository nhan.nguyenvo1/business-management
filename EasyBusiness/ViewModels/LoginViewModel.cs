﻿using EasyBusiness.Commands;
using EasyBusiness.Services;
using EasyBusiness.States.Authenticators;
using EasyBusiness.States.Navigators;
using System.Windows.Input;

namespace EasyBusiness.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        public ICommand LoginCommand { get; }

        public LoginViewModel(IAuthenticator authenticator, IRenavigator loginRenavigator, IToastService toastService)
        {
            LoginCommand = new LoginCommand(this, authenticator, loginRenavigator, toastService);
        }

        private string _userNameOrEmail;

        public string UserNameOrEmail
        {
            get
            {
                return _userNameOrEmail;
            }
            set
            {
                _userNameOrEmail = value;
                OnPropertyChanged(nameof(UserNameOrEmail));
                OnPropertyChanged(nameof(CanLogin));
            }
        }

        private string _password;

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
                OnPropertyChanged(nameof(CanLogin));
            }
        }

        public bool CanLogin => !string.IsNullOrWhiteSpace(UserNameOrEmail) && !string.IsNullOrWhiteSpace(Password);
    }
}
