﻿using EasyBusiness.Commands;
using EasyBusiness.Services;
using EasyBusiness.States.SettingStore;
using PROJECT.Application.Features.Companies.Queries.GetCompanies;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;

namespace EasyBusiness.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        private string _title;

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                OnPropertyChanged(nameof(Title));
            }
        }

        private bool _isSyncing;

        public bool IsSyncing
        {
            get => _isSyncing; set
            {
                _isSyncing = value;
                OnPropertyChanged(nameof(IsSyncing));
            }
        }

        private ObservableCollection<FileTrackingViewModel> _files = new();

        public ObservableCollection<FileTrackingViewModel> Files
        {
            get => _files;
            set
            {
                _files = value;
                OnPropertyChanged(nameof(Files));
            }
        }


        public ICommand InitiateCommand { get; }
        public ICommand OpenFolderCommand { get; }
        public ICommand SettingFolderCommand { get; private set; }

        public HomeViewModel(IDrivesService drivesService, ISettingStore settingStore, IToastService toastService)
        {
            InitiateCommand = new InitiateCommand(this, drivesService, settingStore, toastService);
            OpenFolderCommand = new OpenFolderCommand(settingStore);
            SettingFolderCommand = new SettingFolderCommand(this, settingStore);
        }

        public event EventHandler SettingFolderChanged;

        public void OnSettingFolderChanged(string path)
        {
            SettingFolderChanged?.Invoke(path, new EventArgs());
        }
    }
}
