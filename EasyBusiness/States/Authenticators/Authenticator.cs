﻿using EasyBusiness.Services;
using EasyBusiness.States.TokenStore;
using System;
using System.Threading.Tasks;

namespace EasyBusiness.States.Authenticators
{
    public class Authenticator : IAuthenticator
    {
        private readonly IAuthsService _authsService;
        private readonly ITokenStore _tokenStore;
        public Authenticator(IAuthsService authsService, ITokenStore tokenStore)
        {
            _authsService = authsService;
            _tokenStore = tokenStore;
        }

        public bool IsLoggedIn => !string.IsNullOrWhiteSpace(_tokenStore.AccessToken) && !string.IsNullOrWhiteSpace(_tokenStore.RefreshToken);

        public event Action StateChanged;

        /// <inheritdoc />
        public async Task Login(string username, string password)
        {
            var response = await _authsService.SignIn(username, password);
            _tokenStore.AccessToken = response.AccessToken;
            _tokenStore.RefreshToken = response.RefreshToken;
        }

        /// <inheritdoc />
        public Task Logout()
        {
            _tokenStore.AccessToken = _tokenStore.RefreshToken = string.Empty;
            return Task.CompletedTask;
        }
    }
}
