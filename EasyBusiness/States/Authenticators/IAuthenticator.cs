﻿using System;
using System.Threading.Tasks;

namespace EasyBusiness.States.Authenticators
{
    public interface IAuthenticator
    {
        bool IsLoggedIn { get; }

        event Action StateChanged;

        /// <summary>
        /// Login to the application.
        /// </summary>
        /// <param name="username">The user's name.</param>
        /// <param name="password">The user's password.</param>
        Task Login(string username, string password);

        /// <summary>
        /// Logout from the application
        /// </summary>
        /// <returns></returns>
        Task Logout();
    }
}
