﻿namespace EasyBusiness.States.SettingStore
{
    public interface ISettingStore
    {
        string RootFolderPath { get; set; }
    }
}
