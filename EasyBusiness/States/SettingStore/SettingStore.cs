﻿namespace EasyBusiness.States.SettingStore
{
    public class SettingStore : ISettingStore
    {
        public string RootFolderPath
        {
            get => Properties.System.Default.RootFolderPath;
            set
            {
                Properties.System.Default.RootFolderPath = value;
                Properties.System.Default.Save();
            }
        }
    }
}
