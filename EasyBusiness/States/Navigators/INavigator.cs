﻿using EasyBusiness.ViewModels;
using System;

namespace EasyBusiness.States.Navigators
{
    public enum ViewType
    {
        Login,
        Home
    }

    public interface INavigator
    {
        ViewModelBase CurrentViewModel { get; set; }
        event Action StateChanged;
    }
}
