﻿using PROJECT.Application.Dtos.Account;

namespace EasyBusiness.States.TokenStore
{
    public interface ITokenStore
    {
        string AccessToken { get; set; }

        string RefreshToken { get; set; }
    }
}
