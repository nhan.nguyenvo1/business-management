﻿using System;

namespace EasyBusiness.States.TokenStore
{
    public class TokenStore : ITokenStore
    {
        public string AccessToken
        {
            get => Properties.Token.Default.AccessToken;
            set
            {
                Properties.Token.Default.AccessToken = value;
                Properties.Token.Default.Save();
            }
        }
        public string RefreshToken
        {
            get => Properties.Token.Default.RefreshToken;
            set
            {
                Properties.Token.Default.RefreshToken = value;
                Properties.Token.Default.Save();
            }
        }
    }
}
