﻿using EasyBusiness.Constants;
using EasyBusiness.Extensions;
using Newtonsoft.Json;
using PROJECT.Application.Dtos.Account;
using PROJECT.Application.Wrappers;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EasyBusiness.Services
{
    public class AuthsService : IAuthsService
    {
        /// <inheritdoc />
        public async Task<AuthResponse> SignIn(string email, string password, bool rememberMe = true)
        {
            //Response<AuthResponse>
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(EndPointConstant.Auth.SignIn),
                Method = HttpMethod.Post,
                Headers =
                {
                    { HttpRequestHeader.ContentType.ToString(), "application/json" }
                },
                Content = new StringContent(JsonConvert.SerializeObject(new { email, password, rememberMe }), Encoding.UTF8, "application/json")
            };

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendAsync(request);

                try
                {
                    response.EnsureSuccessStatusCode();
                    return (await response.Content.ReadAs<Response<AuthResponse>>()).Data;
                }
                catch (HttpRequestException)
                {
                    var message = (await response.Content.ReadAs<Response<AuthResponse>>()).Message;
                    throw new Exception(message);
                }
            }
        }
    }
}
