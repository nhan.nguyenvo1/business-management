﻿using PROJECT.Application.Features.Companies.Queries.GetCompanies;
using PROJECT.Application.Features.DesktopDrives.Queries.GetRecursiveDrives;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyBusiness.Services
{
    public interface IDrivesService
    {
        Task<IReadOnlyCollection<GetCompaniesVm>> GetCompaniesAsync();

        Task UploadFileAsync(string folder, string fileName, byte[] file, int companyId);

        Task CreateFolderAsync(string folderPath, string folderName, int companyId);

        Task DeleteAsync(string path, int companyId);

        Task MoveAsync(string sourcePath, string destinationPath, int companyId);

        Task RenameDrive(string sourcePath, string destinationPath, int companyId);

        Task<IReadOnlyCollection<GetRecursivesDrivesVm>> GetRecursiveDrive(int companyId);

        Task<Stream> DownloadFile(int fileId, int companyId);
    }
}
