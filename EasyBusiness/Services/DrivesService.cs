﻿using EasyBusiness.Constants;
using EasyBusiness.Extensions;
using EasyBusiness.States.TokenStore;
using Newtonsoft.Json;
using PROJECT.Application.Features.Companies.Queries.GetCompanies;
using PROJECT.Application.Features.DesktopDrives.Queries.GetRecursiveDrives;
using PROJECT.Application.Wrappers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EasyBusiness.Services
{
    public class DrivesService : IDrivesService
    {
        private readonly ITokenStore _tokenStore;

        public DrivesService(ITokenStore tokenStore)
        {
            _tokenStore = tokenStore;
        }

        public async Task<IReadOnlyCollection<GetCompaniesVm>> GetCompaniesAsync()
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(EndPointConstant.Companies.Get),
                Method = HttpMethod.Get,
                Headers =
                {
                    { HttpRequestHeader.Authorization.ToString(), $"Bearer {_tokenStore.AccessToken}" },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" }
                }
            };

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendWithRetryAsync(request);

                response.EnsureSuccessStatusCode();
                return (await response.Content.ReadAs<PagedResponse<GetCompaniesVm>>()).Data.ToList();
            }
        }

        public async Task UploadFileAsync(string folder, string fileName, byte[] file, int companyId)
        {
            using (var httpClient = new HttpClient())
            {
                using (var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture)))
                {
                    content.Add(new StreamContent(new MemoryStream(file)), "file", fileName);
                    content.Add(new StringContent(folder), folder);

                    var request = new HttpRequestMessage
                    {
                        RequestUri = new Uri(EndPointConstant.Drive.Upload),
                        Method = HttpMethod.Post,
                        Headers =
                        {
                            { HttpRequestHeader.Authorization.ToString(), $"Bearer {_tokenStore.AccessToken}" },
                            { HttpRequestHeader.ContentType.ToString(), "multipart/form-data" },
                            { "X-Company-Id", companyId.ToString() }
                        },
                        Content = content
                    };

                    var response = await httpClient.SendWithRetryAsync(request);
                    response.EnsureSuccessStatusCode();
                }
            }
        }

        public async Task CreateFolderAsync(string folderPath, string folderName, int companyId)
        {
            using (var httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(EndPointConstant.Drive.Create),
                    Method = HttpMethod.Post,
                    Headers =
                        {
                            { HttpRequestHeader.Authorization.ToString(), $"Bearer {_tokenStore.AccessToken}" },
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { "X-Company-Id", companyId.ToString() }
                        },
                    Content = new StringContent(JsonConvert.SerializeObject(new { folderName, folderPath }), Encoding.UTF8, "application/json")
                };

                var response = await httpClient.SendWithRetryAsync(request);
                response.EnsureSuccessStatusCode();

            }
        }

        public async Task DeleteAsync(string path, int companyId)
        {
            using (var httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(EndPointConstant.Drive.Delete),
                    Method = HttpMethod.Post,
                    Headers =
                        {
                            { HttpRequestHeader.Authorization.ToString(), $"Bearer {_tokenStore.AccessToken}" },
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { "X-Company-Id", companyId.ToString() }
                        },
                    Content = new StringContent(JsonConvert.SerializeObject(new { path }), Encoding.UTF8, "application/json")
                };

                var response = await httpClient.SendWithRetryAsync(request);
                response.EnsureSuccessStatusCode();

            }
        }

        public async Task MoveAsync(string sourcePath, string destinationPath, int companyId)
        {
            using (var httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(EndPointConstant.Drive.Move),
                    Method = HttpMethod.Post,
                    Headers =
                        {
                            { HttpRequestHeader.Authorization.ToString(), $"Bearer {_tokenStore.AccessToken}" },
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { "X-Company-Id", companyId.ToString() }
                        },
                    Content = new StringContent(JsonConvert.SerializeObject(new { sourcePath, destinationPath }), Encoding.UTF8, "application/json")
                };

                var response = await httpClient.SendWithRetryAsync(request);
                response.EnsureSuccessStatusCode();

            }
        }

        public async Task RenameDrive(string sourcePath, string destinationPath, int companyId)
        {
            using (var httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(EndPointConstant.Drive.Rename),
                    Method = HttpMethod.Post,
                    Headers =
                        {
                            { HttpRequestHeader.Authorization.ToString(), $"Bearer {_tokenStore.AccessToken}" },
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { "X-Company-Id", companyId.ToString() }
                        },
                    Content = new StringContent(JsonConvert.SerializeObject(new { sourcePath, destinationPath }), Encoding.UTF8, "application/json")
                };

                var response = await httpClient.SendWithRetryAsync(request);
                response.EnsureSuccessStatusCode();

            }
        }

        public async Task<IReadOnlyCollection<GetRecursivesDrivesVm>> GetRecursiveDrive(int companyId)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(EndPointConstant.Drive.Get),
                Method = HttpMethod.Get,
                Headers =
                {
                    { HttpRequestHeader.Authorization.ToString(), $"Bearer {_tokenStore.AccessToken}" },
                    { HttpRequestHeader.ContentType.ToString(), "application/json" },
                    { "X-Company-Id", companyId.ToString() }
                }
            };

            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.SendWithRetryAsync(request);

                response.EnsureSuccessStatusCode();
                return (await response.Content.ReadAs<Response<IReadOnlyCollection<GetRecursivesDrivesVm>>>()).Data;
            }
        }

        public async Task<Stream> DownloadFile(int fileId, int companyId)
        {
            using (var httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage
                {
                    RequestUri = new Uri(string.Format(EndPointConstant.Drive.Download, fileId)),
                    Method = HttpMethod.Get,
                    Headers =
                        {
                            { HttpRequestHeader.Authorization.ToString(), $"Bearer {_tokenStore.AccessToken}" },
                            { HttpRequestHeader.ContentType.ToString(), "application/json" },
                            { "X-Company-Id", companyId.ToString() }
                        },
                };

                var response = await httpClient.SendWithRetryAsync(request);
                response.EnsureSuccessStatusCode();
                
                return await response.Content.ReadAsStreamAsync();
            }
        }
    }
}
