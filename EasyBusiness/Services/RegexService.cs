﻿using System;
using System.Text.RegularExpressions;

namespace EasyBusiness.Services
{
    public static class RegexService
    {
        public static int GetCompanyId(string path)
        {
            var pattern = new Regex(@"(?<companyName>\w)-p(?<companyId>\d+)");
            var match = pattern.Match(path);

            return int.Parse(match.Groups["companyId"].Value);
        }

        public static Tuple<int, string, string> ExtractPath(string text)
        {
            // /1/0ae9088e-7093-48ab-b658-393da0f8309a/test
            var pattern = new Regex(@"/(?<companyId>\d+)/(?<userId>.*)/(?<path>.*)");
            var match = pattern.Match(text);

            return new Tuple<int, string, string>(int.Parse(match.Groups["companyId"].Value), match.Groups["userId"].Value, match.Groups["path"].Value);
        }
    }
}
