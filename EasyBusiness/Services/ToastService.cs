﻿using System;
using System.Collections.Generic;
using System.Windows;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Messages;
using ToastNotifications.Position;

namespace EasyBusiness.Services
{
    public enum ToastType
    {
        Success,
        Info,
        Error,
        Warning
    }

    public class ToastService : IToastService, IDisposable
    {
        private readonly Notifier _notifier;
        private readonly Dictionary<ToastType, Action<string>> _toastDictionary = new();

        public ToastService()
        {
            _notifier = new Notifier(cfg =>
            {
                cfg.PositionProvider = new WindowPositionProvider(
                    parentWindow: Application.Current.MainWindow,
                    corner: Corner.TopRight,
                    offsetX: 10,
                    offsetY: 10);

                cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                    notificationLifetime: TimeSpan.FromSeconds(3),
                    maximumNotificationCount: MaximumNotificationCount.FromCount(5));

                cfg.Dispatcher = Application.Current.Dispatcher;
            });

            _toastDictionary[ToastType.Success] = (messsage) => _notifier.ShowSuccess(messsage);
            _toastDictionary[ToastType.Info] = (messsage) => _notifier.ShowInformation(messsage);
            _toastDictionary[ToastType.Error] = (messsage) => _notifier.ShowError(messsage);
            _toastDictionary[ToastType.Warning] = (messsage) => _notifier.ShowWarning(messsage);
        }

        public void Toast(ToastType type, string message)
        {
            _toastDictionary[type](message);
        }

        public void Dispose()
        {
            _notifier.Dispose();
        }
    }
}
