﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyBusiness.Services
{
    public interface IToastService
    {
        void Toast(ToastType type, string message);
    }
}
