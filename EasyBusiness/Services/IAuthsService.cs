﻿using PROJECT.Application.Dtos.Account;
using System.Threading.Tasks;

namespace EasyBusiness.Services
{
    public interface IAuthsService
    {
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="rememberMe"></param>
        /// <returns></returns>
        Task<AuthResponse> SignIn(string email, string password, bool rememberMe = true);
    }
}
