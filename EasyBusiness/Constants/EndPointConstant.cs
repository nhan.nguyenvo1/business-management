﻿namespace EasyBusiness.Constants
{
    public static class EndPointConstant
    {
        public static class Drive
        {
            public const string Upload = SystemConstant.ServerUrl + "/api/v1/desktop-drive/files";

            public const string Delete = SystemConstant.ServerUrl + "/api/v1/desktop-drive/delete";

            public const string Create = SystemConstant.ServerUrl + "/api/v1/desktop-drive/folder";

            public const string Move = SystemConstant.ServerUrl + "/api/v1/desktop-drive/move";

            public const string Rename = SystemConstant.ServerUrl + "/api/v1/desktop-drive/rename";

            public const string Get = SystemConstant.ServerUrl + "/api/v1/desktop-drive";

            public const string Download = SystemConstant.ServerUrl + "/api/v1/desktop-drive/{0}";
        }

        public static class Companies
        {
            public const string Get = SystemConstant.ServerUrl + "/api/v1/companies?limit=100";
        }

        public static class Auth
        {
            public const string RefreshToken = SystemConstant.ServerUrl + "/api/v1/auth/refresh-token";

            public const string SignIn = SystemConstant.ServerUrl + "/api/v1/auth/sign-in";
        }
    }
}
