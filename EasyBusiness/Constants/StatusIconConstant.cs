﻿namespace EasyBusiness.Constants
{
    public static class StatusIconConstant
    {
        public const string Success = "/Assets/Icons/Success.png";

        public const string Error = "/Assets/Icons/Error.png";
    }
}
