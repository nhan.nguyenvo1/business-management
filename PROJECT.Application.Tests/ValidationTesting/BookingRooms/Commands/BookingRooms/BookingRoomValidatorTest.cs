﻿using System;
using System.Collections.Generic;
using FluentValidation.TestHelper;
using PROJECT.Application.Features.BookingRooms.Commands.BookingRooms;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.BookingRooms.Commands.BookingRooms
{
    public class BookingRoomValidatorTest
    {
        private readonly BookingRoomValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenTitleIsNull()
        {
            var model = new BookingRoomCommand
            {
                Date = DateTime.Now,
                From = DateTime.Now.TimeOfDay,
                UserIds = new List<string>
                {
                    new Guid().ToString()
                }
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.Title);
        }

        [Fact]
        public void ShouldHaveErrorWhenTitleIsEmpty()
        {
            var model = new BookingRoomCommand
            {
                Title = string.Empty,
                Date = DateTime.Now,
                From = DateTime.Now.TimeOfDay,
                UserIds = new List<string>
                {
                    new Guid().ToString()
                }
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.Title);
        }

        [Fact]
        public void ShouldHaveErrorWhenTitleLengthMoreThan200()
        {
            var model = new BookingRoomCommand
            {
                Title = new string('a', 201),
                Date = DateTime.Now,
                From = DateTime.Now.TimeOfDay,
                UserIds = new List<string>
                {
                    new Guid().ToString()
                }
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.Title);
        }

        [Fact]
        public void ShouldHaveErrorWhenDateSmallerThanNow()
        {
            var model = new BookingRoomCommand
            {
                Title = string.Empty,
                Date = DateTime.Now.AddDays(-10),
                From = DateTime.Now.TimeOfDay,
                UserIds = new List<string>
                {
                    new Guid().ToString()
                }
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.Date);
        }

        [Fact]
        public void ShouldHaveErrorWhenFromIsNull()
        {
            var model = new BookingRoomCommand
            {
                Title = "Title",
                Date = DateTime.Now,
                UserIds = new List<string>
                {
                    new Guid().ToString()
                }
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.From);
        }

        [Fact]
        public void ShouldHaveErrorWhenFromSmallerThanNow()
        {
            var model = new BookingRoomCommand
            {
                Title = "Title",
                Date = DateTime.Now,
                From = DateTime.Now.AddMinutes(-1).TimeOfDay,
                UserIds = new List<string>
                {
                    new Guid().ToString()
                }
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.From);
        }

        [Fact]
        public void ShouldHaveErrorWhenToSmallerThanFrom()
        {
            var model = new BookingRoomCommand
            {
                Title = "Title",
                Date = DateTime.Now,
                From = DateTime.Now.AddMinutes(2).TimeOfDay,
                To = DateTime.Now.AddMinutes(1).TimeOfDay,
                UserIds = new List<string>
                {
                    new Guid().ToString()
                }
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.To);
        }

        [Fact]
        public void ShouldHaveErrorWhenUserIdsIsNull()
        {
            var model = new BookingRoomCommand
            {
                Title = "Title",
                Date = DateTime.Now,
                From = DateTime.Now.AddMinutes(1).TimeOfDay
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.UserIds);
        }

        [Fact]
        public void ShouldHaveErrorWhenUserIdsIsEmpty()
        {
            var model = new BookingRoomCommand
            {
                Title = "Title",
                Date = DateTime.Now,
                From = DateTime.Now.AddMinutes(1).TimeOfDay,
                UserIds = Array.Empty<string>()
            };

            var result = _validations.TestValidate(model);

            result.ShouldHaveValidationErrorFor(p => p.UserIds);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelAsExpected()
        {
            var model = new BookingRoomCommand
            {
                Title = "Title",
                Date = DateTime.Now,
                From = DateTime.Now.AddMinutes(1).TimeOfDay,
                To = DateTime.Now.AddMinutes(2).TimeOfDay,
                UserIds = new List<string>
                {
                    new Guid().ToString()
                }
            };

            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}