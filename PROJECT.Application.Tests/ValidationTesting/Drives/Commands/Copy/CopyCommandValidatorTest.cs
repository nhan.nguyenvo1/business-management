﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Drives.Commands.Copy;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Drives.Commands.Copy
{
    public class CopyCommandValidatorTest
    {
        private readonly CopyCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenDriveIdIsEmpty()
        {
            var model = new CopyCommand
            {
                DestinationId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DriveId);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new CopyCommand
            {
                DriveId = 2,
                DestinationId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}