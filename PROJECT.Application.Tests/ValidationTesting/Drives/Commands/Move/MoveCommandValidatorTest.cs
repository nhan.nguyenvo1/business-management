﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Drives.Commands.Move;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Drives.Commands.Move
{
    public class MoveCommandValidatorTest
    {
        private readonly MoveCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenDriveIdIsEmpty()
        {
            var model = new MoveCommand
            {
                DestinationId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DriveId);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new MoveCommand
            {
                DriveId = 1,
                DestinationId = 2
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}