﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Drives.Commands.CreateFolder;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Drives.Commands.CreateFolder
{
    public class CreateFolderValidatorTest
    {
        private readonly CreateFolderValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenFolderNameIsEmpty()
        {
            var model = new CreateFolderCommand
            {
                FolderName = string.Empty,
                ParentId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FolderName);
        }

        [Fact]
        public void ShouldHaveErrorWhenFolderNameIsNull()
        {
            var model = new CreateFolderCommand
            {
                FolderName = null,
                ParentId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FolderName);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new CreateFolderCommand
            {
                FolderName = "a", ParentId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}