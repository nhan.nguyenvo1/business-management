﻿using System;
using FluentValidation.TestHelper;
using PROJECT.Application.Features.Employees.Commands.ChangeDepartment;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Employees.Commands.ChangeDepartment
{
    public class ChangeDepartmentValidatorTest
    {
        private readonly ChangeDepartmentValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenUserIdIsNull()
        {
            var model = new ChangeDepartmentCommand
            {
                UserId = null,
                DepartmentId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.UserId);
        }

        [Fact]
        public void ShouldHaveErrorWhenUserIdIsEmpty()
        {
            var model = new ChangeDepartmentCommand
            {
                UserId = string.Empty,
                DepartmentId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.UserId);
        }

        [Fact]
        public void ShouldHaveErrorWhenDepartmentIdIsEmpty()
        {
            var model = new ChangeDepartmentCommand
            {
                UserId = new Guid().ToString()
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DepartmentId);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new ChangeDepartmentCommand
            {
                UserId = new Guid().ToString(),
                DepartmentId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}