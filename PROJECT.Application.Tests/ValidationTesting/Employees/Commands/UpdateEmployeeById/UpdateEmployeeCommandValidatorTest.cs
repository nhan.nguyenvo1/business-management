﻿using System;
using FluentValidation.TestHelper;
using PROJECT.Application.Features.Employees.Commands.UpdateEmployeeById;
using PROJECT.Domain.Enums;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Employees.Commands.UpdateEmployeeById
{
    public class UpdateEmployeeCommandValidatorTest
    {
        private readonly UpdateEmployeeCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenPhoneNumberIsNull()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.PhoneNumber);
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneNumberIsEmpty()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.PhoneNumber);
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneNumberNotMatchCondition()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.PhoneNumber);
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneNumberIsMoreThan16()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = new string('4', 17)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.PhoneNumber);
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneNumberIsLessThan4()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = new string('3', 3)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.PhoneNumber);
        }

        [Fact]
        public void ShouldHaveErrorWhenFirstNameIsEmpty()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = string.Empty,
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = "3456"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FirstName);
        }

        [Fact]
        public void ShouldHaveErrorWhenFirstNameIsNull()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = null,
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = "3456"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FirstName);
        }

        [Fact]
        public void ShouldHaveErrorWhenFirstNameIsMoreThan100()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = new string('a', 101),
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = "3456"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FirstName);
        }

        [Fact]
        public void ShouldHaveErrorWhenLastNameIsEmpty()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = string.Empty,
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = "3456"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.LastName);
        }

        [Fact]
        public void ShouldHaveErrorWhenLastNameIsNull()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = null,
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = "3456"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.LastName);
        }

        [Fact]
        public void ShouldHaveErrorWhenLastNameIsMoreThan100()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = new string('a', 101),
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = "3456"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.LastName);
        }

        [Fact]
        public void ShouldHaveErrorWhenGenderIsNotInEnum()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = "b",
                Gender = (UserGender) 4,
                Dob = DateTime.Now,
                PhoneNumber = "3456"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Gender);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new UpdateEmployeeCommand
            {
                Id = "1",
                FirstName = "a",
                LastName = "b",
                Gender = (UserGender) 2,
                Dob = DateTime.Now,
                PhoneNumber = "3456"
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}