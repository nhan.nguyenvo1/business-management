﻿using System;
using FluentValidation.TestHelper;
using PROJECT.Application.Features.Tasks.Commands.AssignTask;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Tasks.Commands.AssignTask
{
    public class AssignTaskCommandValidatorTest
    {
        private readonly AssignTaskCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenTitleIsEmpty()
        {
            var model = new AssignTaskCommand
            {
                Title = string.Empty,
                DueDate = DateTime.Now
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Title);
        }

        [Fact]
        public void ShouldHaveErrorWhenTitleIsNull()
        {
            var model = new AssignTaskCommand
            {
                Title = null,
                DueDate = DateTime.Now
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Title);
        }

        [Fact]
        public void ShouldHaveErrorWhenDueDateSmallerThanNow()
        {
            var model = new AssignTaskCommand
            {
                Title = "abc",
                DueDate = DateTime.Now.AddDays(-10)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DueDate);
        }
    }
}