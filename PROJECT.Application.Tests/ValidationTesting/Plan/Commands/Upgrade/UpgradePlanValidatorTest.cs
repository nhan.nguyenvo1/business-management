﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Plans.Commands.Upgrade;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Plan.Commands.Upgrade
{
    public class UpgradePlanValidatorTest
    {
        private readonly UpgradePlanValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenPackageIdIsEmpty()
        {
            var model = new UpgradePlanCommand();
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.PackageId);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new UpgradePlanCommand
            {
                PackageId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}