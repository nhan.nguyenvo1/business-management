﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Roles.Commands.CreateRole;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Roles.Commands.CreateRole
{
    public class CreateRoleCommandValidatorTest
    {
        private readonly CreateRoleCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenNameIsNull()
        {
            var model = new CreateRoleCommand
            {
                Name = null,
                Description = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsEmpty()
        {
            var model = new CreateRoleCommand
            {
                Name = string.Empty,
                Description = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsMoreThan50()
        {
            var model = new CreateRoleCommand
            {
                Name = new string('a', 51),
                Description = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenDescriptionIsNull()
        {
            var model = new CreateRoleCommand
            {
                Name = "a",
                Description = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Description);
        }

        [Fact]
        public void ShouldHaveErrorWhenDescriptionIsEmpty()
        {
            var model = new CreateRoleCommand
            {
                Name = "a",
                Description = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Description);
        }

        [Fact]
        public void ShouldHaveErrorWhenDescriptionIsMoreThan250()
        {
            var model = new CreateRoleCommand
            {
                Name = "a",
                Description = new string('a', 251)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Description);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new CreateRoleCommand
            {
                Name = "a",
                Description = "ac"
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}