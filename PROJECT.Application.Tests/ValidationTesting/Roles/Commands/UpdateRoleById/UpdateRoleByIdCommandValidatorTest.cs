﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Roles.Commands.UpdateRoleById;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Roles.Commands.UpdateRoleById
{
    public class UpdateRoleByIdCommandValidatorTest
    {
        private readonly UpdateRoleByIdCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenNameIsNull()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = null,
                Id = "1",
                Description = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsEmpty()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = string.Empty,
                Id = "1",
                Description = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsMoreThan50()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = new string('a', 51),
                Id = "1",
                Description = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenDescriptionIsEmpty()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = "abc",
                Id = "1",
                Description = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Description);
        }

        [Fact]
        public void ShouldHaveErrorWhenDescriptionIsNull()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = "abc",
                Id = "1",
                Description = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Description);
        }

        [Fact]
        public void ShouldHaveErrorWhenDescriptionIsMoreThan250()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = "abc",
                Id = "1",
                Description = new string('a', 251)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Description);
        }

        [Fact]
        public void ShouldHaveErrorWhenIdIsEmpty()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = "abc",
                Id = string.Empty,
                Description = "bcd"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Id);
        }

        [Fact]
        public void ShouldHaveErrorWhenIdIsNull()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = "abc",
                Id = null,
                Description = "bcd"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Id);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new UpdateRoleByIdCommand
            {
                Name = "abc",
                Id = "1",
                Description = "bcd"
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}