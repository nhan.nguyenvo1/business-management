﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Departments.Commands.UpdateDepartment;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Departments.Commands.UpdateDepartment
{
    public class UpdateDepartmentCommandValidatorTest
    {
        private readonly UpdateDepartmentCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenNameIsEmpty()
        {
            var model = new UpdateDepartmentCommand
            {
                Name = string.Empty,
                Id = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsNull()
        {
            var model = new UpdateDepartmentCommand
            {
                Name = null,
                Id = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsMoreThan100()
        {
            var model = new UpdateDepartmentCommand
            {
                Name = new string('a', 101),
                Id = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenIdIsEmpty()
        {
            var model = new UpdateDepartmentCommand
            {
                Name = "a"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Id);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new UpdateDepartmentCommand
            {
                Name = "abc",
                Id = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}