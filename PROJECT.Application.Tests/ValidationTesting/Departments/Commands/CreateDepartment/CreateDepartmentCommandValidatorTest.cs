﻿using System;
using FluentValidation.TestHelper;
using Moq;
using PROJECT.Application.Features.Departments.Commands.CreateDepartment;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Departments.Commands.CreateDepartment
{
    public class CreateDepartmentCommandValidatorTest
    {
        private CreateDepartmentCommandValidator _validations;

        [Fact]
        public void ShouldHaveErrorWhenDepartmentNotFound()
        {
            var departmentRepository = new Mock<IDepartmentRepository>();
            var employeeRepository = new Mock<IEmployeeRepository>();

            departmentRepository
                .Setup(p => p.ExistAsync(It.IsAny<int>()))
                .ReturnsAsync(false);

            employeeRepository
                .Setup(p => p.GetByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(new AppUser());

            _validations = new CreateDepartmentCommandValidator(departmentRepository.Object, employeeRepository.Object);

            var model = new CreateDepartmentCommand
            {
                Name = "Test",
                DepartmentId = 1,
                SupervisorId = new Guid().ToString()
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DepartmentId);
        }
    }
}