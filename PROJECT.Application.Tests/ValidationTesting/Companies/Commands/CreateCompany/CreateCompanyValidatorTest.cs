﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Companies.Commands.CreateCompany;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Companies.Commands.CreateCompany
{
    public class CreateCompanyValidatorTest
    {
        private readonly CreateCompanyValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenNameIsEmpty()
        {
            var model = new CreateCompanyCommand
            {
                Name = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenNameIsNull()
        {
            var model = new CreateCompanyCommand
            {
                Name = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldHaveErrorWhenNameMoreThan250()
        {
            var model = new CreateCompanyCommand
            {
                Name = new string('a', 251)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Name);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new CreateCompanyCommand
            {
                Name = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}