﻿using System;
using FluentValidation.TestHelper;
using PROJECT.Application.Features.Users.Commands.UpdateProfile;
using PROJECT.Domain.Enums;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Users.UpdateProfile
{
    public class UpdateProfileValidatorTest
    {
        private readonly UpdateProfileValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenPhoneIsNull()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Phone);
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneIsEmpty()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Phone);
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneNotMatchCondition()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = "acb"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Phone);
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneMoreThan16()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 17)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Phone);
        }

        [Fact]
        public void ShouldHaveErrorWhenPhoneLessThan4()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 3)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Phone);
        }

        [Fact]
        public void ShouldHaveErrorWhenFirstNameIsEmpty()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = string.Empty,
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 6)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FirstName);
        }

        [Fact]
        public void ShouldHaveErrorWhenFirstNameIsNull()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = null,
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 6)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FirstName);
        }

        [Fact]
        public void ShouldHaveErrorWhenFirstNameMoreThan100()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = new string('a', 101),
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 6)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FirstName);
        }

        [Fact]
        public void ShouldHaveErrorWhenLastNameIsEmpty()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = string.Empty,
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 6)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.LastName);
        }

        [Fact]
        public void ShouldHaveErrorWhenLastNameIsNull()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = null,
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 6)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.LastName);
        }

        [Fact]
        public void ShouldHaveErrorWhenLastNameMoreThan100()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = new string('a', 101),
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 6)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.LastName);
        }

        [Fact]
        public void ShouldHaveErrorWhenGenderIsNotInEnum()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 4,
                Phone = new string('1', 6)
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Gender);
        }

        [Fact]
        public void ShouldNotHaveErrorModelIsCorrect()
        {
            var model = new UpdateProfileCommand
            {
                FirstName = "a",
                LastName = "b",
                DateOfBirth = DateTime.Now,
                Gender = (UserGender) 2,
                Phone = new string('1', 6)
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}