﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.TrashBins.Commands.Restore;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.TrashBins.Commands.Restore
{
    public class RestoreDeletedDrivesValidatorTest
    {
        private readonly RestoreDeletedDrivesValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenDriveIdsAreEmpty()
        {
            var model = new RestoreDeletedDrivesCommand();
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DriveIds);
        }

        [Fact]
        public void ShouldHaveErrorWhenDriveIdsAreNull()
        {
            var model = new RestoreDeletedDrivesCommand
            {
                DriveIds = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DriveIds);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new RestoreDeletedDrivesCommand
            {
                DriveIds = new int[5]
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}