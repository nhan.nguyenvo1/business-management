﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.TrashBins.Commands.Delete;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.TrashBins.Commands
{
    public class PermanentlyDeleteDrivesValidatorTest
    {
        private readonly PermanentlyDeleteDrivesValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenDriveIdsAreEmpty()
        {
            var model = new PermanentlyDeleteDrivesCommand();
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DriveIds);
        }

        [Fact]
        public void ShouldHaveErrorWhenDriveIdsAreNull()
        {
            var model = new PermanentlyDeleteDrivesCommand
            {
                DriveIds = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DriveIds);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new PermanentlyDeleteDrivesCommand
            {
                DriveIds = new int[5]
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}