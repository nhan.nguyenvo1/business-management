﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Feeds.Commands.CreatePost;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Feeds.Commands.CreatePost
{
    public class CreatePostCommandValidatorTest
    {
        private readonly CreatePostCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenTitleIsEmpty()
        {
            var model = new CreatePostCommand
            {
                Title = string.Empty,
                Description = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Title);
        }

        [Fact]
        public void ShouldHaveErrorWhenTitleIsNull()
        {
            var model = new CreatePostCommand
            {
                Title = null,
                Description = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Title);
        }

        [Fact]
        public void ShouldHaveErrorWhenDescriptionIsNull()
        {
            var model = new CreatePostCommand
            {
                Title = "abc",
                Description = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Description);
        }

        [Fact]
        public void ShouldHaveErrorWhenDescriptionIsEmpty()
        {
            var model = new CreatePostCommand
            {
                Title = "abc",
                Description = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Description);
        }

        [Fact]
        public void ShouldHaveErrorWhenBriefDescriptionIsNull()
        {
            var model = new CreatePostCommand
            {
                Title = "abc",
                Description = "des"
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.BriefDescription);
        }

        [Fact]
        public void ShouldHaveErrorWhenBriefDescriptionIsEmpty()
        {
            var model = new CreatePostCommand
            {
                Title = "abc",
                Description = "des",
                BriefDescription = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.BriefDescription);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new CreatePostCommand
            {
                Title = "abc",
                Description = "def",
                BriefDescription = "Brief"
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}