﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.Feeds.Commands.ApprovePost;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.Feeds.Commands.ApprovePost
{
    public class ApprovePostCommandValidatorTest
    {
        private readonly ApprovePostCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenPostIdIsEmpty()
        {
            var model = new ApprovePostCommand();
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.PostId);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new ApprovePostCommand
            {
                PostId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}