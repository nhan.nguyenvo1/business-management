﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.CompanyDrives.Commands.Move;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.CompanyDrives.Commands.Move
{
    public class CompanyDriveMoveCommandValidatorTest
    {
        private readonly CompanyDriveMoveCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenDriveIdIsEmpty()
        {
            var model = new CompanyDriveMoveCommand
            {
                DestinationId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DriveId);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new CompanyDriveMoveCommand
            {
                DriveId = 2,
                DestinationId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}