﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.CompanyDrives.Commands.CreateFolder;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.CompanyDrives.Commands.CreateFolder
{
    public class CreateCompanyFolderValidatorTest
    {
        private readonly CreateCompanyFolderValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenFolderNameIsEmpty()
        {
            var model = new CreateCompanyFolderCommand
            {
                FolderName = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FolderName);
        }

        [Fact]
        public void ShouldHaveErrorWhenFolderNameIsNull()
        {
            var model = new CreateCompanyFolderCommand
            {
                FolderName = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.FolderName);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new CreateCompanyFolderCommand
            {
                FolderName = "a"
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}