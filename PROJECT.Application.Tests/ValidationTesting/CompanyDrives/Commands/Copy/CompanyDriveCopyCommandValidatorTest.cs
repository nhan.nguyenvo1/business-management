﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.CompanyDrives.Commands.Copy;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.CompanyDrives.Commands.Copy
{
    public class CompanyDriveCopyCommandValidatorTest
    {
        private readonly CompanyDriveCopyCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenDriveIdIsEmpty()
        {
            var model = new CompanyDriveCopyCommand
            {
                DestinationId = 3
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.DriveId);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new CompanyDriveCopyCommand
            {
                DriveId = 1,
                DestinationId = 2
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}