﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.CompanyDrives.Commands.CreateFiles;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.CompanyDrives.Commands.CreateFiles
{
    public class CreateCompanyFilesValidatorTest
    {
        private readonly CreateCompanyFilesValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenFilesAreEmpty()
        {
            var model = new CreateCompanyFilesCommand
            {
                FolderId = 1
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Files);
        }
    }
}