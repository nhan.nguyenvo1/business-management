﻿using FluentValidation.TestHelper;
using PROJECT.Application.Features.UserTimes.Commands.AddSummary;
using Xunit;

namespace PROJECT.Application.Tests.ValidationTesting.UserTimes.Commands.AddSummary
{
    public class AddSummaryCommandValidatorTest
    {
        private readonly AddSummaryCommandValidator _validations = new();

        [Fact]
        public void ShouldHaveErrorWhenSummaryIsEmpty()
        {
            var model = new AddSummaryCommand
            {
                Summary = string.Empty
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Summary);
        }

        [Fact]
        public void ShouldHaveErrorWhenSummaryIsNull()
        {
            var model = new AddSummaryCommand
            {
                Summary = null
            };
            var result = _validations.TestValidate(model);
            result.ShouldHaveValidationErrorFor(p => p.Summary);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelIsCorrect()
        {
            var model = new AddSummaryCommand
            {
                Summary = "abc"
            };
            var result = _validations.TestValidate(model);
            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}