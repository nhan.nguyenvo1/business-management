﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class NotificationConfiguration : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.ToTable("Notification");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.Category)
                .IsRequired();

            builder.Property(p => p.Title)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(p => p.Description)
                .IsRequired()
                .HasMaxLength(600);

            builder.Property(p => p.Url)
                .IsRequired()
                .HasMaxLength(500);

            builder.HasMany(n => n.Users)
                .WithMany(u => u.Notifications)
                .UsingEntity<UserNotification>(
                    u => u.HasOne(nu => nu.User).WithMany().HasForeignKey(nu => nu.UserId).IsRequired(false),
                    n => n.HasOne(nu => nu.Notification).WithMany().HasForeignKey(nu => nu.NotificationId).IsRequired(false)
                );
        }
    }
}