﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class CompanyDriveConfiguration : IEntityTypeConfiguration<CompanyDrive>
    {
        public void Configure(EntityTypeBuilder<CompanyDrive> builder)
        {
            builder.ToTable("CompanyDrive");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.Shared).IsRequired().HasDefaultValue(false);

            builder.Property(p => p.StorageId).IsRequired();

            builder.Property(p => p.Deleted).HasDefaultValue(false);

            builder.Property(p => p.SharedLink)
                .HasMaxLength(500);

            builder.Property(p => p.CreatedDate);
            builder.Property(p => p.UpdatedDate);


            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(p => p.FileType)
                .IsRequired()
                .HasDefaultValue(FileType.Folder);

            builder.Property(p => p.Path)
                .IsRequired()
                .HasMaxLength(500);

            builder.HasOne(p => p.Parent)
                .WithMany(p => p.Children)
                .HasForeignKey(p => p.FileId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasOne(f => f.Created)
                .WithMany()
                .HasForeignKey(f => f.CreatedBy);

            builder.HasOne(f => f.Updated)
                .WithMany()
                .HasForeignKey(f => f.UpdatedBy);

            builder.HasOne(f => f.AppStorage)
                .WithMany(s => s.CompanyDrives)
                .HasForeignKey(f => f.StorageId);
        }
    }
}