﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class UserWorkConfiguration : IEntityTypeConfiguration<UserWork>
    {
        public void Configure(EntityTypeBuilder<UserWork> builder)
        {
            builder.ToTable("UserWork");

            builder.HasKey(e => new {e.UserId, e.WorkId});

            builder.Property(e => e.LastModified).IsRequired();

            builder.Property(e => e.Status).IsRequired().HasDefaultValue(UserWorkStatus.NoAttempt);

            builder.Property(e => e.Attachment).HasMaxLength(250);

            builder
                .HasOne(e => e.AppUser)
                .WithMany(e => e.UserWorks)
                .HasForeignKey(e => e.UserId)
                .OnDelete(DeleteBehavior.NoAction)
                .IsRequired(false);

            builder
                .HasOne(e => e.WorkSchedule)
                .WithMany(e => e.UserWorks)
                .HasForeignKey(e => e.WorkId)
                .OnDelete(DeleteBehavior.NoAction)
                .IsRequired(false);
        }
    }
}