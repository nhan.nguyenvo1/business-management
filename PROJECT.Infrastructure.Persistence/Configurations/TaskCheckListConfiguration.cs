﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class TaskCheckListConfiguration : IEntityTypeConfiguration<TaskCheckList>
    {
        public void Configure(EntityTypeBuilder<TaskCheckList> builder)
        {
            builder.ToTable("TaskCheckList");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.Title)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(e => e.Checked)
                .IsRequired()
                .HasDefaultValue(false);

            builder.Property(e => e.TaskId)
                .IsRequired();

            builder.HasIndex(e => e.Title);

            builder.HasIndex(e => e.Checked);
        }
    }
}