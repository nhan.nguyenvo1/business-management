﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class UserTaskConfiguration : IEntityTypeConfiguration<UserTask>
    {
        public void Configure(EntityTypeBuilder<UserTask> builder)
        {
            builder.ToTable("UserTask");

            builder.HasKey(e => new { e.UserId, e.TaskId });
        }
    }
}
