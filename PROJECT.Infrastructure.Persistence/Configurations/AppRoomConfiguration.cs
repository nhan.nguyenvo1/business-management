﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Constants;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class AppRoomConfiguration : IEntityTypeConfiguration<AppRoom>
    {
        public void Configure(EntityTypeBuilder<AppRoom> builder)
        {
            builder.ToTable("ChatRoom");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.Name).IsRequired().HasMaxLength(200);

            builder.Property(p => p.AvatarUrl).IsRequired().HasMaxLength(600)
                .HasDefaultValue(SystemConstant.DefaultRoomChatAvatar);

            builder.Property(p => p.AvatarPublicId).IsRequired().HasMaxLength(200)
                .HasDefaultValue("Room/57fb3190d0cc1726d782c4e25e8561e9_vj6neb.png");

            builder.Property(p => p.RoomType).IsRequired().HasDefaultValue(RoomType.Public);

            builder.Property(p => p.CreatedDate);

            builder.Property(p => p.UpdatedDate);

            builder.HasMany(r => r.ReadUsers)
                .WithMany(u => u.ReadRooms)
                .UsingEntity(j => j.ToTable("ReadUser"));

            builder.HasMany(r => r.RoomUsers)
                .WithMany(u => u.UserRooms)
                .UsingEntity(j => j.ToTable("RoomUser"));

            builder.HasMany(r => r.BannedUsers)
                .WithMany(u => u.BannedRooms)
                .UsingEntity(j => j.ToTable("BannedRoom"));

            builder.HasOne(r => r.Created)
                .WithMany()
                .HasForeignKey(r => r.CreatedBy);

            builder.HasOne(r => r.Updated)
                .WithMany()
                .HasForeignKey(r => r.UpdatedBy);
        }
    }
}