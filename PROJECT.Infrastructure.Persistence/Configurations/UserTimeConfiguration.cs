﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class UserTimeConfiguration : IEntityTypeConfiguration<UserTime>
    {
        public void Configure(EntityTypeBuilder<UserTime> builder)
        {
            builder.ToTable("UserTime");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.UserId).IsRequired();

            builder.Property(e => e.Date).IsRequired();

            builder.Property(e => e.CheckIn).IsRequired(false);

            builder.Property(e => e.CheckOut).IsRequired(false);

            builder
                .HasOne(e => e.AppUser)
                .WithMany(e => e.UserTimes)
                .HasForeignKey(e => e.UserId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}