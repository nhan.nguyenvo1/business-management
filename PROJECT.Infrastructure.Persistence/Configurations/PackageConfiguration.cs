﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class PackageConfiguration : IEntityTypeConfiguration<Package>
    {
        public void Configure(EntityTypeBuilder<Package> builder)
        {
            builder.ToTable("Package");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.NumOfEmployee).IsRequired().HasDefaultValue(50);

            builder.Property(e => e.SizeOfDrive).IsRequired().HasDefaultValue(5 * 1024);

            builder.Property(e => e.Sale).IsRequired().HasDefaultValue(0);

            builder.Property(e => e.Cost).IsRequired().HasDefaultValue(0);

            builder.Property(e => e.IsDefault).IsRequired().HasDefaultValue(false);

            builder.Property(e => e.Deleted).IsRequired().HasDefaultValue(false);
        }
    }
}