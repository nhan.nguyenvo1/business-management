﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using static Microsoft.EntityFrameworkCore.DeleteBehavior;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class AppTaskConfiguration : IEntityTypeConfiguration<AppTask>
    {
        public void Configure(EntityTypeBuilder<AppTask> builder)
        {
            builder.ToTable("Task");

            builder.HasKey(p => p.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.DueDate).IsRequired(false);

            builder.Property(e => e.Title).IsRequired().HasMaxLength(100);

            builder.Property(e => e.Completed).IsRequired().HasDefaultValue(false);

            builder.Property(e => e.Description).HasColumnType("ntext");

            builder.Property(e => e.CreatedDate);

            builder.Property(e => e.UpdatedDate);

            builder.HasIndex(e => e.DueDate);

            builder.HasIndex(e => e.Title);

            builder
                .HasOne(e => e.Created)
                .WithMany()
                .HasForeignKey(e => e.CreatedBy)
                .OnDelete(NoAction);

            builder
                .HasOne(e => e.Updated)
                .WithMany()
                .HasForeignKey(e => e.UpdatedBy)
                .OnDelete(NoAction);

            builder.HasMany(e => e.Attachments)
                .WithOne(e => e.Task)
                .HasForeignKey(e => e.TaskId);

            builder.HasMany(e => e.CheckLists)
                .WithOne(e => e.Task)
                .HasForeignKey(e => e.TaskId);

            builder.HasMany(t => t.TaskUsers)
                .WithMany(u => u.UserTasks)
                .UsingEntity<UserTask>(
                    u => u.HasOne(tu => tu.AppUser).WithMany().HasForeignKey(tu => tu.UserId).OnDelete(Cascade).IsRequired(false),
                    t => t.HasOne(tu => tu.AppTask).WithMany().HasForeignKey(tu => tu.TaskId).OnDelete(Cascade).IsRequired(false)
                );
        }
    }
}