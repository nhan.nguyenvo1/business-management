﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class AppChatConfiguration : IEntityTypeConfiguration<AppChat>
    {
        public void Configure(EntityTypeBuilder<AppChat> builder)
        {
            builder.ToTable("Chat");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.RoomId).IsRequired();

            builder.Property(p => p.SenderId).IsRequired(false);

            builder.Property(p => p.Content).HasMaxLength(500);

            builder.Property(p => p.When);

            builder.Property(p => p.MessageType).HasDefaultValue(MessageType.Content).IsRequired();

            builder.HasOne(p => p.Sender)
                .WithMany()
                .HasForeignKey(p => p.SenderId)
                .IsRequired(false);

            builder.HasOne(p => p.Room)
                .WithMany(p => p.AppChats)
                .HasForeignKey(p => p.RoomId);
        }
    }
}