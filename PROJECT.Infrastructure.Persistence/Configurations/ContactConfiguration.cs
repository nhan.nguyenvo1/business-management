﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class ContactConfiguration : IEntityTypeConfiguration<Contact>
    {
        public void Configure(EntityTypeBuilder<Contact> builder)
        {
            builder.ToTable("Contact");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.Email).IsRequired().HasMaxLength(60);

            builder.Property(p => p.Address).IsRequired().HasMaxLength(200);

            builder.Property(p => p.MoreDescription).IsRequired().HasMaxLength(600);

            builder.Property(p => p.PhoneNumber).IsRequired().HasMaxLength(20);

            builder.Property(p => p.IsDefault).IsRequired().HasDefaultValue(false);
        }
    }
}
