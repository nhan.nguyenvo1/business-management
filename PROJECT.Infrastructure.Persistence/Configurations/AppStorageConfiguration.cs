﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class AppStorageConfiguration : IEntityTypeConfiguration<AppStorage>
    {
        public void Configure(EntityTypeBuilder<AppStorage> builder)
        {
            builder.ToTable("Storage");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.StorageType).IsRequired().HasDefaultValue(StorageType.PersonalStorage);

            builder.Property(p => p.Limit).IsRequired().HasDefaultValue(500);

            builder.Property(p => p.UserId).IsRequired(false);

            builder.Property(p => p.Path).IsRequired().HasMaxLength(500);

            builder.HasOne(s => s.User)
                .WithMany(u => u.Storages)
                .HasForeignKey(s => s.UserId);
        }
    }
}