﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class PackageRequestConfiguration : IEntityTypeConfiguration<PackageRequest>
    {
        public void Configure(EntityTypeBuilder<PackageRequest> builder)
        {
            builder.ToTable("PackageRequest");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.Status).IsRequired().HasDefaultValue(RequestStatus.WaitingForPayment);

            builder.HasAlternateKey(p => new { p.UserId, p.PackageId });

            builder.HasOne(p => p.Package)
                .WithMany()
                .HasForeignKey(p => p.PackageId)
                .IsRequired(false);

            builder.HasOne(p => p.User)
                .WithMany()
                .HasForeignKey(p => p.UserId)
                .IsRequired(false);
        }
    }
}