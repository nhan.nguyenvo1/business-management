﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class BookingRoomConfiguration : IEntityTypeConfiguration<BookingRoom>
    {
        public void Configure(EntityTypeBuilder<BookingRoom> builder)
        {
            builder.ToTable("BookingRoom");

            builder.HasKey(e => e.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.Date).IsRequired();

            builder.Property(p => p.From).IsRequired();

            builder.Property(p => p.Room).HasMaxLength(200);

            builder.Property(p => p.Title).IsRequired().HasMaxLength(200);

            builder.Property(p => p.Description).HasColumnType("ntext");

            builder.Property(p => p.Type).HasDefaultValue(BookingType.PeerToPeer);

            builder.Property(p => p.Url).HasMaxLength(600);

            builder.HasOne(p => p.Created)
                .WithMany()
                .HasForeignKey(p => p.CreatedBy);

            builder.HasOne(p => p.Updated)
                .WithMany()
                .HasForeignKey(p => p.UpdatedBy);

            builder.HasMany(p => p.AppUsers)
                .WithMany(u => u.BookingRooms)
                .UsingEntity<UserRoom>(
                u => u.HasOne(r => r.AppUser).WithMany().HasForeignKey(r => r.UserId).OnDelete(DeleteBehavior.Cascade).IsRequired(false),
                r => r.HasOne(u => u.AppRoom).WithMany().HasForeignKey(u => u.RoomId).OnDelete(DeleteBehavior.Cascade).IsRequired(false));
        }
    }
}