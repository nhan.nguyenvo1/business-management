﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class PaymentInfoConfiguration : IEntityTypeConfiguration<PaymentInfo>
    {
        public void Configure(EntityTypeBuilder<PaymentInfo> builder)
        {
            builder.ToTable("PaymentInfo");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.CardHolder).IsRequired().HasMaxLength(200);

            builder.Property(p => p.CardBank).IsRequired().HasMaxLength(200);

            builder.Property(p => p.CardNumber).IsRequired().HasMaxLength(200);

            builder.Property(p => p.IsDefault).IsRequired();
        }
    }
}
