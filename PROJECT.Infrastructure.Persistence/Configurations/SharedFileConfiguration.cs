﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class SharedFileConfiguration : IEntityTypeConfiguration<SharedFile>
    {
        public void Configure(EntityTypeBuilder<SharedFile> builder)
        {
            builder.ToTable("SharedFile");

            builder.HasKey(p => new {p.FileId, p.UserId});

            builder.Property(p => p.CanRead).IsRequired().HasDefaultValue(true);

            builder.Property(p => p.CanWrite).IsRequired().HasDefaultValue(false);

            builder.Property(p => p.CanAdd).IsRequired().HasDefaultValue(false);

            builder.Property(p => p.Root).IsRequired().HasDefaultValue(false);
        }
    }
}