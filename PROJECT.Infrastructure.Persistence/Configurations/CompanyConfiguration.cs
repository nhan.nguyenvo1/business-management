﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.ToTable("Company");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id).UseIdentityColumn();

            builder.Property(p => p.Name).IsRequired().HasMaxLength(250);

            builder.Property(p => p.Status).HasDefaultValue(CompanyStatus.Activated);

            builder.Property(p => p.ResonForPending).HasMaxLength(600);

            builder.HasOne(p => p.Created)
                .WithMany()
                .HasForeignKey(p => p.CreatedBy);

            builder.HasOne(p => p.Updated)
                .WithMany()
                .HasForeignKey(p => p.UpdatedBy);

            builder.HasMany(p => p.Users)
                .WithMany(p => p.Companies)
                .UsingEntity<UserCompany>(
                    x => x.HasOne(u => u.User).WithMany().HasForeignKey(c => c.UserId).IsRequired(false),
                    x => x.HasOne(u => u.Company).WithMany().HasForeignKey(c => c.CompanyId).IsRequired(false));
        }
    }
}