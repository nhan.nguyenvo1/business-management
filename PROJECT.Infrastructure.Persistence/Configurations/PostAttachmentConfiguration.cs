﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class PostAttachmentConfiguration : IEntityTypeConfiguration<PostAttachment>
    {
        public void Configure(EntityTypeBuilder<PostAttachment> builder)
        {
            builder.ToTable("PostAttachment");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.Url)
                .IsRequired()
                .HasMaxLength(600);

            builder.Property(e => e.Url)
                .IsRequired()
                .HasMaxLength(200);

            builder.HasOne(e => e.Post)
                .WithMany(e => e.PostAttachments)
                .HasForeignKey(e => e.PostId);
        }
    }
}