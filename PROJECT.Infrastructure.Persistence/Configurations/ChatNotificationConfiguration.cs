﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class ChatNotificationConfiguration : IEntityTypeConfiguration<ChatNotification>
    {
        public void Configure(EntityTypeBuilder<ChatNotification> builder)
        {
            builder.ToTable("ChatNotification");

            builder.HasKey(p => p.Id);

            builder.Property(p => p.Id)
                .UseIdentityColumn();

            builder.Property(p => p.Content)
                .HasMaxLength(500)
                .IsRequired();

            builder.Property(p => p.CreatedDate);
            builder.Property(p => p.UpdatedDate);

            builder.HasMany(c => c.RelatedUser)
                .WithMany(u => u.ChatNotifications)
                .UsingEntity(j => j.ToTable("UserChatNotification"));
        }
    }
}