﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class DepartmentConfiguration : IEntityTypeConfiguration<Department>
    {
        public void Configure(EntityTypeBuilder<Department> builder)
        {
            builder.ToTable("Department");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.Name).IsRequired().HasMaxLength(100);

            builder.Property(e => e.Url).IsRequired().HasMaxLength(250);

            builder.Property(e => e.CreatedDate);

            builder.Property(e => e.UpdatedDate);

            builder.HasIndex(e => e.Name);
            builder.HasIndex(e => e.CreatedDate);

            builder
                .HasOne(e => e.Created)
                .WithMany()
                .HasForeignKey(e => e.CreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(e => e.Updated)
                .WithMany()
                .HasForeignKey(e => e.UpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(e => e.Parent)
                .WithMany(e => e.Children)
                .HasForeignKey(e => e.DepartmentId)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(e => e.Supervisor)
                .WithMany()
                .HasForeignKey(e => e.SupervisorId)
                .OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(e => e.Users)
                .WithMany(e => e.Departments)
                .UsingEntity(t => t.ToTable("DepartmentUsers"));
        }
    }
}