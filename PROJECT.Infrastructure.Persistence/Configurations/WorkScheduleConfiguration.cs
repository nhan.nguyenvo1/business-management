﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class WorkScheduleConfiguration : IEntityTypeConfiguration<WorkSchedule>
    {
        public void Configure(EntityTypeBuilder<WorkSchedule> builder)
        {
            builder.ToTable("WorkSchedule");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.Title).IsRequired().HasMaxLength(100);

            builder.Property(e => e.Description).HasColumnType("ntext");

            builder.Property(e => e.Attachment).HasMaxLength(250);

            builder.Property(e => e.CreatedDate).IsRequired()
                .HasDefaultValue(DateTime.UtcNow);

            builder.Property(e => e.UpdatedDate).IsRequired()
                .HasDefaultValue(DateTime.UtcNow);

            builder.Property(e => e.CreatedBy).IsRequired();

            builder.Property(e => e.UpdatedBy).IsRequired();

            builder
                .HasOne(e => e.Created)
                .WithMany()
                .HasForeignKey(e => e.CreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(e => e.Updated)
                .WithMany()
                .HasForeignKey(e => e.UpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}