﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<AppUser>
    {
        public void Configure(EntityTypeBuilder<AppUser> builder)
        {
            builder.ToTable("User");

            builder.Property(e => e.Id).HasMaxLength(50);

            builder.Property(e => e.AvatarUrl)
                .HasMaxLength(500)
                .HasDefaultValue("https://res.cloudinary.com/dddzstbl8/image/upload/v1615203221/AppUser/default.jpg");

            builder.Property(e => e.AvatarPublicId)
                .HasMaxLength(250)
                .HasDefaultValue("AppUser/default.jpg");

            builder.Property(e => e.FirstName).IsRequired().HasMaxLength(100);

            builder.Property(e => e.LastName).IsRequired().HasMaxLength(100);

            builder.Property(e => e.Gender).IsRequired();

            builder.Property(e => e.Dob).IsRequired();

            builder.Property(e => e.FirstLogin).IsRequired().HasDefaultValue(true);

            builder.Property(e => e.IpAddress).HasMaxLength(16);

            builder.Property(e => e.Status).IsRequired().HasDefaultValue(UserStatus.Offline);

            builder.Property(e => e.CreatedDate).IsRequired()
                .HasDefaultValue(DateTime.UtcNow);

            builder.Property(e => e.UpdatedDate).IsRequired()
                .HasDefaultValue(DateTime.UtcNow);

            builder.Property(e => e.PackageId).IsRequired(false);

            builder.HasIndex(e => e.FirstName);

            builder.HasIndex(e => e.LastName);

            builder.HasIndex(e => e.Gender);

            builder.HasIndex(e => e.UserName);

            builder.HasIndex(e => e.Dob);

            builder.HasIndex(e => e.CreatedDate);

            builder.HasOne(e => e.Package)
                .WithMany(e => e.Users)
                .HasForeignKey(e => e.PackageId);

            builder
                .HasOne(e => e.Created)
                .WithMany()
                .HasForeignKey(e => e.CreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(e => e.Updated)
                .WithMany()
                .HasForeignKey(e => e.UpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}