﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class PostConfiguration : IEntityTypeConfiguration<Post>
    {
        public void Configure(EntityTypeBuilder<Post> builder)
        {
            builder.ToTable("Post");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id).UseIdentityColumn();

            builder.Property(e => e.Title).IsRequired().HasMaxLength(250);

            builder.Property(e => e.BriefDescription).IsRequired().HasMaxLength(250);

            builder.Property(e => e.Description).IsRequired().HasColumnType("ntext");

            builder.Property(e => e.Status).IsRequired().HasDefaultValue(PostStatus.Pending);

            builder.Property(e => e.CreatedDate).IsRequired();

            builder.Property(e => e.UpdatedDate).IsRequired();

            builder.Property(e => e.CreatedBy).IsRequired();

            builder.Property(e => e.UpdatedBy).IsRequired();

            builder.HasIndex(e => e.Title);

            builder.HasIndex(e => e.Status);

            builder.HasIndex(e => e.CreatedDate);

            builder.HasIndex(e => e.UpdatedDate);

            builder
                .HasOne(e => e.Created)
                .WithMany()
                .HasForeignKey(e => e.CreatedBy)
                .OnDelete(DeleteBehavior.NoAction);

            builder
                .HasOne(e => e.ApprovedUser)
                .WithMany()
                .HasForeignKey(e => e.ApprovedBy);

            builder
                .HasOne(e => e.Updated)
                .WithMany()
                .HasForeignKey(e => e.UpdatedBy)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}