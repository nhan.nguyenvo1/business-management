﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PROJECT.Domain.Entities;

namespace PROJECT.Infrastructure.Persistence.Configurations
{
    public class TaskAttachmentConfiguration : IEntityTypeConfiguration<TaskAttachment>
    {
        public void Configure(EntityTypeBuilder<TaskAttachment> builder)
        {
            builder.ToTable("TaskAttachment");

            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .UseIdentityColumn();

            builder.Property(e => e.Url)
                .IsRequired()
                .HasMaxLength(600);

            builder.Property(e => e.FileName)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(e => e.PublicId)
                .IsRequired()
                .HasMaxLength(250);

            builder.Property(e => e.TaskId)
                .IsRequired();
        }
    }
}