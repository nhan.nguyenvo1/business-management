﻿using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;
using PROJECT.Infrastructure.Persistence.Identity.Services;
using PROJECT.Infrastructure.Persistence.Repositories;

namespace PROJECT.Infrastructure.Persistence
{
    public static class ServiceExtension
    {
        public static void AddPersistenceInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("UseInMemoryDatabase"))
                services.AddDbContext<ProjectDbContext>(options =>
                    options.UseInMemoryDatabase("8sqRx5yHO8"));
            else
                services.AddDbContext<ProjectDbContext>(options =>
                {
                    options
                        .UseSqlServer(
                            configuration.GetConnectionString("ProjectDbConnectionString"),
                            b => b.MigrationsAssembly(typeof(ProjectDbContext).Assembly.FullName))
                        .UseLazyLoadingProxies();
                });

            #region Config Identity

            services.AddIdentity<AppUser, AppRole>(options =>
                {
                    options.SignIn.RequireConfirmedEmail = true;
                    options.Password.RequireDigit = options.Password.RequireLowercase =
                        options.Password.RequireUppercase = options.Password.RequireNonAlphanumeric = true;
                    options.User.RequireUniqueEmail = true;
                    options.Lockout.MaxFailedAccessAttempts = 5;
                })
                .AddEntityFrameworkStores<ProjectDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<UserManager<AppUser>>();
            services.AddTransient<RoleManager<AppRole>>();
            services.AddTransient<SignInManager<AppUser>>();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o =>
                {
                    o.RequireHttpsMetadata = false;
                    o.SaveToken = false;
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        ValidIssuer = configuration["JwtSetting:Issuer"],
                        ValidAudience = configuration["JwtSetting:Audience"],
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtSetting:Key"]))
                    };
                    //o.Events = new JwtBearerEvents
                    //{
                    //    OnAuthenticationFailed = context =>
                    //    {
                    //        context.Response.StatusCode = 401;
                    //        context.Response.ContentType = "application/json";
                    //        var result = JsonConvert.SerializeObject(new Response<string>("Token is expired"));
                    //        return context.Response.WriteAsync(result);
                    //    },
                    //    OnChallenge = context =>
                    //    {
                    //        context.HandleResponse();
                    //        var result = JsonConvert.SerializeObject(new Response<string>("You are not authorized"));
                    //        return context.Response.WriteAsync(result);
                    //    },
                    //    OnForbidden = context =>
                    //    {
                    //        context.Response.StatusCode = 403;
                    //        context.Response.ContentType = "application/json";
                    //        var result =
                    //            JsonConvert.SerializeObject(
                    //                new Response<string>("You are not authorized to access this resource"));
                    //        return context.Response.WriteAsync(result);
                    //    }
                    //};
                });

            #endregion

            #region Dependency Injection

            services.AddTransient<IAuthService, AuthService>();

            #endregion

            #region Repositories

            services.AddTransient<IUserTaskRepository, UserTaskRepository>();
            services.AddTransient<IPaymentInfoRepository, PaymentInfoRepository>();
            services.AddTransient<IActivityRepository, ActivityRepository>();
            services.AddTransient<IContactRepository, ContactRepository>();
            services.AddTransient<INotificationRepository, NotificationRepository>();
            services.AddTransient<IBookingRoomRepository, BookingRoomRepository>();
            services.AddTransient(typeof(IGenericRepository<,>), typeof(GenericRepository<,>));
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient(typeof(IUnitOfWork), typeof(UnitOfWork.UnitOfWork));
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
            services.AddTransient<IRoleRepository, RoleRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPostRepository, PostRepository>();
            services.AddTransient<IPostAttachmentRepository, PostAttachmentRepository>();
            services.AddTransient<IAppTaskRepository, AppTaskRepository>();
            services.AddTransient<ITaskCheckListRepository, TaskCheckListRepository>();
            services.AddTransient<ITaskAttachmentRepository, TaskAttachmentRepository>();
            services.AddTransient<IUserTimeRepository, UserTimeRepository>();
            services.AddTransient<IAppChatRepository, AppChatRepository>();
            services.AddTransient<IAppChatRoomRepository, AppChatRoomRepository>();
            services.AddTransient<IAppFileRepository, AppFileRepository>();
            services.AddTransient<IStorageRepository, StorageRepository>();
            services.AddTransient<ICompanyRepository, CompanyRepository>();
            services.AddTransient<ICompanyDriveRepository, CompanyDriveRepository>();
            services.AddTransient<IPackageRequestRepository, PackageRequestRepository>();
            services.AddTransient<IPackageRepository, PackageRepository>();

            #endregion
        }
    }
}