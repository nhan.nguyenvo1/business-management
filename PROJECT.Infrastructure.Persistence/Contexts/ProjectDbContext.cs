﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Interfaces;
using PROJECT.Infrastructure.Persistence.Configurations;
using PROJECT.Infrastructure.Persistence.Extensions;

namespace PROJECT.Infrastructure.Persistence.Contexts
{
    public sealed class ProjectDbContext : IdentityDbContext<AppUser, AppRole, string>
    {
        private readonly IAuthUserService _authUserService;
        private readonly ICompanyService _companyService;
        private readonly IDateTimeService _dateTimeService;

        public ProjectDbContext(DbContextOptions<ProjectDbContext> options, IDateTimeService dateTimeService,
            IAuthUserService authUserService, ICompanyService companyService) :
            base(options)
        {
            _dateTimeService = dateTimeService;
            _authUserService = authUserService;
            _companyService = companyService;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
        {
            foreach (var entry in ChangeTracker.Entries<IHasOwner<AppUser, string>>())
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = entry.Entity.UpdatedBy = _authUserService.UserId;
                        break;
                    case EntityState.Modified:
                        entry.Entity.UpdatedBy = _authUserService.UserId;
                        break;
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

            foreach (var entry in ChangeTracker.Entries<IDateTimeTracking>())
                switch (entry.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        break;
                    case EntityState.Modified:
                        entry.Entity.UpdatedDate = _dateTimeService.UtcNow;
                        break;
                    case EntityState.Added:
                        entry.Entity.CreatedDate = entry.Entity.UpdatedDate = _dateTimeService.UtcNow;
                        break;
                }

            foreach (var entry in ChangeTracker.Entries<ISoftDelete>())
                switch (entry.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        if (!entry.Entity.Deleted)
                        {
                            entry.Entity.Deleted = true;
                            entry.State = EntityState.Modified;
                        }

                        break;
                    case EntityState.Modified:
                        break;
                    case EntityState.Added:
                        entry.Entity.Deleted = false;
                        break;
                }

            foreach (var entry in ChangeTracker.Entries<IHasCompany>())
                switch (entry.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        break;
                    case EntityState.Modified:
                        break;
                    case EntityState.Added:
                        if (entry.Entity.CompanyId == 0) entry.Entity.CompanyId = _companyService.CompanyId.Value;
                        break;
                }

            foreach (var entry in ChangeTracker.Entries<AppChat>())
                switch (entry.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        break;
                    case EntityState.Modified:
                        break;
                    case EntityState.Added:
                        entry.Entity.When = _dateTimeService.UtcNow;
                        break;
                }

            foreach (var enty in ChangeTracker.Entries<History>())
                switch (enty.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        break;
                    case EntityState.Modified:
                        break;
                    case EntityState.Added:
                        enty.Entity.CreatedDate = _dateTimeService.UtcNow;
                        enty.Entity.CreatedBy = _authUserService.UserId;
                        break;
                }

            foreach (var enty in ChangeTracker.Entries<Notification>())
                switch (enty.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        break;
                    case EntityState.Modified:
                        break;
                    case EntityState.Added:
                        enty.Entity.CreatedDate = _dateTimeService.UtcNow;
                        break;
                }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            foreach (var property in builder.Model.GetEntityTypes().SelectMany(e => e.GetProperties())
                .Where(p => p.ClrType == typeof(decimal) || p.ClrType == typeof(decimal?)))
                property.SetColumnType("decimal(18, 6)");

            #region Identity Configuration

            builder.ApplyConfiguration(new UserConfiguration());

            builder.ApplyConfiguration(new RoleConfiguration());

            builder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.ToTable("UserRole");
                entity.HasKey(e => new {e.UserId, e.RoleId});
            });

            builder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.ToTable("UserClaim");
                entity.HasKey(e => e.Id);
            });

            builder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.ToTable("UserLogin");
                entity.HasKey(e => e.UserId);
            });

            builder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.ToTable("RoleClaim");
                entity.HasKey(e => e.Id);
            });

            builder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.ToTable("UserToken");
                entity.HasKey(e => e.UserId);
            });

            #endregion

            #region Persistence Configuration

            builder.ApplyConfiguration(new UserRoomConfiguration());

            builder.ApplyConfiguration(new UserTaskConfiguration());

            builder.ApplyConfiguration(new PaymentInfoConfiguration());

            builder.ApplyConfiguration(new ActivityConfiguration());

            builder.ApplyConfiguration(new ContactConfiguration());

            builder.ApplyConfiguration(new UserNotificationConfiguration());

            builder.ApplyConfiguration(new BookingRoomConfiguration());

            builder.ApplyConfiguration(new PackageRequestConfiguration());

            builder.ApplyConfiguration(new PackageConfiguration());

            builder.ApplyConfiguration(new CompanyDriveConfiguration());

            builder.ApplyConfiguration(new UserCompanyConfiguration());

            builder.ApplyConfiguration(new CompanyConfiguration());

            builder.ApplyConfiguration(new AppStorageConfiguration());

            builder.ApplyConfiguration(new AppFileConfiguration());

            builder.ApplyConfiguration(new AppFileConfiguration());

            builder.ApplyConfiguration(new ChatNotificationConfiguration());

            builder.ApplyConfiguration(new AppChatConfiguration());

            builder.ApplyConfiguration(new PostConfiguration());

            builder.ApplyConfiguration(new DepartmentConfiguration());

            builder.ApplyConfiguration(new HistoryConfiguration());

            builder.ApplyConfiguration(new UserTimeConfiguration());

            builder.ApplyConfiguration(new UserWorkConfiguration());

            builder.ApplyConfiguration(new WorkScheduleConfiguration());

            builder.ApplyConfiguration(new NotificationConfiguration());

            builder.ApplyConfiguration(new AppTaskConfiguration());

            builder.ApplyConfiguration(new TaskAttachmentConfiguration());

            builder.ApplyConfiguration(new TaskCheckListConfiguration());

            builder.ApplyConfiguration(new AppRoomConfiguration());

            #endregion

            builder.ApplyGlobalFilters<IHasCompany>(p =>
                !_companyService.CompanyId.HasValue || _companyService.CompanyId == p.CompanyId);

            builder.ApplyGlobalFilters<ISoftDelete>(p => !p.Deleted);

            builder.Entity<AppUser>().HasQueryFilter(user =>
                !_companyService.CompanyId.HasValue || user.Companies.Any(u => u.Id == _companyService.CompanyId));

            builder.Entity<AppFile>().HasQueryFilter(file =>
                !file.Deleted && (!_companyService.CompanyId.HasValue || _companyService.CompanyId == file.CompanyId));

            builder.Seed();
        }

        #region Entities

        public DbSet<UserRoom> UserRooms { get; set; }

        public DbSet<UserTask> TaskUsers { get; set; }

        public DbSet<PaymentInfo> PaymentInfos { get; set; }

        public DbSet<Activity> Activities { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<UserNotification> UserNotifications { get; set; }

        public DbSet<BookingRoom> BookingRooms { get; set; }

        public DbSet<PackageRequest> PackageRequests { get; set; }

        public DbSet<Package> Packages { get; set; }

        public DbSet<CompanyDrive> CompanyDrives { get; set; }

        public DbSet<UserCompany> UserCompanies { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<AppStorage> AppStorages { get; set; }

        public DbSet<SharedFile> SharedFiles { get; set; }

        public DbSet<AppFile> AppFiles { get; set; }

        public DbSet<ChatNotification> ChatNotifications { get; set; }

        public DbSet<AppRoom> AppRooms { get; set; }

        public DbSet<AppChat> AppChats { get; set; }

        public DbSet<AppTask> AppTasks { get; set; }

        public DbSet<TaskAttachment> TaskAttachment { get; set; }

        public DbSet<TaskCheckList> TaskCheckLists { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Department> Departments { get; set; }

        public DbSet<PostAttachment> PostAttachments { get; set; }

        public DbSet<History> Histories { get; set; }

        public override DbSet<AppRole> Roles { get; set; }

        public override DbSet<AppUser> Users { get; set; }

        public DbSet<UserTime> UserTimes { get; set; }

        public DbSet<UserWork> UserWorks { get; set; }

        public DbSet<WorkSchedule> WorkSchedules { get; set; }

        public DbSet<Notification> Notifications { get; set; }

        #endregion
    }
}