﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Package_Add : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "8b01e321-8c1d-43cc-aa43-37d79be0e777");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "8b01e321-8c1d-43cc-aa43-37d79be0e777", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 996, DateTimeKind.Utc).AddTicks(3374),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 839, DateTimeKind.Utc).AddTicks(7341));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 996, DateTimeKind.Utc).AddTicks(2978),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 839, DateTimeKind.Utc).AddTicks(7041));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 908, DateTimeKind.Utc).AddTicks(8526),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 764, DateTimeKind.Utc).AddTicks(3985));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 908, DateTimeKind.Utc).AddTicks(7855),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 764, DateTimeKind.Utc).AddTicks(3322));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 977, DateTimeKind.Utc).AddTicks(4507),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 821, DateTimeKind.Utc).AddTicks(5880));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 977, DateTimeKind.Utc).AddTicks(4154),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 821, DateTimeKind.Utc).AddTicks(5529));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 998, DateTimeKind.Utc).AddTicks(3266),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 841, DateTimeKind.Utc).AddTicks(7477));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 990, DateTimeKind.Utc).AddTicks(3643),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 833, DateTimeKind.Utc).AddTicks(7897));

            migrationBuilder.AddColumn<int>(
                name: "PackageId",
                table: "Company",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Package",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NumOfEmployee = table.Column<int>(type: "int", nullable: false, defaultValue: 50),
                    SizeOfDrive = table.Column<int>(type: "int", nullable: false, defaultValue: 5120),
                    Cost = table.Column<float>(type: "real", nullable: false, defaultValue: 0f)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Package", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 20, 54, 35, 43, DateTimeKind.Local).AddTicks(3079), new DateTime(2021, 6, 13, 20, 54, 35, 43, DateTimeKind.Local).AddTicks(6809) });

            migrationBuilder.InsertData(
                table: "Package",
                columns: new[] { "Id", "Name", "NumOfEmployee", "SizeOfDrive" },
                values: new object[] { 1, "Free", 50, 5120 });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "504350f7-cdb5-4e6c-9106-2ef4115f5d9a", 1, "1bed2459-5c33-4977-851e-3d4b14aeff06", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "31e4f64b-c71e-40de-a835-3a9bad36cad0", new DateTime(2021, 6, 13, 20, 54, 35, 45, DateTimeKind.Local).AddTicks(2634), "AQAAAAEAACcQAAAAEOzvSveLNRXTKxKZk79Y4GGKb9ZjgcoS/ZKdn5kl/WntAnsVqbHfZ/p66BF5JhjX2w==", "7cf801d1-c261-4c83-93d8-cf322d0a7ffd", new DateTime(2021, 6, 13, 20, 54, 35, 45, DateTimeKind.Local).AddTicks(2905) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "504350f7-cdb5-4e6c-9106-2ef4115f5d9a", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "PackageId", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 13, 54, 35, 59, DateTimeKind.Utc).AddTicks(2111), 1, new DateTime(2021, 6, 13, 13, 54, 35, 59, DateTimeKind.Utc).AddTicks(2440) });

            migrationBuilder.CreateIndex(
                name: "IX_Company_PackageId",
                table: "Company",
                column: "PackageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Package_PackageId",
                table: "Company",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Package_PackageId",
                table: "Company");

            migrationBuilder.DropTable(
                name: "Package");

            migrationBuilder.DropIndex(
                name: "IX_Company_PackageId",
                table: "Company");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "504350f7-cdb5-4e6c-9106-2ef4115f5d9a");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "504350f7-cdb5-4e6c-9106-2ef4115f5d9a", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "PackageId",
                table: "Company");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 839, DateTimeKind.Utc).AddTicks(7341),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 996, DateTimeKind.Utc).AddTicks(3374));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 839, DateTimeKind.Utc).AddTicks(7041),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 996, DateTimeKind.Utc).AddTicks(2978));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 764, DateTimeKind.Utc).AddTicks(3985),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 908, DateTimeKind.Utc).AddTicks(8526));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 764, DateTimeKind.Utc).AddTicks(3322),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 908, DateTimeKind.Utc).AddTicks(7855));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 821, DateTimeKind.Utc).AddTicks(5880),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 977, DateTimeKind.Utc).AddTicks(4507));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 821, DateTimeKind.Utc).AddTicks(5529),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 977, DateTimeKind.Utc).AddTicks(4154));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 841, DateTimeKind.Utc).AddTicks(7477),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 998, DateTimeKind.Utc).AddTicks(3266));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 9, 37, 45, 833, DateTimeKind.Utc).AddTicks(7897),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 990, DateTimeKind.Utc).AddTicks(3643));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 9, 37, 45, 894, DateTimeKind.Utc).AddTicks(3078), new DateTime(2021, 6, 13, 9, 37, 45, 894, DateTimeKind.Utc).AddTicks(3446) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 16, 37, 45, 882, DateTimeKind.Local).AddTicks(2599), new DateTime(2021, 6, 13, 16, 37, 45, 882, DateTimeKind.Local).AddTicks(7682) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "8b01e321-8c1d-43cc-aa43-37d79be0e777", 1, "db5e23cb-aaec-4970-b05e-31966782afe2", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "3b28b0c0-481f-475c-aa5f-6ffea83d199c", new DateTime(2021, 6, 13, 16, 37, 45, 884, DateTimeKind.Local).AddTicks(559), "AQAAAAEAACcQAAAAEJsf64EWfbjbg9wJqNkCsefplmLf3gCNL3TTvDyKYyGo+MRmFGriFmqRoUqimtfRjw==", "f1d1fd0d-acb9-4648-9b1c-a8796cd23692", new DateTime(2021, 6, 13, 16, 37, 45, 884, DateTimeKind.Local).AddTicks(831) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "8b01e321-8c1d-43cc-aa43-37d79be0e777", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
