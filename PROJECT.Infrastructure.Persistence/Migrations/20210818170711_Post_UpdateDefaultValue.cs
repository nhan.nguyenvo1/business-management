﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Post_UpdateDefaultValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "acce7778-a851-4882-8be7-6d8d8e43b938");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "acce7778-a851-4882-8be7-6d8d8e43b938", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 709, DateTimeKind.Utc).AddTicks(5212),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 819, DateTimeKind.Utc).AddTicks(7138));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 709, DateTimeKind.Utc).AddTicks(4779),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 819, DateTimeKind.Utc).AddTicks(6784));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 573, DateTimeKind.Utc).AddTicks(8269),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 702, DateTimeKind.Utc).AddTicks(1757));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 573, DateTimeKind.Utc).AddTicks(7162),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 702, DateTimeKind.Utc).AddTicks(551));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 791, DateTimeKind.Utc).AddTicks(3280));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 791, DateTimeKind.Utc).AddTicks(2966));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 702, DateTimeKind.Utc).AddTicks(7759),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 811, DateTimeKind.Utc).AddTicks(6692));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 18, 17, 7, 9, 785, DateTimeKind.Utc).AddTicks(5122), new DateTime(2021, 8, 18, 17, 7, 9, 785, DateTimeKind.Utc).AddTicks(5444) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 19, 0, 7, 9, 761, DateTimeKind.Local).AddTicks(3786), new DateTime(2021, 8, 19, 0, 7, 9, 761, DateTimeKind.Local).AddTicks(5735) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "5868979f-db3a-4069-b3af-2fe72379bb82", 1, "e300406e-c8f2-4232-b7f2-da30b1982f88", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "80a2c85c-2d62-4830-9dcd-bf1e9dabe181", new DateTime(2021, 8, 19, 0, 7, 9, 765, DateTimeKind.Local).AddTicks(1866), "AQAAAAEAACcQAAAAEKvDJcKYVxVIO6IhE+yM535P9m3SnVbvR/PYHsG0dj5yJCGdWk3MDY1/UDz6xvFYlA==", "77401d3c-a65e-4c82-b3d7-b2832b8e9bd1", new DateTime(2021, 8, 19, 0, 7, 9, 765, DateTimeKind.Local).AddTicks(2160) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "5868979f-db3a-4069-b3af-2fe72379bb82", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "5868979f-db3a-4069-b3af-2fe72379bb82");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "5868979f-db3a-4069-b3af-2fe72379bb82", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 819, DateTimeKind.Utc).AddTicks(7138),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 709, DateTimeKind.Utc).AddTicks(5212));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 819, DateTimeKind.Utc).AddTicks(6784),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 709, DateTimeKind.Utc).AddTicks(4779));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 702, DateTimeKind.Utc).AddTicks(1757),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 573, DateTimeKind.Utc).AddTicks(8269));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 702, DateTimeKind.Utc).AddTicks(551),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 573, DateTimeKind.Utc).AddTicks(7162));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 791, DateTimeKind.Utc).AddTicks(3280),
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 791, DateTimeKind.Utc).AddTicks(2966),
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 811, DateTimeKind.Utc).AddTicks(6692),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 702, DateTimeKind.Utc).AddTicks(7759));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 1, 14, 23, 26, 875, DateTimeKind.Utc).AddTicks(125), new DateTime(2021, 8, 1, 14, 23, 26, 875, DateTimeKind.Utc).AddTicks(528) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 1, 21, 23, 26, 863, DateTimeKind.Local).AddTicks(1790), new DateTime(2021, 8, 1, 21, 23, 26, 863, DateTimeKind.Local).AddTicks(5490) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "acce7778-a851-4882-8be7-6d8d8e43b938", 1, "e2d9dded-bb4e-462a-8a07-5485cced3227", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "e323fd7c-0818-4a67-b132-454f83fb952a", new DateTime(2021, 8, 1, 21, 23, 26, 864, DateTimeKind.Local).AddTicks(9017), "AQAAAAEAACcQAAAAEBgs/hePEbmldJe99Hu3lW64+R9nOF/TvWPyC52CHd2977oAtFNcoh624grC1ukIzg==", "957ab18a-70ae-4948-bdca-80e3164c85a2", new DateTime(2021, 8, 1, 21, 23, 26, 864, DateTimeKind.Local).AddTicks(9290) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "acce7778-a851-4882-8be7-6d8d8e43b938", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
