﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Package_AddIsDefaultColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "17ad37a2-d7bb-4a4d-8132-8d648cd2e8a3");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "17ad37a2-d7bb-4a4d-8132-8d648cd2e8a3", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 605, DateTimeKind.Utc).AddTicks(6538),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 37, DateTimeKind.Utc).AddTicks(1758));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 605, DateTimeKind.Utc).AddTicks(6137),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 37, DateTimeKind.Utc).AddTicks(1148));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 504, DateTimeKind.Utc).AddTicks(1783),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 9, 751, DateTimeKind.Utc).AddTicks(2118));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 504, DateTimeKind.Utc).AddTicks(1038),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 9, 751, DateTimeKind.Utc).AddTicks(931));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 584, DateTimeKind.Utc).AddTicks(3922),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 6, DateTimeKind.Utc).AddTicks(85));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 584, DateTimeKind.Utc).AddTicks(3595),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 5, DateTimeKind.Utc).AddTicks(9521));

            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "Package",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 598, DateTimeKind.Utc).AddTicks(6346),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 24, DateTimeKind.Utc).AddTicks(3594));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 18, 9, 11, 15, 687, DateTimeKind.Utc).AddTicks(5713), new DateTime(2021, 7, 18, 9, 11, 15, 687, DateTimeKind.Utc).AddTicks(6220) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 18, 16, 11, 15, 659, DateTimeKind.Local).AddTicks(3434), new DateTime(2021, 7, 18, 16, 11, 15, 659, DateTimeKind.Local).AddTicks(8528) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "f3857cea-4461-44ed-9f41-8771b2dfe8ab", 1, "894ae908-d4f8-43ab-b0e0-7bbf7a5d96f3", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "66de9660-9b92-4263-bc18-a4ace6c2a095", new DateTime(2021, 7, 18, 16, 11, 15, 661, DateTimeKind.Local).AddTicks(5975), "AQAAAAEAACcQAAAAENfmWeoWPLFOiOfnsUETFAzH2aiZSm/c/FXRLyT80mgk/QF8FfGIznWoIf/YIpqzYw==", "f48f9df2-7acb-4c57-af14-613363dc5204", new DateTime(2021, 7, 18, 16, 11, 15, 661, DateTimeKind.Local).AddTicks(6445) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "f3857cea-4461-44ed-9f41-8771b2dfe8ab", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "f3857cea-4461-44ed-9f41-8771b2dfe8ab");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "f3857cea-4461-44ed-9f41-8771b2dfe8ab", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "Package");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 37, DateTimeKind.Utc).AddTicks(1758),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 605, DateTimeKind.Utc).AddTicks(6538));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 37, DateTimeKind.Utc).AddTicks(1148),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 605, DateTimeKind.Utc).AddTicks(6137));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 9, 751, DateTimeKind.Utc).AddTicks(2118),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 504, DateTimeKind.Utc).AddTicks(1783));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 9, 751, DateTimeKind.Utc).AddTicks(931),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 504, DateTimeKind.Utc).AddTicks(1038));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 6, DateTimeKind.Utc).AddTicks(85),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 584, DateTimeKind.Utc).AddTicks(3922));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 5, DateTimeKind.Utc).AddTicks(9521),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 584, DateTimeKind.Utc).AddTicks(3595));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 24, DateTimeKind.Utc).AddTicks(3594),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 598, DateTimeKind.Utc).AddTicks(6346));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 9, 17, 51, 10, 336, DateTimeKind.Utc).AddTicks(9787), new DateTime(2021, 7, 9, 17, 51, 10, 337, DateTimeKind.Utc).AddTicks(474) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 10, 0, 51, 10, 306, DateTimeKind.Local).AddTicks(6883), new DateTime(2021, 7, 10, 0, 51, 10, 307, DateTimeKind.Local).AddTicks(2605) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "17ad37a2-d7bb-4a4d-8132-8d648cd2e8a3", 1, "7a792bd0-6f87-41e9-b32f-07f9863fb16f", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "4e497eb0-c2b6-4034-bc4c-a7614ca883cb", new DateTime(2021, 7, 10, 0, 51, 10, 309, DateTimeKind.Local).AddTicks(4662), "AQAAAAEAACcQAAAAEGK1i5Y9RFp2LcAZevExnUX6LRin3nkrxeZtAyaDkkcNwMKIEeKqB40+QBX2bSqmwQ==", "76a7be6d-5c6d-4606-8669-062424598f96", new DateTime(2021, 7, 10, 0, 51, 10, 309, DateTimeKind.Local).AddTicks(5018) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "17ad37a2-d7bb-4a4d-8132-8d648cd2e8a3", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
