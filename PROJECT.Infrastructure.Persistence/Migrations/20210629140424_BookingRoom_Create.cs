﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class BookingRoom_Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "a28c974e-8222-40b9-917b-5b936bda1e06");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "a28c974e-8222-40b9-917b-5b936bda1e06", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 431, DateTimeKind.Utc).AddTicks(6324),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 961, DateTimeKind.Utc).AddTicks(7958));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 431, DateTimeKind.Utc).AddTicks(6015),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 961, DateTimeKind.Utc).AddTicks(7513));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 334, DateTimeKind.Utc).AddTicks(6589),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 836, DateTimeKind.Utc).AddTicks(7453));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 334, DateTimeKind.Utc).AddTicks(5868),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 836, DateTimeKind.Utc).AddTicks(6321));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 412, DateTimeKind.Utc).AddTicks(2833),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 932, DateTimeKind.Utc).AddTicks(8503));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 412, DateTimeKind.Utc).AddTicks(2507),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 932, DateTimeKind.Utc).AddTicks(7990));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 433, DateTimeKind.Utc).AddTicks(8717),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 964, DateTimeKind.Utc).AddTicks(8721));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 425, DateTimeKind.Utc).AddTicks(5766),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 951, DateTimeKind.Utc).AddTicks(6685));

            migrationBuilder.CreateTable(
                name: "BookingRoom",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "ntext", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    From = table.Column<TimeSpan>(type: "time", nullable: false),
                    To = table.Column<TimeSpan>(type: "time", nullable: true),
                    Room = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Url = table.Column<string>(type: "nvarchar(600)", maxLength: 600, nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false, defaultValue: 0),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    UpdatedBy = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingRoom", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BookingRoom_User_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BookingRoom_User_UpdatedBy",
                        column: x => x.UpdatedBy,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BookingRoomUser",
                columns: table => new
                {
                    AppUsersId = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    BookingRoomsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingRoomUser", x => new { x.AppUsersId, x.BookingRoomsId });
                    table.ForeignKey(
                        name: "FK_BookingRoomUser_BookingRoom_BookingRoomsId",
                        column: x => x.BookingRoomsId,
                        principalTable: "BookingRoom",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookingRoomUser_User_AppUsersId",
                        column: x => x.AppUsersId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 29, 14, 4, 23, 489, DateTimeKind.Utc).AddTicks(5309), new DateTime(2021, 6, 29, 14, 4, 23, 489, DateTimeKind.Utc).AddTicks(5747) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 29, 21, 4, 23, 476, DateTimeKind.Local).AddTicks(3631), new DateTime(2021, 6, 29, 21, 4, 23, 476, DateTimeKind.Local).AddTicks(7227) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "c8dedae9-b40d-49c4-8de8-b98f57ba2ce1", 1, "db3b8efa-701d-4b93-8cf1-6200dc45d65a", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "34fd68d9-d1c3-49f8-93bd-1bebdc0e0c5e", new DateTime(2021, 6, 29, 21, 4, 23, 478, DateTimeKind.Local).AddTicks(506), "AQAAAAEAACcQAAAAEIfvR4vKmTPaaxbieYgyy51ziIjs/sSZ06tnIHzuBWkLxEa+/5Nn7ZVoEzm/4KbKQQ==", "410d2365-7d35-4dfd-80e4-0fbc506690ad", new DateTime(2021, 6, 29, 21, 4, 23, 478, DateTimeKind.Local).AddTicks(771) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "c8dedae9-b40d-49c4-8de8-b98f57ba2ce1", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_BookingRoom_CreatedBy",
                table: "BookingRoom",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_BookingRoom_UpdatedBy",
                table: "BookingRoom",
                column: "UpdatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_BookingRoomUser_BookingRoomsId",
                table: "BookingRoomUser",
                column: "BookingRoomsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookingRoomUser");

            migrationBuilder.DropTable(
                name: "BookingRoom");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "c8dedae9-b40d-49c4-8de8-b98f57ba2ce1");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "c8dedae9-b40d-49c4-8de8-b98f57ba2ce1", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 961, DateTimeKind.Utc).AddTicks(7958),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 431, DateTimeKind.Utc).AddTicks(6324));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 961, DateTimeKind.Utc).AddTicks(7513),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 431, DateTimeKind.Utc).AddTicks(6015));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 836, DateTimeKind.Utc).AddTicks(7453),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 334, DateTimeKind.Utc).AddTicks(6589));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 836, DateTimeKind.Utc).AddTicks(6321),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 334, DateTimeKind.Utc).AddTicks(5868));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 932, DateTimeKind.Utc).AddTicks(8503),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 412, DateTimeKind.Utc).AddTicks(2833));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 932, DateTimeKind.Utc).AddTicks(7990),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 412, DateTimeKind.Utc).AddTicks(2507));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 964, DateTimeKind.Utc).AddTicks(8721),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 433, DateTimeKind.Utc).AddTicks(8717));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 951, DateTimeKind.Utc).AddTicks(6685),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 425, DateTimeKind.Utc).AddTicks(5766));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 24, 16, 1, 3, 39, DateTimeKind.Utc).AddTicks(6862), new DateTime(2021, 6, 24, 16, 1, 3, 39, DateTimeKind.Utc).AddTicks(7210) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 24, 23, 1, 3, 26, DateTimeKind.Local).AddTicks(6822), new DateTime(2021, 6, 24, 23, 1, 3, 27, DateTimeKind.Local).AddTicks(1163) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "a28c974e-8222-40b9-917b-5b936bda1e06", 1, "44205158-9269-4129-8c5f-6217c2362ecb", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "9f10e338-dab0-4d2e-ba69-fec226d6e915", new DateTime(2021, 6, 24, 23, 1, 3, 28, DateTimeKind.Local).AddTicks(6291), "AQAAAAEAACcQAAAAEJX3xBg/k+SyjqzOfgupDhoPmfPyac4Sj0PJVTN5WBtivdqi4Hc37fY4QfWWNmefJA==", "5f48afde-85b5-40f3-a8b0-6027c9615575", new DateTime(2021, 6, 24, 23, 1, 3, 28, DateTimeKind.Local).AddTicks(6603) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "a28c974e-8222-40b9-917b-5b936bda1e06", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
