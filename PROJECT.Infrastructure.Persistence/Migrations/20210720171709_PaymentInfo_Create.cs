﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class PaymentInfo_Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "4eb10f28-e228-4882-9d2e-da1aa6225ff0");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "4eb10f28-e228-4882-9d2e-da1aa6225ff0", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 637, DateTimeKind.Utc).AddTicks(7611),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 760, DateTimeKind.Utc).AddTicks(762));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 637, DateTimeKind.Utc).AddTicks(6711),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 760, DateTimeKind.Utc).AddTicks(15));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 512, DateTimeKind.Utc).AddTicks(5994),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 647, DateTimeKind.Utc).AddTicks(5567));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 512, DateTimeKind.Utc).AddTicks(5193),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 647, DateTimeKind.Utc).AddTicks(4732));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 605, DateTimeKind.Utc).AddTicks(6869),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 737, DateTimeKind.Utc).AddTicks(9762));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 605, DateTimeKind.Utc).AddTicks(6484),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 737, DateTimeKind.Utc).AddTicks(9375));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 625, DateTimeKind.Utc).AddTicks(7226),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 752, DateTimeKind.Utc).AddTicks(7752));

            migrationBuilder.CreateTable(
                name: "PaymentInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CardNumber = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    CardBank = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    CardHolder = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentInfo", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 17, 17, 7, 732, DateTimeKind.Utc).AddTicks(6230), new DateTime(2021, 7, 20, 17, 17, 7, 732, DateTimeKind.Utc).AddTicks(6950) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 21, 0, 17, 7, 714, DateTimeKind.Local).AddTicks(9750), new DateTime(2021, 7, 21, 0, 17, 7, 716, DateTimeKind.Local).AddTicks(43) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "60d4c178-6bc3-455d-ad90-1d3528460ce3", 1, "ba8d055f-7008-441e-a5cb-87b489beab51", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "bf1bfb3b-0c4d-4b2d-a17b-5e0067decbad", new DateTime(2021, 7, 21, 0, 17, 7, 717, DateTimeKind.Local).AddTicks(9424), "AQAAAAEAACcQAAAAEMyFrOVad3wR7xnMxBpYoUZOLzemNxp4kys7aezazQLgr7sf5V9m1mhyP/j3FtmukA==", "c62048aa-7ea0-4336-9c5e-28b24b0c9c25", new DateTime(2021, 7, 21, 0, 17, 7, 717, DateTimeKind.Local).AddTicks(9737) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "60d4c178-6bc3-455d-ad90-1d3528460ce3", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentInfo");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "60d4c178-6bc3-455d-ad90-1d3528460ce3");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "60d4c178-6bc3-455d-ad90-1d3528460ce3", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 760, DateTimeKind.Utc).AddTicks(762),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 637, DateTimeKind.Utc).AddTicks(7611));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 760, DateTimeKind.Utc).AddTicks(15),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 637, DateTimeKind.Utc).AddTicks(6711));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 647, DateTimeKind.Utc).AddTicks(5567),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 512, DateTimeKind.Utc).AddTicks(5994));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 647, DateTimeKind.Utc).AddTicks(4732),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 512, DateTimeKind.Utc).AddTicks(5193));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 737, DateTimeKind.Utc).AddTicks(9762),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 605, DateTimeKind.Utc).AddTicks(6869));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 737, DateTimeKind.Utc).AddTicks(9375),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 605, DateTimeKind.Utc).AddTicks(6484));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 752, DateTimeKind.Utc).AddTicks(7752),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 625, DateTimeKind.Utc).AddTicks(7226));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 15, 59, 19, 819, DateTimeKind.Utc).AddTicks(8061), new DateTime(2021, 7, 20, 15, 59, 19, 819, DateTimeKind.Utc).AddTicks(8374) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 22, 59, 19, 807, DateTimeKind.Local).AddTicks(7083), new DateTime(2021, 7, 20, 22, 59, 19, 808, DateTimeKind.Local).AddTicks(929) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "4eb10f28-e228-4882-9d2e-da1aa6225ff0", 1, "a0f15a89-10c7-4863-b0fc-8cc4d7f2d368", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "bb657692-4b47-4541-b943-1a1e5bb7f505", new DateTime(2021, 7, 20, 22, 59, 19, 809, DateTimeKind.Local).AddTicks(5237), "AQAAAAEAACcQAAAAENC2NhQ/kBspJrvgCYOw208LmhQJTARl5Wh+gl74FQZBghV7MH+jVrl6NMDnJpLHGA==", "ad982f9e-8088-4262-8f7e-831b6e012c99", new DateTime(2021, 7, 20, 22, 59, 19, 809, DateTimeKind.Local).AddTicks(5632) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "4eb10f28-e228-4882-9d2e-da1aa6225ff0", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
