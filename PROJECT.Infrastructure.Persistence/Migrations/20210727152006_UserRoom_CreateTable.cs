﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class UserRoom_CreateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BookingRoomUser");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "f08c171a-096c-4337-afb8-2fa3e4777168");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "f08c171a-096c-4337-afb8-2fa3e4777168", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 753, DateTimeKind.Utc).AddTicks(3712),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 543, DateTimeKind.Utc).AddTicks(5360));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 753, DateTimeKind.Utc).AddTicks(3366),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 543, DateTimeKind.Utc).AddTicks(5002));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 608, DateTimeKind.Utc).AddTicks(3742),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 440, DateTimeKind.Utc).AddTicks(1796));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 608, DateTimeKind.Utc).AddTicks(3053),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 440, DateTimeKind.Utc).AddTicks(1119));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 718, DateTimeKind.Utc).AddTicks(1466),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 519, DateTimeKind.Utc).AddTicks(5348));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 718, DateTimeKind.Utc).AddTicks(1133),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 519, DateTimeKind.Utc).AddTicks(4922));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 743, DateTimeKind.Utc).AddTicks(7090),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 533, DateTimeKind.Utc).AddTicks(9810));

            migrationBuilder.CreateTable(
                name: "UserRoom",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    RoomId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoom", x => new { x.RoomId, x.UserId });
                    table.ForeignKey(
                        name: "FK_UserRoom_BookingRoom_RoomId",
                        column: x => x.RoomId,
                        principalTable: "BookingRoom",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRoom_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 15, 20, 4, 826, DateTimeKind.Utc).AddTicks(7141), new DateTime(2021, 7, 27, 15, 20, 4, 826, DateTimeKind.Utc).AddTicks(7464) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 22, 20, 4, 812, DateTimeKind.Local).AddTicks(5107), new DateTime(2021, 7, 27, 22, 20, 4, 812, DateTimeKind.Local).AddTicks(9021) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "bcc46456-4c4d-42a7-9a10-47450b6294d6", 1, "f44a6d53-644e-411a-972c-133dfe3adb2f", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "f1665aab-9da6-4e27-b0e3-487d638587c7", new DateTime(2021, 7, 27, 22, 20, 4, 814, DateTimeKind.Local).AddTicks(5138), "AQAAAAEAACcQAAAAEAXMkF2J6DknZnlCC/TFcz2dnKzMR+BAcvMF07Ko13G4GjNUr1ZC6NX/0F7iuIDJOA==", "412e3b22-bc2f-4a35-b038-9564b05a1b1a", new DateTime(2021, 7, 27, 22, 20, 4, 814, DateTimeKind.Local).AddTicks(5436) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "bcc46456-4c4d-42a7-9a10-47450b6294d6", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_UserRoom_UserId",
                table: "UserRoom",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserRoom");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "bcc46456-4c4d-42a7-9a10-47450b6294d6");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "bcc46456-4c4d-42a7-9a10-47450b6294d6", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 543, DateTimeKind.Utc).AddTicks(5360),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 753, DateTimeKind.Utc).AddTicks(3712));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 543, DateTimeKind.Utc).AddTicks(5002),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 753, DateTimeKind.Utc).AddTicks(3366));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 440, DateTimeKind.Utc).AddTicks(1796),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 608, DateTimeKind.Utc).AddTicks(3742));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 440, DateTimeKind.Utc).AddTicks(1119),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 608, DateTimeKind.Utc).AddTicks(3053));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 519, DateTimeKind.Utc).AddTicks(5348),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 718, DateTimeKind.Utc).AddTicks(1466));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 519, DateTimeKind.Utc).AddTicks(4922),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 718, DateTimeKind.Utc).AddTicks(1133));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 533, DateTimeKind.Utc).AddTicks(9810),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 743, DateTimeKind.Utc).AddTicks(7090));

            migrationBuilder.CreateTable(
                name: "BookingRoomUser",
                columns: table => new
                {
                    AppUsersId = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    BookingRoomsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BookingRoomUser", x => new { x.AppUsersId, x.BookingRoomsId });
                    table.ForeignKey(
                        name: "FK_BookingRoomUser_BookingRoom_BookingRoomsId",
                        column: x => x.BookingRoomsId,
                        principalTable: "BookingRoom",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BookingRoomUser_User_AppUsersId",
                        column: x => x.AppUsersId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 14, 48, 23, 605, DateTimeKind.Utc).AddTicks(17), new DateTime(2021, 7, 27, 14, 48, 23, 605, DateTimeKind.Utc).AddTicks(307) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 21, 48, 23, 593, DateTimeKind.Local).AddTicks(1870), new DateTime(2021, 7, 27, 21, 48, 23, 593, DateTimeKind.Local).AddTicks(5475) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "f08c171a-096c-4337-afb8-2fa3e4777168", 1, "e2546247-e981-4979-9ee4-83dc1cffcba3", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "4bdfbbd8-8af2-424b-865d-5d6c44f69029", new DateTime(2021, 7, 27, 21, 48, 23, 594, DateTimeKind.Local).AddTicks(9050), "AQAAAAEAACcQAAAAENpbX9fQuOGAbpgbmd8nOwQJpgaOoGboGI41aOzWH58bbF0VaUWm9nt42q5+3C4SVQ==", "2802b5c5-8c4c-49a9-9fa4-3768b3e12d2e", new DateTime(2021, 7, 27, 21, 48, 23, 594, DateTimeKind.Local).AddTicks(9332) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "f08c171a-096c-4337-afb8-2fa3e4777168", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_BookingRoomUser_BookingRoomsId",
                table: "BookingRoomUser",
                column: "BookingRoomsId");
        }
    }
}
