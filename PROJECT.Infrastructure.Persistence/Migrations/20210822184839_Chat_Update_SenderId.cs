﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Chat_Update_SenderId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chat_User_SenderId",
                table: "Chat");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "5868979f-db3a-4069-b3af-2fe72379bb82");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "5868979f-db3a-4069-b3af-2fe72379bb82", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 400, DateTimeKind.Utc).AddTicks(7020),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 709, DateTimeKind.Utc).AddTicks(5212));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 400, DateTimeKind.Utc).AddTicks(6404),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 709, DateTimeKind.Utc).AddTicks(4779));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 301, DateTimeKind.Utc).AddTicks(9475),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 573, DateTimeKind.Utc).AddTicks(8269));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 301, DateTimeKind.Utc).AddTicks(8821),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 573, DateTimeKind.Utc).AddTicks(7162));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 394, DateTimeKind.Utc).AddTicks(6577),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 702, DateTimeKind.Utc).AddTicks(7759));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 22, 18, 48, 38, 455, DateTimeKind.Utc).AddTicks(6923), new DateTime(2021, 8, 22, 18, 48, 38, 455, DateTimeKind.Utc).AddTicks(7229) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 23, 1, 48, 38, 443, DateTimeKind.Local).AddTicks(6534), new DateTime(2021, 8, 23, 1, 48, 38, 443, DateTimeKind.Local).AddTicks(8032) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "69d271fa-436c-40be-afe3-2d545961b4bf", 1, "e28fe5ef-1a23-4cad-91db-a798023bfa55", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "a101e591-b667-4bbf-aa35-a4fa7973099e", new DateTime(2021, 8, 23, 1, 48, 38, 445, DateTimeKind.Local).AddTicks(8898), "AQAAAAEAACcQAAAAEPVLcFy5PwbBZslYOVzsyY2k1KzEZjnyBGQY3smu7aGPGBCNKhdSoPkEczWIPj9Pew==", "b21d35f6-fe34-4b7d-a12f-10f320a5c29d", new DateTime(2021, 8, 23, 1, 48, 38, 445, DateTimeKind.Local).AddTicks(9182) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "69d271fa-436c-40be-afe3-2d545961b4bf", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_Chat_User_SenderId",
                table: "Chat",
                column: "SenderId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Chat_User_SenderId",
                table: "Chat");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "69d271fa-436c-40be-afe3-2d545961b4bf");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "69d271fa-436c-40be-afe3-2d545961b4bf", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 709, DateTimeKind.Utc).AddTicks(5212),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 400, DateTimeKind.Utc).AddTicks(7020));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 709, DateTimeKind.Utc).AddTicks(4779),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 400, DateTimeKind.Utc).AddTicks(6404));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 573, DateTimeKind.Utc).AddTicks(8269),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 301, DateTimeKind.Utc).AddTicks(9475));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 573, DateTimeKind.Utc).AddTicks(7162),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 301, DateTimeKind.Utc).AddTicks(8821));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 18, 17, 7, 9, 702, DateTimeKind.Utc).AddTicks(7759),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 394, DateTimeKind.Utc).AddTicks(6577));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 18, 17, 7, 9, 785, DateTimeKind.Utc).AddTicks(5122), new DateTime(2021, 8, 18, 17, 7, 9, 785, DateTimeKind.Utc).AddTicks(5444) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 19, 0, 7, 9, 761, DateTimeKind.Local).AddTicks(3786), new DateTime(2021, 8, 19, 0, 7, 9, 761, DateTimeKind.Local).AddTicks(5735) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "5868979f-db3a-4069-b3af-2fe72379bb82", 1, "e300406e-c8f2-4232-b7f2-da30b1982f88", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "80a2c85c-2d62-4830-9dcd-bf1e9dabe181", new DateTime(2021, 8, 19, 0, 7, 9, 765, DateTimeKind.Local).AddTicks(1866), "AQAAAAEAACcQAAAAEKvDJcKYVxVIO6IhE+yM535P9m3SnVbvR/PYHsG0dj5yJCGdWk3MDY1/UDz6xvFYlA==", "77401d3c-a65e-4c82-b3d7-b2832b8e9bd1", new DateTime(2021, 8, 19, 0, 7, 9, 765, DateTimeKind.Local).AddTicks(2160) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "5868979f-db3a-4069-b3af-2fe72379bb82", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_Chat_User_SenderId",
                table: "Chat",
                column: "SenderId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
