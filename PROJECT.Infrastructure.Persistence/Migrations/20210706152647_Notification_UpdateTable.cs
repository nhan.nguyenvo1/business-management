﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Notification_UpdateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Notification_User_AssignedUserId",
                table: "Notification");

            migrationBuilder.DropForeignKey(
                name: "FK_Notification_User_ResponsibleUserId",
                table: "Notification");

            migrationBuilder.DropIndex(
                name: "IX_Notification_AssignedUserId",
                table: "Notification");

            migrationBuilder.DropIndex(
                name: "IX_Notification_ResponsibleUserId",
                table: "Notification");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "d577b81d-0fd8-45cf-861b-d2e2559589ff");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "d577b81d-0fd8-45cf-861b-d2e2559589ff", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "Action",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "AssignedUserId",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "ResponsibleUserId",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "UrlAlias",
                table: "Notification");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 606, DateTimeKind.Utc).AddTicks(1824),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 284, DateTimeKind.Utc).AddTicks(531));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 606, DateTimeKind.Utc).AddTicks(791),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 284, DateTimeKind.Utc).AddTicks(179));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 491, DateTimeKind.Utc).AddTicks(8478),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 193, DateTimeKind.Utc).AddTicks(5099));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 491, DateTimeKind.Utc).AddTicks(7760),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 193, DateTimeKind.Utc).AddTicks(4441));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 581, DateTimeKind.Utc).AddTicks(415),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 266, DateTimeKind.Utc).AddTicks(2500));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 580, DateTimeKind.Utc).AddTicks(9882),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 266, DateTimeKind.Utc).AddTicks(1764));

            migrationBuilder.AlterColumn<string>(
                name: "Url",
                table: "Notification",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 285, DateTimeKind.Utc).AddTicks(9251));

            migrationBuilder.AddColumn<int>(
                name: "Category",
                table: "Notification",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Notification",
                type: "nvarchar(600)",
                maxLength: 600,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Notification",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 598, DateTimeKind.Utc).AddTicks(5400),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 277, DateTimeKind.Utc).AddTicks(9448));

            migrationBuilder.CreateTable(
                name: "UserNotification",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    NotificationId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserNotification", x => new { x.UserId, x.NotificationId });
                    table.ForeignKey(
                        name: "FK_UserNotification_Notification_NotificationId",
                        column: x => x.NotificationId,
                        principalTable: "Notification",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserNotification_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 6, 15, 26, 45, 722, DateTimeKind.Utc).AddTicks(7763), new DateTime(2021, 7, 6, 15, 26, 45, 722, DateTimeKind.Utc).AddTicks(8102) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 6, 22, 26, 45, 668, DateTimeKind.Local).AddTicks(8227), new DateTime(2021, 7, 6, 22, 26, 45, 669, DateTimeKind.Local).AddTicks(9714) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "6219bf4e-1709-4fbe-9f26-d3384a3ad12e", 1, "033d9283-caf8-4892-bd5b-d7e4afc8c9fd", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "ef310c4c-0548-47e5-9f56-6459bd3f0e7c", new DateTime(2021, 7, 6, 22, 26, 45, 702, DateTimeKind.Local).AddTicks(34), "AQAAAAEAACcQAAAAELGyLPLaO2/Neo20GObUHjM8O+xkvGnBjRi3X6DtD7/FPyex8z+r6gT1TRH+eRjp2Q==", "3aabb7fe-461a-47a5-ac04-1042016c9617", new DateTime(2021, 7, 6, 22, 26, 45, 702, DateTimeKind.Local).AddTicks(387) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "6219bf4e-1709-4fbe-9f26-d3384a3ad12e", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_UserNotification_NotificationId",
                table: "UserNotification",
                column: "NotificationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserNotification");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "6219bf4e-1709-4fbe-9f26-d3384a3ad12e");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "6219bf4e-1709-4fbe-9f26-d3384a3ad12e", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "Category",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Notification");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "Notification");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 284, DateTimeKind.Utc).AddTicks(531),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 606, DateTimeKind.Utc).AddTicks(1824));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 284, DateTimeKind.Utc).AddTicks(179),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 606, DateTimeKind.Utc).AddTicks(791));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 193, DateTimeKind.Utc).AddTicks(5099),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 491, DateTimeKind.Utc).AddTicks(8478));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 193, DateTimeKind.Utc).AddTicks(4441),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 491, DateTimeKind.Utc).AddTicks(7760));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 266, DateTimeKind.Utc).AddTicks(2500),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 581, DateTimeKind.Utc).AddTicks(415));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 266, DateTimeKind.Utc).AddTicks(1764),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 580, DateTimeKind.Utc).AddTicks(9882));

            migrationBuilder.AlterColumn<string>(
                name: "Url",
                table: "Notification",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(500)",
                oldMaxLength: 500);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 285, DateTimeKind.Utc).AddTicks(9251),
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "Action",
                table: "Notification",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "AssignedUserId",
                table: "Notification",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ResponsibleUserId",
                table: "Notification",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "UrlAlias",
                table: "Notification",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 277, DateTimeKind.Utc).AddTicks(9448),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 26, 45, 598, DateTimeKind.Utc).AddTicks(5400));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 29, 17, 15, 51, 337, DateTimeKind.Utc).AddTicks(1966), new DateTime(2021, 6, 29, 17, 15, 51, 337, DateTimeKind.Utc).AddTicks(2255) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 30, 0, 15, 51, 326, DateTimeKind.Local).AddTicks(1922), new DateTime(2021, 6, 30, 0, 15, 51, 326, DateTimeKind.Local).AddTicks(5362) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "d577b81d-0fd8-45cf-861b-d2e2559589ff", 1, "e71cdaef-23ee-4670-bf2e-ffc20c2c1cc6", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "06ee8863-b75b-4c15-affb-67af797f68f7", new DateTime(2021, 6, 30, 0, 15, 51, 327, DateTimeKind.Local).AddTicks(7986), "AQAAAAEAACcQAAAAEKhTGuP+WhJtecxk0FRARJuQM+fBBedpVfzbwRA3qOLdHIpe6E29nxl+hv6alMzK/g==", "b4151445-83ea-4b9c-b40a-5f3abb57372c", new DateTime(2021, 6, 30, 0, 15, 51, 327, DateTimeKind.Local).AddTicks(8257) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "d577b81d-0fd8-45cf-861b-d2e2559589ff", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_Notification_AssignedUserId",
                table: "Notification",
                column: "AssignedUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Notification_ResponsibleUserId",
                table: "Notification",
                column: "ResponsibleUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Notification_User_AssignedUserId",
                table: "Notification",
                column: "AssignedUserId",
                principalTable: "User",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Notification_User_ResponsibleUserId",
                table: "Notification",
                column: "ResponsibleUserId",
                principalTable: "User",
                principalColumn: "Id");
        }
    }
}
