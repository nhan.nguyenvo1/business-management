﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Chat_UpdateSenderId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "69d271fa-436c-40be-afe3-2d545961b4bf");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "69d271fa-436c-40be-afe3-2d545961b4bf", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 630, DateTimeKind.Utc).AddTicks(5246),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 400, DateTimeKind.Utc).AddTicks(7020));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 630, DateTimeKind.Utc).AddTicks(4981),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 400, DateTimeKind.Utc).AddTicks(6404));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 547, DateTimeKind.Utc).AddTicks(1490),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 301, DateTimeKind.Utc).AddTicks(9475));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 547, DateTimeKind.Utc).AddTicks(927),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 301, DateTimeKind.Utc).AddTicks(8821));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 625, DateTimeKind.Utc).AddTicks(2430),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 394, DateTimeKind.Utc).AddTicks(6577));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 24, 15, 57, 34, 680, DateTimeKind.Utc).AddTicks(356), new DateTime(2021, 8, 24, 15, 57, 34, 680, DateTimeKind.Utc).AddTicks(611) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 24, 22, 57, 34, 666, DateTimeKind.Local).AddTicks(932), new DateTime(2021, 8, 24, 22, 57, 34, 666, DateTimeKind.Local).AddTicks(2232) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "2faf4d0f-aad8-4ccc-a00c-5f6cdd8f9b0b", 1, "25bf6895-9359-4ad8-b7c0-a3bb24f643fa", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "318d2471-323c-429e-bd3f-cf7def5cf0f0", new DateTime(2021, 8, 24, 22, 57, 34, 669, DateTimeKind.Local).AddTicks(5508), "AQAAAAEAACcQAAAAEMakm4toEn4QNWdq1x+IhwHGNAO8xmh5ppelwLt0pmWEmiOOWHtqdBQ1JgzY6Qbptw==", "70423ce8-711e-41b7-b7a6-4e947e6523e7", new DateTime(2021, 8, 24, 22, 57, 34, 669, DateTimeKind.Local).AddTicks(5742) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "2faf4d0f-aad8-4ccc-a00c-5f6cdd8f9b0b", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "2faf4d0f-aad8-4ccc-a00c-5f6cdd8f9b0b");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "2faf4d0f-aad8-4ccc-a00c-5f6cdd8f9b0b", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 400, DateTimeKind.Utc).AddTicks(7020),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 630, DateTimeKind.Utc).AddTicks(5246));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 400, DateTimeKind.Utc).AddTicks(6404),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 630, DateTimeKind.Utc).AddTicks(4981));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 301, DateTimeKind.Utc).AddTicks(9475),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 547, DateTimeKind.Utc).AddTicks(1490));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 301, DateTimeKind.Utc).AddTicks(8821),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 547, DateTimeKind.Utc).AddTicks(927));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 22, 18, 48, 38, 394, DateTimeKind.Utc).AddTicks(6577),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 625, DateTimeKind.Utc).AddTicks(2430));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 22, 18, 48, 38, 455, DateTimeKind.Utc).AddTicks(6923), new DateTime(2021, 8, 22, 18, 48, 38, 455, DateTimeKind.Utc).AddTicks(7229) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 23, 1, 48, 38, 443, DateTimeKind.Local).AddTicks(6534), new DateTime(2021, 8, 23, 1, 48, 38, 443, DateTimeKind.Local).AddTicks(8032) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "69d271fa-436c-40be-afe3-2d545961b4bf", 1, "e28fe5ef-1a23-4cad-91db-a798023bfa55", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "a101e591-b667-4bbf-aa35-a4fa7973099e", new DateTime(2021, 8, 23, 1, 48, 38, 445, DateTimeKind.Local).AddTicks(8898), "AQAAAAEAACcQAAAAEPVLcFy5PwbBZslYOVzsyY2k1KzEZjnyBGQY3smu7aGPGBCNKhdSoPkEczWIPj9Pew==", "b21d35f6-fe34-4b7d-a12f-10f320a5c29d", new DateTime(2021, 8, 23, 1, 48, 38, 445, DateTimeKind.Local).AddTicks(9182) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "69d271fa-436c-40be-afe3-2d545961b4bf", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
