﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class PackageRequest_AddOptionClosedDateAndReasonColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Package_PackageId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_Company_PackageId",
                table: "Company");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "98a2fb2c-7179-4df0-a071-f0cc8d221630");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "98a2fb2c-7179-4df0-a071-f0cc8d221630", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "PackageId",
                table: "Company");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 760, DateTimeKind.Utc).AddTicks(2440),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 689, DateTimeKind.Utc).AddTicks(3546));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 760, DateTimeKind.Utc).AddTicks(2028),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 689, DateTimeKind.Utc).AddTicks(3144));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 677, DateTimeKind.Utc).AddTicks(5975),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 599, DateTimeKind.Utc).AddTicks(3429));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 677, DateTimeKind.Utc).AddTicks(5339),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 599, DateTimeKind.Utc).AddTicks(2693));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 741, DateTimeKind.Utc).AddTicks(8921),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 669, DateTimeKind.Utc).AddTicks(9697));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 741, DateTimeKind.Utc).AddTicks(8623),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 669, DateTimeKind.Utc).AddTicks(9285));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ClosedDate",
                table: "PackageRequest",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "PackageRequest",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 762, DateTimeKind.Utc).AddTicks(5123),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 691, DateTimeKind.Utc).AddTicks(3497));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 754, DateTimeKind.Utc).AddTicks(1405),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 682, DateTimeKind.Utc).AddTicks(8615));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 15, 20, 32, 28, 819, DateTimeKind.Utc).AddTicks(2516), new DateTime(2021, 6, 15, 20, 32, 28, 819, DateTimeKind.Utc).AddTicks(2806) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 16, 3, 32, 28, 803, DateTimeKind.Local).AddTicks(6325), new DateTime(2021, 6, 16, 3, 32, 28, 803, DateTimeKind.Local).AddTicks(6613) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "fbaa81db-1688-41cd-96d1-167255ce301a", 1, "c004c118-879d-4a92-847c-b20fcfe289b3", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "02883587-9ece-4806-8c9f-c85ade278cbf", new DateTime(2021, 6, 16, 3, 32, 28, 805, DateTimeKind.Local).AddTicks(2774), "AQAAAAEAACcQAAAAEHmSgzXwYRy8aZR48bL7gb3+wz7Tfz4tkFzTeAR1bawB39v9uRVO+8CUipRJq+0Edg==", "e0c4e198-fff1-4537-ba83-2707a4cb3a7e", new DateTime(2021, 6, 16, 3, 32, 28, 805, DateTimeKind.Local).AddTicks(3042) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "fbaa81db-1688-41cd-96d1-167255ce301a", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "fbaa81db-1688-41cd-96d1-167255ce301a");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "fbaa81db-1688-41cd-96d1-167255ce301a", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "Reason",
                table: "PackageRequest");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 689, DateTimeKind.Utc).AddTicks(3546),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 760, DateTimeKind.Utc).AddTicks(2440));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 689, DateTimeKind.Utc).AddTicks(3144),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 760, DateTimeKind.Utc).AddTicks(2028));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 599, DateTimeKind.Utc).AddTicks(3429),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 677, DateTimeKind.Utc).AddTicks(5975));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 599, DateTimeKind.Utc).AddTicks(2693),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 677, DateTimeKind.Utc).AddTicks(5339));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 669, DateTimeKind.Utc).AddTicks(9697),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 741, DateTimeKind.Utc).AddTicks(8921));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 669, DateTimeKind.Utc).AddTicks(9285),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 741, DateTimeKind.Utc).AddTicks(8623));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ClosedDate",
                table: "PackageRequest",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 691, DateTimeKind.Utc).AddTicks(3497),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 762, DateTimeKind.Utc).AddTicks(5123));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 682, DateTimeKind.Utc).AddTicks(8615),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 754, DateTimeKind.Utc).AddTicks(1405));

            migrationBuilder.AddColumn<int>(
                name: "PackageId",
                table: "Company",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 17, 25, 44, 754, DateTimeKind.Utc).AddTicks(1014), new DateTime(2021, 6, 13, 17, 25, 44, 754, DateTimeKind.Utc).AddTicks(1303) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 14, 0, 25, 44, 739, DateTimeKind.Local).AddTicks(5891), new DateTime(2021, 6, 14, 0, 25, 44, 739, DateTimeKind.Local).AddTicks(6194) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "98a2fb2c-7179-4df0-a071-f0cc8d221630", 1, "072b6183-94dd-48c8-863f-43cf93de1f9d", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "1a26f189-5a8a-44e7-a9a7-36765af757b5", new DateTime(2021, 6, 14, 0, 25, 44, 741, DateTimeKind.Local).AddTicks(2703), "AQAAAAEAACcQAAAAENVLOx8dwZE6jew3WgAnEfPSpQ2bLTr5jJxrgOCImzOokI3MUg3W7A1qUMsvQeS0HQ==", "f9172dcb-57cb-45ed-8a5d-b75750aa59c2", new DateTime(2021, 6, 14, 0, 25, 44, 741, DateTimeKind.Local).AddTicks(2983) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "98a2fb2c-7179-4df0-a071-f0cc8d221630", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_Company_PackageId",
                table: "Company",
                column: "PackageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Package_PackageId",
                table: "Company",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
