﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Post_AddBriefDescriptionColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "44362718-9aa4-4952-91dd-7b373cd62066");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "44362718-9aa4-4952-91dd-7b373cd62066", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 37, DateTimeKind.Utc).AddTicks(1758),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 179, DateTimeKind.Utc).AddTicks(8061));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 37, DateTimeKind.Utc).AddTicks(1148),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 179, DateTimeKind.Utc).AddTicks(7237));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 9, 751, DateTimeKind.Utc).AddTicks(2118),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 17, DateTimeKind.Utc).AddTicks(377));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 9, 751, DateTimeKind.Utc).AddTicks(931),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 16, DateTimeKind.Utc).AddTicks(8043));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 6, DateTimeKind.Utc).AddTicks(85),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 133, DateTimeKind.Utc).AddTicks(278));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 5, DateTimeKind.Utc).AddTicks(9521),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 132, DateTimeKind.Utc).AddTicks(9901));

            migrationBuilder.AddColumn<string>(
                name: "BriefDescription",
                table: "Post",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 24, DateTimeKind.Utc).AddTicks(3594),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 166, DateTimeKind.Utc).AddTicks(120));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 9, 17, 51, 10, 336, DateTimeKind.Utc).AddTicks(9787), new DateTime(2021, 7, 9, 17, 51, 10, 337, DateTimeKind.Utc).AddTicks(474) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 10, 0, 51, 10, 306, DateTimeKind.Local).AddTicks(6883), new DateTime(2021, 7, 10, 0, 51, 10, 307, DateTimeKind.Local).AddTicks(2605) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "17ad37a2-d7bb-4a4d-8132-8d648cd2e8a3", 1, "7a792bd0-6f87-41e9-b32f-07f9863fb16f", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "4e497eb0-c2b6-4034-bc4c-a7614ca883cb", new DateTime(2021, 7, 10, 0, 51, 10, 309, DateTimeKind.Local).AddTicks(4662), "AQAAAAEAACcQAAAAEGK1i5Y9RFp2LcAZevExnUX6LRin3nkrxeZtAyaDkkcNwMKIEeKqB40+QBX2bSqmwQ==", "76a7be6d-5c6d-4606-8669-062424598f96", new DateTime(2021, 7, 10, 0, 51, 10, 309, DateTimeKind.Local).AddTicks(5018) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "17ad37a2-d7bb-4a4d-8132-8d648cd2e8a3", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "17ad37a2-d7bb-4a4d-8132-8d648cd2e8a3");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "17ad37a2-d7bb-4a4d-8132-8d648cd2e8a3", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "BriefDescription",
                table: "Post");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 179, DateTimeKind.Utc).AddTicks(8061),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 37, DateTimeKind.Utc).AddTicks(1758));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 179, DateTimeKind.Utc).AddTicks(7237),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 37, DateTimeKind.Utc).AddTicks(1148));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 17, DateTimeKind.Utc).AddTicks(377),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 9, 751, DateTimeKind.Utc).AddTicks(2118));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 16, DateTimeKind.Utc).AddTicks(8043),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 9, 751, DateTimeKind.Utc).AddTicks(931));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 133, DateTimeKind.Utc).AddTicks(278),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 6, DateTimeKind.Utc).AddTicks(85));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 132, DateTimeKind.Utc).AddTicks(9901),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 5, DateTimeKind.Utc).AddTicks(9521));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 6, 15, 29, 24, 166, DateTimeKind.Utc).AddTicks(120),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 9, 17, 51, 10, 24, DateTimeKind.Utc).AddTicks(3594));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 6, 15, 29, 24, 297, DateTimeKind.Utc).AddTicks(750), new DateTime(2021, 7, 6, 15, 29, 24, 297, DateTimeKind.Utc).AddTicks(1581) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 6, 22, 29, 24, 268, DateTimeKind.Local).AddTicks(1752), new DateTime(2021, 7, 6, 22, 29, 24, 268, DateTimeKind.Local).AddTicks(8941) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "44362718-9aa4-4952-91dd-7b373cd62066", 1, "a6557bc3-cb91-4593-adb6-023d61b62597", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "cd41113a-9bc4-41d0-8aea-fdf1f903037b", new DateTime(2021, 7, 6, 22, 29, 24, 276, DateTimeKind.Local).AddTicks(8021), "AQAAAAEAACcQAAAAEBPbvNOByawB7tDV+4gdDGEWbyjHw2EuR75vOfdTw6ogpXKU12yInSvZFY4N6+IUKw==", "92803590-3d50-4c69-9d0e-87b378a40380", new DateTime(2021, 7, 6, 22, 29, 24, 276, DateTimeKind.Local).AddTicks(8498) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "44362718-9aa4-4952-91dd-7b373cd62066", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
