﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Package_UpdateSaleAndCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "3c6cc89a-6762-4710-bbbe-c502efc011b5");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "3c6cc89a-6762-4710-bbbe-c502efc011b5", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 554, DateTimeKind.Utc).AddTicks(3691),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 979, DateTimeKind.Utc).AddTicks(6342));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 554, DateTimeKind.Utc).AddTicks(3425),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 979, DateTimeKind.Utc).AddTicks(5790));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 464, DateTimeKind.Utc).AddTicks(3336),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 871, DateTimeKind.Utc).AddTicks(4220));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 464, DateTimeKind.Utc).AddTicks(2776),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 871, DateTimeKind.Utc).AddTicks(3389));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 537, DateTimeKind.Utc).AddTicks(3038),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 962, DateTimeKind.Utc).AddTicks(5289));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 537, DateTimeKind.Utc).AddTicks(2750),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 962, DateTimeKind.Utc).AddTicks(4992));

            migrationBuilder.AddColumn<float>(
                name: "Cost",
                table: "PackageRequest",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "Sale",
                table: "Package",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 549, DateTimeKind.Utc).AddTicks(4583),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 974, DateTimeKind.Utc).AddTicks(1707));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 9, 23, 13, 620, DateTimeKind.Utc).AddTicks(5276), new DateTime(2021, 7, 22, 9, 23, 13, 620, DateTimeKind.Utc).AddTicks(5534) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 16, 23, 13, 588, DateTimeKind.Local).AddTicks(2502), new DateTime(2021, 7, 22, 16, 23, 13, 588, DateTimeKind.Local).AddTicks(6057) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "bba5f5c1-6403-42fe-a7bb-89b136947d5f", 1, "504e84a3-c3e9-4d55-a3bc-53df9717e74f", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "90e03c0a-ca16-4b1f-8865-8ad6c699f905", new DateTime(2021, 7, 22, 16, 23, 13, 604, DateTimeKind.Local).AddTicks(253), "AQAAAAEAACcQAAAAEJTQol+6MtpmOF468SAiKkVOcMCAnlTR98yWX7aiCOImdG8OJU0HsaLPQ/0m3C2Q3A==", "4bffdd83-b28e-4ff5-b107-b65f676af614", new DateTime(2021, 7, 22, 16, 23, 13, 604, DateTimeKind.Local).AddTicks(494) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "bba5f5c1-6403-42fe-a7bb-89b136947d5f", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "bba5f5c1-6403-42fe-a7bb-89b136947d5f");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "bba5f5c1-6403-42fe-a7bb-89b136947d5f", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "Cost",
                table: "PackageRequest");

            migrationBuilder.DropColumn(
                name: "Sale",
                table: "Package");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 979, DateTimeKind.Utc).AddTicks(6342),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 554, DateTimeKind.Utc).AddTicks(3691));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 979, DateTimeKind.Utc).AddTicks(5790),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 554, DateTimeKind.Utc).AddTicks(3425));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 871, DateTimeKind.Utc).AddTicks(4220),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 464, DateTimeKind.Utc).AddTicks(3336));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 871, DateTimeKind.Utc).AddTicks(3389),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 464, DateTimeKind.Utc).AddTicks(2776));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 962, DateTimeKind.Utc).AddTicks(5289),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 537, DateTimeKind.Utc).AddTicks(3038));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 962, DateTimeKind.Utc).AddTicks(4992),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 537, DateTimeKind.Utc).AddTicks(2750));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 974, DateTimeKind.Utc).AddTicks(1707),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 549, DateTimeKind.Utc).AddTicks(4583));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 8, 45, 11, 37, DateTimeKind.Utc).AddTicks(5600), new DateTime(2021, 7, 22, 8, 45, 11, 37, DateTimeKind.Utc).AddTicks(5869) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 15, 45, 11, 23, DateTimeKind.Local).AddTicks(5360), new DateTime(2021, 7, 22, 15, 45, 11, 23, DateTimeKind.Local).AddTicks(8573) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "3c6cc89a-6762-4710-bbbe-c502efc011b5", 1, "9c38c97c-bccc-4c17-81e7-34c0ca7a0658", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "1d1d77b9-7e59-48a2-8042-5288dd0e7edd", new DateTime(2021, 7, 22, 15, 45, 11, 27, DateTimeKind.Local).AddTicks(6640), "AQAAAAEAACcQAAAAEJ2sNpcTL6oYzlNqafNvmCTH1/VPBp/MTgtFtklmp+VLdYpVhvt5/GVAmfGDrJVoiQ==", "42c5d960-439d-4231-a2f8-79a054764867", new DateTime(2021, 7, 22, 15, 45, 11, 27, DateTimeKind.Local).AddTicks(6966) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "3c6cc89a-6762-4710-bbbe-c502efc011b5", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
