﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Chat_SenderId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "2faf4d0f-aad8-4ccc-a00c-5f6cdd8f9b0b");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "2faf4d0f-aad8-4ccc-a00c-5f6cdd8f9b0b", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 431, DateTimeKind.Utc).AddTicks(8106),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 630, DateTimeKind.Utc).AddTicks(5246));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 431, DateTimeKind.Utc).AddTicks(7800),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 630, DateTimeKind.Utc).AddTicks(4981));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 331, DateTimeKind.Utc).AddTicks(2972),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 547, DateTimeKind.Utc).AddTicks(1490));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 331, DateTimeKind.Utc).AddTicks(2354),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 547, DateTimeKind.Utc).AddTicks(927));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 425, DateTimeKind.Utc).AddTicks(3541),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 625, DateTimeKind.Utc).AddTicks(2430));

            migrationBuilder.AlterColumn<string>(
                name: "SenderId",
                table: "Chat",
                type: "nvarchar(50)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)");

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 24, 16, 0, 51, 485, DateTimeKind.Utc).AddTicks(4285), new DateTime(2021, 8, 24, 16, 0, 51, 485, DateTimeKind.Utc).AddTicks(4559) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 24, 23, 0, 51, 471, DateTimeKind.Local).AddTicks(4797), new DateTime(2021, 8, 24, 23, 0, 51, 471, DateTimeKind.Local).AddTicks(6153) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "5e0a73de-007b-4a19-bf3e-d2bbc05e876d", 1, "c9a33192-3b2f-422d-bae6-6e605d85645c", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "0185fcdb-652c-4b07-b40a-e5dfdb182420", new DateTime(2021, 8, 24, 23, 0, 51, 475, DateTimeKind.Local).AddTicks(5411), "AQAAAAEAACcQAAAAEMsJ7TY8+Z655U6L5kR9gfH0XHeA0Zwv8w11y07aVoMxL3PkP8OHdsd4K7J0zSb+Lg==", "2e0869b8-0d64-4773-af1a-73637f4d5ed5", new DateTime(2021, 8, 24, 23, 0, 51, 475, DateTimeKind.Local).AddTicks(5704) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "5e0a73de-007b-4a19-bf3e-d2bbc05e876d", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "5e0a73de-007b-4a19-bf3e-d2bbc05e876d");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "5e0a73de-007b-4a19-bf3e-d2bbc05e876d", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 630, DateTimeKind.Utc).AddTicks(5246),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 431, DateTimeKind.Utc).AddTicks(8106));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 630, DateTimeKind.Utc).AddTicks(4981),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 431, DateTimeKind.Utc).AddTicks(7800));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 547, DateTimeKind.Utc).AddTicks(1490),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 331, DateTimeKind.Utc).AddTicks(2972));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 547, DateTimeKind.Utc).AddTicks(927),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 331, DateTimeKind.Utc).AddTicks(2354));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 24, 15, 57, 34, 625, DateTimeKind.Utc).AddTicks(2430),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 24, 16, 0, 51, 425, DateTimeKind.Utc).AddTicks(3541));

            migrationBuilder.AlterColumn<string>(
                name: "SenderId",
                table: "Chat",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 24, 15, 57, 34, 680, DateTimeKind.Utc).AddTicks(356), new DateTime(2021, 8, 24, 15, 57, 34, 680, DateTimeKind.Utc).AddTicks(611) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 24, 22, 57, 34, 666, DateTimeKind.Local).AddTicks(932), new DateTime(2021, 8, 24, 22, 57, 34, 666, DateTimeKind.Local).AddTicks(2232) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "2faf4d0f-aad8-4ccc-a00c-5f6cdd8f9b0b", 1, "25bf6895-9359-4ad8-b7c0-a3bb24f643fa", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "318d2471-323c-429e-bd3f-cf7def5cf0f0", new DateTime(2021, 8, 24, 22, 57, 34, 669, DateTimeKind.Local).AddTicks(5508), "AQAAAAEAACcQAAAAEMakm4toEn4QNWdq1x+IhwHGNAO8xmh5ppelwLt0pmWEmiOOWHtqdBQ1JgzY6Qbptw==", "70423ce8-711e-41b7-b7a6-4e947e6523e7", new DateTime(2021, 8, 24, 22, 57, 34, 669, DateTimeKind.Local).AddTicks(5742) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "2faf4d0f-aad8-4ccc-a00c-5f6cdd8f9b0b", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
