﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class UpdateLeftJoin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PackageRequest_Package_PackageId",
                table: "PackageRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_PackageRequest_User_UserId",
                table: "PackageRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_UserNotification_Notification_NotificationId",
                table: "UserNotification");

            migrationBuilder.DropForeignKey(
                name: "FK_UserNotification_User_UserId",
                table: "UserNotification");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "bba5f5c1-6403-42fe-a7bb-89b136947d5f");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "bba5f5c1-6403-42fe-a7bb-89b136947d5f", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 410, DateTimeKind.Utc).AddTicks(7952),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 554, DateTimeKind.Utc).AddTicks(3691));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 410, DateTimeKind.Utc).AddTicks(7613),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 554, DateTimeKind.Utc).AddTicks(3425));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 294, DateTimeKind.Utc).AddTicks(6446),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 464, DateTimeKind.Utc).AddTicks(3336));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 294, DateTimeKind.Utc).AddTicks(5447),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 464, DateTimeKind.Utc).AddTicks(2776));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 389, DateTimeKind.Utc).AddTicks(7253),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 537, DateTimeKind.Utc).AddTicks(3038));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 389, DateTimeKind.Utc).AddTicks(6926),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 537, DateTimeKind.Utc).AddTicks(2750));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 404, DateTimeKind.Utc).AddTicks(4156),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 549, DateTimeKind.Utc).AddTicks(4583));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 9, 38, 23, 478, DateTimeKind.Utc).AddTicks(5530), new DateTime(2021, 7, 22, 9, 38, 23, 478, DateTimeKind.Utc).AddTicks(5844) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 16, 38, 23, 462, DateTimeKind.Local).AddTicks(405), new DateTime(2021, 7, 22, 16, 38, 23, 462, DateTimeKind.Local).AddTicks(4787) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "4ad82889-f218-42cc-87fa-a06fdd34bd13", 1, "18204557-89bc-4f7b-a6ff-9ee405b8a99d", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "fa1a6999-2f05-4bde-99e3-c132a217b84e", new DateTime(2021, 7, 22, 16, 38, 23, 466, DateTimeKind.Local).AddTicks(3451), "AQAAAAEAACcQAAAAEAkdFJjE7KHXlD7Shy06TnhkBigRhp2X9TRWA7a7TFvG6AnVF66KF9Xt9GkUvaWJwA==", "90504aee-b056-46a3-92f6-88623258610f", new DateTime(2021, 7, 22, 16, 38, 23, 466, DateTimeKind.Local).AddTicks(3742) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "4ad82889-f218-42cc-87fa-a06fdd34bd13", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_PackageRequest_Package_PackageId",
                table: "PackageRequest",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PackageRequest_User_UserId",
                table: "PackageRequest",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserNotification_Notification_NotificationId",
                table: "UserNotification",
                column: "NotificationId",
                principalTable: "Notification",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserNotification_User_UserId",
                table: "UserNotification",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PackageRequest_Package_PackageId",
                table: "PackageRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_PackageRequest_User_UserId",
                table: "PackageRequest");

            migrationBuilder.DropForeignKey(
                name: "FK_UserNotification_Notification_NotificationId",
                table: "UserNotification");

            migrationBuilder.DropForeignKey(
                name: "FK_UserNotification_User_UserId",
                table: "UserNotification");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "4ad82889-f218-42cc-87fa-a06fdd34bd13");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "4ad82889-f218-42cc-87fa-a06fdd34bd13", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 554, DateTimeKind.Utc).AddTicks(3691),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 410, DateTimeKind.Utc).AddTicks(7952));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 554, DateTimeKind.Utc).AddTicks(3425),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 410, DateTimeKind.Utc).AddTicks(7613));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 464, DateTimeKind.Utc).AddTicks(3336),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 294, DateTimeKind.Utc).AddTicks(6446));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 464, DateTimeKind.Utc).AddTicks(2776),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 294, DateTimeKind.Utc).AddTicks(5447));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 537, DateTimeKind.Utc).AddTicks(3038),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 389, DateTimeKind.Utc).AddTicks(7253));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 537, DateTimeKind.Utc).AddTicks(2750),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 389, DateTimeKind.Utc).AddTicks(6926));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 23, 13, 549, DateTimeKind.Utc).AddTicks(4583),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 404, DateTimeKind.Utc).AddTicks(4156));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 9, 23, 13, 620, DateTimeKind.Utc).AddTicks(5276), new DateTime(2021, 7, 22, 9, 23, 13, 620, DateTimeKind.Utc).AddTicks(5534) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 16, 23, 13, 588, DateTimeKind.Local).AddTicks(2502), new DateTime(2021, 7, 22, 16, 23, 13, 588, DateTimeKind.Local).AddTicks(6057) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "bba5f5c1-6403-42fe-a7bb-89b136947d5f", 1, "504e84a3-c3e9-4d55-a3bc-53df9717e74f", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "90e03c0a-ca16-4b1f-8865-8ad6c699f905", new DateTime(2021, 7, 22, 16, 23, 13, 604, DateTimeKind.Local).AddTicks(253), "AQAAAAEAACcQAAAAEJTQol+6MtpmOF468SAiKkVOcMCAnlTR98yWX7aiCOImdG8OJU0HsaLPQ/0m3C2Q3A==", "4bffdd83-b28e-4ff5-b107-b65f676af614", new DateTime(2021, 7, 22, 16, 23, 13, 604, DateTimeKind.Local).AddTicks(494) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "bba5f5c1-6403-42fe-a7bb-89b136947d5f", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_PackageRequest_Package_PackageId",
                table: "PackageRequest",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PackageRequest_User_UserId",
                table: "PackageRequest",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserNotification_Notification_NotificationId",
                table: "UserNotification",
                column: "NotificationId",
                principalTable: "Notification",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserNotification_User_UserId",
                table: "UserNotification",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
