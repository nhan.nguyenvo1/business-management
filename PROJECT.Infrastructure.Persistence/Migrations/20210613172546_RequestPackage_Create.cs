﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class RequestPackage_Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "f649e9e0-50db-4757-b405-eabf41683639");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "f649e9e0-50db-4757-b405-eabf41683639", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 689, DateTimeKind.Utc).AddTicks(3546),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 151, DateTimeKind.Utc).AddTicks(5747));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 689, DateTimeKind.Utc).AddTicks(3144),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 151, DateTimeKind.Utc).AddTicks(5394));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 599, DateTimeKind.Utc).AddTicks(3429),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 66, DateTimeKind.Utc).AddTicks(8194));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 599, DateTimeKind.Utc).AddTicks(2693),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 66, DateTimeKind.Utc).AddTicks(7522));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 669, DateTimeKind.Utc).AddTicks(9697),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 132, DateTimeKind.Utc).AddTicks(1468));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 669, DateTimeKind.Utc).AddTicks(9285),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 132, DateTimeKind.Utc).AddTicks(1119));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 691, DateTimeKind.Utc).AddTicks(3497),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 153, DateTimeKind.Utc).AddTicks(6005));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 682, DateTimeKind.Utc).AddTicks(8615),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 143, DateTimeKind.Utc).AddTicks(9247));

            migrationBuilder.CreateTable(
                name: "PackageRequest",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    PackageId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false, defaultValue: 1),
                    ClosedDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackageRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PackageRequest_Package_PackageId",
                        column: x => x.PackageId,
                        principalTable: "Package",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PackageRequest_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 17, 25, 44, 754, DateTimeKind.Utc).AddTicks(1014), new DateTime(2021, 6, 13, 17, 25, 44, 754, DateTimeKind.Utc).AddTicks(1303) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 14, 0, 25, 44, 739, DateTimeKind.Local).AddTicks(5891), new DateTime(2021, 6, 14, 0, 25, 44, 739, DateTimeKind.Local).AddTicks(6194) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "98a2fb2c-7179-4df0-a071-f0cc8d221630", 1, "072b6183-94dd-48c8-863f-43cf93de1f9d", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "1a26f189-5a8a-44e7-a9a7-36765af757b5", new DateTime(2021, 6, 14, 0, 25, 44, 741, DateTimeKind.Local).AddTicks(2703), "AQAAAAEAACcQAAAAENVLOx8dwZE6jew3WgAnEfPSpQ2bLTr5jJxrgOCImzOokI3MUg3W7A1qUMsvQeS0HQ==", "f9172dcb-57cb-45ed-8a5d-b75750aa59c2", new DateTime(2021, 6, 14, 0, 25, 44, 741, DateTimeKind.Local).AddTicks(2983) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "98a2fb2c-7179-4df0-a071-f0cc8d221630", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_PackageRequest_PackageId",
                table: "PackageRequest",
                column: "PackageId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PackageRequest_UserId",
                table: "PackageRequest",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PackageRequest");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "98a2fb2c-7179-4df0-a071-f0cc8d221630");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "98a2fb2c-7179-4df0-a071-f0cc8d221630", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 151, DateTimeKind.Utc).AddTicks(5747),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 689, DateTimeKind.Utc).AddTicks(3546));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 151, DateTimeKind.Utc).AddTicks(5394),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 689, DateTimeKind.Utc).AddTicks(3144));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 66, DateTimeKind.Utc).AddTicks(8194),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 599, DateTimeKind.Utc).AddTicks(3429));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 66, DateTimeKind.Utc).AddTicks(7522),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 599, DateTimeKind.Utc).AddTicks(2693));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 132, DateTimeKind.Utc).AddTicks(1468),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 669, DateTimeKind.Utc).AddTicks(9697));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 132, DateTimeKind.Utc).AddTicks(1119),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 669, DateTimeKind.Utc).AddTicks(9285));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 153, DateTimeKind.Utc).AddTicks(6005),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 691, DateTimeKind.Utc).AddTicks(3497));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 143, DateTimeKind.Utc).AddTicks(9247),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 25, 44, 682, DateTimeKind.Utc).AddTicks(8615));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 17, 4, 1, 212, DateTimeKind.Utc).AddTicks(9197), new DateTime(2021, 6, 13, 17, 4, 1, 212, DateTimeKind.Utc).AddTicks(9514) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 14, 0, 4, 1, 197, DateTimeKind.Local).AddTicks(547), new DateTime(2021, 6, 14, 0, 4, 1, 197, DateTimeKind.Local).AddTicks(1007) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "f649e9e0-50db-4757-b405-eabf41683639", 1, "4cf387ab-59cf-4fb6-a226-652ad6180ec8", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "70281250-9bb5-460e-ba5f-4a97110801ed", new DateTime(2021, 6, 14, 0, 4, 1, 199, DateTimeKind.Local).AddTicks(6092), "AQAAAAEAACcQAAAAEF9klYFUi6DoH7QRQaVvm6Kn+0NtlylQ9eJOsigl7JhvOjeLJpQBaHoQE2kbG/d6uA==", "47943184-d688-4509-9fe1-65c610372784", new DateTime(2021, 6, 14, 0, 4, 1, 199, DateTimeKind.Local).AddTicks(6373) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "f649e9e0-50db-4757-b405-eabf41683639", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
