﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class TaskUser_UpdateForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTask_Task_TaskId",
                table: "UserTask");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTask_User_UserId",
                table: "UserTask");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "3532412b-2759-4c20-aa5d-ee2a9c0e0b91");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "3532412b-2759-4c20-aa5d-ee2a9c0e0b91", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 543, DateTimeKind.Utc).AddTicks(5360),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 677, DateTimeKind.Utc).AddTicks(2311));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 543, DateTimeKind.Utc).AddTicks(5002),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 677, DateTimeKind.Utc).AddTicks(1928));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 440, DateTimeKind.Utc).AddTicks(1796),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 570, DateTimeKind.Utc).AddTicks(8705));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 440, DateTimeKind.Utc).AddTicks(1119),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 570, DateTimeKind.Utc).AddTicks(8041));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 519, DateTimeKind.Utc).AddTicks(5348),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 654, DateTimeKind.Utc).AddTicks(3415));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 519, DateTimeKind.Utc).AddTicks(4922),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 654, DateTimeKind.Utc).AddTicks(3030));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 533, DateTimeKind.Utc).AddTicks(9810),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 670, DateTimeKind.Utc).AddTicks(1537));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 14, 48, 23, 605, DateTimeKind.Utc).AddTicks(17), new DateTime(2021, 7, 27, 14, 48, 23, 605, DateTimeKind.Utc).AddTicks(307) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 21, 48, 23, 593, DateTimeKind.Local).AddTicks(1870), new DateTime(2021, 7, 27, 21, 48, 23, 593, DateTimeKind.Local).AddTicks(5475) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "f08c171a-096c-4337-afb8-2fa3e4777168", 1, "e2546247-e981-4979-9ee4-83dc1cffcba3", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "4bdfbbd8-8af2-424b-865d-5d6c44f69029", new DateTime(2021, 7, 27, 21, 48, 23, 594, DateTimeKind.Local).AddTicks(9050), "AQAAAAEAACcQAAAAENpbX9fQuOGAbpgbmd8nOwQJpgaOoGboGI41aOzWH58bbF0VaUWm9nt42q5+3C4SVQ==", "2802b5c5-8c4c-49a9-9fa4-3768b3e12d2e", new DateTime(2021, 7, 27, 21, 48, 23, 594, DateTimeKind.Local).AddTicks(9332) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "f08c171a-096c-4337-afb8-2fa3e4777168", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_UserTask_Task_TaskId",
                table: "UserTask",
                column: "TaskId",
                principalTable: "Task",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTask_User_UserId",
                table: "UserTask",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTask_Task_TaskId",
                table: "UserTask");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTask_User_UserId",
                table: "UserTask");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "f08c171a-096c-4337-afb8-2fa3e4777168");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "f08c171a-096c-4337-afb8-2fa3e4777168", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 677, DateTimeKind.Utc).AddTicks(2311),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 543, DateTimeKind.Utc).AddTicks(5360));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 677, DateTimeKind.Utc).AddTicks(1928),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 543, DateTimeKind.Utc).AddTicks(5002));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 570, DateTimeKind.Utc).AddTicks(8705),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 440, DateTimeKind.Utc).AddTicks(1796));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 570, DateTimeKind.Utc).AddTicks(8041),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 440, DateTimeKind.Utc).AddTicks(1119));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 654, DateTimeKind.Utc).AddTicks(3415),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 519, DateTimeKind.Utc).AddTicks(5348));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 654, DateTimeKind.Utc).AddTicks(3030),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 519, DateTimeKind.Utc).AddTicks(4922));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 670, DateTimeKind.Utc).AddTicks(1537),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 48, 23, 533, DateTimeKind.Utc).AddTicks(9810));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 14, 42, 35, 750, DateTimeKind.Utc).AddTicks(9801), new DateTime(2021, 7, 27, 14, 42, 35, 751, DateTimeKind.Utc).AddTicks(158) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 21, 42, 35, 735, DateTimeKind.Local).AddTicks(9962), new DateTime(2021, 7, 27, 21, 42, 35, 736, DateTimeKind.Local).AddTicks(3757) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "3532412b-2759-4c20-aa5d-ee2a9c0e0b91", 1, "d1e5b93e-aad1-4104-b9c1-91d9248a4aee", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "ca53cec8-18db-4a87-82a6-e29b83bb1445", new DateTime(2021, 7, 27, 21, 42, 35, 738, DateTimeKind.Local).AddTicks(2371), "AQAAAAEAACcQAAAAEKN48AEIBul8bjIwC5NNo7bBwEL3jcjFzpeKqu4ZdUtwqOZKHnHYmjSyo1upv0Q4ZQ==", "c74c7eeb-10ce-4f55-8d64-94a2e90f82e5", new DateTime(2021, 7, 27, 21, 42, 35, 738, DateTimeKind.Local).AddTicks(2939) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "3532412b-2759-4c20-aa5d-ee2a9c0e0b91", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_UserTask_Task_TaskId",
                table: "UserTask",
                column: "TaskId",
                principalTable: "Task",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTask_User_UserId",
                table: "UserTask",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
