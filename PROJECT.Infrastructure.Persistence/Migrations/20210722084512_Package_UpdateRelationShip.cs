﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Package_UpdateRelationShip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PackageRequest_User_UserId",
                table: "PackageRequest");

            migrationBuilder.DropIndex(
                name: "IX_PackageRequest_PackageId",
                table: "PackageRequest");

            migrationBuilder.DropIndex(
                name: "IX_PackageRequest_UserId",
                table: "PackageRequest");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "60d4c178-6bc3-455d-ad90-1d3528460ce3");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "60d4c178-6bc3-455d-ad90-1d3528460ce3", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 979, DateTimeKind.Utc).AddTicks(6342),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 637, DateTimeKind.Utc).AddTicks(7611));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 979, DateTimeKind.Utc).AddTicks(5790),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 637, DateTimeKind.Utc).AddTicks(6711));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 871, DateTimeKind.Utc).AddTicks(4220),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 512, DateTimeKind.Utc).AddTicks(5994));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 871, DateTimeKind.Utc).AddTicks(3389),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 512, DateTimeKind.Utc).AddTicks(5193));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 962, DateTimeKind.Utc).AddTicks(5289),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 605, DateTimeKind.Utc).AddTicks(6869));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 962, DateTimeKind.Utc).AddTicks(4992),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 605, DateTimeKind.Utc).AddTicks(6484));

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "PackageRequest",
                type: "nvarchar(50)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 974, DateTimeKind.Utc).AddTicks(1707),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 625, DateTimeKind.Utc).AddTicks(7226));

            migrationBuilder.AddUniqueConstraint(
                name: "AK_PackageRequest_UserId_PackageId",
                table: "PackageRequest",
                columns: new[] { "UserId", "PackageId" });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 8, 45, 11, 37, DateTimeKind.Utc).AddTicks(5600), new DateTime(2021, 7, 22, 8, 45, 11, 37, DateTimeKind.Utc).AddTicks(5869) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 15, 45, 11, 23, DateTimeKind.Local).AddTicks(5360), new DateTime(2021, 7, 22, 15, 45, 11, 23, DateTimeKind.Local).AddTicks(8573) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "3c6cc89a-6762-4710-bbbe-c502efc011b5", 1, "9c38c97c-bccc-4c17-81e7-34c0ca7a0658", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "1d1d77b9-7e59-48a2-8042-5288dd0e7edd", new DateTime(2021, 7, 22, 15, 45, 11, 27, DateTimeKind.Local).AddTicks(6640), "AQAAAAEAACcQAAAAEJ2sNpcTL6oYzlNqafNvmCTH1/VPBp/MTgtFtklmp+VLdYpVhvt5/GVAmfGDrJVoiQ==", "42c5d960-439d-4231-a2f8-79a054764867", new DateTime(2021, 7, 22, 15, 45, 11, 27, DateTimeKind.Local).AddTicks(6966) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "3c6cc89a-6762-4710-bbbe-c502efc011b5", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_PackageRequest_PackageId",
                table: "PackageRequest",
                column: "PackageId");

            migrationBuilder.AddForeignKey(
                name: "FK_PackageRequest_User_UserId",
                table: "PackageRequest",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PackageRequest_User_UserId",
                table: "PackageRequest");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_PackageRequest_UserId_PackageId",
                table: "PackageRequest");

            migrationBuilder.DropIndex(
                name: "IX_PackageRequest_PackageId",
                table: "PackageRequest");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "3c6cc89a-6762-4710-bbbe-c502efc011b5");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "3c6cc89a-6762-4710-bbbe-c502efc011b5", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 637, DateTimeKind.Utc).AddTicks(7611),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 979, DateTimeKind.Utc).AddTicks(6342));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 637, DateTimeKind.Utc).AddTicks(6711),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 979, DateTimeKind.Utc).AddTicks(5790));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 512, DateTimeKind.Utc).AddTicks(5994),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 871, DateTimeKind.Utc).AddTicks(4220));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 512, DateTimeKind.Utc).AddTicks(5193),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 871, DateTimeKind.Utc).AddTicks(3389));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 605, DateTimeKind.Utc).AddTicks(6869),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 962, DateTimeKind.Utc).AddTicks(5289));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 605, DateTimeKind.Utc).AddTicks(6484),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 962, DateTimeKind.Utc).AddTicks(4992));

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "PackageRequest",
                type: "nvarchar(50)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 17, 17, 7, 625, DateTimeKind.Utc).AddTicks(7226),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 8, 45, 10, 974, DateTimeKind.Utc).AddTicks(1707));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 17, 17, 7, 732, DateTimeKind.Utc).AddTicks(6230), new DateTime(2021, 7, 20, 17, 17, 7, 732, DateTimeKind.Utc).AddTicks(6950) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 21, 0, 17, 7, 714, DateTimeKind.Local).AddTicks(9750), new DateTime(2021, 7, 21, 0, 17, 7, 716, DateTimeKind.Local).AddTicks(43) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "60d4c178-6bc3-455d-ad90-1d3528460ce3", 1, "ba8d055f-7008-441e-a5cb-87b489beab51", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "bf1bfb3b-0c4d-4b2d-a17b-5e0067decbad", new DateTime(2021, 7, 21, 0, 17, 7, 717, DateTimeKind.Local).AddTicks(9424), "AQAAAAEAACcQAAAAEMyFrOVad3wR7xnMxBpYoUZOLzemNxp4kys7aezazQLgr7sf5V9m1mhyP/j3FtmukA==", "c62048aa-7ea0-4336-9c5e-28b24b0c9c25", new DateTime(2021, 7, 21, 0, 17, 7, 717, DateTimeKind.Local).AddTicks(9737) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "60d4c178-6bc3-455d-ad90-1d3528460ce3", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_PackageRequest_PackageId",
                table: "PackageRequest",
                column: "PackageId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PackageRequest_UserId",
                table: "PackageRequest",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_PackageRequest_User_UserId",
                table: "PackageRequest",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
