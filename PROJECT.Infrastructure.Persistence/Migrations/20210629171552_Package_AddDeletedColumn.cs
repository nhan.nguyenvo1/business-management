﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Package_AddDeletedColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "c8dedae9-b40d-49c4-8de8-b98f57ba2ce1");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "c8dedae9-b40d-49c4-8de8-b98f57ba2ce1", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 284, DateTimeKind.Utc).AddTicks(531),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 431, DateTimeKind.Utc).AddTicks(6324));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 284, DateTimeKind.Utc).AddTicks(179),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 431, DateTimeKind.Utc).AddTicks(6015));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 193, DateTimeKind.Utc).AddTicks(5099),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 334, DateTimeKind.Utc).AddTicks(6589));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 193, DateTimeKind.Utc).AddTicks(4441),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 334, DateTimeKind.Utc).AddTicks(5868));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 266, DateTimeKind.Utc).AddTicks(2500),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 412, DateTimeKind.Utc).AddTicks(2833));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 266, DateTimeKind.Utc).AddTicks(1764),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 412, DateTimeKind.Utc).AddTicks(2507));

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "Package",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 285, DateTimeKind.Utc).AddTicks(9251),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 433, DateTimeKind.Utc).AddTicks(8717));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 277, DateTimeKind.Utc).AddTicks(9448),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 425, DateTimeKind.Utc).AddTicks(5766));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 29, 17, 15, 51, 337, DateTimeKind.Utc).AddTicks(1966), new DateTime(2021, 6, 29, 17, 15, 51, 337, DateTimeKind.Utc).AddTicks(2255) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 30, 0, 15, 51, 326, DateTimeKind.Local).AddTicks(1922), new DateTime(2021, 6, 30, 0, 15, 51, 326, DateTimeKind.Local).AddTicks(5362) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "d577b81d-0fd8-45cf-861b-d2e2559589ff", 1, "e71cdaef-23ee-4670-bf2e-ffc20c2c1cc6", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "06ee8863-b75b-4c15-affb-67af797f68f7", new DateTime(2021, 6, 30, 0, 15, 51, 327, DateTimeKind.Local).AddTicks(7986), "AQAAAAEAACcQAAAAEKhTGuP+WhJtecxk0FRARJuQM+fBBedpVfzbwRA3qOLdHIpe6E29nxl+hv6alMzK/g==", "b4151445-83ea-4b9c-b40a-5f3abb57372c", new DateTime(2021, 6, 30, 0, 15, 51, 327, DateTimeKind.Local).AddTicks(8257) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "d577b81d-0fd8-45cf-861b-d2e2559589ff", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "d577b81d-0fd8-45cf-861b-d2e2559589ff");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "d577b81d-0fd8-45cf-861b-d2e2559589ff", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "Package");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 431, DateTimeKind.Utc).AddTicks(6324),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 284, DateTimeKind.Utc).AddTicks(531));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 431, DateTimeKind.Utc).AddTicks(6015),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 284, DateTimeKind.Utc).AddTicks(179));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 334, DateTimeKind.Utc).AddTicks(6589),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 193, DateTimeKind.Utc).AddTicks(5099));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 334, DateTimeKind.Utc).AddTicks(5868),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 193, DateTimeKind.Utc).AddTicks(4441));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 412, DateTimeKind.Utc).AddTicks(2833),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 266, DateTimeKind.Utc).AddTicks(2500));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 412, DateTimeKind.Utc).AddTicks(2507),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 266, DateTimeKind.Utc).AddTicks(1764));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 433, DateTimeKind.Utc).AddTicks(8717),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 285, DateTimeKind.Utc).AddTicks(9251));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 29, 14, 4, 23, 425, DateTimeKind.Utc).AddTicks(5766),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 29, 17, 15, 51, 277, DateTimeKind.Utc).AddTicks(9448));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 29, 14, 4, 23, 489, DateTimeKind.Utc).AddTicks(5309), new DateTime(2021, 6, 29, 14, 4, 23, 489, DateTimeKind.Utc).AddTicks(5747) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 29, 21, 4, 23, 476, DateTimeKind.Local).AddTicks(3631), new DateTime(2021, 6, 29, 21, 4, 23, 476, DateTimeKind.Local).AddTicks(7227) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "c8dedae9-b40d-49c4-8de8-b98f57ba2ce1", 1, "db3b8efa-701d-4b93-8cf1-6200dc45d65a", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "34fd68d9-d1c3-49f8-93bd-1bebdc0e0c5e", new DateTime(2021, 6, 29, 21, 4, 23, 478, DateTimeKind.Local).AddTicks(506), "AQAAAAEAACcQAAAAEIfvR4vKmTPaaxbieYgyy51ziIjs/sSZ06tnIHzuBWkLxEa+/5Nn7ZVoEzm/4KbKQQ==", "410d2365-7d35-4dfd-80e4-0fbc506690ad", new DateTime(2021, 6, 29, 21, 4, 23, 478, DateTimeKind.Local).AddTicks(771) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "c8dedae9-b40d-49c4-8de8-b98f57ba2ce1", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
