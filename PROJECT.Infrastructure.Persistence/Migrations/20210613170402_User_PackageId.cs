﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class User_PackageId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Package_PackageId",
                table: "Company");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "504350f7-cdb5-4e6c-9106-2ef4115f5d9a");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "504350f7-cdb5-4e6c-9106-2ef4115f5d9a", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 151, DateTimeKind.Utc).AddTicks(5747),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 996, DateTimeKind.Utc).AddTicks(3374));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 151, DateTimeKind.Utc).AddTicks(5394),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 996, DateTimeKind.Utc).AddTicks(2978));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 66, DateTimeKind.Utc).AddTicks(8194),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 908, DateTimeKind.Utc).AddTicks(8526));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 66, DateTimeKind.Utc).AddTicks(7522),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 908, DateTimeKind.Utc).AddTicks(7855));

            migrationBuilder.AddColumn<int>(
                name: "PackageId",
                table: "User",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 132, DateTimeKind.Utc).AddTicks(1468),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 977, DateTimeKind.Utc).AddTicks(4507));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 132, DateTimeKind.Utc).AddTicks(1119),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 977, DateTimeKind.Utc).AddTicks(4154));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 153, DateTimeKind.Utc).AddTicks(6005),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 998, DateTimeKind.Utc).AddTicks(3266));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 143, DateTimeKind.Utc).AddTicks(9247),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 990, DateTimeKind.Utc).AddTicks(3643));

            migrationBuilder.AlterColumn<int>(
                name: "PackageId",
                table: "Company",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "PackageId", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 17, 4, 1, 212, DateTimeKind.Utc).AddTicks(9197), null, new DateTime(2021, 6, 13, 17, 4, 1, 212, DateTimeKind.Utc).AddTicks(9514) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 14, 0, 4, 1, 197, DateTimeKind.Local).AddTicks(547), new DateTime(2021, 6, 14, 0, 4, 1, 197, DateTimeKind.Local).AddTicks(1007) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "f649e9e0-50db-4757-b405-eabf41683639", 1, "4cf387ab-59cf-4fb6-a226-652ad6180ec8", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PackageId", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "70281250-9bb5-460e-ba5f-4a97110801ed", new DateTime(2021, 6, 14, 0, 4, 1, 199, DateTimeKind.Local).AddTicks(6092), 1, "AQAAAAEAACcQAAAAEF9klYFUi6DoH7QRQaVvm6Kn+0NtlylQ9eJOsigl7JhvOjeLJpQBaHoQE2kbG/d6uA==", "47943184-d688-4509-9fe1-65c610372784", new DateTime(2021, 6, 14, 0, 4, 1, 199, DateTimeKind.Local).AddTicks(6373) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "f649e9e0-50db-4757-b405-eabf41683639", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.CreateIndex(
                name: "IX_User_PackageId",
                table: "User",
                column: "PackageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Package_PackageId",
                table: "Company",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User_Package_PackageId",
                table: "User",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Package_PackageId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_User_Package_PackageId",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_User_PackageId",
                table: "User");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "f649e9e0-50db-4757-b405-eabf41683639");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "f649e9e0-50db-4757-b405-eabf41683639", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.DropColumn(
                name: "PackageId",
                table: "User");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 996, DateTimeKind.Utc).AddTicks(3374),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 151, DateTimeKind.Utc).AddTicks(5747));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 996, DateTimeKind.Utc).AddTicks(2978),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 151, DateTimeKind.Utc).AddTicks(5394));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 908, DateTimeKind.Utc).AddTicks(8526),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 66, DateTimeKind.Utc).AddTicks(8194));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 908, DateTimeKind.Utc).AddTicks(7855),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 66, DateTimeKind.Utc).AddTicks(7522));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 977, DateTimeKind.Utc).AddTicks(4507),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 132, DateTimeKind.Utc).AddTicks(1468));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 977, DateTimeKind.Utc).AddTicks(4154),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 132, DateTimeKind.Utc).AddTicks(1119));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 998, DateTimeKind.Utc).AddTicks(3266),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 153, DateTimeKind.Utc).AddTicks(6005));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 13, 13, 54, 34, 990, DateTimeKind.Utc).AddTicks(3643),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 13, 17, 4, 1, 143, DateTimeKind.Utc).AddTicks(9247));

            migrationBuilder.AlterColumn<int>(
                name: "PackageId",
                table: "Company",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "PackageId", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 13, 54, 35, 59, DateTimeKind.Utc).AddTicks(2111), 1, new DateTime(2021, 6, 13, 13, 54, 35, 59, DateTimeKind.Utc).AddTicks(2440) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 13, 20, 54, 35, 43, DateTimeKind.Local).AddTicks(3079), new DateTime(2021, 6, 13, 20, 54, 35, 43, DateTimeKind.Local).AddTicks(6809) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "504350f7-cdb5-4e6c-9106-2ef4115f5d9a", 1, "1bed2459-5c33-4977-851e-3d4b14aeff06", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "31e4f64b-c71e-40de-a835-3a9bad36cad0", new DateTime(2021, 6, 13, 20, 54, 35, 45, DateTimeKind.Local).AddTicks(2634), "AQAAAAEAACcQAAAAEOzvSveLNRXTKxKZk79Y4GGKb9ZjgcoS/ZKdn5kl/WntAnsVqbHfZ/p66BF5JhjX2w==", "7cf801d1-c261-4c83-93d8-cf322d0a7ffd", new DateTime(2021, 6, 13, 20, 54, 35, 45, DateTimeKind.Local).AddTicks(2905) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "504350f7-cdb5-4e6c-9106-2ef4115f5d9a", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Package_PackageId",
                table: "Company",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
