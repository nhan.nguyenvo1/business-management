﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Contact_CreateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "f3857cea-4461-44ed-9f41-8771b2dfe8ab");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "f3857cea-4461-44ed-9f41-8771b2dfe8ab", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 881, DateTimeKind.Utc).AddTicks(4771),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 605, DateTimeKind.Utc).AddTicks(6538));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 881, DateTimeKind.Utc).AddTicks(4393),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 605, DateTimeKind.Utc).AddTicks(6137));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 777, DateTimeKind.Utc).AddTicks(3678),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 504, DateTimeKind.Utc).AddTicks(1783));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 777, DateTimeKind.Utc).AddTicks(2976),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 504, DateTimeKind.Utc).AddTicks(1038));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 860, DateTimeKind.Utc).AddTicks(130),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 584, DateTimeKind.Utc).AddTicks(3922));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 859, DateTimeKind.Utc).AddTicks(9684),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 584, DateTimeKind.Utc).AddTicks(3595));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 873, DateTimeKind.Utc).AddTicks(9700),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 598, DateTimeKind.Utc).AddTicks(6346));

            migrationBuilder.CreateTable(
                name: "Contact",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Address = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: false),
                    MoreDescription = table.Column<string>(type: "nvarchar(600)", maxLength: 600, nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    IsDefault = table.Column<bool>(type: "bit", nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 14, 41, 13, 944, DateTimeKind.Utc).AddTicks(8333), new DateTime(2021, 7, 20, 14, 41, 13, 944, DateTimeKind.Utc).AddTicks(8698) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 21, 41, 13, 930, DateTimeKind.Local).AddTicks(9183), new DateTime(2021, 7, 20, 21, 41, 13, 931, DateTimeKind.Local).AddTicks(3185) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "fb32c61d-57f1-4c20-afb4-a7a30e799973", 1, "31c6d49a-869e-487c-af78-80bf0068b62d", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "431bfd81-1090-47fb-9978-6d035bae6e4b", new DateTime(2021, 7, 20, 21, 41, 13, 932, DateTimeKind.Local).AddTicks(7026), "AQAAAAEAACcQAAAAEHO6sd94+CUtlyZorzNdxfgT+wJlK4ufLD7FZyUIOcgG4asZ7bAtTVY0DDbUpBtLXg==", "7f28f5fd-2afc-4b71-88ef-dace244a64a8", new DateTime(2021, 7, 20, 21, 41, 13, 932, DateTimeKind.Local).AddTicks(7313) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "fb32c61d-57f1-4c20-afb4-a7a30e799973", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contact");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "fb32c61d-57f1-4c20-afb4-a7a30e799973");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "fb32c61d-57f1-4c20-afb4-a7a30e799973", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 605, DateTimeKind.Utc).AddTicks(6538),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 881, DateTimeKind.Utc).AddTicks(4771));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 605, DateTimeKind.Utc).AddTicks(6137),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 881, DateTimeKind.Utc).AddTicks(4393));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 504, DateTimeKind.Utc).AddTicks(1783),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 777, DateTimeKind.Utc).AddTicks(3678));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 504, DateTimeKind.Utc).AddTicks(1038),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 777, DateTimeKind.Utc).AddTicks(2976));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 584, DateTimeKind.Utc).AddTicks(3922),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 860, DateTimeKind.Utc).AddTicks(130));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 584, DateTimeKind.Utc).AddTicks(3595),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 859, DateTimeKind.Utc).AddTicks(9684));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 18, 9, 11, 15, 598, DateTimeKind.Utc).AddTicks(6346),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 873, DateTimeKind.Utc).AddTicks(9700));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 18, 9, 11, 15, 687, DateTimeKind.Utc).AddTicks(5713), new DateTime(2021, 7, 18, 9, 11, 15, 687, DateTimeKind.Utc).AddTicks(6220) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 18, 16, 11, 15, 659, DateTimeKind.Local).AddTicks(3434), new DateTime(2021, 7, 18, 16, 11, 15, 659, DateTimeKind.Local).AddTicks(8528) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "f3857cea-4461-44ed-9f41-8771b2dfe8ab", 1, "894ae908-d4f8-43ab-b0e0-7bbf7a5d96f3", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "66de9660-9b92-4263-bc18-a4ace6c2a095", new DateTime(2021, 7, 18, 16, 11, 15, 661, DateTimeKind.Local).AddTicks(5975), "AQAAAAEAACcQAAAAENfmWeoWPLFOiOfnsUETFAzH2aiZSm/c/FXRLyT80mgk/QF8FfGIznWoIf/YIpqzYw==", "f48f9df2-7acb-4c57-af14-613363dc5204", new DateTime(2021, 7, 18, 16, 11, 15, 661, DateTimeKind.Local).AddTicks(6445) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "f3857cea-4461-44ed-9f41-8771b2dfe8ab", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
