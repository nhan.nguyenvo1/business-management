﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class AppUser_SetPackageIdNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_Package_PackageId",
                table: "User");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "fbaa81db-1688-41cd-96d1-167255ce301a");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "fbaa81db-1688-41cd-96d1-167255ce301a", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 961, DateTimeKind.Utc).AddTicks(7958),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 760, DateTimeKind.Utc).AddTicks(2440));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 961, DateTimeKind.Utc).AddTicks(7513),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 760, DateTimeKind.Utc).AddTicks(2028));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 836, DateTimeKind.Utc).AddTicks(7453),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 677, DateTimeKind.Utc).AddTicks(5975));

            migrationBuilder.AlterColumn<int>(
                name: "PackageId",
                table: "User",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 836, DateTimeKind.Utc).AddTicks(6321),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 677, DateTimeKind.Utc).AddTicks(5339));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 932, DateTimeKind.Utc).AddTicks(8503),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 741, DateTimeKind.Utc).AddTicks(8921));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 932, DateTimeKind.Utc).AddTicks(7990),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 741, DateTimeKind.Utc).AddTicks(8623));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 964, DateTimeKind.Utc).AddTicks(8721),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 762, DateTimeKind.Utc).AddTicks(5123));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 951, DateTimeKind.Utc).AddTicks(6685),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 754, DateTimeKind.Utc).AddTicks(1405));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 24, 16, 1, 3, 39, DateTimeKind.Utc).AddTicks(6862), new DateTime(2021, 6, 24, 16, 1, 3, 39, DateTimeKind.Utc).AddTicks(7210) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 24, 23, 1, 3, 26, DateTimeKind.Local).AddTicks(6822), new DateTime(2021, 6, 24, 23, 1, 3, 27, DateTimeKind.Local).AddTicks(1163) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "a28c974e-8222-40b9-917b-5b936bda1e06", 1, "44205158-9269-4129-8c5f-6217c2362ecb", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "9f10e338-dab0-4d2e-ba69-fec226d6e915", new DateTime(2021, 6, 24, 23, 1, 3, 28, DateTimeKind.Local).AddTicks(6291), "AQAAAAEAACcQAAAAEJX3xBg/k+SyjqzOfgupDhoPmfPyac4Sj0PJVTN5WBtivdqi4Hc37fY4QfWWNmefJA==", "5f48afde-85b5-40f3-a8b0-6027c9615575", new DateTime(2021, 6, 24, 23, 1, 3, 28, DateTimeKind.Local).AddTicks(6603) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "a28c974e-8222-40b9-917b-5b936bda1e06", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_User_Package_PackageId",
                table: "User",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_Package_PackageId",
                table: "User");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "a28c974e-8222-40b9-917b-5b936bda1e06");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "a28c974e-8222-40b9-917b-5b936bda1e06", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 760, DateTimeKind.Utc).AddTicks(2440),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 961, DateTimeKind.Utc).AddTicks(7958));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 760, DateTimeKind.Utc).AddTicks(2028),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 961, DateTimeKind.Utc).AddTicks(7513));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 677, DateTimeKind.Utc).AddTicks(5975),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 836, DateTimeKind.Utc).AddTicks(7453));

            migrationBuilder.AlterColumn<int>(
                name: "PackageId",
                table: "User",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 677, DateTimeKind.Utc).AddTicks(5339),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 836, DateTimeKind.Utc).AddTicks(6321));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 741, DateTimeKind.Utc).AddTicks(8921),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 932, DateTimeKind.Utc).AddTicks(8503));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 741, DateTimeKind.Utc).AddTicks(8623),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 932, DateTimeKind.Utc).AddTicks(7990));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Notification",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 762, DateTimeKind.Utc).AddTicks(5123),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 964, DateTimeKind.Utc).AddTicks(8721));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 6, 15, 20, 32, 28, 754, DateTimeKind.Utc).AddTicks(1405),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 6, 24, 16, 1, 2, 951, DateTimeKind.Utc).AddTicks(6685));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 15, 20, 32, 28, 819, DateTimeKind.Utc).AddTicks(2516), new DateTime(2021, 6, 15, 20, 32, 28, 819, DateTimeKind.Utc).AddTicks(2806) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 6, 16, 3, 32, 28, 803, DateTimeKind.Local).AddTicks(6325), new DateTime(2021, 6, 16, 3, 32, 28, 803, DateTimeKind.Local).AddTicks(6613) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "fbaa81db-1688-41cd-96d1-167255ce301a", 1, "c004c118-879d-4a92-847c-b20fcfe289b3", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "02883587-9ece-4806-8c9f-c85ade278cbf", new DateTime(2021, 6, 16, 3, 32, 28, 805, DateTimeKind.Local).AddTicks(2774), "AQAAAAEAACcQAAAAEHmSgzXwYRy8aZR48bL7gb3+wz7Tfz4tkFzTeAR1bawB39v9uRVO+8CUipRJq+0Edg==", "e0c4e198-fff1-4537-ba83-2707a4cb3a7e", new DateTime(2021, 6, 16, 3, 32, 28, 805, DateTimeKind.Local).AddTicks(3042) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "fbaa81db-1688-41cd-96d1-167255ce301a", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_User_Package_PackageId",
                table: "User",
                column: "PackageId",
                principalTable: "Package",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
