﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class TaskUser_CreateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTask_Task_UserTasksId",
                table: "UserTask");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTask_User_TaskUsersId",
                table: "UserTask");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "4ad82889-f218-42cc-87fa-a06fdd34bd13");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "4ad82889-f218-42cc-87fa-a06fdd34bd13", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.RenameColumn(
                name: "UserTasksId",
                table: "UserTask",
                newName: "TaskId");

            migrationBuilder.RenameColumn(
                name: "TaskUsersId",
                table: "UserTask",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_UserTask_UserTasksId",
                table: "UserTask",
                newName: "IX_UserTask_TaskId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 677, DateTimeKind.Utc).AddTicks(2311),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 410, DateTimeKind.Utc).AddTicks(7952));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 677, DateTimeKind.Utc).AddTicks(1928),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 410, DateTimeKind.Utc).AddTicks(7613));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 570, DateTimeKind.Utc).AddTicks(8705),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 294, DateTimeKind.Utc).AddTicks(6446));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 570, DateTimeKind.Utc).AddTicks(8041),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 294, DateTimeKind.Utc).AddTicks(5447));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 654, DateTimeKind.Utc).AddTicks(3415),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 389, DateTimeKind.Utc).AddTicks(7253));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 654, DateTimeKind.Utc).AddTicks(3030),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 389, DateTimeKind.Utc).AddTicks(6926));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 670, DateTimeKind.Utc).AddTicks(1537),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 404, DateTimeKind.Utc).AddTicks(4156));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 14, 42, 35, 750, DateTimeKind.Utc).AddTicks(9801), new DateTime(2021, 7, 27, 14, 42, 35, 751, DateTimeKind.Utc).AddTicks(158) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 21, 42, 35, 735, DateTimeKind.Local).AddTicks(9962), new DateTime(2021, 7, 27, 21, 42, 35, 736, DateTimeKind.Local).AddTicks(3757) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "3532412b-2759-4c20-aa5d-ee2a9c0e0b91", 1, "d1e5b93e-aad1-4104-b9c1-91d9248a4aee", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "ca53cec8-18db-4a87-82a6-e29b83bb1445", new DateTime(2021, 7, 27, 21, 42, 35, 738, DateTimeKind.Local).AddTicks(2371), "AQAAAAEAACcQAAAAEKN48AEIBul8bjIwC5NNo7bBwEL3jcjFzpeKqu4ZdUtwqOZKHnHYmjSyo1upv0Q4ZQ==", "c74c7eeb-10ce-4f55-8d64-94a2e90f82e5", new DateTime(2021, 7, 27, 21, 42, 35, 738, DateTimeKind.Local).AddTicks(2939) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "3532412b-2759-4c20-aa5d-ee2a9c0e0b91", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_UserTask_Task_TaskId",
                table: "UserTask",
                column: "TaskId",
                principalTable: "Task",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTask_User_UserId",
                table: "UserTask",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTask_Task_TaskId",
                table: "UserTask");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTask_User_UserId",
                table: "UserTask");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "3532412b-2759-4c20-aa5d-ee2a9c0e0b91");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "3532412b-2759-4c20-aa5d-ee2a9c0e0b91", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.RenameColumn(
                name: "TaskId",
                table: "UserTask",
                newName: "UserTasksId");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "UserTask",
                newName: "TaskUsersId");

            migrationBuilder.RenameIndex(
                name: "IX_UserTask_TaskId",
                table: "UserTask",
                newName: "IX_UserTask_UserTasksId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 410, DateTimeKind.Utc).AddTicks(7952),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 677, DateTimeKind.Utc).AddTicks(2311));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 410, DateTimeKind.Utc).AddTicks(7613),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 677, DateTimeKind.Utc).AddTicks(1928));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 294, DateTimeKind.Utc).AddTicks(6446),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 570, DateTimeKind.Utc).AddTicks(8705));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 294, DateTimeKind.Utc).AddTicks(5447),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 570, DateTimeKind.Utc).AddTicks(8041));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 389, DateTimeKind.Utc).AddTicks(7253),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 654, DateTimeKind.Utc).AddTicks(3415));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 389, DateTimeKind.Utc).AddTicks(6926),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 654, DateTimeKind.Utc).AddTicks(3030));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 22, 9, 38, 23, 404, DateTimeKind.Utc).AddTicks(4156),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 14, 42, 35, 670, DateTimeKind.Utc).AddTicks(1537));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 9, 38, 23, 478, DateTimeKind.Utc).AddTicks(5530), new DateTime(2021, 7, 22, 9, 38, 23, 478, DateTimeKind.Utc).AddTicks(5844) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 22, 16, 38, 23, 462, DateTimeKind.Local).AddTicks(405), new DateTime(2021, 7, 22, 16, 38, 23, 462, DateTimeKind.Local).AddTicks(4787) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "4ad82889-f218-42cc-87fa-a06fdd34bd13", 1, "18204557-89bc-4f7b-a6ff-9ee405b8a99d", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "fa1a6999-2f05-4bde-99e3-c132a217b84e", new DateTime(2021, 7, 22, 16, 38, 23, 466, DateTimeKind.Local).AddTicks(3451), "AQAAAAEAACcQAAAAEAkdFJjE7KHXlD7Shy06TnhkBigRhp2X9TRWA7a7TFvG6AnVF66KF9Xt9GkUvaWJwA==", "90504aee-b056-46a3-92f6-88623258610f", new DateTime(2021, 7, 22, 16, 38, 23, 466, DateTimeKind.Local).AddTicks(3742) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "4ad82889-f218-42cc-87fa-a06fdd34bd13", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AddForeignKey(
                name: "FK_UserTask_Task_UserTasksId",
                table: "UserTask",
                column: "UserTasksId",
                principalTable: "Task",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTask_User_TaskUsersId",
                table: "UserTask",
                column: "TaskUsersId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
