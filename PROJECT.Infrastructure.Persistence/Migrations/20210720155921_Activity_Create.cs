﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class Activity_Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "fb32c61d-57f1-4c20-afb4-a7a30e799973");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "fb32c61d-57f1-4c20-afb4-a7a30e799973", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 760, DateTimeKind.Utc).AddTicks(762),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 881, DateTimeKind.Utc).AddTicks(4771));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 760, DateTimeKind.Utc).AddTicks(15),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 881, DateTimeKind.Utc).AddTicks(4393));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 647, DateTimeKind.Utc).AddTicks(5567),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 777, DateTimeKind.Utc).AddTicks(3678));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 647, DateTimeKind.Utc).AddTicks(4732),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 777, DateTimeKind.Utc).AddTicks(2976));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 737, DateTimeKind.Utc).AddTicks(9762),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 860, DateTimeKind.Utc).AddTicks(130));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 737, DateTimeKind.Utc).AddTicks(9375),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 859, DateTimeKind.Utc).AddTicks(9684));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 752, DateTimeKind.Utc).AddTicks(7752),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 873, DateTimeKind.Utc).AddTicks(9700));

            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(type: "Date", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ActivityType = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.Id);
                    table.UniqueConstraint("AK_Activity_Date_UserId_ActivityType", x => new { x.Date, x.UserId, x.ActivityType });
                });

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 15, 59, 19, 819, DateTimeKind.Utc).AddTicks(8061), new DateTime(2021, 7, 20, 15, 59, 19, 819, DateTimeKind.Utc).AddTicks(8374) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 22, 59, 19, 807, DateTimeKind.Local).AddTicks(7083), new DateTime(2021, 7, 20, 22, 59, 19, 808, DateTimeKind.Local).AddTicks(929) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "4eb10f28-e228-4882-9d2e-da1aa6225ff0", 1, "a0f15a89-10c7-4863-b0fc-8cc4d7f2d368", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "bb657692-4b47-4541-b943-1a1e5bb7f505", new DateTime(2021, 7, 20, 22, 59, 19, 809, DateTimeKind.Local).AddTicks(5237), "AQAAAAEAACcQAAAAENC2NhQ/kBspJrvgCYOw208LmhQJTARl5Wh+gl74FQZBghV7MH+jVrl6NMDnJpLHGA==", "ad982f9e-8088-4262-8f7e-831b6e012c99", new DateTime(2021, 7, 20, 22, 59, 19, 809, DateTimeKind.Local).AddTicks(5632) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "4eb10f28-e228-4882-9d2e-da1aa6225ff0", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activity");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "4eb10f28-e228-4882-9d2e-da1aa6225ff0");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "4eb10f28-e228-4882-9d2e-da1aa6225ff0", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 881, DateTimeKind.Utc).AddTicks(4771),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 760, DateTimeKind.Utc).AddTicks(762));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 881, DateTimeKind.Utc).AddTicks(4393),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 760, DateTimeKind.Utc).AddTicks(15));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 777, DateTimeKind.Utc).AddTicks(3678),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 647, DateTimeKind.Utc).AddTicks(5567));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 777, DateTimeKind.Utc).AddTicks(2976),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 647, DateTimeKind.Utc).AddTicks(4732));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 860, DateTimeKind.Utc).AddTicks(130),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 737, DateTimeKind.Utc).AddTicks(9762));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 859, DateTimeKind.Utc).AddTicks(9684),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 737, DateTimeKind.Utc).AddTicks(9375));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 20, 14, 41, 13, 873, DateTimeKind.Utc).AddTicks(9700),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 20, 15, 59, 19, 752, DateTimeKind.Utc).AddTicks(7752));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 14, 41, 13, 944, DateTimeKind.Utc).AddTicks(8333), new DateTime(2021, 7, 20, 14, 41, 13, 944, DateTimeKind.Utc).AddTicks(8698) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 20, 21, 41, 13, 930, DateTimeKind.Local).AddTicks(9183), new DateTime(2021, 7, 20, 21, 41, 13, 931, DateTimeKind.Local).AddTicks(3185) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "fb32c61d-57f1-4c20-afb4-a7a30e799973", 1, "31c6d49a-869e-487c-af78-80bf0068b62d", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "431bfd81-1090-47fb-9978-6d035bae6e4b", new DateTime(2021, 7, 20, 21, 41, 13, 932, DateTimeKind.Local).AddTicks(7026), "AQAAAAEAACcQAAAAEHO6sd94+CUtlyZorzNdxfgT+wJlK4ufLD7FZyUIOcgG4asZ7bAtTVY0DDbUpBtLXg==", "7f28f5fd-2afc-4b71-88ef-dace244a64a8", new DateTime(2021, 7, 20, 21, 41, 13, 932, DateTimeKind.Local).AddTicks(7313) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "fb32c61d-57f1-4c20-afb4-a7a30e799973", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
