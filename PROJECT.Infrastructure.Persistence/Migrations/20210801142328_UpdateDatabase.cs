﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PROJECT.Infrastructure.Persistence.Migrations
{
    public partial class UpdateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "bcc46456-4c4d-42a7-9a10-47450b6294d6");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "bcc46456-4c4d-42a7-9a10-47450b6294d6", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 819, DateTimeKind.Utc).AddTicks(7138),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 753, DateTimeKind.Utc).AddTicks(3712));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 819, DateTimeKind.Utc).AddTicks(6784),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 753, DateTimeKind.Utc).AddTicks(3366));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 702, DateTimeKind.Utc).AddTicks(1757),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 608, DateTimeKind.Utc).AddTicks(3742));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 702, DateTimeKind.Utc).AddTicks(551),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 608, DateTimeKind.Utc).AddTicks(3053));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 791, DateTimeKind.Utc).AddTicks(3280),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 718, DateTimeKind.Utc).AddTicks(1466));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 791, DateTimeKind.Utc).AddTicks(2966),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 718, DateTimeKind.Utc).AddTicks(1133));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 811, DateTimeKind.Utc).AddTicks(6692),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 743, DateTimeKind.Utc).AddTicks(7090));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 1, 14, 23, 26, 875, DateTimeKind.Utc).AddTicks(125), new DateTime(2021, 8, 1, 14, 23, 26, 875, DateTimeKind.Utc).AddTicks(528) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 8, 1, 21, 23, 26, 863, DateTimeKind.Local).AddTicks(1790), new DateTime(2021, 8, 1, 21, 23, 26, 863, DateTimeKind.Local).AddTicks(5490) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "acce7778-a851-4882-8be7-6d8d8e43b938", 1, "e2d9dded-bb4e-462a-8a07-5485cced3227", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "e323fd7c-0818-4a67-b132-454f83fb952a", new DateTime(2021, 8, 1, 21, 23, 26, 864, DateTimeKind.Local).AddTicks(9017), "AQAAAAEAACcQAAAAEBgs/hePEbmldJe99Hu3lW64+R9nOF/TvWPyC52CHd2977oAtFNcoh624grC1ukIzg==", "957ab18a-70ae-4948-bdca-80e3164c85a2", new DateTime(2021, 8, 1, 21, 23, 26, 864, DateTimeKind.Local).AddTicks(9290) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "acce7778-a851-4882-8be7-6d8d8e43b938", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "acce7778-a851-4882-8be7-6d8d8e43b938");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "acce7778-a851-4882-8be7-6d8d8e43b938", "0ae9088e-7093-48ab-b658-393da0f8309a" });

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 753, DateTimeKind.Utc).AddTicks(3712),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 819, DateTimeKind.Utc).AddTicks(7138));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "WorkSchedule",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 753, DateTimeKind.Utc).AddTicks(3366),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 819, DateTimeKind.Utc).AddTicks(6784));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 608, DateTimeKind.Utc).AddTicks(3742),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 702, DateTimeKind.Utc).AddTicks(1757));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "User",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 608, DateTimeKind.Utc).AddTicks(3053),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 702, DateTimeKind.Utc).AddTicks(551));

            migrationBuilder.AlterColumn<DateTime>(
                name: "UpdatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 718, DateTimeKind.Utc).AddTicks(1466),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 791, DateTimeKind.Utc).AddTicks(3280));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "Post",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 718, DateTimeKind.Utc).AddTicks(1133),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 791, DateTimeKind.Utc).AddTicks(2966));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDate",
                table: "History",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(2021, 7, 27, 15, 20, 4, 743, DateTimeKind.Utc).AddTicks(7090),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldDefaultValue: new DateTime(2021, 8, 1, 14, 23, 26, 811, DateTimeKind.Utc).AddTicks(6692));

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 15, 20, 4, 826, DateTimeKind.Utc).AddTicks(7141), new DateTime(2021, 7, 27, 15, 20, 4, 826, DateTimeKind.Utc).AddTicks(7464) });

            migrationBuilder.UpdateData(
                table: "Department",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "UpdatedDate" },
                values: new object[] { new DateTime(2021, 7, 27, 22, 20, 4, 812, DateTimeKind.Local).AddTicks(5107), new DateTime(2021, 7, 27, 22, 20, 4, 812, DateTimeKind.Local).AddTicks(9021) });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "CompanyId", "ConcurrencyStamp", "Description", "Name", "NormalizedName" },
                values: new object[] { "bcc46456-4c4d-42a7-9a10-47450b6294d6", 1, "f44a6d53-644e-411a-972c-133dfe3adb2f", "1-SupperAdmin", "1-SupperAdmin", null });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: "0ae9088e-7093-48ab-b658-393da0f8309a",
                columns: new[] { "ConcurrencyStamp", "CreatedDate", "PasswordHash", "SecurityStamp", "UpdatedDate" },
                values: new object[] { "f1665aab-9da6-4e27-b0e3-487d638587c7", new DateTime(2021, 7, 27, 22, 20, 4, 814, DateTimeKind.Local).AddTicks(5138), "AQAAAAEAACcQAAAAEAXMkF2J6DknZnlCC/TFcz2dnKzMR+BAcvMF07Ko13G4GjNUr1ZC6NX/0F7iuIDJOA==", "412e3b22-bc2f-4a35-b038-9564b05a1b1a", new DateTime(2021, 7, 27, 22, 20, 4, 814, DateTimeKind.Local).AddTicks(5436) });

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "bcc46456-4c4d-42a7-9a10-47450b6294d6", "0ae9088e-7093-48ab-b658-393da0f8309a" });
        }
    }
}
