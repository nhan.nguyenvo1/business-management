﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using PROJECT.Application.Enums;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.Infrastructure.Persistence.Extensions
{
    public static class ModelBuilderExtension
    {
        public static void ApplyGlobalFilters<TInterface>(this ModelBuilder modelBuilder,
            Expression<Func<TInterface, bool>> expression)
        {
            var entities = modelBuilder.Model
                .GetEntityTypes()
                .Where(e => e.ClrType.GetInterface(typeof(TInterface).Name) != null)
                .Select(e => e.ClrType);
            foreach (var entity in entities)
            {
                var newParam = Expression.Parameter(entity);
                var newbody =
                    ReplacingExpressionVisitor.Replace(expression.Parameters.Single(), newParam, expression.Body);
                modelBuilder.Entity(entity).HasQueryFilter(Expression.Lambda(newbody, newParam));
            }
        }

        public static void Seed(this ModelBuilder builder)
        {
            builder.Entity<Package>().HasData(new Package
            {
                Id = 1,
                Cost = 0,
                Name = "Free",
                SizeOfDrive = 5 * 1024,
                NumOfEmployee = 50
            });

            var supperAdmin = new AppRole($"1-{Role.SupperAdmin}")
            {
                CompanyId = 1
            };

            builder.Entity<AppRole>().HasData(supperAdmin);

            builder.Entity<Department>().HasData(
                new Department
                {
                    Id = 1,
                    Name = "Admin",
                    Url = "/admin",
                    DepartmentId = null,
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    CreatedBy = null,
                    UpdatedBy = null,
                    CompanyId = 1
                });

            var userId = new Guid("0AE9088E-7093-48AB-B658-393DA0F8309A");
            var hasher = new PasswordHasher<AppUser>();

            builder.Entity<AppUser>().HasData(
                new AppUser
                {
                    Id = userId.ToString(),
                    FirstName = "Nguyen",
                    LastName = "Vo Nhan",
                    Gender = UserGender.Male,
                    Dob = new DateTime(1999, 3, 15),
                    FirstLogin = false,
                    IpAddress = "192.168.11.4",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    CreatedBy = null,
                    UpdatedBy = null,
                    Email = "nhan.nguyenvo1@gmail.com",
                    UserName = "vonhanyt123",
                    PasswordHash = hasher.HashPassword(null, "@dmin12345"),
                    NormalizedEmail = "NHAN.NGUYENVO1@GMAIL.COM",
                    NormalizedUserName = "VONHANYT123",
                    PhoneNumber = "0348310590",
                    PhoneNumberConfirmed = true,
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    AvatarUrl = "https://res.cloudinary.com/dddzstbl8/image/upload/v1615203221/AppUser/default.jpg",
                    AvatarPublicId = "AppUser/default.jpg",
                    PackageId = 1
                });

            builder.Entity<Company>().HasData(new Company
            {
                Id = 1,
                Name = "Easy Business",
                CreatedBy = userId.ToString(),
                UpdatedBy = userId.ToString(),
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow
            });

            builder.Entity<UserCompany>().HasData(new UserCompany
            {
                CompanyId = 1,
                UserId = userId.ToString()
            });

            builder.Entity<AppStorage>().HasData(new AppStorage
                {
                    Id = 1,
                    CompanyId = 1,
                    StorageType = StorageType.CompanyStorage,
                    Limit = 2000,
                    Path = "/1/Shared"
                },
                new AppStorage
                {
                    Id = 2,
                    CompanyId = 1,
                    StorageType = StorageType.PersonalStorage,
                    Limit = 500,
                    Path = $"/1/{userId}",
                    UserId = userId.ToString()
                });

            builder.Entity<IdentityUserRole<string>>()
                .HasData(new IdentityUserRole<string>
                {
                    UserId = userId.ToString(),
                    RoleId = supperAdmin.Id
                });
        }
    }
}