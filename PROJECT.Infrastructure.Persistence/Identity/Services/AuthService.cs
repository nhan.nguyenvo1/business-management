﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PROJECT.Application.BCL;
using PROJECT.Application.Claims;
using PROJECT.Application.ClaimTypes;
using PROJECT.Application.Dtos.Account;
using PROJECT.Application.Dtos.Email;
using PROJECT.Application.EmailTemplate;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.LogEvent;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Settings;
using PROJECT.Infrastructure.Persistence.Helpers;

namespace PROJECT.Infrastructure.Persistence.Identity.Services
{
    public class AuthService : IAuthService
    {
        private readonly IAuthUserService _authUserService;
        private readonly ClientSetting _clientSetting;
        private readonly IEmailService _emailService;
        private readonly JwtSetting _jwtSetting;
        private readonly IMapper _mapper;
        private readonly ClaimsIdentityOptions _options = new();
        private readonly RoleManager<AppRole> _roleManager;
        private readonly IDateTimeService _dateTimeService;

        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IActivityRepository _activityRepository;
        private readonly ILogger _logger;
        private ICompanyService _companyService;

        public AuthService(UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IOptions<JwtSetting> jwtSetting,
            IAuthUserService authUserService,
            IMapper mapper,
            RoleManager<AppRole> roleManager,
            IEmailService emailService,
            IOptions<ClientSetting> clientSetting,
            IActivityRepository activityRepository,
            IDateTimeService dateTimeService,
            ILogger<AuthService> logger,
            ICompanyService companyService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtSetting = jwtSetting.Value;
            _authUserService = authUserService;
            _mapper = mapper;
            _roleManager = roleManager;
            _emailService = emailService;
            _clientSetting = clientSetting.Value;
            _activityRepository = activityRepository;
            _dateTimeService = dateTimeService;
            _logger = logger;
            _companyService = companyService;
        }


        public async Task<Response<string>> ChangePasswordAsync(ChangePasswordRequest request)
        {
            var userId = _authUserService.UserId;
            _logger.LogInformation(LogEvent.UpdateItem, "User {userId} update password", userId);

            var appUser = await _userManager.FindByIdAsync(userId);

            if (appUser == null)
            {
                _logger.LogError(LogEvent.UpdateItemNotFound, "Get AppUser {userId}", userId);
            }

            var isCorrectPassword = await _userManager.CheckPasswordAsync(appUser, request.CurrentPassword);

            if (!isCorrectPassword)
            {
                _logger.LogError(LogEvent.UpdateItemFail, "User {userId} input wrong password", userId);
                throw new ApiException("Incorrect password");
            }

            var changePasswordResult =
                await _userManager.ChangePasswordAsync(appUser, request.CurrentPassword, request.NewPassword);

            if (!changePasswordResult.Succeeded)
            {
                _logger.LogError(LogEvent.UpdateItemFail, "User {userId} change password fail", userId);
                throw new ApiException($"{changePasswordResult.Errors.First().Description}");
            }

            return new Response<string>(appUser.Id);
        }

        public async Task<Response<AuthResponse>> SignInAsync(AuthRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email) ??
                       await _userManager.FindByNameAsync(request.Email);

            if (user == null) throw new KeyNotFoundException("User name/email or password is incorrect");

            if (!user.EmailConfirmed) throw new ApiException($"{request.Email} have not been confirmed email.");

            var result =
                await _signInManager.PasswordSignInAsync(user.UserName, request.Password, request.RememberMe, true);

            if (!result.Succeeded) throw new ApiException($"Invalid credentials for '{request.Email}'.");

            var jwtSecurityToken = await GenerateJwtAsync(user);

            if (string.IsNullOrEmpty(user.RefreshToken))
            {
                user.RefreshToken = GenerateRefreshToken();
                var refreshResult = await _userManager.UpdateAsync(user);
                if (!refreshResult.Succeeded)
                    throw new ApiException($"Cannot generate refresh token for '{request.Email}'");
            }

            var response = new AuthResponse
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
                RefreshToken = user.RefreshToken
            };

            return new Response<AuthResponse>(response, $"Authenticated {user.UserName}");
        }

        public async Task<Response<AuthResponse>> RefreshTokenAsync(RefreshTokenRequest request)
        {
            var userId = _authUserService.UserId;

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null) throw new ApiException("No Accounts Registered.");

            if (string.IsNullOrEmpty(user.RefreshToken) || !user.RefreshToken.Equals(request.RefreshToken))
                throw new ApiException("Invalid Credentials.");

            var jwtSecurityToken = await GenerateJwtAsync(user);
            var handler = new JwtSecurityTokenHandler();

            var jwtToken = handler.WriteToken(jwtSecurityToken);

            var authResponse = new AuthResponse
            {
                RefreshToken = user.RefreshToken,
                AccessToken = jwtToken
            };

            return new Response<AuthResponse>(authResponse);
        }

        public async Task<Response<string>> RevokeTokenAsync(RevokeTokenRequest request)
        {
            var handler = new JwtSecurityTokenHandler();
            var tokenS = handler.ReadToken(request.AccessToken) as JwtSecurityToken;
            var email = tokenS?.Claims.First(claim => claim.Type == JwtRegisteredClaimNames.Email)?.Value;

            if (string.IsNullOrEmpty(email)) throw new ApiException("Invalid Token");

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null) throw new ApiException($"No Accounts Registered With Email '{email}'.");

            if (!user.RefreshToken.Equals(request.RefreshToken))
                throw new ApiException($"Invalid Credentials for '{email}'.");

            user.RefreshToken = string.Empty;
            if (!(await _userManager.UpdateAsync(user)).Succeeded) throw new ApiException("Revoke token fail");

            return new Response<string>("Token has been revoke");
        }

        public async Task<Response<string>> ForgotPasswordAsync(ForgotPasswordRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null) throw new ApiException($"No Accounts Registered With Email '{request.Email}'.");

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            var url = $"{_clientSetting.Url}/reset-password";

            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["email"] = request.Email;
            query["token"] = token;
            uriBuilder.Query = query.ToString();
            url = uriBuilder.ToString();

            var emailRequest = new EmailRequest
            {
                To = user.Email,
                Subject = "Reset password",
                Body = ResetPasswordTemplate.Body.Replace("#ResetPasswordLink#", url)
            };

            await _emailService.SendAsync(emailRequest);

            return new Response<string>("Reset password link has been sent to your email");
        }

        public async Task<Response<string>> VerifyEmailAsync(VerifyEmailRequest request)
        {
            var user = await _userManager.Users.IgnoreQueryFilters().FirstOrDefaultAsync(u => u.Email == request.Email);

            if (user == null) throw new ApiException($"No Accounts Registered With Email '{request.Email}'.");

            var result = await _userManager.ConfirmEmailAsync(user, request.Token);

            if (result.Succeeded)
            {
                await _activityRepository.AddAsync(new Activity
                {
                    Date = _dateTimeService.UtcNow,
                    UserId = user.Id,
                    ActivityType = ActivityType.Register
                });

                return new Response<string>("Verify email successully");
            }

            throw new ApiException(result.Errors.FirstOrDefault()?.Description);
        }

        public async Task<Response<string>> ResetPasswordAsync(ResetPasswordRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null) throw new ApiException($"No Accounts Registered With Email '{request.Email}'.");

            var result = await _userManager.ResetPasswordAsync(user, request.Token, request.Password);

            if (result.Succeeded) return new Response<string>("Password has been reset");

            throw new ApiException(result.Errors.First().Description);
        }

        public async Task<Response<AuthVm>> GetMeAsync()
        {
            var userId = _authUserService.UserId;

            var user = await _userManager.FindByIdAsync(userId);
            var userRoleNames = await _userManager.GetRolesAsync(user);
            var userRoles = await _roleManager.Roles.Where(x => userRoleNames.Contains(x.Name)).ToListAsync();
            var permissions = new List<string>();
            foreach (var role in userRoles)
            {
                var roleClaims = await _roleManager.GetClaimsAsync(role);
                var permissionClaims = roleClaims.Where(x => x.Type == AuthorityClaimType.Permission &&
                                                             x.Issuer == "LOCAL AUTHORITY")
                    .Select(x => x.Value);

                permissions.AddRange(permissionClaims);
            }

            var response = _mapper.Map<AuthVm>(user);
            response.Roles = userRoleNames;
            response.Permissions = permissions;

            return new Response<AuthVm>(response);
        }

        public async Task RegisterAsync(RegisterRequest request)
        {
            var user = _mapper.Map<AppUser>(request);

            user.NormalizedEmail = _userManager.NormalizeEmail(request.Email);
            user.NormalizedUserName = _userManager.NormalizeName(request.UserName);

            var createdResult = await _userManager.CreateAsync(user, request.Password);

            if (!createdResult.Succeeded) throw new ApiException(createdResult.Errors.FirstOrDefault()?.Description);

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var url = $"{_clientSetting.Url}/verify-email";

            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["email"] = user.Email;
            query["token"] = token;
            uriBuilder.Query = query.ToString();
            url = uriBuilder.ToString();

            var body = string.Format(VerifyEmailTemplate.Body, user.FullName, url);

            var emailRequest = new EmailRequest
            {
                To = user.Email,
                Subject = "Welcome to easy business family",
                Body = body
            };

            if (!await _emailService.SendAsync(emailRequest)) throw new ApiException("Server mail error");
        }

        private static string GenerateRefreshToken()
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[40];
            rngCryptoServiceProvider.GetBytes(randomBytes);
            // convert random bytes to hex string
            return BitConverter.ToString(randomBytes).Replace("-", "");
        }

        private async Task<JwtSecurityToken> GenerateJwtAsync(AppUser user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roles = await _userManager.GetRolesAsync(user);

            var roleClaims = roles.Select(t => new Claim(_options.RoleClaimType, t)).ToList();

            var ipAddress = IpHelper.GetIpAddress();

            var claims = new[]
                {
                    new Claim(_options.UserNameClaimType, user.UserName),
                    new Claim(_options.UserIdClaimType, user.Id)
                }
                .Union(userClaims)
                .Union(roleClaims);

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSetting.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            var jwtSecurityToken = new JwtSecurityToken(
                _jwtSetting.Issuer,
                _jwtSetting.Audience,
                claims,
                expires: DateTime.UtcNow.AddMinutes(_jwtSetting.DurationInMinutes),
                signingCredentials: signingCredentials);
            return jwtSecurityToken;
        }

        public async Task<string> GetTokenAsync()
        {
            Preconditions.NotNullOrWhitespace(_authUserService.UserId, nameof(_authUserService.UserId));
            Preconditions.MustNotEqual(_companyService.CompanyId.HasValue, false, nameof(_companyService.CompanyId));

            var user = await GetMeAsync();

            var claims = new Claim[]
            {
                new Claim(_options.UserNameClaimType, user.Data.UserName),
                    new Claim(_options.UserIdClaimType, user.Data.Id),
                    new ClientCustomClaim(_companyService.CompanyId.Value),
                    new UserCustomClaim(_authUserService.UserId),
            };

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSetting.Key));

            var jwtSecurityToken = new JwtSecurityToken(
                _jwtSetting.Issuer,
                _jwtSetting.Audience,
                claims,
                expires: DateTime.UtcNow.AddMinutes(_jwtSetting.DurationInMinutes),
                signingCredentials: new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }
    }
}