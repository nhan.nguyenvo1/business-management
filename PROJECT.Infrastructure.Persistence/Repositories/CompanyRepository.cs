﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class CompanyRepository : GenericRepository<Company, int>, ICompanyRepository
    {
        private readonly IAuthUserService _authUserService;

        public CompanyRepository(ProjectDbContext context, IMapper mapper, IAuthUserService authUserService) : base(
            context, mapper)
        {
            _authUserService = authUserService;
        }

        public async Task AddUserAsync(int companyId, int departmentId, string userId)
        {
            await Context.UserCompanies.AddAsync(new UserCompany
            {
                CompanyId = companyId,
                UserId = userId
            });

            var department = await Context.Departments.IgnoreQueryFilters()
                .FirstOrDefaultAsync(d => d.Id == departmentId)
                ??  await Context.Departments.FirstOrDefaultAsync();
            department.Users.Add(await Context.Users.IgnoreQueryFilters().FirstOrDefaultAsync(u => u.Id == userId));
        }

        public async Task<ICollection<TViewModel>> GetPagedResponseAsync<TViewModel>(int page, int limit, string search)
        {
            var flag = string.IsNullOrEmpty(search);
            return await Context.Set<Company>()
                .IgnoreQueryFilters()
                .Where(e => !e.Deleted)
                .Where(e => flag || e.Name.Contains(search))
                .Where(e => e.Users.Any(u => u.Id == _authUserService.UserId))
                .OrderBy(c => c.Name)
                .Skip((page - 1) * limit)
                .Take(limit)
                .AsNoTracking()
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}