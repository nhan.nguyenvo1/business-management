﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class PackageRepository : GenericRepository<Package, int>, IPackageRepository
    {
        public PackageRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}