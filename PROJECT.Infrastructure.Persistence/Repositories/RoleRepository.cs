﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.ClaimTypes;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class RoleRepository : GenericRepository<AppRole, string>, IRoleRepository
    {
        private readonly ICompanyService _companyService;
        private readonly List<string> _permissions;
        private readonly RoleManager<AppRole> _roleManager;
        private readonly UserManager<AppUser> _userManager;

        public RoleRepository(ProjectDbContext context, IMapper mapper, RoleManager<AppRole> roleManager,
            List<string> permissions, ICompanyService companyService, UserManager<AppUser> userManager) :
            base(context, mapper)
        {
            _roleManager = roleManager;
            _permissions = permissions;
            _companyService = companyService;
            _userManager = userManager;
        }

        public new async Task<AppRole> AddAsync(AppRole appRole)
        {
            var result = await _roleManager.CreateAsync(appRole);

            if (!result.Succeeded) throw new ApiException($"{result.Errors.First().Description}");

            return appRole;
        }

        public new async Task DeleteAsync(AppRole appRole)
        {
            var result = await _roleManager.DeleteAsync(appRole);
            if (!result.Succeeded) throw new ApiException($"{result.Errors.First().Description}");
        }

        public new async Task<AppRole> UpdateAsync(AppRole appRole)
        {
            var result = await _roleManager.UpdateAsync(appRole);
            if (!result.Succeeded) throw new ApiException(result.Errors.First().Description);

            return appRole;
        }

        public new async Task<AppRole> GetByIdAsync(string id)
        {
            return await _roleManager.Roles.FirstOrDefaultAsync(role =>
                role.Id == id && role.CompanyId == _companyService.CompanyId);
        }

        public async Task<IReadOnlyList<AppRole>> GetPagedResponseAsync(int page, int limit, string search, string sort,
            string order, Dictionary<string, string> filters)
        {
            var roles = _roleManager.Roles.Where(role => role.CompanyId == _companyService.CompanyId);

            if (!string.IsNullOrEmpty(search))
                roles = roles.Where(role => role.Name.Contains(search) || role.Description.Contains(search));

            roles = order switch
            {
                "desc" => sort switch
                {
                    "description" => roles.OrderByDescending(role => role.Description),
                    _ => roles.OrderByDescending(role => role.Name)
                },
                _ => sort switch
                {
                    "description" => roles.OrderBy(role => role.Description),
                    _ => roles.OrderBy(role => role.Name)
                }
            };

            if (limit > 0)
                return await roles.Skip((page - 1) * limit)
                    .Take(limit)
                    .AsNoTracking()
                    .ToListAsync();

            return await roles.ToListAsync();
        }

        public async Task<int> CountAsync(string search, Dictionary<string, string> filters)
        {
            if (!string.IsNullOrEmpty(search))
                return await _roleManager.Roles.CountAsync(role =>
                    role.Name.Contains(search) || role.Description.Contains(search));

            return await _roleManager.Roles.CountAsync();
        }

        public async Task<bool> AssignPermissions(AppRole appRole, IEnumerable<string> requestPermissions)
        {
            var transaction = await Context.Database.BeginTransactionAsync();
            try
            {
                foreach (var permission in requestPermissions)
                {
                    if (!_permissions.Any(p => p.Equals(permission, StringComparison.OrdinalIgnoreCase))) continue;

                    var result = await _roleManager.AddClaimAsync(appRole,
                        new Claim(AuthorityClaimType.Permission, permission));

                    if (result.Succeeded) continue;

                    await transaction.RollbackAsync();
                    throw new ApiException($"Error: {result.Errors.First().Description}");
                }

                await transaction.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                await transaction.RollbackAsync();
                return false;
            }
        }

        public async Task<bool> CancelPermissions(AppRole appRole, ICollection<string> requestPermissions)
        {
            var transaction = await Context.Database.BeginTransactionAsync();
            var claims = (await _roleManager.GetClaimsAsync(appRole))
                .Where(claim => claim.Type == AuthorityClaimType.Permission)
                .ToList();
            try
            {
                foreach (var claim in claims.Where(claim => requestPermissions.Any(permission =>
                    permission.Equals(claim.Value, StringComparison.OrdinalIgnoreCase))))
                {
                    var result = await _roleManager.RemoveClaimAsync(appRole, claim);
                    if (result.Succeeded) continue;

                    await transaction.RollbackAsync();
                    throw new ApiException($"Error: {result.Errors.First().Description}");
                }

                await transaction.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                await transaction.RollbackAsync();
                return false;
            }
        }

        public async Task<ICollection<string>> GetByUserIdAsync(string userId)
        {
            return await _userManager.GetRolesAsync(await _userManager.FindByIdAsync(userId));
        }
    }
}