﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class ContactRepository : GenericRepository<Contact, int>, IContactRepository
    {
        public ContactRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
