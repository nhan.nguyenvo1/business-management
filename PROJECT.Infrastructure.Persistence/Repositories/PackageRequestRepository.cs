﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class PackageRequestRepository : GenericRepository<PackageRequest, int>, IPackageRequestRepository
    {
        public PackageRequestRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}