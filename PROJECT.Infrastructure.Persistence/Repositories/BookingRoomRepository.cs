﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class BookingRoomRepository : GenericRepository<BookingRoom, int>, IBookingRoomRepository
    {
        private readonly IAuthUserService _authUserService;

        public BookingRoomRepository(ProjectDbContext context, IMapper mapper, IAuthUserService authUserService) : base(context, mapper)
        {
            _authUserService = authUserService;
        }

        public async Task AddUserToRoomAsync(int roomId, ICollection<string> userIds)
        {
            await Context.UserRooms.AddRangeAsync(userIds.Select(userId => new UserRoom { UserId = userId, RoomId = roomId }));
            if (!userIds.Contains(_authUserService.UserId))
            {
                await Context.UserRooms.AddAsync(new UserRoom { UserId = _authUserService.UserId, RoomId = roomId });
            }
        }
    }
}