﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class UserRepository : GenericRepository<AppUser, string>, IUserRepository
    {
        private readonly ICloudinaryService _cloudinaryService;
        private readonly UserManager<AppUser> _userManager;

        public UserRepository(ProjectDbContext context, IMapper mapper, UserManager<AppUser> userManager,
            ICloudinaryService cloudinaryService) : base(context, mapper)
        {
            _userManager = userManager;
            _cloudinaryService = cloudinaryService;
        }

        public async Task<AppUser> UpdateAvatarAsync(string userId, IFormFile avatar)
        {
            var appUser = await _userManager.FindByIdAsync(userId);

            if (appUser == null) return null;

            if (!string.IsNullOrEmpty(appUser.AvatarPublicId))
                await _cloudinaryService.RemoveAsync(appUser.AvatarPublicId);

            await using var stream = avatar.OpenReadStream();
            var cloudinaryResponse = await _cloudinaryService.UploadAsync(userId, stream, true, "AppUser");

            appUser.AvatarUrl = cloudinaryResponse.Url;
            appUser.AvatarPublicId = cloudinaryResponse.PublicId;

            var result = await _userManager.UpdateAsync(appUser);

            if (result.Succeeded) return appUser;

            throw new ApiException("Update avatar fail");
        }

        public async Task<AppUser> AssignRoleAsync(AppUser appUser, ICollection<string> roleNames)
        {
            var addToRolesResult = await _userManager.AddToRolesAsync(appUser, roleNames);

            if (!addToRolesResult.Succeeded) throw new ApiException($"{addToRolesResult.Errors.First().Description}");

            return appUser;
        }

        public new async Task<AppUser> GetByIdAsync(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<bool> IsInCompanyAsync(int companyId)
        {
            return await Context.Set<AppUser>()
                .Include(u => u.Companies)
                .AnyAsync();
        }

        public async Task<AppUser> CancleRolesAsync(AppUser appUser, ICollection<string> roleNames)
        {
            var result = await _userManager.RemoveFromRolesAsync(appUser, roleNames);

            if (!result.Succeeded) throw new ApiException($"{result.Errors.First().Description}");

            return appUser;
        }
    }
}