﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using PROJECT.Application.Dtos.Email;
using PROJECT.Application.EmailTemplate;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Settings;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class EmployeeRepository : GenericRepository<AppUser, string>, IEmployeeRepository
    {
        private readonly ClientSetting _clientSetting;
        private readonly ICompanyService _companyService;
        private readonly IEmailService _emailService;
        private readonly UserManager<AppUser> _userManager;

        public EmployeeRepository
        (ProjectDbContext context, IMapper mapper, UserManager<AppUser> userManager, IEmailService emailService,
            IOptions<ClientSetting> clientSetting, ICompanyService companyService) : base(context, mapper)
        {
            _userManager = userManager;
            _emailService = emailService;
            _clientSetting = clientSetting.Value;
            _companyService = companyService;
        }

        public new async Task<AppUser> AddAsync(AppUser entity)
        {
            entity.FirstLogin = true;

            entity.NormalizedUserName = _userManager.NormalizeName(entity.UserName);
            entity.NormalizedEmail = _userManager.NormalizeEmail(entity.Email);
            var password = GenerateRandomPassword();

            var result = await _userManager.CreateAsync(entity, password);
            if (!result.Succeeded) throw new ApiException($"Error: {result.Errors.ToList()[0].Description}");

            var token = await _userManager.GenerateEmailConfirmationTokenAsync(entity);
            var url = $"{_clientSetting.Url}/change-password";

            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["email"] = entity.Email;
            query["token"] = token;
            uriBuilder.Query = query.ToString();
            url = uriBuilder.ToString();

            var body = WelcomeTemplete.Body.Replace("#Password#", password);
            body = body.Replace("#LinkWebsite#", url);
            // Send email
            var emailRequest = new EmailRequest
            {
                To = entity.Email,
                Subject = "Welcome to easy business family",
                Body = body
            };

            if (!await _emailService.SendAsync(emailRequest)) throw new ApiException("Server mail error");

            return entity;
        }

        public new async Task DeleteAsync(AppUser entity)
        {
            Context.UserCompanies.Remove(await Context.UserCompanies.FirstOrDefaultAsync(e =>
                e.UserId == entity.Id && e.CompanyId == _companyService.CompanyId));
        }

        public async Task<bool> ExistAsync(string usernameOrEmail)
        {
            return await _userManager.FindByEmailAsync(usernameOrEmail) != null ||
                   await _userManager.FindByNameAsync(usernameOrEmail) != null;
        }

        public new async Task<AppUser> UpdateAsync(AppUser entity)
        {
            var result = await _userManager.UpdateAsync(entity);
            if (result.Succeeded) return entity;

            throw new ApiException($"Error: {result.Errors.First().Description}");
        }

        public new async Task<AppUser> GetByIdAsync(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<IReadOnlyList<AppUser>> GetPagedResponseAsync(int page, int limit, string search, string sort,
            string order, Dictionary<string, string> filters)
        {
            var users = _userManager
                .Users
                .Include(user => user.Created)
                .Include(user => user.Updated)
                .Include(user => user.Departments)
                .Where(user => string.IsNullOrEmpty(search)
                               || user.UserName.Contains(search)
                               || user.Email.Contains(search)
                               || user.PhoneNumber.Contains(search)
                               || user.FirstName.Contains(search)
                               || user.LastName.Contains(search));

            users = order switch
            {
                "desc" => sort switch
                {
                    "email" => users.OrderByDescending(user => user.Email),
                    "phoneNumber" => users.OrderByDescending(user => user.PhoneNumber),
                    "firstName" => users.OrderByDescending(user => user.FirstName),
                    "lastName" => users.OrderByDescending(user => user.LastName),
                    "status" => users.OrderByDescending(user => user.Status),
                    _ => users.OrderByDescending(user => user.UserName)
                },
                _ => sort switch
                {
                    "email" => users.OrderBy(user => user.Email),
                    "phoneNumber" => users.OrderBy(user => user.PhoneNumber),
                    "firstName" => users.OrderBy(user => user.FirstName),
                    "lastName" => users.OrderBy(user => user.LastName),
                    "status" => users.OrderBy(user => user.Status),
                    _ => users.OrderBy(user => user.UserName)
                }
            };
            if (limit > 0)
                return await users
                    .Skip((page - 1) * limit)
                    .Take(limit)
                    .AsNoTracking()
                    .ToListAsync();

            return await users.ToListAsync();
        }

        public async Task<int> CountAsync(string search, Dictionary<string, string> filters)
        {
            return string.IsNullOrEmpty(search)
                ? await _userManager.Users.CountAsync()
                : await _userManager.Users.CountAsync(user =>
                    user.UserName.Contains(search) || user.Email.Contains(search) ||
                    user.PhoneNumber.Contains(search) ||
                    user.FirstName.Contains(search) || user.LastName.Contains(search));
        }

        public async Task<IReadOnlyList<AppUser>> GetByDepartmentIdAsync(Department department, int page, int limit,
            string search,
            string sort, string order, Dictionary<string, string> filters)
        {
            var users = _userManager
                .Users
                .Include(user => user.Created)
                .Include(user => user.Updated)
                .Include(user => user.Departments)
                .AsQueryable();

            users = users.Where(user => user.Departments.Any(d => d.Id == department.Id));

            if (!string.IsNullOrEmpty(search))
                users = users.Where(user =>
                    user.UserName.Contains(search) || user.Email.Contains(search) ||
                    user.PhoneNumber.Contains(search) ||
                    user.FirstName.Contains(search) || user.LastName.Contains(search));

            users = order switch
            {
                "desc" => sort switch
                {
                    "email" => users.OrderByDescending(user => user.Email),
                    "phoneNumber" => users.OrderByDescending(user => user.PhoneNumber),
                    "firstName" => users.OrderByDescending(user => user.FirstName),
                    "lastName" => users.OrderByDescending(user => user.LastName),
                    "status" => users.OrderByDescending(user => user.Status),
                    _ => users.OrderByDescending(user => user.UserName)
                },
                _ => sort switch
                {
                    "email" => users.OrderBy(user => user.Email),
                    "phoneNumber" => users.OrderBy(user => user.PhoneNumber),
                    "firstName" => users.OrderBy(user => user.FirstName),
                    "lastName" => users.OrderBy(user => user.LastName),
                    "status" => users.OrderBy(user => user.Status),
                    _ => users.OrderBy(user => user.UserName)
                }
            };
            if (limit > 0)
                return await users
                    .Skip((page - 1) * limit)
                    .Take(limit)
                    .AsNoTracking()
                    .ToListAsync();

            return await users.ToListAsync();
        }

        public async Task<int> CountByDepartmentIdAsync(Department department, string search,
            Dictionary<string, string> filters)
        {
            var users = _userManager
                .Users
                .Include(user => user.Created)
                .Include(user => user.Updated)
                .AsQueryable();

            users = users.Where(user => user.Departments.Any(d => d.Id == department.Id));
            if (!string.IsNullOrEmpty(search))
                users = users.Where(user =>
                    user.UserName.Contains(search) || user.Email.Contains(search) ||
                    user.PhoneNumber.Contains(search) ||
                    user.FirstName.Contains(search) || user.LastName.Contains(search));

            return await users.CountAsync();
        }

        private static string GenerateRandomPassword(PasswordOptions options = null)
        {
            options ??= new PasswordOptions
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true
            };

            var randomChars = new[]
            {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ", // uppercase 
                "abcdefghijkmnopqrstuvwxyz", // lowercase
                "0123456789", // digits
                "!@$?_-" // non-alphanumeric
            };

            var rand = new Random(Environment.TickCount);
            var chars = new List<char>();

            if (options.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (options.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (options.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (options.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (var i = chars.Count;
                i < options.RequiredLength
                || chars.Distinct().Count() < options.RequiredUniqueChars;
                i++)
            {
                var rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }
    }
}