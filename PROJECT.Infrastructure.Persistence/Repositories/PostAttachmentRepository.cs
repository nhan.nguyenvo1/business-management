﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class PostAttachmentRepository : GenericRepository<PostAttachment, int>, IPostAttachmentRepository
    {
        public PostAttachmentRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}