﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class TaskAttachmentRepository : GenericRepository<TaskAttachment, int>, ITaskAttachmentRepository
    {
        public TaskAttachmentRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}