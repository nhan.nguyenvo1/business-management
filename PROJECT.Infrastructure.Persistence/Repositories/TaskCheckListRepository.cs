﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class TaskCheckListRepository : GenericRepository<TaskCheckList, int>, ITaskCheckListRepository
    {
        public TaskCheckListRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}