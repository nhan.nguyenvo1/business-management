﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class AppFileRepository : GenericRepository<AppFile, int>, IAppFileRepository
    {
        public AppFileRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public new async Task<AppFile> GetByIdAsync(int id)
        {
            return await Context.Set<AppFile>()
                .Include(p => p.AppStorage)
                .Include(p => p.Children)
                .AsNoTracking()
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public new async Task DeleteAsync(AppFile appFile)
        {
            await base.DeleteAsync(appFile);

            if (appFile.FileType != FileType.Folder) return;

            foreach (var drive in await Context.AppFiles.Where(f => f.FileId == appFile.Id).ToListAsync())
                await DeleteAsync(drive);
        }

        public async Task AddChildrenAsync(int desId, int srcId)
        {
            var folderPath = await Context
                .AppFiles
                .Where(file => file.Id == desId && file.FileType == FileType.Folder)
                .Select(file => file.Path)
                .FirstOrDefaultAsync();

            if (folderPath == string.Empty) return;

            var folderIds = new List<Tuple<int, int>>();
            var result = new List<AppFile>();

            var drives = await Context
                .AppFiles
                .Where(file => file.FileId == srcId)
                .ToListAsync();

            foreach (var drive in drives)
            {
                var newDrive = new AppFile
                {
                    Name = drive.Name,
                    Path = $"{folderPath}/{drive.Name}",
                    FileId = desId,
                    FileSize = drive.FileSize,
                    FileType = drive.FileType,
                    StorageId = drive.StorageId,
                    Shared = drive.Shared,
                    SharedLink = drive.SharedLink
                };

                var entity = await Context.AppFiles.AddAsync(newDrive);
                await Context.SaveChangesAsync();

                if (drive.FileType == FileType.Folder) folderIds.Add(new Tuple<int, int>(entity.Entity.Id, drive.Id));
            }

            foreach (var folder in folderIds) await AddChildrenAsync(folder.Item1, folder.Item2);
        }

        public async Task AddToSharedAsync(AppFile drive, string userId, bool canAdd, bool canRead, bool canWrite,
            bool root = true)
        {
            var sharedFile =
                await Context.SharedFiles.FirstOrDefaultAsync(s => s.UserId == userId && s.FileId == drive.FileId);
            if (sharedFile == null)
            {
                await Context.SharedFiles.AddAsync(new SharedFile
                {
                    FileId = drive.Id,
                    UserId = userId,
                    CanAdd = canAdd,
                    CanRead = canRead,
                    CanWrite = canWrite,
                    Root = root
                });
            }
            else
            {
                sharedFile.Root = root;
                sharedFile.CanAdd = canAdd;
                sharedFile.CanRead = canRead;
                sharedFile.CanWrite = canWrite;

                Context.Entry(sharedFile).State = EntityState.Modified;
            }

            if (drive.FileType == FileType.Folder)
                foreach (var child in drive.Children)
                    await AddToSharedAsync(child, userId, canAdd, canRead, canWrite, false);
        }

        public async Task<bool> AlreadyShared(int driveId, string userId)
        {
            return await Context.SharedFiles.AnyAsync(d => d.FileId == driveId && d.UserId.Equals(userId));
        }

        public async Task<IReadOnlyList<TViewModel>> GetSharedAsync<TViewModel>(int? folderId, string userId)
        {
            return folderId.HasValue
                ? await Context
                    .AppFiles
                    .Where(f => f.AppUsers.Any(u => u.Id.Equals(userId) && f.FileId == folderId.Value))
                    .AsNoTracking()
                    .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                    .ToListAsync()
                : await Context
                    .AppFiles
                    .Where(f => f.SharedFiles.Any(sf => sf.Root && sf.UserId.Equals(userId)))
                    .AsNoTracking()
                    .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                    .ToListAsync();
        }

        public async Task<TViewModel> GetSharedByIdAsync<TViewModel>(int driveId, string userId)
        {
            return await Context
                .SharedFiles
                .Where(sf => !sf.File.Deleted && sf.UserId == userId && sf.FileId == driveId)
                .AsNoTracking()
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<IReadOnlyList<TViewModel>> GetDeletedAsync<TViewModel>(string userId)
        {
            return await Context
                .AppFiles
                .IgnoreQueryFilters()
                .Where(f => f.Deleted && (f.Parent == null || !f.Parent.Deleted) && f.CreatedBy == userId)
                .AsNoTracking()
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<AppFile> GetDeletedByIdAsync(int driveId, string userId)
        {
            return await Context
                .AppFiles
                .IgnoreQueryFilters()
                .AsNoTracking()
                .FirstOrDefaultAsync(f => f.Deleted && f.CreatedBy == userId && f.Id == driveId);
        }

        public async Task<IReadOnlyCollection<T>> GetRecursive<T>(string userId)
        {
            return await Context.AppFiles
                .Where(e => userId.Equals(e.AppStorage.UserId))
                .ProjectTo<T>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}