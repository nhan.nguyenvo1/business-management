﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class CompanyDriveRepository : GenericRepository<CompanyDrive, int>, ICompanyDriveRepository
    {
        public CompanyDriveRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task AddChildrenAsync(int desId, int srcId)
        {
            var folderPath = await Context
                .CompanyDrives
                .Where(file => file.Id == desId && file.FileType == FileType.Folder)
                .Select(file => file.Path)
                .FirstOrDefaultAsync();

            if (folderPath == string.Empty) return;

            var folderIds = new List<Tuple<int, int>>();
            var result = new List<AppFile>();

            var drives = await Context
                .CompanyDrives
                .Where(file => file.FileId == srcId)
                .ToListAsync();

            foreach (var drive in drives)
            {
                var newDrive = new CompanyDrive
                {
                    Name = drive.Name,
                    Path = $"{folderPath}/{drive.Name}",
                    FileId = desId,
                    FileSize = drive.FileSize,
                    FileType = drive.FileType,
                    StorageId = drive.StorageId,
                    Shared = drive.Shared,
                    SharedLink = drive.SharedLink
                };

                var entity = await Context.CompanyDrives.AddAsync(newDrive);
                await Context.SaveChangesAsync();

                if (drive.FileType == FileType.Folder) folderIds.Add(new Tuple<int, int>(entity.Entity.Id, drive.Id));
            }

            foreach (var folder in folderIds) await AddChildrenAsync(folder.Item1, folder.Item2);
        }

        public new async Task DeleteAsync(CompanyDrive appFile)
        {
            await base.DeleteAsync(appFile);

            if (appFile.FileType != FileType.Folder) return;

            foreach (var drive in await Context.CompanyDrives.Where(f => f.FileId == appFile.Id).ToListAsync())
                await DeleteAsync(drive);
        }
    }
}