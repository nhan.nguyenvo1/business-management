﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;
using System.Threading.Tasks;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class ActivityRepository : GenericRepository<Activity, int>, IActivityRepository
    {
        public ActivityRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public new Task<Activity> AddAsync(Activity entity)
        {
            try
            {
                return base.AddAsync(entity);
            }
            catch (System.Exception)
            {
                return Task.FromResult(new Activity());
            }
        }
    }
}
