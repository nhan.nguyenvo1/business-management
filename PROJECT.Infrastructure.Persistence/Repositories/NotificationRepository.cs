﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class NotificationRepository : GenericRepository<Notification, int>, INotificationRepository
    {
        private readonly ICompanyService _companyService;
        public NotificationRepository(ProjectDbContext context,
                                      IMapper mapper,
                                      ICompanyService companyService) : base(context, mapper)
        {
            _companyService = companyService;
        }

        public async Task<IReadOnlyList<T>> GetByUserIdAsync<T>(string userId)
        {
            return await Context.Set<UserNotification>()
                .IgnoreQueryFilters()
                .Where(un => un.UserId == userId && un.Notification.CompanyId == _companyService.CompanyId)
                .OrderByDescending(un => un.Notification.CreatedDate)
                .ProjectTo<T>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task MarkAllAsync(string userId, int companyId, bool read)
        {
            var notifications = Context.Set<UserNotification>()
                .IgnoreQueryFilters()
                .Where(un => un.UserId == userId && un.Notification.CompanyId == companyId);

            await notifications.ForEachAsync(e =>
            {
                e.Read = read;
                Context.Entry(e).State = EntityState.Modified;
            });
        }

        public async Task MarkAsReadAsync(string userId, int notificationId)
        {
            var userNotification = await Context.Set<UserNotification>()
                .FirstOrDefaultAsync(un => un.UserId == userId && un.NotificationId == notificationId);
            userNotification.Read = true;

            Context.Entry(userNotification).State = EntityState.Modified;
        }
    }
}