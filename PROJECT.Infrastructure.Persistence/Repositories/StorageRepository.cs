﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class StorageRepository : GenericRepository<AppStorage, int>, IStorageRepository
    {
        private readonly IDriveStorageService _driveStorageService;

        public StorageRepository(ProjectDbContext context, IMapper mapper, IDriveStorageService driveStorageService) :
            base(context, mapper)
        {
            _driveStorageService = driveStorageService;
        }

        public new async Task<AppStorage> AddAsync(AppStorage storage)
        {
            var result = await _driveStorageService.CreateFolderAsync(storage.Path);

            storage.Path = result.Path;
            return await base.AddAsync(storage);
        }

        public new async Task<AppStorage> GetSingleAsync(Expression<Func<AppStorage, bool>> predicates)
        {
            return await Context.Set<AppStorage>()
                .Include(p => p.User)
                .AsNoTracking()
                .FirstOrDefaultAsync(predicates);
        }
    }
}