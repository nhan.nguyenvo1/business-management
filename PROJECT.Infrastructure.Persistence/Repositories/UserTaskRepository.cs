﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class UserTaskRepository : GenericRepository<UserTask, int>, IUserTaskRepository
    {
        public UserTaskRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
