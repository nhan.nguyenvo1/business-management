﻿using AutoMapper;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class PaymentInfoRepository : GenericRepository<PaymentInfo, int>, IPaymentInfoRepository
    {
        public PaymentInfoRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }
    }
}
