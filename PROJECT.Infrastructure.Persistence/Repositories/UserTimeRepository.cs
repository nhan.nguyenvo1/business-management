﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class UserTimeRepository : GenericRepository<UserTime, int>, IUserTimeRepository
    {
        public UserTimeRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<int> CountAsync(string search)
        {
            return await Context
                .Users
                .Include(user => user.UserTimes)
                .CountAsync(user =>
                    string.IsNullOrEmpty(search) || (user.FirstName + " " + user.LastName).Contains(search));
        }

        public async Task<ICollection<TViewModel>> GetPagingAsync<TViewModel>(int page, int limit, string search)
        {
            if (limit == -1)
                return await Context
                    .Users
                    .Include(user => user.UserTimes)
                    .Where(user =>
                        string.IsNullOrEmpty(search) || (user.FirstName + " " + user.LastName).Contains(search))
                    .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                    .ToListAsync();

            return await Context
                .Users
                .Include(user => user.UserTimes)
                .Where(user => string.IsNullOrEmpty(search) || (user.FirstName + " " + user.LastName).Contains(search))
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .Skip((page - 1) * limit)
                .Take(limit)
                .ToListAsync();
        }
    }
}