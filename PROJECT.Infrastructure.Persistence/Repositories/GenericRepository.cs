﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Common;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class GenericRepository<T, TKey> : IGenericRepository<T, TKey> where T : class
    {
        protected readonly ProjectDbContext Context;
        protected readonly IMapper Mapper;

        public GenericRepository(ProjectDbContext context, IMapper mapper)
        {
            Context = context;
            Mapper = mapper;
        }

        public async Task<T> AddAsync(T entity)
        {
            var result = await Context.Set<T>().AddAsync(entity);
            result.State = EntityState.Added;
            return result.Entity;
        }

        public async Task<T> FindSingle(Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>()
                .Where(predicate)
                .Take(1)
                .FirstOrDefaultAsync();
        }

        public async Task AddRangeAsync(ICollection<T> entities)
        {
            await Context.Set<T>().AddRangeAsync(entities);
        }

        public Task DeleteAsync(T entity)
        {
            return Task.FromResult(Context.Set<T>().Remove(entity));
        }

        public async Task DeleteAsync(TKey id)
        {
            var entity = await Context.Set<T>().FindAsync(id);

            Context.Set<T>().Remove(entity);
        }

        public async Task<IReadOnlyList<T>> GetAllAsync()
        {
            return await Context.Set<T>().ToListAsync();
        }

        public async Task<IReadOnlyList<TViewModel>> GetPagedResponseAsync<TViewModel>(int page, int limit)
        {
            return await Context
                .Set<T>()
                .Skip((page - 1) * limit)
                .Take(limit)
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<T> GetByIdAsync(TKey id)
        {
            return await Context.Set<T>().FindAsync(id);
        }

        public async Task<IReadOnlyList<T>> GetPagedResponseAsync(int page, int limit)
        {
            return await Context
                .Set<T>()
                .Skip((page - 1) * limit)
                .Take(limit)
                .AsNoTracking()
                .ToListAsync();
        }

        public Task<T> UpdateAsync(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return Task.FromResult(entity);
        }

        public async Task<IReadOnlyList<TViewModel>> GetAllAsync<TViewModel>(bool ignoreQueryFilters = false)
        {
            return !ignoreQueryFilters
                ? await Context
                    .Set<T>()
                    .AsNoTracking()
                    .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                    .ToListAsync()
                : await Context
                    .Set<T>()
                    .IgnoreQueryFilters()
                    .AsNoTracking()
                    .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                    .ToListAsync();
        }

        public async Task<TViewModel> GetSingleAsync<TViewModel>(Expression<Func<T, bool>> predicate)
        {
            return await Context
                .Set<T>()
                .Where(predicate)
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .Take(1)
                .FirstOrDefaultAsync();
        }

        public async Task<T> GetSingleAsync(Expression<Func<T, bool>> predicate, bool ignoreQueryFilters = false)
        {
            return !ignoreQueryFilters 
                ? await Context
                    .Set<T>()
                    .Where(predicate)
                    .Take(1)
                    .FirstOrDefaultAsync()
                : await Context
                    .Set<T>()
                    .IgnoreQueryFilters()
                    .Where(predicate)
                    .Take(1)
                    .FirstOrDefaultAsync();
        }

        public async Task<int> CountAsync(Expression<Func<T, bool>> predicate)
        {
            return await Context
                .Set<T>()
                .CountAsync(predicate);
        }

        public async Task<bool> AnyAsync(Expression<Func<T, bool>> predicate, bool ignoreQueryFilters = false)
        {
            return !ignoreQueryFilters
                ? await Context
                    .Set<T>()
                    .AnyAsync(predicate)
                : await Context.Set<T>()
                    .IgnoreQueryFilters()
                    .AnyAsync(predicate);
        }

        public async Task<TViewModel> GetByIdAsync<TViewModel>(TKey id, bool ignoreQueryFilters = false)
        {
            return !ignoreQueryFilters
                ? (await Context
                    .Set<T>()
                    .Where(entry => (entry as BaseEntity).Id.Equals(id))
                    .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                    .ToListAsync()).FirstOrDefault()
                : (await Context
                    .Set<T>()
                    .Where(entry => (entry as BaseEntity).Id.Equals(id))
                    .IgnoreQueryFilters()
                    .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                    .ToListAsync()).FirstOrDefault();
        }

        public Task DeleteByConditionAsync(Expression<Func<T, bool>> predicate)
        {
            var entities = Context.Set<T>().Where(predicate);
            Context.Set<T>().RemoveRange(entities);

            return Task.CompletedTask;
        }

        public async Task<IReadOnlyList<TViewModel>> GetByConditionAsync<TViewModel>(
            Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>()
                .Where(predicate)
                .AsNoTracking()
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}