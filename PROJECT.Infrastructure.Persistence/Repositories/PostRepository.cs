﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class PostRepository : GenericRepository<Post, int>, IPostRepository
    {
        private readonly IAuthUserService _authUserService;

        public PostRepository(ProjectDbContext context, IMapper mapper, IAuthUserService authUserService) : base(
            context, mapper)
        {
            _authUserService = authUserService;
        }

        public new async Task<Post> GetByIdAsync(int id)
        {
            return await Context
                .Posts
                .Include(post => post.PostAttachments)
                .Include(post => post.Created)
                .Include(post => post.ApprovedUser)
                .FirstOrDefaultAsync(post => post.Id == id);
        }

        public new async Task<Post> GetSingleAsync(Expression<Func<Post, bool>> predicate)
        {
            return await Context.Posts
                .Include(post => post.PostAttachments)
                .Where(predicate)
                .FirstOrDefaultAsync();
        }

        public async Task<ICollection<TViewModel>> GetPagedResponseAsync<TViewModel>(int page, int limit, string search,
            string sort,
            string order, Dictionary<string, string> filters, bool isRaw = true, bool personal = false)
        {
            var posts = Context
                .Set<Post>()
                .AsQueryable();

            if (personal)
            {
                posts = posts.Where(post => post.CreatedBy == _authUserService.UserId);
            }

            if (!isRaw)
            {
                posts = posts.Where(post => post.Status == PostStatus.Approved);
            }

            if (!string.IsNullOrEmpty(search))
            {
                posts = posts.Where(post => post.Title.Contains(search) || (post.Created.LastName + " " + post.Created.FirstName + " ").Contains(search));
            }

            posts = search switch
            {
                "author" => order switch
                {
                    "desc" => posts.OrderByDescending(post => new {post.Created.FirstName, post.Created.LastName}),
                    _ => posts.OrderBy(post => new {post.Created.FirstName, post.Created.LastName})
                },
                "createdAt" => order switch
                {
                    "desc" => posts.OrderByDescending(post => post.CreatedDate),
                    _ => posts.OrderBy(post => post.CreatedDate)
                },
                "status" => order switch
                {
                    "desc" => posts.OrderByDescending(post => post.Status),
                    _ => posts.OrderBy(post => post.Status)
                },
                _ => order switch
                {
                    "desc" => posts.OrderByDescending(post => post.Title),
                    _ => posts.OrderBy(post => post.Title)
                }
            };

            if (limit == -1)
                return await posts.AsNoTracking().ProjectTo<TViewModel>(Mapper.ConfigurationProvider).ToListAsync();

            return await posts
                .Skip((page - 1) * limit)
                .Take(limit)
                .AsNoTracking()
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<int> CountAsync(string search, Dictionary<string, string> filters, bool isRaw = true,
            bool personal = false)
        {
            if (!string.IsNullOrEmpty(search))
                return await Context
                    .Set<Post>()
                    .CountAsync(post =>
                        post.Title.Contains(search) || post.Created.FirstName.Contains(search) ||
                        post.Created.LastName.Contains(search) && (isRaw || post.Status == PostStatus.Approved) &&
                        (!personal || post.CreatedBy == _authUserService.UserId));

            return await Context
                .Set<Post>()
                .CountAsync(post =>
                    (isRaw || post.Status == PostStatus.Approved) &&
                    (!personal || post.CreatedBy == _authUserService.UserId));
        }
    }
}