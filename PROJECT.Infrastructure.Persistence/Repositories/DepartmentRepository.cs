﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class DepartmentRepository : GenericRepository<Department, int>, IDepartmentRepository
    {
        private readonly ICompanyService _companyService;

        public DepartmentRepository(ProjectDbContext context, IMapper mapper, ICompanyService companyService) : base(
            context, mapper)
        {
            _companyService = companyService;
        }

        public new Task<Department> GetByIdAsync(int id)
        {
            var department = Context.Departments
                .Include(e => e.Children)
                .ThenInclude(e => e.Parent)
                .AsEnumerable()
                .Where(e => e.Id == id)
                .ToList()
                .SingleOrDefault();
            return Task.FromResult(department);
        }

        public Task<List<Department>> GetAsync()
        {
            var departments = Context.Departments
                .Include(e => e.Children)
                .ThenInclude(e => e.Parent)
                .Where(e => e.DepartmentId == null)
                .ToList();
            return Task.FromResult(departments);
        }

        public new async Task DeleteAsync(int id)
        {
            using (var transaction = await Context.Database.BeginTransactionAsync())
            {
                try
                {
                    // Get current department
                    var department = await Context.Departments
                        .Include(e => e.Parent)
                        .Include(e => e.Children)
                        .Include(e => e.Users)
                        .FirstOrDefaultAsync(e => e.Id == id);
                    // Get parent
                    var parent = department.Parent;
                    if (parent == null) throw new ApiException("Cannot delete root department");
                    // Get children
                    var children = department.Children;
                    // Move children
                    foreach (var child in children)
                    {
                        child.DepartmentId = parent.Id;
                        Context.Entry(child).State = EntityState.Modified;
                    }

                    await Context.SaveChangesAsync();
                    // Get Users Of Department
                    var users = department.Users;
                    // Move User
                    foreach (var user in users)
                    {
                        user.Departments.Remove(
                            user.Departments.FirstOrDefault(d => d.CompanyId == _companyService.CompanyId));
                        user.Departments.Add(parent);
                        Context.Entry(user).State = EntityState.Modified;
                    }

                    await Context.SaveChangesAsync();
                    // Delete department
                    await base.DeleteAsync(id);

                    await Context.SaveChangesAsync();

                    await transaction.CommitAsync();
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
        }

        public async Task<bool> ExistAsync(int departmentId)
        {
            return await Context.Departments.AnyAsync(department => department.Id == departmentId);
        }

        public async Task<int> CountAsync(string search, Dictionary<string, string> filters)
        {
            return string.IsNullOrEmpty(search)
                ? await Context.Departments.CountAsync()
                : await Context.Departments.CountAsync(department =>
                    department.Name.Contains(search, StringComparison.OrdinalIgnoreCase));
        }
    }
}