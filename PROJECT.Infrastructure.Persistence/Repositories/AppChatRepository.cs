﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class AppChatRepository : GenericRepository<AppChat, int>, IAppChatRepository
    {
        public AppChatRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<ICollection<TViewModel>> GetByRoomId<TViewModel>(int roomId, int page, int limit)
        {
            return await Context.AppChats
                .Include(chat => chat.Sender)
                .Where(chat => chat.RoomId == roomId)
                .OrderByDescending(chat => chat.When)
                .Skip((page - 1) * limit)
                .Take(limit)
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<ICollection<TViewModel>> GetChatListAsync<TViewModel>(string userId, int page, int limit,
            string search)
        {
            throw new NotImplementedException();
        }
    }
}