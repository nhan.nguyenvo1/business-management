﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class AppTaskRepository : GenericRepository<AppTask, int>, IAppTaskRepository
    {
        private readonly ICloudinaryService _cloudinaryService;

        public AppTaskRepository(ProjectDbContext context, IMapper mapper, ICloudinaryService cloudinaryService) : base(context, mapper)
        {
            _cloudinaryService = cloudinaryService;
        }

        public async Task<ICollection<TViewModel>> GetPagedResponseAsync<TViewModel>(int page, int limit, string search,
            string sort,
            string order, Dictionary<string, string> filters)
        {
            var tasks = Context
                .Set<AppTask>()
                .Include(p => p.Created)
                .Include(p => p.TaskUsers)
                .Where(task => string.IsNullOrEmpty(search) ||
                               task.Title.Contains(search) ||
                               task.Description.Contains(search));

            tasks = sort switch
            {
                "description" => order switch
                {
                    "desc" => tasks.OrderByDescending(task => task.Description),
                    _ => tasks.OrderBy(task => task.Description)
                },
                "deadline" => order switch
                {
                    "desc" => tasks.OrderByDescending(task => task.DueDate),
                    _ => tasks.OrderBy(task => task.DueDate)
                },
                _ => order switch
                {
                    "desc" => tasks.OrderByDescending(task => task.Title),
                    _ => tasks.OrderBy(task => task.Title)
                }
            };

            if (limit == -1)
                return await tasks.AsNoTracking().ProjectTo<TViewModel>(Mapper.ConfigurationProvider).ToListAsync();

            return await tasks.Skip((page - 1) * limit)
                .Take(limit)
                .AsNoTracking()
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public new async Task<TViewModel> GetByIdAsync<TViewModel>(int id, bool ignoreQueryFilters = true)
        {
            return await Context.AppTasks
                .Include(task => task.TaskUsers)
                .Include(task => task.CheckLists)
                .Include(task => task.Attachments)
                .Where(task => task.Id == id)
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<int> CountAsync(string search, Dictionary<string, string> filters)
        {
            return string.IsNullOrEmpty(search)
                ? await Context.Set<AppTask>().CountAsync()
                : await Context.Set<AppTask>().CountAsync(task =>
                    task.Title.Contains(search) ||
                    task.Description.Contains(search));
        }

        public async Task<ICollection<TViewModel>> GetPersonalPagedResponseAsync<TViewModel>(string userId, int page,
            int limit,
            string search, string sort, string order, Dictionary<string, string> filters)
        {
            var tasks = Context
                .Set<AppTask>()
                .Include(p => p.Created)
                .Include(t => t.TaskUsers)
                .Where(task => task.TaskUsers.Any(user => user.Id == userId))
                .Where(task => string.IsNullOrEmpty(search) ||
                               task.Title.Contains(search) ||
                               task.Description.Contains(search));

            tasks = sort switch
            {
                "description" => order switch
                {
                    "desc" => tasks.OrderByDescending(task => task.Description),
                    _ => tasks.OrderBy(task => task.Description)
                },
                "deadline" => order switch
                {
                    "desc" => tasks.OrderByDescending(task => task.DueDate),
                    _ => tasks.OrderBy(task => task.DueDate)
                },
                _ => order switch
                {
                    "desc" => tasks.OrderByDescending(task => task.Title),
                    _ => tasks.OrderBy(task => task.Title)
                }
            };

            if (limit == -1)
                return await tasks.AsNoTracking().ProjectTo<TViewModel>(Mapper.ConfigurationProvider).ToListAsync();

            return await tasks.Skip((page - 1) * limit)
                .Take(limit)
                .AsNoTracking()
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<int> CountPersonalAsync(string userId, string search, Dictionary<string, string> filters)
        {
            var tasks = Context.Set<AppTask>()
                .Where(task => task.TaskUsers.Any(userTask => userTask.Id == userId));
            return string.IsNullOrEmpty(search)
                ? await tasks.CountAsync()
                : await tasks.CountAsync(task =>
                    task.Title.Contains(search) ||
                    task.Description.Contains(search));
        }

        public async Task<IReadOnlyList<TViewModel>> GetByUserIdAsync<TViewModel>(string userId)
        {
            return await Context
                .Set<AppTask>()
                .Include(task => task.TaskUsers)
                .Where(task => task.CreatedBy == userId || task.TaskUsers.Any(user => user.Id == userId))
                .ProjectTo<TViewModel>(Mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public new async Task DeleteAsync(AppTask entity)
        {
            var taskAttackments = Context.TaskAttachment.Where(e => e.TaskId == entity.Id);

            foreach (var attachment in taskAttackments)
            {
                await _cloudinaryService.RemoveAsync(attachment.PublicId);
            }
            
            Context.TaskAttachment.RemoveRange(taskAttackments);
            Context.TaskCheckLists.RemoveRange(Context.TaskCheckLists.Where(e => e.TaskId == entity.Id));
            
            await base.DeleteAsync(entity);
        }

        public new async Task<AppTask> GetByIdAsync(int id)
        {
            return await Context
                .AppTasks
                .Include(e => e.TaskUsers)
                .FirstOrDefaultAsync(e=> e.Id == id);
        }
    }
}