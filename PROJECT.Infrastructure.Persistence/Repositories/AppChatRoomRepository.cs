﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;

namespace PROJECT.Infrastructure.Persistence.Repositories
{
    public class AppChatRoomRepository : GenericRepository<AppRoom, int>, IAppChatRoomRepository
    {
        public AppChatRoomRepository(ProjectDbContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public Task<IQueryable<AppRoom>> GetAvailableByUserId(string userId)
        {
            var rooms = Context
                .AppRooms
                .Include(r => r.BannedUsers)
                .Include(r => r.RoomUsers)
                .Where(room => !room.BannedUsers.Any(user => user.Id != userId))
                .Where(room => !room.RoomUsers.Any(user => user.Id == userId))
                .AsNoTracking();
            return Task.FromResult(rooms);
        }

        public Task<IQueryable<AppRoom>> GetOwnByUserId(string userId, int page, int limit, string search)
        {
            var rooms = Context
                .AppRooms
                .Include(r => r.RoomUsers)
                .Include(r => r.ReadUsers)
                .Where(room => room.RoomUsers.Any(user => user.Id == userId))
                .Where(room => string.IsNullOrEmpty(search) || room.Name.Contains(search))
                .Skip((page - 1) * limit)
                .Take(limit)
                .AsNoTracking();
            return Task.FromResult(rooms);
        }

        public new async Task<AppRoom> GetByIdAsync(int id)
        {
            return await Context.AppRooms
                .Include(room => room.ReadUsers)
                .FirstOrDefaultAsync(room => room.Id == id);
        }
    }
}