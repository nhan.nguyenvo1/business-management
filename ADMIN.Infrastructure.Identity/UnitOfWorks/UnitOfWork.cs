﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Interfaces;
using ADMIN.Infrastructure.Identity.Contexts;
using Microsoft.EntityFrameworkCore.Storage;

namespace ADMIN.Infrastructure.Identity.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AdminDbContext _context;

        public UnitOfWork(AdminDbContext context)
        {
            _context = context;
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellation = default)
        {
            return await _context.Database.BeginTransactionAsync(cancellation);
        }

        public async Task CommitAsync(CancellationToken cancellation = default)
        {
            await _context.SaveChangesAsync(cancellation);
        }
    }
}