﻿using ADMIN.Infrastructure.Identity.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ADMIN.Infrastructure.Identity.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");

            builder.HasKey(p => p.Id);

            builder.Property(e => e.Id).HasMaxLength(50);

            builder.Property(e => e.AvatarUrl)
                .HasMaxLength(500)
                .HasDefaultValue("https://res.cloudinary.com/dddzstbl8/image/upload/v1615203221/AppUser/default.jpg");

            builder.Property(e => e.AvatarPublicId)
                .HasMaxLength(250)
                .HasDefaultValue("AppUser/default.jpg");

            builder.Property(e => e.FirstName).IsRequired().HasMaxLength(100);

            builder.Property(e => e.LastName).IsRequired().HasMaxLength(100);

            builder.Property(e => e.Gender).IsRequired();

            builder.Property(e => e.Dob).IsRequired();

            builder.Property(e => e.CreatedDate).IsRequired();

            builder.Property(e => e.UpdatedDate).IsRequired();
        }
    }
}