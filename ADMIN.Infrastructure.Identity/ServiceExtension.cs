﻿using System;
using System.Text;
using ADMIN.Application.Interfaces;
using ADMIN.Infrastructure.Identity.Contexts;
using ADMIN.Infrastructure.Identity.Entities;
using ADMIN.Infrastructure.Identity.Services;
using ADMIN.Infrastructure.Identity.UnitOfWorks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace ADMIN.Infrastructure.Identity
{
    public static class ServiceExtension
    {
        public static void AddAdminIdentityInfracstructure(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddDbContext<AdminDbContext>(options =>
            {
                options
                    .UseSqlServer(configuration.GetConnectionString("AdminDbConnectionString"),
                        b => b.MigrationsAssembly(typeof(AdminDbContext).Assembly.FullName))
                    .UseLazyLoadingProxies();
            });

            services.AddIdentity<User, Role>(options =>
                {
                    options.SignIn.RequireConfirmedEmail = true;
                    options.Password.RequireDigit = options.Password.RequireLowercase =
                        options.Password.RequireUppercase = options.Password.RequireNonAlphanumeric = true;
                    options.User.RequireUniqueEmail = true;
                    options.Lockout.MaxFailedAccessAttempts = 5;
                })
                .AddEntityFrameworkStores<AdminDbContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<UserManager<User>>();
            services.AddTransient<RoleManager<Role>>();
            services.AddTransient<SignInManager<User>>();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(o =>
                {
                    o.RequireHttpsMetadata = false;
                    o.SaveToken = false;
                    o.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero,
                        ValidIssuer = configuration["JwtSetting:Issuer"],
                        ValidAudience = configuration["JwtSetting:Audience"],
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtSetting:Key"]))
                    };
                });

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IAuthService, AuthService>();
        }
    }
}