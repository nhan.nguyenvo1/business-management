﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ADMIN.Application.ClaimTypes;
using ADMIN.Application.Dtos.Account;
using ADMIN.Application.Dtos.Email;
using ADMIN.Application.EmailTemplate;
using ADMIN.Application.Exceptions;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.Identity.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using PROJECT.Domain.Settings;

namespace ADMIN.Infrastructure.Identity.Services
{
    public class AuthService : IAuthService
    {
        private readonly IAuthUserService _authUserService;
        private readonly ClientSetting _clientSetting;
        private readonly IDateTimeService _dateTimeService;
        private readonly IEmailService _emailService;
        private readonly JwtSetting _jwtSetting;
        private readonly ClaimsIdentityOptions _options = new();
        private readonly RoleManager<Role> _roleManager;

        private readonly SignInManager<User> _signInManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;

        public AuthService(UserManager<User> userManager,
            SignInManager<User> signInManager,
            IOptions<JwtSetting> jwtSetting,
            IAuthUserService authUserService,
            RoleManager<Role> roleManager,
            IEmailService emailService,
            IOptions<ClientSetting> clientSetting,
            IDateTimeService dateTimeService, IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtSetting = jwtSetting.Value;
            _authUserService = authUserService;
            _roleManager = roleManager;
            _emailService = emailService;
            _clientSetting = clientSetting.Value;
            _dateTimeService = dateTimeService;
            _unitOfWork = unitOfWork;
        }


        public async Task<Response<string>> ChangePasswordAsync(ChangePasswordRequest request)
        {
            var userId = _authUserService.UserId;

            var appUser = await _userManager.FindByIdAsync(userId);

            var isCorrectPassword = await _userManager.CheckPasswordAsync(appUser, request.CurrentPassword);

            if (!isCorrectPassword) throw new ApiException("Incorrect password");

            var changePasswordResult =
                await _userManager.ChangePasswordAsync(appUser, request.CurrentPassword, request.NewPassword);

            if (!changePasswordResult.Succeeded)
                throw new ApiException($"{changePasswordResult.Errors.First().Description}");

            return new Response<string>(appUser.Id);
        }

        public async Task<Response<AuthResponse>> SignInAsync(AuthRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email) ??
                       await _userManager.FindByNameAsync(request.Email);

            if (user == null) throw new KeyNotFoundException("User name/email or password is incorrect");

            if (!user.EmailConfirmed) throw new ApiException($"{request.Email} have not been confirmed email.");

            var result =
                await _signInManager.PasswordSignInAsync(user.UserName, request.Password, request.RememberMe, true);

            if (!result.Succeeded) throw new ApiException($"Invalid credentials for '{request.Email}'.");

            var jwtSecurityToken = await GenerateJwtAsync(user);

            if (string.IsNullOrEmpty(user.RefreshToken))
            {
                user.RefreshToken = GenerateRefreshToken();
                var refreshResult = await _userManager.UpdateAsync(user);
                if (!refreshResult.Succeeded)
                    throw new ApiException($"Cannot generate refresh token for '{request.Email}'");
            }

            var response = new AuthResponse
            {
                AccessToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
                RefreshToken = user.RefreshToken
            };

            return new Response<AuthResponse>(response, $"Authenticated {user.UserName}");
        }

        public async Task<Response<AuthResponse>> RefreshTokenAsync(RefreshTokenRequest request)
        {
            var userId = _authUserService.UserId;

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null) throw new ApiException("No Accounts Registered.");

            if (string.IsNullOrEmpty(user.RefreshToken) || !user.RefreshToken.Equals(request.RefreshToken))
                throw new ApiException("Invalid Credentials.");

            var jwtSecurityTokenToken = await GenerateJwtAsync(user);
            var handler = new JwtSecurityTokenHandler();

            var jwtToken = handler.WriteToken(jwtSecurityTokenToken);

            var authResponse = new AuthResponse
            {
                RefreshToken = user.RefreshToken,
                AccessToken = jwtToken
            };

            return new Response<AuthResponse>(authResponse);
        }

        public async Task<Response<string>> RevokeTokenAsync(RevokeTokenRequest request)
        {
            var handler = new JwtSecurityTokenHandler();
            var tokenS = handler.ReadToken(request.AccessToken) as JwtSecurityToken;
            var email = tokenS?.Claims.First(claim => claim.Type == JwtRegisteredClaimNames.Email)?.Value;

            if (string.IsNullOrEmpty(email)) throw new ApiException("Invalid Token");

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null) throw new ApiException($"No Accounts Registered With Email '{email}'.");

            if (!user.RefreshToken.Equals(request.RefreshToken))
                throw new ApiException($"Invalid Credentials for '{email}'.");

            user.RefreshToken = string.Empty;
            if (!(await _userManager.UpdateAsync(user)).Succeeded) throw new ApiException("Revoke token fail");

            return new Response<string>("Token has been revoke");
        }

        public async Task<Response<string>> ForgotPasswordAsync(ForgotPasswordRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null) throw new ApiException($"No Accounts Registered With Email '{request.Email}'.");

            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            var url = $"{_clientSetting.Url}/reset-password";

            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["email"] = request.Email;
            query["token"] = token;
            uriBuilder.Query = query.ToString();
            url = uriBuilder.ToString();

            var emailRequest = new EmailRequest
            {
                To = user.Email,
                Subject = "Reset password",
                Body = ResetPasswordTemplate.Body.Replace("#ResetPasswordLink#", url)
            };

            await _emailService.SendAsync(emailRequest);

            return new Response<string>("Reset password link has been sent to your email");
        }

        public async Task<Response<string>> VerifyEmailAsync(VerifyEmailRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);

            if (user == null) throw new ApiException($"No Accounts Registered With Email '{request.Email}'.");

            var result = await _userManager.ConfirmEmailAsync(user, request.Token);

            if (result.Succeeded)
            {
                var changePasswordResult =
                    await _userManager.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);

                if (changePasswordResult.Succeeded) return new Response<string>("Verify email successully");

                throw new ApiException(changePasswordResult.Errors.FirstOrDefault().Description);
            }

            throw new ApiException("Confirm email failure");
        }

        public async Task<Response<string>> ResetPasswordAsync(ResetPasswordRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if (user == null) throw new ApiException($"No Accounts Registered With Email '{request.Email}'.");

            var result = await _userManager.ResetPasswordAsync(user, request.Token, request.Password);

            if (result.Succeeded) return new Response<string>("Password has been reset");

            throw new ApiException(result.Errors.First().Description);
        }

        public async Task<Response<AuthVm>> GetMeAsync()
        {
            var userId = _authUserService.UserId;

            var user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
            var userRoleNames = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
            var userRoles = await _roleManager.Roles.Where(x => userRoleNames.Contains(x.Name)).ToListAsync();
            var permissions = new List<string>();
            foreach (var role in userRoles)
            {
                var roleClaims = await _roleManager.GetClaimsAsync(role);
                var permissionClaims = roleClaims.Where(x => x.Type == AuthorityClaimType.Permission &&
                                                             x.Issuer == "LOCAL AUTHORITY")
                    .Select(x => x.Value);

                permissions.AddRange(permissionClaims);
            }

            var response = new AuthVm
            {
                Id = user.Id,
                Avatar = user.AvatarUrl,
                UserName = user.UserName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Gender = user.Gender.ToString(),
                Dob = user.Dob.ToString("yyyy-MM-dd")
            };

            response.Roles = userRoleNames;
            response.Permissions = permissions;

            return new Response<AuthVm>(response);
        }

        private static string GenerateRefreshToken()
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[40];
            rngCryptoServiceProvider.GetBytes(randomBytes);
            // convert random bytes to hex string
            return BitConverter.ToString(randomBytes).Replace("-", "");
        }

        private async Task<JwtSecurityToken> GenerateJwtAsync(User user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roles = await _userManager.GetRolesAsync(user);

            var roleClaims = roles.Select(t => new Claim(_options.RoleClaimType, t)).ToList();

            var claims = new[]
                {
                    new Claim(_options.UserNameClaimType, user.UserName),
                    new Claim(_options.UserIdClaimType, user.Id)
                }
                .Union(userClaims)
                .Union(roleClaims);

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSetting.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            var jwtSecurityToken = new JwtSecurityToken(
                _jwtSetting.Issuer,
                _jwtSetting.Audience,
                claims,
                expires: DateTime.UtcNow.AddMinutes(_jwtSetting.DurationInMinutes),
                signingCredentials: signingCredentials);
            return jwtSecurityToken;
        }
    }
}