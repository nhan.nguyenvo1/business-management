﻿using Microsoft.AspNetCore.Identity;

namespace ADMIN.Infrastructure.Identity.Entities
{
    public class Role : IdentityRole
    {
        public Role(string name) : base(name)
        {
        }

        public string Description { get; set; }
    }
}