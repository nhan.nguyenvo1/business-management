﻿using System;
using Microsoft.AspNetCore.Identity;
using PROJECT.Domain.Enums;

namespace ADMIN.Infrastructure.Identity.Entities
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime Dob { get; set; }

        public string AvatarUrl { get; set; }

        public string AvatarPublicId { get; set; }

        public UserGender Gender { get; set; }

        public string RefreshToken { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }
    }
}