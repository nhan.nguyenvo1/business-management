﻿using System;
using ADMIN.Infrastructure.Identity.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PROJECT.Domain.Enums;

namespace ADMIN.Infrastructure.Identity.Extensions
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder builder)
        {
            var userId = new Guid("0AE9088E-7093-48AB-B658-393DA0F8309A");
            var hasher = new PasswordHasher<User>();

            builder.Entity<User>().HasData(
                new User
                {
                    Id = userId.ToString(),
                    FirstName = "Nguyen",
                    LastName = "Vo Nhan",
                    Gender = UserGender.Male,
                    Dob = new DateTime(1999, 3, 15),
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    Email = "nhan.nguyenvo1@gmail.com",
                    UserName = "vonhanyt123",
                    PasswordHash = hasher.HashPassword(null, "@dmin12345"),
                    NormalizedEmail = "NHAN.NGUYENVO1@GMAIL.COM",
                    NormalizedUserName = "VONHANYT123",
                    PhoneNumber = "0348310590",
                    PhoneNumberConfirmed = true,
                    EmailConfirmed = true,
                    AccessFailedCount = 0,
                    AvatarUrl = "https://res.cloudinary.com/dddzstbl8/image/upload/v1615203221/AppUser/default.jpg",
                    AvatarPublicId = "AppUser/default.jpg"
                });

            var supperAdminRole = new Role("Supper Admin")
            {
                NormalizedName = "SUPPER ADMIN",
                Description = "Supper Admin Role"
            };

            builder.Entity<Role>().HasData(supperAdminRole);

            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                UserId = userId.ToString(),
                RoleId = supperAdminRole.Id
            });
        }
    }
}