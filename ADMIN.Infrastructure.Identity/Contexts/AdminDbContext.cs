﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Interfaces;
using ADMIN.Infrastructure.Identity.Configurations;
using ADMIN.Infrastructure.Identity.Entities;
using ADMIN.Infrastructure.Identity.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PROJECT.Domain.Interfaces;

namespace ADMIN.Infrastructure.Identity.Contexts
{
    public class AdminDbContext : IdentityDbContext<User, Role, string>
    {
        private readonly IDateTimeService _dateTimeService;

        public AdminDbContext(DbContextOptions options,
            IDateTimeService dateTimeService) : base(options)
        {
            _dateTimeService = dateTimeService;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            #region Date Time Tracking

            foreach (var entry in ChangeTracker.Entries<IDateTimeTracking>())
                switch (entry.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        break;
                    case EntityState.Modified:
                        entry.Entity.UpdatedDate = _dateTimeService.UtcNow;
                        break;
                    case EntityState.Added:
                        entry.Entity.CreatedDate = _dateTimeService.UtcNow;
                        entry.Entity.UpdatedDate = _dateTimeService.UtcNow;
                        break;
                }

            #endregion

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            #region Configuration

            builder.ApplyConfiguration(new UserConfiguration());

            builder.ApplyConfiguration(new RoleConfiguration());

            builder.Entity<IdentityUserRole<string>>(entity =>
            {
                entity.ToTable("UserRole");
                entity.HasKey(e => new {e.UserId, e.RoleId});
            });

            builder.Entity<IdentityUserClaim<string>>(entity =>
            {
                entity.ToTable("UserClaim");
                entity.HasKey(e => e.Id);
            });

            builder.Entity<IdentityUserLogin<string>>(entity =>
            {
                entity.ToTable("UserLogin");
                entity.HasKey(e => e.UserId);
            });

            builder.Entity<IdentityRoleClaim<string>>(entity =>
            {
                entity.ToTable("RoleClaim");
                entity.HasKey(e => e.Id);
            });

            builder.Entity<IdentityUserToken<string>>(entity =>
            {
                entity.ToTable("UserToken");
                entity.HasKey(e => e.UserId);
            });

            #endregion

            #region Query Filters

            #endregion

            #region Seed

            builder.Seed();

            #endregion
        }
    }
}