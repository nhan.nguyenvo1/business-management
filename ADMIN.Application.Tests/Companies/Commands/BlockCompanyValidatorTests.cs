﻿using ADMIN.Application.Features.Companies.Commands.BlockCompany;
using FluentValidation.TestHelper;
using Xunit;

namespace ADMIN.Application.Tests.Companies.Commands
{
    public class BlockCompanyValidatorTests
    {
        private readonly BlockCompanyValidator _validator = new();

        [Fact]
        public void ShouldHaveErrorWhenCompanyIdIsEmpty()
        {
            var model = new BlockCompanyCommand
            {
                CompanyId = 0,
                Reason = "Test"
            };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(e => e.CompanyId);
            result.ShouldNotHaveValidationErrorFor(e => e.Reason);
        }

        [Fact]
        public void ShouldHaveErrorWhenReasonIsEmpty()
        {
            var model = new BlockCompanyCommand
            {
                CompanyId = 1,
                Reason = string.Empty
            };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(e => e.Reason);
            result.ShouldNotHaveValidationErrorFor(e => e.CompanyId);
        }

        [Fact]
        public void ShouldHaveErrorWhenReasonIsNull()
        {
            var model = new BlockCompanyCommand
            {
                CompanyId = 1,
                Reason = null
            };

            var result = _validator.TestValidate(model);

            result.ShouldHaveValidationErrorFor(e => e.Reason);
            result.ShouldNotHaveValidationErrorFor(e => e.CompanyId);
        }

        [Fact]
        public void ShouldNotHaveErrorWhenModelAsExpected()
        {
            var model = new BlockCompanyCommand
            {
                CompanyId = 1,
                Reason = "Test"
            };

            var result = _validator.TestValidate(model);

            result.ShouldNotHaveAnyValidationErrors();
        }
    }
}
