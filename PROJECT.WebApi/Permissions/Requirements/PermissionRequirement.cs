﻿using Microsoft.AspNetCore.Authorization;

namespace PROJECT.WebApi.Permissions.Requirements
{
    public class PermissionRequirement : IAuthorizationRequirement
    {
        public PermissionRequirement(string permission)
        {
            Permission = permission;
        }

        public string Permission { get; }
    }
}