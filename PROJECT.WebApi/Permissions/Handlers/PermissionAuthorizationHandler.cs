﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.ClaimTypes;
using PROJECT.Application.Enums;
using PROJECT.Domain.Entities;
using PROJECT.WebApi.Permissions.Requirements;

namespace PROJECT.WebApi.Permissions.Handlers
{
    public class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        private readonly ClaimsIdentityOptions _options = new();
        private readonly RoleManager<AppRole> _roleManager;
        private readonly UserManager<AppUser> _userManager;

        public PermissionAuthorizationHandler(UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
            PermissionRequirement requirement)
        {
            var userId = context.User.Claims.FirstOrDefault(claim => claim.Type == _options.UserIdClaimType)?.Value;
            if (string.IsNullOrEmpty(userId)) return;

            var user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
            if (user == null) return;
            var userRoleNames = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
            var userRoles = await _roleManager.Roles.Where(x => userRoleNames.Contains(x.Name)).ToListAsync();

            if (userRoles.Any(x => x.Name.Contains(Role.SupperAdmin.ToString(), StringComparison.OrdinalIgnoreCase)))
            {
                context.Succeed(requirement);
                return;
            }

            foreach (var role in userRoles)
            {
                var roleClaims = await _roleManager.GetClaimsAsync(role);
                var permissions = roleClaims.Where(x => x.Type == AuthorityClaimType.Permission &&
                                                        x.Value == requirement.Permission &&
                                                        x.Issuer == "LOCAL AUTHORITY")
                    .Select(x => x.Value);

                if (!permissions.Any()) continue;

                context.Succeed(requirement);
                return;
            }
        }
    }
}