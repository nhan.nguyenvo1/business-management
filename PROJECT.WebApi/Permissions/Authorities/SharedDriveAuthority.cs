﻿namespace PROJECT.WebApi.Permissions.Authorities
{
    /// <summary>
    /// Shared Drive Authority
    /// </summary>
    public static class SharedDriveAuthority
    {
        public const string View = "Permission.SharedDrives.View";

        public const string Download = "Permission.SharedDrives.Download";
    }
}