﻿namespace PROJECT.WebApi.Permissions.Authorities
{
    public static class UserTimeAuthority
    {
        public const string View = "Permission.WorkTimes.View";
    }
}