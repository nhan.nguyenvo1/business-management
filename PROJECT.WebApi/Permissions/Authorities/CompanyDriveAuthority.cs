﻿namespace PROJECT.WebApi.Permissions.Authorities
{
    /// <summary>
    /// Company Drive Authority
    /// </summary>
    public static class CompanyDriveAuthority
    {
        public const string View = "Permission.CompanyDrives.View";

        public const string Copy = "Permission.CompanyDrives.Copy";

        public const string Upload = "Permission.CompanyDrives.Upload";

        public const string CreateFolder = "Permission.CompanyDrives.CreateFolder";

        public const string Delete = "Permission.CompanyDrives.Delete";

        public const string Download = "Permission.CompanyDrives.Download";

        public const string Move = "Permission.CompanyDrives.Move";
    }
}