﻿namespace PROJECT.WebApi.Permissions.Authorities
{
    /// <summary>
    /// Feed Authority
    /// </summary>
    public static class FeedAuthority
    {
        public const string View = "Permission.Feeds.View";

        public const string Approve = "Permission.Feeds.Approval";
    }
}