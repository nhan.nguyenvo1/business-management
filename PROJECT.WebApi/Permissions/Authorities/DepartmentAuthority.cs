﻿namespace PROJECT.WebApi.Permissions.Authorities
{
    /// <summary>
    /// Department Authority
    /// </summary>
    public static class DepartmentAuthority
    {
        public const string View = "Permission.Departments.View";

        public const string Detail = "Permission.Departments.Detail";

        public const string Create = "Permission.Departments.Create";

        public const string Edit = "Permission.Departments.Edit";

        public const string Delete = "Permission.Departments.Delete";
    }
}