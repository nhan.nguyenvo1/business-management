﻿namespace PROJECT.WebApi.Permissions.Authorities
{
    /// <summary>
    /// Booking Room Authority
    /// </summary>
    public static class BookingRoomAuthority
    {
        public const string Book = "Permission.BookingRooms.Book";
    }
}