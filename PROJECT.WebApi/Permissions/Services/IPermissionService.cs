﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PROJECT.Application.Dtos.Permissions;
using PROJECT.Application.Wrappers;

namespace PROJECT.WebApi.Permissions.Services
{
    /// <summary>
    /// Permission Service
    /// </summary>
    public interface IPermissionService
    {
        /// <summary>
        /// Get Checked By Role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        Task<Response<ICollection<PermissionVm>>> GetCheckedByRoleId(string roleId);

        /// <summary>
        /// Get Unchecked by Role Id
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        Task<Response<ICollection<PermissionVm>>> GetUnCheckedByRoleId(string roleId);
    }
}