﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using PROJECT.Application.ClaimTypes;
using PROJECT.Application.Dtos.Permissions;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Entities;
using PROJECT.WebApi.Permissions.Display;

namespace PROJECT.WebApi.Permissions.Services
{
    public class PermissionService : IPermissionService
    {
        private readonly List<string> _permissions;
        private readonly RoleManager<AppRole> _roleManager;

        public PermissionService(RoleManager<AppRole> roleManager, List<string> permissions)
        {
            _roleManager = roleManager;
            _permissions = permissions;
        }

        public async Task<Response<ICollection<PermissionVm>>> GetCheckedByRoleId(string roleId)
        {
            var appRole = await _roleManager.FindByIdAsync(roleId);

            if (appRole == null) throw new ApiException($"Role with id = {roleId} does not exists");

            var claims = (await _roleManager.GetClaimsAsync(appRole))
                .Where(claim => claim.Type == AuthorityClaimType.Permission).ToList();

            var result = _permissions.Where(permission =>
                claims.Any(claim => claim.Value.Equals(permission, StringComparison.OrdinalIgnoreCase))).Select(e => new PermissionVm
                {
                    Key = e,
                    Value = AuthorityDisplay.Display[e]
                }).ToList();
            return new Response<ICollection<PermissionVm>>(result);
        }

        public async Task<Response<ICollection<PermissionVm>>> GetUnCheckedByRoleId(string roleId)
        {
            var appRole = await _roleManager.FindByIdAsync(roleId);

            if (appRole == null) throw new ApiException($"Role with id = {roleId} does not exists");

            var claims = (await _roleManager.GetClaimsAsync(appRole))
                .Where(claim => claim.Type == AuthorityClaimType.Permission).ToList();
            var result = _permissions.Where(permission =>
                !claims.Any(claim => claim.Value.Equals(permission, StringComparison.OrdinalIgnoreCase))).Select(e => new PermissionVm
                {
                    Key = e,
                    Value = AuthorityDisplay.Display[e]
                }).ToList();
            return new Response<ICollection<PermissionVm>>(result);
        }
    }
}