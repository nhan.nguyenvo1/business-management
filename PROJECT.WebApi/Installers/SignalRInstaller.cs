﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// SignalR Installer
    /// </summary>
    public class SignalRInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSignalR();
        }
    }
}