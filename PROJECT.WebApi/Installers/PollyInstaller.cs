﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.WebApi.Polly;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// Polly Installer
    /// </summary>
    public class PollyInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IExecuteWithRetry, ExecuteWithRetry>();
        }
    }
}