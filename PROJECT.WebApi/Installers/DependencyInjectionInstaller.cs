﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Application.Interfaces;
using PROJECT.WebApi.Hubs.Implements;
using PROJECT.WebApi.NewHubs.Services;
using PROJECT.WebApi.Permissions.Handlers;
using PROJECT.WebApi.Permissions.Services;
using PROJECT.WebApi.Services;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// DI Installer
    /// </summary>
    public class DependencyInjectionInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();
            services.AddScoped<IAuthUserService, AuthUserService>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ICloudinaryService, CloudinaryService>();
            services.AddTransient<IPermissionService, PermissionService>();
            services.AddTransient<ICompanyService, CompanyService>();
            services.AddTransient<INotificationWrapper, NotificationsWrapper>();
            services.AddTransient<IRoomService, RoomService>();
            services.AddTransient<IHubService, HubService>();
        }
    }
}