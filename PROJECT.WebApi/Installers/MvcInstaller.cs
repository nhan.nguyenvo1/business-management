﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using PROJECT.WebApi.Extensions;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// MVC installer
    /// </summary>
    public class MvcInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddControllers()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                });

            services.AddSwaggerExtension();
            services.AddHealthChecks();
        }
    }
}