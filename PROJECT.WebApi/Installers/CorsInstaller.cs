﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// Cors installer
    /// </summary>
    public class CorsInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", builder =>
                {
                    builder.AllowAnyOrigin();
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });

                options.AddPolicy("_myAllowSpecificOrigins", builder =>
                {
                    builder.WithOrigins(
                        "http://localhost:8080",
                        "https://easy-business.netlify.app",
                        "http://ec2-54-175-95-231.compute-1.amazonaws.com:8080",
                        "http://54.175.95.231:8080");
                    builder.AllowCredentials();
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                });
            });
        }
    }
}