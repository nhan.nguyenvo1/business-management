﻿using Dropbox.Api;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Application.Interfaces;
using PROJECT.Domain.Settings;
using PROJECT.WebApi.Services;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// Storage Installer
    /// </summary>
    public class StorageInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            var dropboxSetting = new DropboxSetting();
            configuration.GetSection("DropboxApiSetting").Bind(dropboxSetting);

            var dropboxClient = new DropboxClient(dropboxSetting.AccessToken);
            services.AddSingleton(dropboxClient);

            services.AddTransient<IDriveStorageService, DropboxStorageService>();
        }
    }
}