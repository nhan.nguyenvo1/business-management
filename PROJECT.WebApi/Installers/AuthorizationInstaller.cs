﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Application.Constants;
using PROJECT.WebApi.Permissions.Requirements;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// Authorization Installer
    /// </summary>
    public class AuthorizationInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            var policies = typeof(Startup).Assembly.GetExportedTypes()
                .Where(x => !string.IsNullOrEmpty(x.Namespace) &&
                            x.Namespace.Equals(PolicyConstant.PolicyNamespace, StringComparison.OrdinalIgnoreCase))
                .ToList();

            var permissions = new List<string>();

            services.AddAuthorization(options =>
            {
                policies.ForEach(policy =>
                {
                    var fieldInfos = policy.GetFields(BindingFlags.Public | BindingFlags.Static |
                                                      BindingFlags.FlattenHierarchy)
                        .Where(fi => fi.IsLiteral && !fi.IsInitOnly)
                        .Select(x => (string) x.GetRawConstantValue())
                        .ToList();

                    fieldInfos.ForEach(info =>
                    {
                        options.AddPolicy(info,
                            builder => builder.AddRequirements(new PermissionRequirement(info)));
                    });

                    permissions.AddRange(fieldInfos);
                });
            });

            services.AddSingleton(permissions);
        }
    }
}