﻿using CloudinaryDotNet;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Domain.Settings;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// Cloudinary Installer
    /// </summary>
    public class CloudinaryInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            var cloudinarySetting = new CloudinarySetting();
            configuration.GetSection(nameof(CloudinarySetting)).Bind(cloudinarySetting);
            services.AddSingleton(cloudinarySetting);

            var account = new Account(cloudinarySetting.CloudName, cloudinarySetting.ApiKey,
                cloudinarySetting.ApiSecret);
            var cloudinary = new Cloudinary(account);

            services.AddSingleton(cloudinary);
        }
    }
}