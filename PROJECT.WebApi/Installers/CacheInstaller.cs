﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Domain.Settings;
using PROJECT.WebApi.Services;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// Cache Installer
    /// </summary>
    public class CacheInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            var redisCacheSetting = new RedisCacheSetting();
            configuration.GetSection(nameof(RedisCacheSetting)).Bind(redisCacheSetting);
            services.AddSingleton(redisCacheSetting);

            if (!redisCacheSetting.Enabled) return;

            services.AddStackExchangeRedisCache(options => options.Configuration = redisCacheSetting.ConnectionString);
            services.AddSingleton<IResponseCacheService, ResponseCacheService>();
        }
    }
}