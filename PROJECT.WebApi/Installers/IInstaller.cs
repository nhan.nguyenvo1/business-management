﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// Installer interface
    /// </summary>
    public interface IInstaller
    {
        /// <summary>
        /// Install Services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        void InstallServices(IServiceCollection services, IConfiguration configuration);
    }
}