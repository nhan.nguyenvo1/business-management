﻿using ADMIN.Infrastructure.DataAccess;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Application;
using PROJECT.Infrastructure.Persistence;
using PROJECT.Infrastructure.Shared;
using PROJECT.WebApi.Extensions;

namespace PROJECT.WebApi.Installers
{
    /// <summary>
    /// Layer Installer
    /// </summary>
    public class LayerInstaller : IInstaller
    {
        /// <inheritdoc />
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddApplicationLayer();
            services.AddPersistenceInfrastructure(configuration);
            services.AddSharedInfrastructure(configuration);
            services.AddInfrastructureDataAccessLayer(configuration);
            services.AddApiVersioningExtension();
        }
    }
}