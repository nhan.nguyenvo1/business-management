﻿using System.Threading.Tasks;
using Quartz;
using Quartz.Impl;

namespace PROJECT.WebApi.CronJobs
{
#pragma warning disable 1591
    public static class JobScheduler
#pragma warning restore 1591
    {
#pragma warning disable 1591
        public static async Task Start()
#pragma warning restore 1591
        {
            var scheduler = await StdSchedulerFactory.GetDefaultScheduler();

            await scheduler.Start();

            var job = JobBuilder.Create<RemoveEmployeesJob>().Build();
            var trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule(s =>
                {
                    s.WithIntervalInHours(24)
                        .OnEveryDay()
                        .StartingDailyAt(TimeOfDay.HourAndMinuteOfDay(0, 0));
                })
                .Build();
            await scheduler.ScheduleJob(job, trigger);
        }
    }
}