﻿using System.Threading.Tasks;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using Quartz;

namespace PROJECT.WebApi.CronJobs
{
#pragma warning disable 1591
    public class RemoveEmployeesJob : IJob
#pragma warning restore 1591
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IUnitOfWork _unitOfWork;

#pragma warning disable 1591
        public RemoveEmployeesJob(IEmployeeRepository employeeRepository, IDateTimeService dateTimeService,
#pragma warning restore 1591
            IUnitOfWork unitOfWork)
        {
            _employeeRepository = employeeRepository;
            _dateTimeService = dateTimeService;
            _unitOfWork = unitOfWork;
        }

#pragma warning disable 1591
        public async Task Execute(IJobExecutionContext context)
#pragma warning restore 1591
        {
            var now = _dateTimeService.UtcNow;
            now = now.AddDays(-7);
            await _employeeRepository.DeleteByConditionAsync(employee =>
                !employee.EmailConfirmed && employee.CreatedDate.Date < now.Date);
            await _unitOfWork.CommitAsync();
        }
    }
}