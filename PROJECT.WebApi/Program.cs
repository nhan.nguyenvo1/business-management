using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace PROJECT.WebApi
{
#pragma warning disable 1591
    public class Program
#pragma warning restore 1591
    {
#pragma warning disable 1591
        public static void Main(string[] args)
#pragma warning restore 1591
        {
            CreateHostBuilder(args).Build().Run();
        }

#pragma warning disable 1591
        public static IHostBuilder CreateHostBuilder(string[] args)
#pragma warning restore 1591
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddConsole();
                })
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
        }
    }
}