FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY ["PROJECT.WebApi/PROJECT.WebApi.csproj", "PROJECT.WebApi/"]
COPY ["PROJECT.WebApi/Setup.sh", "PROJECT.WebApi/"]
COPY ["PROJECT.Infrastructure.Persistence/PROJECT.Infrastructure.Persistence.csproj", "PROJECT.Infrastructure.Persistence/"]
COPY ["PROJECT.Application/PROJECT.Application.csproj", "PROJECT.Application/"]
COPY ["PROJECT.Domain/PROJECT.Domain.csproj", "PROJECT.Domain/"]
COPY ["PROJECT.Infrastructure.Shared/PROJECT.Infrastructure.Shared.csproj", "PROJECT.Infrastructure.Shared/"]
RUN dotnet restore "PROJECT.WebApi/PROJECT.WebApi.csproj"

RUN dotnet tool install --global dotnet-ef

COPY . .
WORKDIR "/src/PROJECT.Infrastructure.Persistence"

RUN /root/.dotnet/tools/dotnet-ef migrations add InitialMigrations --startup-project ../PROJECT.WebApi

WORKDIR "/src/PROJECT.WebApi"
RUN chmod +x ./Setup.sh
CMD /bin/bash ./Setup.sh