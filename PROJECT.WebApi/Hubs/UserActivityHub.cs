﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.Dtos.User;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;

namespace PROJECT.WebApi.Hubs
{
    [Authorize]
    public class UserActivityHub : Hub
    {
        private static readonly ICollection<ActivityUserVm> Users = new List<ActivityUserVm>();
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;
        private readonly IActivityRepository _activityRepository;
        private readonly IDateTimeService _dateTimeService;

        public UserActivityHub(IUserRepository userRepository,
                               IMapper mapper,
                               IUnitOfWork unitOfWork,
                               IActivityRepository activityRepository,
                               IDateTimeService dateTimeService)
        {
            _userRepository = userRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _activityRepository = activityRepository;
            _dateTimeService = dateTimeService;
        }

        public override async Task OnConnectedAsync()
        {
            await _activityRepository.AddAsync(new Activity
            {
                Date = _dateTimeService.UtcNow,
                UserId = Context.UserIdentifier,
                ActivityType = ActivityType.Login
            });

            if (!Users.Any(user => user.Id.Equals(Context.UserIdentifier)))
            {
                var user = await _userRepository.GetByIdAsync(Context.UserIdentifier);
                user.Status = UserStatus.Online;
                user = await _userRepository.UpdateAsync(user);
                await _unitOfWork.CommitAsync();
                var result = _mapper.Map<ActivityUserVm>(user);
                Users.Add(result);
                await SendUserList(Users);
            }

            await base.OnConnectedAsync();
        }

        public async Task SendUserList(ICollection<ActivityUserVm> users)
        {
            await Clients.All.SendAsync("UpdateActivityUser", users);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if (Users.Any(entry => entry.Id.Equals(Context.UserIdentifier)))
            {
                var user = await _userRepository.GetByIdAsync(Context.UserIdentifier);
                user.Status = UserStatus.Online;
                user = await _userRepository.UpdateAsync(user);
                await _unitOfWork.CommitAsync();

                Users.Remove(Users.FirstOrDefault(entry => entry.Id.Equals(Context.UserIdentifier)));
                await SendUserList(Users);
            }

            await base.OnDisconnectedAsync(exception);
        }
    }
}