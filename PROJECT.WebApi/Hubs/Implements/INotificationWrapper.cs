﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using PROJECT.WebApi.Hubs.Interfaces;
using PROJECT.WebApi.Hubs.Models;

namespace PROJECT.WebApi.Hubs.Implements
{
    public interface INotificationWrapper
    {
        INotificationWrapper SetHubClient(IHubClients<INotifications> hubClients);

        Task SendNotification(NotificationModel model);

        Task SendNotification(IEnumerable<string> userIds, NotificationModel model);

        Task SendNotification(string userId, NotificationModel model);
    }
}