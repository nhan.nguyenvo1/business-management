﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PROJECT.Application.Dtos.Notifications;
using PROJECT.Application.Interfaces;
using PROJECT.Domain.Entities;
using PROJECT.Infrastructure.Persistence.Contexts;
using PROJECT.WebApi.Hubs.Groups;
using PROJECT.WebApi.Hubs.Interfaces;
using PROJECT.WebApi.Hubs.Models;
using PROJECT.WebApi.Polly;

namespace PROJECT.WebApi.Hubs.Implements
{
    public class NotificationsWrapper : INotificationWrapper
    {
        private readonly ICompanyService _companyService;
        private readonly ProjectDbContext _context;

        private readonly IExecuteWithRetry _executeWithRetry;
        private readonly ILogger<NotificationsWrapper> _logger;
        private readonly IMapper _mapper;
        private IHubClients<INotifications> _hubClients;

        public NotificationsWrapper(IExecuteWithRetry executeWithRetry,
            ILogger<NotificationsWrapper> logger,
            ICompanyService companyService,
            ProjectDbContext context, IMapper mapper)
        {
            _executeWithRetry = executeWithRetry;
            _logger = logger;
            _companyService = companyService;
            _context = context;
            _mapper = mapper;
        }

        public INotificationWrapper SetHubClient(IHubClients<INotifications> hubClients)
        {
            _hubClients = hubClients;
            return this;
        }

        public async Task SendNotification(NotificationModel model)
        {
            var hub = IsolateClientGroup();
            var notification = new Notification
            {
                Category = model.Category,
                Title = model.Title,
                Description = model.Description,
                Url = model.Url
            };
            notification = (await _context.Notifications.AddAsync(notification)).Entity;
            await _context.SaveChangesAsync();
            await _context.UserNotifications.AddRangeAsync(_context.Users.Select(user => new UserNotification
            {
                Read = false,
                UserId = user.Id,
                NotificationId = notification.Id
            }));
            await _context.SaveChangesAsync();

            await _executeWithRetry.ExecuteAsync(() =>
                hub.SendNotification(_mapper.Map<NotificationDto>(notification)));
        }

        public async Task SendNotification(IEnumerable<string> userIds, NotificationModel model)
        {
            var hub = IsolateUsersGroup(userIds);

            var notification = new Notification
            {
                Category = model.Category,
                Title = model.Title,
                Description = model.Description,
                Url = model.Url
            };
            notification = (await _context.Notifications.AddAsync(notification)).Entity;
            await _context.SaveChangesAsync();
            await _context.UserNotifications.AddRangeAsync(userIds.Select(userId => new UserNotification
            {
                Read = false,
                UserId = userId,
                NotificationId = notification.Id
            }));
            await _context.SaveChangesAsync();

            await _executeWithRetry.ExecuteAsync(() =>
                hub.SendNotification(_mapper.Map<NotificationDto>(notification)));
        }

        public async Task SendNotification(string userId, NotificationModel model)
        {
            var hub = IsolateUsersGroup(userId);

            var notification = new Notification
            {
                Category = model.Category,
                Title = model.Title,
                Description = model.Description,
                Url = model.Url
            };
            notification = (await _context.Notifications.AddAsync(notification)).Entity;
            await _context.SaveChangesAsync();
            await _context.UserNotifications.AddRangeAsync(new UserNotification
            {
                Read = false,
                UserId = userId,
                NotificationId = notification.Id
            });
            await _context.SaveChangesAsync();

            await _executeWithRetry.ExecuteAsync(() =>
                hub.SendNotification(_mapper.Map<NotificationDto>(notification)));
        }

        private INotifications IsolateClientGroup()
        {
            var group = new ClientNotificationGroup(_companyService.CompanyId);
            _logger.LogDebug($"Broadcasting to notification group {group.Name}");
            return _hubClients.Group(group.Name);
        }

        private INotifications IsolateUsersGroup(IEnumerable<string> userIds)
        {
            var groups = userIds.Select(userId => new UserGroup(_companyService.CompanyId, userId).Name);
            return _hubClients.Groups(groups);
        }

        private INotifications IsolateUsersGroup(string userId)
        {
            var group = new UserGroup(_companyService.CompanyId, userId);
            return _hubClients.Group(group.Name);
        }
    }
}