﻿using System.Threading.Tasks;
using PROJECT.Application.Dtos.Notifications;

namespace PROJECT.WebApi.Hubs.Interfaces
{
    public interface INotifications
    {
        Task SendNotification(NotificationDto notification);

        Task MarkAsRead(int notificationId);

        Task MarkAll(bool read);
    }
}