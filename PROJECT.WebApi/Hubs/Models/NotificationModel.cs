﻿using PROJECT.Domain.Enums;

namespace PROJECT.WebApi.Hubs.Models
{
    public class NotificationModel
    {
        public NotificationCaterogy Category { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }
    }
}