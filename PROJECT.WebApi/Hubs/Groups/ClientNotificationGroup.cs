﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.BCL;

namespace PROJECT.WebApi.Hubs.Groups
{
    public class ClientNotificationGroup
    {
        private readonly int _companyId;

        public ClientNotificationGroup(HttpContext httpContext)
        {
            Preconditions.NotNull(httpContext, nameof(httpContext));

            var companyHeader = httpContext.Request.Headers["X-Company-Id"];
            _companyId = int.Parse(companyHeader.First());
        }

        public ClientNotificationGroup(int? companyId)
        {
            if (!companyId.HasValue)
                throw new ArgumentNullException(nameof(companyId), $"{nameof(companyId)} cannot be null");

            _companyId = companyId.Value;
        }

        public string Name => $"{nameof(ClientNotificationGroup)}-{_companyId}";
    }
}