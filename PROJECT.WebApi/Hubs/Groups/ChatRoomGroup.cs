﻿using Microsoft.AspNetCore.Http;
using PROJECT.Application.BCL;
using System.Linq;

namespace PROJECT.WebApi.Hubs.Groups
{
    public class ChatRoomGroup
    {
        private readonly int _companyId;
        private readonly string _roomName;

        public ChatRoomGroup(HttpContext httpContext, string roomName)
        {
            Preconditions.NotNull(httpContext, nameof(httpContext));
            Preconditions.NotNullOrEmpty(roomName, nameof(roomName));

            var companyHeader = httpContext.Request.Headers["X-Company-Id"];
            _companyId = int.Parse(companyHeader.First());
            _roomName = roomName;
        }

        public ChatRoomGroup(int companyId, string roomName)
        {
            Preconditions.MustNotEqual(companyId, 0, nameof(companyId));
            Preconditions.NotNullOrEmpty(roomName, nameof(roomName));

            _companyId = companyId;
            _roomName = roomName;
        }

        public string Name => $"{nameof(ChatRoomGroup)}-{_companyId}-{_roomName}";
    }
}
