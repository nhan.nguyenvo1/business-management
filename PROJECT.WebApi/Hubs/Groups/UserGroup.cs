﻿using Microsoft.AspNetCore.Http;
using PROJECT.Application.BCL;
using System;
using System.Linq;

namespace PROJECT.WebApi.Hubs.Groups
{
    public class UserGroup
    {
        private readonly int _companyId;
        private readonly string _userId;

        public UserGroup(HttpContext httpContext, string userId)
        {
            Preconditions.NotNull(httpContext, nameof(httpContext));
            Preconditions.NotNullOrEmpty(userId, nameof(userId));

            var companyHeader = httpContext.Request.Headers["X-Company-Id"];
            _companyId = int.Parse(companyHeader.First());
            _userId = userId;
        }

        public UserGroup(int? companyId, string userId)
        {
            if (!companyId.HasValue)
                throw new ArgumentNullException(nameof(companyId), $"{nameof(companyId)} cannot be null");

            Preconditions.NotNullOrEmpty(userId, nameof(userId));

            _companyId = companyId.Value;
            _userId = userId;
        }

        public string Name => $"{nameof(UserGroup)}-{_companyId}-{_userId}";
    }
}
