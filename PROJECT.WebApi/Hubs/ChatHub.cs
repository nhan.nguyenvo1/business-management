﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using PROJECT.Application.Dtos.Chat;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Entities;
using PROJECT.Domain.Enums;
using PROJECT.WebApi.Hubs.Groups;

namespace PROJECT.WebApi.Hubs
{
    [Authorize]
    public class ChatHub : Hub, IChatService
    {
        private readonly IAppChatRepository _appChatRepository;
        private readonly IAppChatRoomRepository _appRoomRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _userRepository;

        public ChatHub(IAppChatRepository appChatRepository, IAppChatRoomRepository appChatRoomRepository,
            IUserRepository userRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _appChatRepository = appChatRepository;
            _appRoomRepository = appChatRoomRepository;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Task BroadcastMessage(string userId, string message)
        {
            throw new NotImplementedException();
        }

        public async Task CreateRoom(string roomName, string password = "")
        {
            var room = new AppRoom
            {
                Name = roomName,
                RoomType = password == string.Empty ? RoomType.Public : RoomType.Private,
                Password = password == string.Empty ? string.Empty : password,
                CompanyId = int.Parse(Context.GetHttpContext().Request.Headers["X-Company-Id"])
            };

            // Create Room
            room = await _appRoomRepository.AddAsync(room);

            // Join Caller To Room
            room.RoomUsers.Add(await _userRepository.GetByIdAsync(Context.UserIdentifier));

            // Commit
            await _unitOfWork.CommitAsync();

            // Send New Room To All Clients
            var clientGroup = new ClientNotificationGroup(Context.GetHttpContext());
            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);

            await Clients.Group(userGroup.Name).SendAsync("ReceiveYourRoomInfo", _mapper.Map<RoomVm>(room));
            await Clients.GroupExcept(clientGroup.Name, Context.ConnectionId).SendAsync("ReceiveNewRoomInfo", _mapper.Map<RoomVm>(room));
        }

        public async Task GetRoomInfo(int roomId, int page, int limit)
        {
            var room = await _appRoomRepository.GetByIdAsync(roomId);
            var userId = Context.UserIdentifier;

            if (!room.ReadUsers.Any(user => user.Id == userId))
            {
                room.ReadUsers.Add(await _userRepository.GetByIdAsync(userId));
                await _unitOfWork.CommitAsync();
            }

            var roomInfo = _mapper.Map<RoomInfoVm>(room);

            roomInfo.Total = await _appChatRepository.CountAsync(chat => chat.RoomId == roomId);
            roomInfo.Page = page;
            roomInfo.Limit = limit;

            roomInfo.Messages = await _appChatRepository.GetByRoomId<MessageVm>(roomId, page, limit);

            roomInfo.Messages = roomInfo.Messages.Select(message =>
            {
                message.Me = message.Sender.Id == Context.UserIdentifier;
                return message;
            }).Reverse().ToList();

            var chatGroup = new ChatRoomGroup(Context.GetHttpContext(), roomInfo.Name);

            await Groups.AddToGroupAsync(Context.ConnectionId, chatGroup.Name);

            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);

            await Clients.Group(userGroup.Name).SendAsync("ReceiveRoomInfo", roomInfo);
        }

        /// <summary>
        ///     Get list rooms
        /// </summary>
        /// <returns></returns>
        public async Task GetAvailableRooms(int page, int limit, string search)
        {
            var userId = Context.UserIdentifier;
            var rooms = await _appRoomRepository.GetAvailableByUserId(userId);
            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);

            await Clients.Group(userGroup.Name).SendAsync("ReceiveAvailableRooms",
                await rooms.ProjectTo<RoomVm>(_mapper.ConfigurationProvider).ToListAsync());
        }

        public Task InviteUserToRoom(string userId, int roomId)
        {
            throw new NotImplementedException();
        }

        public async Task JoinRoom(int roomId, string password = "")
        {
            var room = await _appRoomRepository.GetByIdAsync(roomId);
            if (!string.IsNullOrEmpty(password) && room.RoomType == RoomType.Private && room.Password != password)
                throw new HubException("Room password is wrong");
            room.RoomUsers.Add(await _userRepository.GetByIdAsync(Context.UserIdentifier));
            await _unitOfWork.CommitAsync();

            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);

            await Clients.Group(userGroup.Name).SendAsync("ReceiveNewOwnRoom", _mapper.Map<RoomVm>(room));
        }

        public Task KickUser(string targetId, int roomId, string callingId, string reason)
        {
            throw new NotImplementedException();
        }

        public Task LeaveRoom(int roomId)
        {
            throw new NotImplementedException();
        }

        public Task PostNotification(int roomId, string imageUrl, string source, string content)
        {
            throw new NotImplementedException();
        }

        public async Task SendMessage(string message, int roomId)
        {
            try
            {
                var room = await _appRoomRepository.GetByIdAsync(roomId);
                var chat = new AppChat
                {
                    SenderId = Context.UserIdentifier,
                    Content = message,
                    CompanyId = int.Parse(Context.GetHttpContext().Request.Headers["X-Company-Id"])
                };
                room.AppChats.Add(chat);

                await _unitOfWork.CommitAsync();

                var messageVm = _mapper.Map<MessageVm>(chat);
                var group = new ChatRoomGroup(Context.GetHttpContext(), room.Name);
                await Clients.GroupExcept(group.Name, Context.ConnectionId).SendAsync("ReceiveNewMessage", messageVm)
                    .ConfigureAwait(false);
                messageVm.Me = true;
                var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);

                await Clients.Group(userGroup.Name).SendAsync("ReplaceMessage", messageVm).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                throw new HubException(e.Message);
            }
        }

        public async Task Typing(int roomId, bool flag)
        {
            var room = await _appRoomRepository.GetByIdAsync<RoomNameVm>(roomId);
            var user = await _userRepository.GetByIdAsync(Context.UserIdentifier);
            var group = new ChatRoomGroup(Context.GetHttpContext(), room.Name);
            await Clients.GroupExcept(group.Name, Context.ConnectionId)
                .SendAsync("SetTyping", $"{user.FullName} is typing", flag);
        }

        public async Task GetOwnRooms(int page, int limit, string search)
        {
            var userId = Context.UserIdentifier;
            var rooms = await _appRoomRepository.GetOwnByUserId(userId, page, limit, search);

            var roomVms = await rooms.Select(room => new RoomVm
            {
                Id = room.Id,
                Name = room.Name,
                AvatarUrl = room.AvatarUrl,
                RoomType = room.RoomType,
                IsRead = room.ReadUsers.Any(user => user.Id.Equals(userId))
            }).ToListAsync();

            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);

            await Clients.Group(userGroup.Name).SendAsync("ReceiveOwnRooms", roomVms);
        }

        public override async Task OnConnectedAsync()
        {
            Console.WriteLine("Connected");
            var clientNotificationGroup = new ClientNotificationGroup(Context.GetHttpContext());
            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);

            await Groups.AddToGroupAsync(Context.ConnectionId, clientNotificationGroup.Name);
            await Groups.AddToGroupAsync(Context.ConnectionId, userGroup.Name);

            await GetOwnRooms(1, 10, string.Empty);
            await GetAvailableRooms(1, 10, string.Empty);

            await base.OnConnectedAsync();
        }
    }
}