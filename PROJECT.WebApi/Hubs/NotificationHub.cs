﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.WebApi.Hubs.Groups;
using PROJECT.WebApi.Hubs.Interfaces;

namespace PROJECT.WebApi.Hubs
{
    [Authorize]
    public class NotificationHub : Hub<INotifications>
    {
        private readonly ILogger<NotificationHub> _logger;
        private readonly INotificationRepository _notificationRepository;
        private readonly IUnitOfWork _unitOfWork;

        public NotificationHub(ILogger<NotificationHub> logger, INotificationRepository notificationRepository,
            IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _notificationRepository = notificationRepository;
            _unitOfWork = unitOfWork;
        }

        public override Task OnConnectedAsync()
        {
            var group = new ClientNotificationGroup(Context.GetHttpContext());
            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);

            Groups.AddToGroupAsync(Context.ConnectionId, group.Name);
            Groups.AddToGroupAsync(Context.ConnectionId, userGroup.Name);

            _logger.LogDebug($"NotificationHub: Connected to group: {group.Name}");

            return base.OnConnectedAsync();
        }

        public async Task MarkAsRead(int notificationId)
        {
            await _notificationRepository.MarkAsReadAsync(Context.UserIdentifier, notificationId);
            await _unitOfWork.CommitAsync();
            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);
            await Clients.Group(userGroup.Name).MarkAsRead(notificationId);
        }

        public async Task MarkAll(bool read)
        {
            var companyHeader = Context.GetHttpContext().Request.Headers["X-Company-Id"];
            var companyId = int.Parse(companyHeader.First());

            await _notificationRepository.MarkAllAsync(Context.UserIdentifier, companyId, read);
            await _unitOfWork.CommitAsync();
            var userGroup = new UserGroup(Context.GetHttpContext(), Context.UserIdentifier);
            await Clients.Group(userGroup.Name).MarkAll(read);
        }
    }
}