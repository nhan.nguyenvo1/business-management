using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PROJECT.WebApi.Extensions;
using PROJECT.WebApi.Hubs;
using PROJECT.WebApi.Installers;
using PROJECT.WebApi.NewHubs.Hubs;

namespace PROJECT.WebApi
{
#pragma warning disable 1591
    public class Startup
#pragma warning restore 1591
    {
        private readonly IConfiguration _configuration;

#pragma warning disable 1591
        public Startup(IConfiguration configuration)
#pragma warning restore 1591
        {
            _configuration = configuration;
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            var installers = typeof(Startup).Assembly.ExportedTypes
                .Where(x => typeof(IInstaller).IsAssignableFrom(x) && !x.IsAbstract && !x.IsInterface)
                .Select(Activator.CreateInstance).Cast<IInstaller>().ToList();

            installers.ForEach(installer => installer.InstallServices(services, _configuration));
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseSwaggerExtension();

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseCors("_myAllowSpecificOrigins");

            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseErrorHandlingMiddleware();

            app.UseHealthChecks("/health");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<ChatHub>("/hubs/chat");
                endpoints.MapHub<UserActivityHub>("/hubs/activity-users");
                endpoints.MapHub<NotificationHub>("/hubs/notifications");
                endpoints.MapHub<RoomHub>("/hubs/room");
            });
        }
    }
}