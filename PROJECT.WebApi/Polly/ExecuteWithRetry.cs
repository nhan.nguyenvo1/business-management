﻿using System;
using System.Threading.Tasks;
using Polly;
using Polly.CircuitBreaker;
using Polly.Retry;

namespace PROJECT.WebApi.Polly
{
    public class ExecuteWithRetry : IExecuteWithRetry
    {
        private const int RetryCount = 3;
        private const int ExceptionsAllowedBeforeBreaking = 3;
        private static readonly TimeSpan DurationOfBreak = TimeSpan.FromMinutes(1);

        private static readonly CircuitBreakerPolicy defaultCircuitBreakerPolicy = Policy
            .Handle<Exception>()
            .CircuitBreaker(ExceptionsAllowedBeforeBreaking, DurationOfBreak);

        private static readonly RetryPolicy defaultRetryPolicy = Policy
            .Handle<Exception>()
            .WaitAndRetry(
                RetryCount,
                retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                (exception, timespan, context) =>
                {
                    Console.WriteLine($"Retrying - After: {timespan} Message: {exception.Message}");
                });


        private static readonly AsyncCircuitBreakerPolicy defaultAsyncCircuitBreakerPolicy = Policy
            .Handle<Exception>()
            .CircuitBreakerAsync(ExceptionsAllowedBeforeBreaking, DurationOfBreak);

        private static readonly AsyncRetryPolicy defaultAsyncRetryPolicy = Policy
            .Handle<Exception>()
            .WaitAndRetryAsync(
                RetryCount,
                retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                (exception, timespan, context) =>
                {
                    Console.WriteLine($"Retrying - After: {timespan} Message: {exception.Message}");
                });


        public T Execute<T>(Func<T> function)
        {
            return Execute(defaultRetryPolicy, defaultCircuitBreakerPolicy, function);
        }

        public T Execute<T>(
            RetryPolicy retryPolicy,
            CircuitBreakerPolicy circuitBreakerPolicy,
            Func<T> function)
        {
            return retryPolicy
                .Wrap(circuitBreakerPolicy)
                .Execute(function);
        }


        public Task<T> ExecuteAsync<T>(Func<Task<T>> asyncFunction)
        {
            return ExecuteAsync(defaultAsyncRetryPolicy, defaultAsyncCircuitBreakerPolicy, asyncFunction);
        }

        public Task<T> ExecuteAsync<T>(
            AsyncRetryPolicy retryPolicy,
            AsyncCircuitBreakerPolicy circuitBreakerPolicy,
            Func<Task<T>> asyncFunction)
        {
            return retryPolicy
                .WrapAsync(circuitBreakerPolicy)
                .ExecuteAsync(asyncFunction);
        }

        public Task ExecuteAsync(Func<Task> asyncFunction)
        {
            return ExecuteAsync(defaultAsyncRetryPolicy, defaultAsyncCircuitBreakerPolicy, asyncFunction);
        }

        public Task ExecuteAsync(AsyncRetryPolicy retryPolicy, AsyncCircuitBreakerPolicy circuitBreakerPolicy,
            Func<Task> asyncFunction)
        {
            return retryPolicy
                .WrapAsync(circuitBreakerPolicy)
                .ExecuteAsync(asyncFunction);
        }
    }
}