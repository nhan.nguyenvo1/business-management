﻿using System;
using System.Threading.Tasks;
using Polly.CircuitBreaker;
using Polly.Retry;

namespace PROJECT.WebApi.Polly
{
    /// <summary>
    /// Execute with retry
    /// </summary>
    public interface IExecuteWithRetry
    {
        /// <summary>
        /// Execute
        /// </summary>
        /// <param name="function"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T Execute<T>(Func<T> function);

        /// <summary>
        /// Execute
        /// </summary>
        /// <param name="retryPolicy"></param>
        /// <param name="circuitBreakerPolicy"></param>
        /// <param name="function"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T Execute<T>(RetryPolicy retryPolicy, CircuitBreakerPolicy circuitBreakerPolicy, Func<T> function);

        /// <summary>
        /// Execute Async
        /// </summary>
        /// <param name="asyncFunction"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<T> ExecuteAsync<T>(Func<Task<T>> asyncFunction);

        /// <summary>
        /// Execute Async
        /// </summary>
        /// <param name="retryPolicy"></param>
        /// <param name="circuitBreakerPolicy"></param>
        /// <param name="asyncFunction"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<T> ExecuteAsync<T>(AsyncRetryPolicy retryPolicy, AsyncCircuitBreakerPolicy circuitBreakerPolicy,
            Func<Task<T>> asyncFunction);

        /// <summary>
        /// Execute Async
        /// </summary>
        /// <param name="asyncFunction"></param>
        /// <returns></returns>
        Task ExecuteAsync(Func<Task> asyncFunction);

        /// <summary>
        /// Execute Async
        /// </summary>
        /// <param name="retryPolicy"></param>
        /// <param name="circuitBreakerPolicy"></param>
        /// <param name="asyncFunction"></param>
        /// <returns></returns>
        Task ExecuteAsync(AsyncRetryPolicy retryPolicy, AsyncCircuitBreakerPolicy circuitBreakerPolicy,
            Func<Task> asyncFunction);
    }
}