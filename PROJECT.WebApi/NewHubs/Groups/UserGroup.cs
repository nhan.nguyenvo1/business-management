﻿namespace PROJECT.WebApi.NewHubs.Groups
{
    /// <summary>
    /// User Group
    /// </summary>
    public class UserGroup
    {
        private readonly string userId;
        private readonly int companyId;

        /// <summary>
        /// Create a new instance of <see cref="UserGroup"/>
        /// </summary>
        public UserGroup(string userId, int companyId)
        {
            this.userId = userId;
            this.companyId = companyId;
        }

        /// <summary>
        /// Name
        /// </summary>
        public string Name => $"{nameof(UserGroup)}-{userId}-{companyId}";
    }
}
