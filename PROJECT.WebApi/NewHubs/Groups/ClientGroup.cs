﻿namespace PROJECT.WebApi.NewHubs.Groups
{
    /// <summary>
    /// Client Group
    /// </summary>
    public class ClientGroup
    {
        private readonly int companyId;

        /// <summary>
        /// Create a new instance of <see cref="ClientGroup" />
        /// </summary>
        public ClientGroup(int companyId)
        {
            this.companyId = companyId;
        }

        /// <summary>
        /// Client Group Name
        /// </summary>
        public string Name => $"{nameof(ClientGroup)}-{companyId}";
    }
}
