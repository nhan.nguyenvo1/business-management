﻿namespace PROJECT.WebApi.NewHubs.Groups
{
    /// <summary>
    /// Room group
    /// </summary>
    public class RoomGroup
    {
        private readonly int roomId;
        private readonly int companyId;

        /// <summary>
        /// Create a new instance of <see cref="RoomGroup"/>
        /// </summary>
        public RoomGroup(int roomId, int companyId)
        {
            this.roomId = roomId;
            this.companyId = companyId;
        }

        /// <summary>
        /// Name
        /// </summary>
        public string Name => $"{nameof(RoomGroup)}-{roomId}-{companyId}";
    }
}
