﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.BCL;
using PROJECT.Application.Interfaces;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Controllers;
using PROJECT.WebApi.NewHubs.Groups;
using PROJECT.WebApi.NewHubs.Hubs;
using PROJECT.WebApi.NewHubs.Implements;
using PROJECT.WebApi.NewHubs.Models;
using PROJECT.WebApi.NewHubs.Requests;
using PROJECT.WebApi.NewHubs.Services;
using PROJECT.WebApi.Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PROJECT.WebApi.NewHubs.Controllers.v1
{
    /// <summary>
    /// Controller
    /// </summary>
    [CompanyAuthorize]
    public class RoomsController : BaseController
    {
        private readonly IRoomHub notifier;
        private readonly IRoomService roomService;
        private readonly ICompanyService companyService;
        private readonly IHubService hubService;
        private readonly IHubContext<RoomHub, IRoomHub> hubContext;
        /// <summary>
        /// Create a new instance of <see cref="RoomsController"/>
        /// </summary>
        public RoomsController(IHubContext<RoomHub, IRoomHub> hubContext,
                               IExecuteWithRetry executeWithRetry,
                               IRoomService roomService,
                               IHubService hubService, ICompanyService companyService)
        {
            notifier = new RoomNotification(executeWithRetry, hubContext.Clients, hubService);
            this.roomService = roomService;
            this.companyService = companyService;
            this.hubService = hubService;
            this.hubContext = hubContext;
        }

        /// <summary>
        /// GET api/v1/rooms
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Task<IReadOnlyCollection<ChatRoomVm>> Get()
        {
            return roomService.GetAsync();
        }

        /// <summary>
        /// POST api/v1/rooms/joined
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("joined")]
        public Task JoinRoom([FromBody] JoinRoomRequest request)
        {
            Preconditions.NotNull(request, nameof(request));
            var currentRoom = request.CurrentRoom;
            var previousRoom = request.PreviousRoom;

            Preconditions.NotNull(currentRoom, nameof(currentRoom));
            RoomGroup roomGroup;
            var tasks = new List<Task>();

            if (previousRoom.Id > 0)
            {
                roomGroup = new RoomGroup(previousRoom.Id, companyService.CompanyId.Value);
                tasks.Add(hubContext.Groups.RemoveFromGroupAsync(hubService.ConnectionId, roomGroup.Name));
            }

            roomGroup = new RoomGroup(currentRoom.Id, companyService.CompanyId.Value);
            tasks.Add(hubContext.Groups.AddToGroupAsync(hubService.ConnectionId, roomGroup.Name));

            return Task.WhenAll(tasks);
        }

        /// <summary>
        /// GET api/v1/rooms/{roomId}/users
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpGet("{roomId}/users")]
        public Task<IReadOnlyCollection<UserVm>> GetUsers(int roomId)
        {
            return roomService.GetUsersAsync(roomId);
        }

        /// <summary>
        /// GET api/v1/rooms/users/{userId}
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("users/{userId}")]
        public Task<UserVm> GetUserById(string userId)
        {
            return roomService.GetUserByIdAsync(userId);
        }

        /// <summary>
        /// GET api/v1/rooms/{roomId}/messages
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet("{roomId}/messages")]
        public Task<(IReadOnlyCollection<ChatMessageVm>, int)> GetMessages(int roomId, int page = 1, int limit = 10)
        {
            return roomService.GetMessagesByRoomIdAsync(roomId, page, limit);
        }

        /// <summary>
        /// POST api/v1/rooms/created
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task RoomCreated([FromBody] CreateRoomRequest request)
        {
            Preconditions.NotNullOrWhitespace(request.RoomName, nameof(request.RoomName));

            var room = await roomService.CreateAsync(request.RoomName);
            await notifier.RoomCreated(room);
        }

        /// <summary>
        /// POST api/v1/rooms/invited
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost("{roomId}/users/{userId}/invited")]
        public async Task RoomInvited(int roomId, string userId)
        {
            Preconditions.MustNotEqual(roomId, 0, nameof(roomId));
            Preconditions.NotNullOrWhitespace(userId, nameof(userId));

            var room = await roomService.InviteAsync(userId, roomId);

            if (room == null) return;
            var user = await roomService.GetUserByIdAsync(userId);
            var owner = await roomService.GetUserByIdAsync(room.CreatedBy);
            var message = await roomService.SendSystemMessageAsync(room.Id, $"{user.FullName} was invited into this room by {owner.FullName}");

            room.ExternalUserId = userId;
            await Task.WhenAll(notifier.RoomInvited(room), notifier.SystemSent(message));
        }

        /// <summary>
        /// POST api/v1/rooms/invited
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        [HttpPost("{roomId}/departments/{departmentId}/invited")]
        public async Task RoomInvited(int roomId, int departmentId)
        {
            Preconditions.MustNotEqual(roomId, 0, nameof(roomId));
            Preconditions.MustNotEqual(departmentId, 0, nameof(departmentId));

            var (room, userIds) = await roomService.InviteAsync(departmentId, roomId);

            foreach (var userId in userIds)
            {
                room.ExternalUserId = userId;
                await notifier.RoomInvited(room);
            }
        }

        /// <summary>
        /// POST api/v1/rooms/kicked
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost("{roomId}/users/{userId}/kicked")]
        public async Task RoomKicked(int roomId, string userId)
        {
            var room = await roomService.KickAsync(userId, roomId);

            var user = await roomService.GetUserByIdAsync(userId);
            var owner = await roomService.GetUserByIdAsync(room.CreatedBy);
            var message = await roomService.SendSystemMessageAsync(room.Id, $"{user.FullName} was kicked out this room by {owner.FullName}");

            room.ExternalUserId = userId;
            await Task.WhenAll(notifier.RoomKicked(room), notifier.SystemSent(message));
        }

        /// <summary>
        /// POST api/v1/rooms/leaved
        /// </summary>
        /// <param name="roomId"></param>
        /// <returns></returns>
        [HttpPost("{roomId}/leaved")]
        public async Task RoomLeaved(int roomId)
        {
            var (room, userId) = await roomService.LeaveAsync(roomId);

            var user = await roomService.GetUserByIdAsync(userId);
            var message = await roomService.SendSystemMessageAsync(room.Id, $"{user.FullName} has left this room");

            room.ExternalUserId = userId;
            await Task.WhenAll(notifier.RoomLeaved(room), notifier.SystemSent(message));
        }

        /// <summary>
        /// POST api/v1/rooms/{roomId}/messages
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("{roomId}/messages")]
        public async Task<ChatMessageVm> SendMessage(int roomId, [FromBody] SendMessageRequest request)
        {
            Preconditions.MustNotEqual(roomId, 0, nameof(roomId));
            Preconditions.NotNull(request, nameof(request));
            Preconditions.NotNullOrWhitespace(request.Content, nameof(request.Content));

            var message = await roomService.SendMessageAsync(roomId, request);

            await notifier.MessageSent(message);

            var room = await roomService.GetByIdAsync(message.RoomId);
            var userIds = (await roomService.GetUsersAsync(message.RoomId)).Select(e => e.Id);

            foreach (var userId in userIds)
            {
                room.ExternalUserId = userId;
                await notifier.RoomUpdated(room);
            }

            message.Me = true;

            return message;
        }
    }
}
