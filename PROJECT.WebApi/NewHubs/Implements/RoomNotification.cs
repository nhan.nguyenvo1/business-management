﻿using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.BCL;
using PROJECT.Application.Interfaces;
using PROJECT.WebApi.NewHubs.Groups;
using PROJECT.WebApi.NewHubs.Hubs;
using PROJECT.WebApi.NewHubs.Models;
using PROJECT.WebApi.Polly;
using System.Threading.Tasks;

namespace PROJECT.WebApi.NewHubs.Implements
{
    /// <summary>
    /// Room notification
    /// </summary>
    public class RoomNotification : IRoomHub
    {
        private readonly IExecuteWithRetry executeWithRetry;
        private readonly IHubClients<IRoomHub> clients;
        private readonly IHubService hubService;

        /// <summary>
        /// Create a new instance of <see cref="RoomNotification"/>
        /// </summary>
        /// <param name="executeWithRetry"></param>
        /// <param name="clients"></param>
        /// <param name="hubService"></param>
        public RoomNotification(IExecuteWithRetry executeWithRetry, IHubClients<IRoomHub> clients, IHubService hubService)
        {
            this.executeWithRetry = executeWithRetry;
            this.clients = clients;
            this.hubService = hubService;
        }

        /// <inheritdoc />
        public Task RoomCreated(ChatRoomVm room)
        {
            Preconditions.NotNull(room, nameof(room));

            return executeWithRetry.ExecuteAsync(() =>
            {
                var notificationUser = IsolateUserGroup(room.CreatedBy, room.CompanyId);

                return notificationUser.RoomCreated(room);
            });
        }

        /// <inheritdoc />
        public Task RoomInvited(ChatRoomVm room)
        {
            Preconditions.NotNull(room, nameof(room));

            return executeWithRetry.ExecuteAsync(() =>
            {
                var group = IsolateUserGroup(room.ExternalUserId, room.CompanyId);

                return group.RoomInvited(room);
            });
        }


        /// <inheritdoc />
        public Task RoomKicked(ChatRoomVm room)
        {
            Preconditions.NotNull(room, nameof(room));

            return executeWithRetry.ExecuteAsync(() =>
            {
                var group = IsolateUserGroup(room.ExternalUserId, room.CompanyId);

                return group.RoomKicked(room);
            });
        }

        /// <inheritdoc />
        public Task RoomLeaved(ChatRoomVm room)
        {
            Preconditions.NotNull(room, nameof(room));

            return executeWithRetry.ExecuteAsync(() =>
            {
                var group = IsolateRoomGroup(room.Id, room.CompanyId);
                return group.RoomLeaved(room);
            });
        }

        /// <inheritdoc />
        public Task MessageSent(ChatMessageVm message)
        {
            Preconditions.NotNull(message, nameof(message));

            return executeWithRetry.ExecuteAsync(() =>
            {
                message.Me = false;
                var groupExceptCaller = IsolateRoomGroupExceptCaller(message.RoomId, message.CompanyId, hubService.ConnectionId);

                return groupExceptCaller.MessageSent(message);
            });
        }

        /// <inheritdoc />
        public Task SystemSent(ChatMessageVm message)
        {
            Preconditions.NotNull(message, nameof(message));

            return executeWithRetry.ExecuteAsync(() =>
            {
                message.Me = false;
                var group = IsolateRoomGroup(message.RoomId, message.CompanyId);

                return group.SystemSent(message);
            });
        }


        /// <inheritdoc />
        public Task RoomUpdated(ChatRoomVm room)
        {
            Preconditions.NotNull(room, nameof(room));

            return executeWithRetry.ExecuteAsync(() =>
            {
                var group = IsolateUserGroup(room.ExternalUserId, room.CompanyId);

                return group.RoomUpdated(room);
            });
        }

        private IRoomHub IsolateUserGroup(string userId, int companyId)
        {
            var userGroup = new UserGroup(userId, companyId);

            return clients.Group(userGroup.Name);
        }

        private IRoomHub IsolateRoomGroup(int roomId, int companyId)
        {
            var group = new RoomGroup(roomId, companyId);

            return clients.Group(group.Name);
        }

        private IRoomHub IsolateRoomGroupExceptCaller(int roomId, int companyId, string connectionId)
        {
            var group = new RoomGroup(roomId, companyId);

            return clients.GroupExcept(group.Name, connectionId);
        }
    }
}
