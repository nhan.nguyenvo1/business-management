﻿using System;

namespace PROJECT.WebApi.NewHubs.Models
{
    /// <summary>
    /// Room Vm
    /// </summary>
    public class ChatRoomVm
    {
        /// <summary>
        /// Room Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Room Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Room Last Message
        /// </summary>
        public string LastMessage { get; set; }

        /// <summary>
        /// Room Last Time
        /// </summary>
        public DateTime LastTime { get; set; }

        /// <summary>
        /// User Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Full name
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Created by
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Company id
        /// </summary>
        public int CompanyId { get; set; }

        /// <summary>
        /// ExternalUserId
        /// </summary>
        public string ExternalUserId { get; set; }
    }
}
