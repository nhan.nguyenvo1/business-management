﻿using System;

namespace PROJECT.WebApi.NewHubs.Models
{
    /// <summary>
    /// Message vm
    /// </summary>
    public class ChatMessageVm
    {
        /// <summary>
        /// Message Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Room Id
        /// </summary>
        public int RoomId { get; set; }

        /// <summary>
        /// Message Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Me
        /// </summary>
        public bool Me { get; set; }

        /// <summary>
        /// Message When
        /// </summary>
        public DateTime When { get; set; }

        /// <summary>
        /// Sender Avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Sender Id
        /// </summary>
        public string SenderId { get; set; }

        /// <summary>
        /// Sender Name
        /// </summary>
        public string SenderName { get; set; }

        /// <summary>
        /// CompanyId
        /// </summary>
        public int CompanyId { get; set; }
    }
}
