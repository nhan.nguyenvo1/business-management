﻿namespace PROJECT.WebApi.NewHubs.Models
{
    /// <summary>
    /// User view model
    /// </summary>
    public class UserVm
    {
        /// <summary>
        /// User Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// User full name
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// User avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// Is owner
        /// </summary>
        public bool Owner { get; set; } = false;
    }
}
