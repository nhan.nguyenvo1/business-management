﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.BCL;
using PROJECT.Application.Claims;
using PROJECT.Application.Dtos.Chat;
using PROJECT.Application.Interfaces;
using PROJECT.WebApi.NewHubs.Groups;
using PROJECT.WebApi.NewHubs.Requests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PROJECT.WebApi.NewHubs.Hubs
{
    /// <summary>
    /// Room Hub
    /// </summary>
    [Authorize]
    public class RoomHub : Hub<IRoomHub>
    {
        /// <inheritdoc />
        public override Task OnConnectedAsync()
        {
            var clientClaim = Context.User.FindFirst(nameof(ClientCustomClaim));
            var userClaim = Context.User.FindFirst(nameof(UserCustomClaim));

            Preconditions.NotNull(clientClaim, nameof(clientClaim));
            Preconditions.NotNull(userClaim, nameof(userClaim));
            var companyId = int.Parse(clientClaim.Value);

            var clientGroup = new ClientGroup(companyId);
            var userGroup = new UserGroup(userClaim.Value, companyId);

            Groups.AddToGroupAsync(Context.ConnectionId, clientGroup.Name);
            Groups.AddToGroupAsync(Context.ConnectionId, userGroup.Name);

            return base.OnConnectedAsync();
        }

        /// <summary>
        /// Get connection id
        /// </summary>
        /// <returns></returns>
        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }

        /// <summary>
        /// Join Room
        /// </summary>
        /// <returns></returns>
        public Task JoinRoom(JoinRoomRequest request)
        {
            var currentRoom = request.CurrentRoom;
            var previousRoom = request.PreviousRoom;

            Preconditions.NotNull(currentRoom, nameof(currentRoom));
            var companyService = Context.GetHttpContext().RequestServices.GetService(typeof(ICompanyService)) as ICompanyService;
            Preconditions.MustNotEqual(companyService.CompanyId.HasValue, false, nameof(companyService.CompanyId));
            RoomGroup roomGroup;
            var tasks = new List<Task>();

            if (previousRoom is not null)
            {
                roomGroup = new RoomGroup(previousRoom.Id, companyService.CompanyId.Value);
                tasks.Add(Groups.RemoveFromGroupAsync(Context.ConnectionId, roomGroup.Name));
            }

            roomGroup = new RoomGroup(currentRoom.Id, companyService.CompanyId.Value);
            tasks.Add(Groups.AddToGroupAsync(Context.ConnectionId, roomGroup.Name));

            return Task.WhenAll(tasks);
        }
    }
}
