﻿using PROJECT.WebApi.NewHubs.Models;
using System.Threading.Tasks;

namespace PROJECT.WebApi.NewHubs.Hubs
{
    /// <summary>
    /// Room hub interface
    /// </summary>
    public interface IRoomHub
    {
        /// <summary>
        /// Handle room invited
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        Task RoomInvited(ChatRoomVm room);

        /// <summary>
        /// Handle room created
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        Task RoomCreated(ChatRoomVm room);

        /// <summary>
        /// Handle room kicked
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        Task RoomKicked(ChatRoomVm room);

        /// <summary>
        /// Handle user leave room
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        Task RoomLeaved(ChatRoomVm room);

        /// <summary>
        /// Send message hub
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task MessageSent(ChatMessageVm message);

        /// <summary>
        /// Handle room updated
        /// </summary>
        /// <param name="room"></param>
        /// <returns></returns>
        Task RoomUpdated(ChatRoomVm room);

        /// <summary>
        /// System sent
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        Task SystemSent(ChatMessageVm message);
    }
}
