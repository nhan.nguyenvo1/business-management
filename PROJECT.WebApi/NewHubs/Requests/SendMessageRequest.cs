﻿namespace PROJECT.WebApi.NewHubs.Requests
{
    /// <summary>
    /// Send message request
    /// </summary>
    public class SendMessageRequest
    {
        /// <summary>
        /// Message content
        /// </summary>
        public string Content { get; set; }
    }
}
