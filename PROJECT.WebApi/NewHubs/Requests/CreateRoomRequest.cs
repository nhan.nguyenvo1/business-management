﻿namespace PROJECT.WebApi.NewHubs.Requests
{
    /// <summary>
    /// Create room request
    /// </summary>
    public class CreateRoomRequest
    {
        public string RoomName { get; set; }
    }
}
