﻿using PROJECT.Application.Dtos.Chat;

namespace PROJECT.WebApi.NewHubs.Requests
{
    /// <summary>
    /// Join room request
    /// </summary>
    public class JoinRoomRequest
    {
        /// <summary>
        /// Previous room
        /// </summary>
        public RoomVm PreviousRoom { get; set; }

        /// <summary>
        /// Current room
        /// </summary>
        public RoomVm CurrentRoom { get; set; }
    }
}
