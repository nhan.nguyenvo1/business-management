﻿namespace PROJECT.WebApi.NewHubs.Services.Queries
{
    internal static class MessageQueries
    {
        public const string Create = @"
            INSERT INTO Chat (RoomId, SenderId, Content, [When], CompanyId)
			OUTPUT inserted.Id
			VALUES (@RoomId, @UserId, @Content, @UtcNow, @CompanyId);
        ";

		public const string GetById = @"
			SELECT c.Id
				, c.RoomId
				, c.Content
				, CASE
					WHEN c.SenderId = @UserId THEN 1
					ELSE 0
				  END AS Me
				, c.[When]
				, c.CompanyId
				, u.AvatarUrl AS Avatar
				, c.SenderId
				, u.FirstName + ' ' + u.LastName AS SenderName
			FROM [EasyBusiness].[dbo].[Chat] c
			LEFT JOIN [User] u ON u.Id = c.SenderId
			WHERE c.Id = @id;
		";


		public const string Get = @"
			SELECT c.Id
				, c.RoomId
				, c.Content
				, CASE
					WHEN c.SenderId = @UserId THEN 1
					ELSE 0
				  END AS Me
				, c.[When]
				, c.CompanyId
				, u.AvatarUrl AS Avatar
				, c.SenderId
				, u.FirstName + ' ' + u.LastName AS SenderName
			FROM [EasyBusiness].[dbo].[Chat] c
			LEFT JOIN [User] u ON u.Id = c.SenderId
			WHERE c.RoomId = @roomId
			ORDER BY c.[When] DESC
			OFFSET @page ROWS FETCH NEXT @limit ROWS ONLY
		";

		public const string Total = @"
			SELECT COUNT(Id)
			FROM Chat
			WHERE RoomId = @roomId;
		";

	}
}
