﻿namespace PROJECT.WebApi.NewHubs.Services.Queries
{
    internal static class UserQueries
    {
        public const string GetByDepartmentId = @"
            SELECT du.UsersId
            FROM Department d
            JOIN [DepartmentUsers] du ON du.DepartmentsId = d.Id
            WHERE d.Id = @DepartmentId AND d.CompanyId = @CompanyId;
        ";

        public const string GetByRoomId = @"
              SELECT u.Id
                , u.FirstName + ' ' + u.LastName AS FullName
                , u.AvatarUrl AS Avatar
                , CASE 
					WHEN r.CreatedBy = u.Id THEN 1
					ELSE 0
				  END AS [Owner]
            FROM ChatRoom r
            JOIN RoomUser ru ON r.Id = ru.UserRoomsId
			JOIN [User] u ON ru.RoomUsersId = u.Id
			WHERE r.Id = @roomId AND r.CompanyId = @CompanyId
        ";

        public const string GetUserById = @"
            SELECT TOP 1 u.Id
                , u.FirstName + ' ' + u.LastName AS FullName
                , u.AvatarUrl AS Avatar
			FROM [User] u
			WHERE u.Id = @userId
        ";
    }
}
