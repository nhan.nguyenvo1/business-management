﻿namespace PROJECT.WebApi.NewHubs.Services.Queries
{
    /// <summary>
    /// Room sql queries
    /// </summary>
    internal static class RoomQueries
    {
        /// <summary>
        /// Get rooms queries
        /// </summary>
        public const string Get = @"
            SELECT TOP (1000) c.[Id]
              , c.[Name]
	          , chat.Content AS LastMessage
	          , chat.SenderId AS UserId
	          , chat.[When] AS LastTime
	          , chat.FullName
	          , c.CreatedBy
	          , c.CompanyId
              , c.AvatarUrl AS Avatar
            FROM [EasyBusiness].[dbo].[ChatRoom] c
            OUTER APPLY
	        (
		        SELECT TOP 1 ch.Content, ch.[When], ch.SenderId, cu.FirstName + ' ' + cu.LastName AS FullName
		        FROM Chat ch
		        JOIN [User] cu ON cu.Id = ch.SenderId
		        WHERE ch.RoomId = c.Id
		        ORDER BY ch.[When] DESC
	        ) chat
            WHERE EXISTS 
				(
					SELECT 1 FROM RoomUser ru WHERE ru.UserRoomsId = c.Id AND ru.RoomUsersId = @UserId
				) AND c.CompanyId = @CompanyId
            ORDER BY LastTime DESC
        ";

        /// <summary>
        /// Get by id queries
        /// </summary>
        public const string GetById = @"
            SELECT TOP 1 c.[Id]
              , c.[Name]
	          , chat.Content AS LastMessage
	          , chat.SenderId AS UserId
	          , chat.[When] AS LastTime
	          , chat.FullName
	          , c.CreatedBy
	          , c.CompanyId
              , c.AvatarUrl AS Avatar
            FROM [EasyBusiness].[dbo].[ChatRoom] c
            OUTER APPLY
	        (
		        SELECT TOP 1 ch.Content, ch.[When], ch.SenderId, cu.FirstName + ' ' + cu.LastName AS FullName
		        FROM Chat ch
		        JOIN [User] cu ON cu.Id = ch.SenderId
		        WHERE ch.RoomId = c.Id
		        ORDER BY ch.[When] DESC
	        ) chat
            WHERE c.Id = @roomId AND c.CompanyId = @CompanyId
        ";

        /// <summary>
        /// Insert queries
        /// </summary>
        public const string Insert = @"
            INSERT INTO ChatRoom ([Name], CreatedDate, UpdatedDate, CreatedBy, UpdatedBy, CompanyId)
            OUTPUT inserted.Id
            VALUES (@roomName, @UtcNow, @UtcNow, @UserId, @UserId, @CompanyId);
        ";

        /// <summary>
        /// Insert user to room queries
        /// </summary>
        public const string InsertUser = @"
            INSERT INTO RoomUser (UserRoomsId, RoomUsersId)
            VALUES (@roomId, @UserId)
        ";

        public const string DeleteUser = @"
            DELETE RoomUser
            WHERE UserRoomsId = @roomId AND RoomUsersId = @userId
        ";
    }
}
