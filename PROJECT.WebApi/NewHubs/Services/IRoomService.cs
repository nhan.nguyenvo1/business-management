﻿using PROJECT.WebApi.NewHubs.Models;
using PROJECT.WebApi.NewHubs.Requests;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace PROJECT.WebApi.NewHubs.Services
{
    /// <summary>
    /// Room service interface
    /// </summary>
    public interface IRoomService
    {
        /// <summary>
        /// Returns room view model map with user id
        /// </summary>
        /// <returns></returns>
        Task<IReadOnlyCollection<ChatRoomVm>> GetAsync(IDbTransaction transaction = null);

        /// <summary>
        /// Create new room
        /// </summary>
        /// <param name="roomName"></param>
        /// <param name="transation"></param>
        /// <returns></returns>
        Task<ChatRoomVm> CreateAsync(string roomName, IDbTransaction transation = null);

        /// <summary>
        /// Invite user join room
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roomId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<ChatRoomVm> InviteAsync(string userId, int roomId, IDbTransaction transaction = null);

        /// <summary>
        /// Invite department join room
        /// </summary>
        /// <param name="departmentId"></param>
        /// <param name="roomId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<(ChatRoomVm, IReadOnlyCollection<string>)> InviteAsync(int departmentId, int roomId, IDbTransaction transaction = null);

        /// <summary>
        /// Kick out user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="roomId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<ChatRoomVm> KickAsync(string userId, int roomId, IDbTransaction transaction = null);

        /// <summary>
        /// Get users
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<IReadOnlyCollection<UserVm>> GetUsersAsync(int roomId, IDbTransaction transaction = null);

        /// <summary>
        /// User leave room
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<(ChatRoomVm, string)> LeaveAsync(int roomId, IDbTransaction transaction = null);

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<UserVm> GetUserByIdAsync(string userId, IDbTransaction transaction = null);

        /// <summary>
        /// Send message to room
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="request"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<ChatMessageVm> SendMessageAsync(int roomId, SendMessageRequest request, IDbTransaction transaction = null);

        /// <summary>
        /// Returns list of messages
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<(IReadOnlyCollection<ChatMessageVm>, int)> GetMessagesByRoomIdAsync(int roomId, int page, int limit, IDbTransaction transaction = null);

        /// <summary>
        /// Return Room
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<ChatRoomVm> GetByIdAsync(int roomId, IDbTransaction transaction = null);

        /// <summary>
        /// Returns message map with id
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<ChatMessageVm> GetMessageByIdAsync(int messageId, IDbTransaction transaction = null);

        /// <summary>
        /// Return inserted message
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="content"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        Task<ChatMessageVm> SendSystemMessageAsync(int roomId, string content, IDbTransaction transaction = null);
    }
}
