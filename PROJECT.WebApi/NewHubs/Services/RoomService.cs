﻿using ADMIN.Infrastructure.DataAccess;
using Dapper;
using PROJECT.Application.Interfaces;
using PROJECT.WebApi.NewHubs.Models;
using PROJECT.WebApi.NewHubs.Requests;
using PROJECT.WebApi.NewHubs.Services.Queries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace PROJECT.WebApi.NewHubs.Services
{
    /// <summary>
    /// Room service implement
    /// </summary>
    public class RoomService : IRoomService
    {
        private readonly IQueryExecutorAsync queryExecutorAsync;
        private readonly IAuthUserService authUserService;
        private readonly ICompanyService companyService;
        private readonly IDateTimeService dateTimeService;

        /// <summary>
        /// Create a new instance of <see cref="RoomService"/>
        /// </summary>
        public RoomService(IQueryExecutorAsync queryExecutorAsync,
                           IAuthUserService authUserService,
                           ICompanyService companyService,
                           IDateTimeService dateTimeService)
        {
            this.queryExecutorAsync = queryExecutorAsync;
            this.authUserService = authUserService;
            this.companyService = companyService;
            this.dateTimeService = dateTimeService;
        }

        /// <inheritdoc />
        public async Task<ChatRoomVm> CreateAsync(string roomName, IDbTransaction transation = null)
        {
            var roomId = await queryExecutorAsync.ExecuteInTransactionAsync(async trans =>
            {
                var roomId = await queryExecutorAsync.FetchOne<int>(
                    RoomQueries.Insert,
                    new { roomName, dateTimeService.UtcNow, authUserService.UserId, companyService.CompanyId },
                    trans);

                await queryExecutorAsync.Execute(RoomQueries.InsertUser, new { roomId, authUserService.UserId }, trans);
                return roomId;
            }, transation);

            return await GetByIdAsync(roomId, transation);
        }

        /// <inheritdoc />
        public Task<ChatRoomVm> GetByIdAsync(int roomId, IDbTransaction transaction = null)
        {
            return queryExecutorAsync.FetchOne<ChatRoomVm>(RoomQueries.GetById, new { roomId, companyService.CompanyId }, transaction);
        }

        /// <inheritdoc />
        public Task<IReadOnlyCollection<ChatRoomVm>> GetAsync(IDbTransaction transaction = null)
        {
            return queryExecutorAsync.Fetch<ChatRoomVm>(RoomQueries.Get, new { authUserService.UserId, companyService.CompanyId }, transaction);
        }

        /// <inheritdoc />
        public async Task<ChatRoomVm> InviteAsync(string userId, int roomId, IDbTransaction transaction = null)
        {
            var isInRoom = false;
            await queryExecutorAsync.ExecuteInTransactionAsync(async trans =>
            {
                try
                {
                    await queryExecutorAsync.Execute(RoomQueries.InsertUser, new { UserId = userId, roomId }, trans);
                }
                catch (Exception)
                {
                    isInRoom = true;
                }
            }, transaction);
            return isInRoom ? null : await GetByIdAsync(roomId, transaction);
        }

        /// <inheritdoc />
        public async Task<(ChatRoomVm, IReadOnlyCollection<string>)> InviteAsync(int departmentId, int roomId, IDbTransaction transaction = null)
        {
            var userIds = await queryExecutorAsync.Fetch<string>(UserQueries.GetByDepartmentId, new { departmentId, companyService.CompanyId }, transaction);
            var actualUserIds = new List<string>(userIds);
            await queryExecutorAsync.ExecuteInTransactionAsync(async trans =>
            {
                foreach (var userId in userIds)
                {
                    var room = await InviteAsync(userId, roomId, trans);

                    if (room == null)
                    {
                        actualUserIds.Remove(userId);
                    }
                }
            }, transaction);

            return (await GetByIdAsync(roomId, transaction), actualUserIds);
        }

        /// <inheritdoc />
        public async Task<ChatRoomVm> KickAsync(string userId, int roomId, IDbTransaction transaction = null)
        {
            await queryExecutorAsync.Execute(RoomQueries.DeleteUser, new { userId, roomId }, transaction);

            return await GetByIdAsync(roomId, transaction);
        }

        /// <inheritdoc />
        public async Task<(ChatRoomVm, string)> LeaveAsync(int roomId, IDbTransaction transaction = null)
        {
            await queryExecutorAsync.Execute(RoomQueries.DeleteUser, new { userId = authUserService.UserId, roomId }, transaction);

            return (await GetByIdAsync(roomId, transaction), authUserService.UserId);
        }

        /// <inheritdoc />
        public Task<IReadOnlyCollection<UserVm>> GetUsersAsync(int roomId, IDbTransaction transaction = null)
        {
            return queryExecutorAsync.Fetch<UserVm>(UserQueries.GetByRoomId, new { roomId, companyService.CompanyId }, transaction);
        }

        /// <inheritdoc />
        public Task<UserVm> GetUserByIdAsync(string userId, IDbTransaction transaction = null)
        {
            return queryExecutorAsync.FetchOne<UserVm>(UserQueries.GetUserById, new { userId }, transaction);
        }

        /// <inheritdoc />
        public async Task<ChatMessageVm> SendMessageAsync(int roomId, SendMessageRequest request, IDbTransaction transaction = null)
        {
            var parameters = new DynamicParameters();
            parameters.AddDynamicParams(request);
            parameters.Add("@RoomId", roomId);
            parameters.Add("@UtcNow", dateTimeService.UtcNow);
            parameters.Add("@CompanyId", companyService.CompanyId);
            parameters.Add("@UserId", authUserService.UserId);

            var id = await queryExecutorAsync.FetchOne<int>(MessageQueries.Create, parameters, transaction);

            return await GetMessageByIdAsync(id, transaction);
        }

        /// <inheritdoc />
        public Task<ChatMessageVm> GetMessageByIdAsync(int id, IDbTransaction transaction = null)
        {
            return queryExecutorAsync.FetchOne<ChatMessageVm>(MessageQueries.GetById, new { id, authUserService.UserId }, transaction);
        }

        /// <inheritdoc />
        public async Task<(IReadOnlyCollection<ChatMessageVm>, int)> GetMessagesByRoomIdAsync(int roomId, int page, int limit, IDbTransaction transaction = null)
        {
            return (
                await queryExecutorAsync.Fetch<ChatMessageVm>(MessageQueries.Get, new { roomId, page, limit, authUserService.UserId }, transaction),
                await queryExecutorAsync.FetchOne<int>(MessageQueries.Total, new { roomId }, transaction)
                );
        }

        /// <inheritdoc />
        public async Task<ChatMessageVm> SendSystemMessageAsync(int roomId, string content, IDbTransaction transaction = null)
        {
            var id = await queryExecutorAsync.FetchOne<int>(
                MessageQueries.Create,
                new { Content = content, dateTimeService.UtcNow, companyService.CompanyId, RoomId = roomId, UserId = (string)null },
                transaction);

            return await GetMessageByIdAsync(id, transaction);
        }
    }
}
