﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Contacts.Queries.GetContact;
using PROJECT.Domain.Entities;
using System.Threading.Tasks;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// Contacts Api
    /// </summary>
    public class ContactsController : BaseController
    {
        /// <summary>
        /// Get Contact
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(Contact), 200)]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetContactQueries()));
        }
    }
}
