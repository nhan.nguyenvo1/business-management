﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.Features.Tasks.Commands.AssignTask;
using PROJECT.Application.Features.Tasks.Commands.DeleteTask;
using PROJECT.Application.Features.Tasks.Commands.EditTaskById;
using PROJECT.Application.Features.Tasks.Commands.EditTaskById.Requests;
using PROJECT.Application.Features.Tasks.Commands.MarkAsCompleted;
using PROJECT.Application.Features.Tasks.Queries.GetMyTasksPaging;
using PROJECT.Application.Features.Tasks.Queries.GetTaskById;
using PROJECT.Application.Features.Tasks.Queries.GetTasksPaging;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Hubs;
using PROJECT.WebApi.Hubs.Implements;
using PROJECT.WebApi.Hubs.Interfaces;
using PROJECT.WebApi.Hubs.Models;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// Task Api
    /// </summary>
    public class TasksController : BaseController
    {
        private readonly INotificationWrapper _notificationWrapper;

        /// <summary>
        /// Controller.
        /// </summary>
        /// <param name="hubContext"></param>
        /// <param name="notificationWrapper"></param>
        public TasksController(IHubContext<NotificationHub, INotifications> hubContext,
            INotificationWrapper notificationWrapper)
        {
            _notificationWrapper = notificationWrapper;
            _notificationWrapper.SetHubClient(hubContext.Clients);
        }

        /// <summary>
        /// Assign Task
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [CompanyAuthorize(TaskAuthority.Assign)]
        public async Task<IActionResult> AssignTask([FromForm] AssignTaskCommand command)
        {
            var response = await Mediator.Send(command);
            await _notificationWrapper.SendNotification(command.ResponsibleIds, new NotificationModel
            {
                Category = NotificationCaterogy.MyTasks,
                Title = "Tasks",
                Description = "New task has been created",
                Url = "/tasks"
            });
            return Ok(response);
        }

        /// <summary>
        /// Get
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet]
        [CompanyAuthorize(TaskAuthority.View)]
        public async Task<IActionResult> GetPaging([FromQuery] GetTasksPagingQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        /// <summary>
        /// Get Personal
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        [HttpGet("personal")]
        [CompanyAuthorize]
        public async Task<IActionResult> GetPersonalPaging([FromQuery] GetMyTasksPagingQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:int}")]
        [CompanyAuthorize(TaskAuthority.Edit)]
        public async Task<IActionResult> Edit(int id, [FromForm] EditTaskByIdRequest request)
        {
            var command = new EditTaskByIdCommand(id, request);

            var response = await Mediator.Send(command);

            return Ok(response);
        }

        /// <summary>
        /// Get By id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [CompanyAuthorize]
        public async Task<IActionResult> GetById(int id)
        {
            var query = new GetTaskByIdQuery(id);

            var response = await Mediator.Send(query);

            return Ok(response);
        }

        /// <summary>
        /// Mark Task complete
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("mark-as-completed")]
        [CompanyAuthorize]
        [ProducesResponseType(typeof(Response<int>), 200)]
        public async Task<IActionResult> MarkAsCompleted([FromBody] MarkAsCompletedCommand request)
        {
            return Ok(await Mediator.Send(request));
        }

        /// <summary>
        /// Delete By id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [CompanyAuthorize(TaskAuthority.Delete)]
        [ProducesResponseType(typeof(Response<int>), 200)]
        public async Task<IActionResult> Delete(int id)
        {
            var query = new DeleteTaskByIdCommand
            {
                Id = id
            };

            var response = await Mediator.Send(query);

            return Ok(response);
        }
    }
}