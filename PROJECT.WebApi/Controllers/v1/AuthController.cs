﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Dtos.Account;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Wrappers;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Extensions;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// Auths Controller
    /// </summary>
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="authService"></param>
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// Sign in
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("sign-in")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(Response<AuthResponse>), 200)]
        [ProducesResponseType(typeof(Response<string>), 400)]
        public async Task<IActionResult> SignInAsync([FromBody] AuthRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.SignInAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Refresh token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("refresh-token")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(Response<AuthResponse>), 200)]
        [ProducesResponseType(typeof(Response<string>), 400)]
        public async Task<IActionResult> RefreshTokenAsync([FromBody] RefreshTokenRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.RefreshTokenAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Revoke Token
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("revoke-token")]
        [CompanyAuthorize(CompanyRequired = false)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        [ProducesResponseType(typeof(Response<string>), 400)]
        public async Task<IActionResult> RevokeTokenAsync([FromBody] RevokeTokenRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.RevokeTokenAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Change Password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("change-password")]
        [CompanyAuthorize(CompanyRequired = false)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        [ProducesResponseType(typeof(Response<string>), 400)]     
        public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePasswordRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.ChangePasswordAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Forgot password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("forgot-password")]
        [ProducesResponseType(typeof(Response<AuthResponse>), 200)]
        [ProducesResponseType(typeof(Response<string>), 400)]
        public async Task<IActionResult> ForgotPasswordAsync([FromBody] ForgotPasswordRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.ForgotPasswordAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Verify Email
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("verify-email")]
        [ProducesResponseType(typeof(Response<AuthResponse>), 200)]
        [ProducesResponseType(typeof(Response<string>), 400)]
        public async Task<IActionResult> VerifyEmailAsync([FromBody] VerifyEmailRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.VerifyEmailAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("reset-password")]
        [ProducesResponseType(typeof(Response<string>), 400)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.ResetPasswordAsync(request);

            return Ok(response);
        }

        /// <summary>
        /// Get me
        /// </summary>
        /// <returns></returns>
        [HttpGet("me")]
        [CompanyAuthorize(CompanyRequired = false)]
        [ProducesResponseType(typeof(Response<AuthVm>), 200)]
        public async Task<IActionResult> GetMeAsync()
        {
            var response = await _authService.GetMeAsync();

            return Ok(response);
        }

        /// <summary>
        /// Get Token api
        /// </summary>
        /// <returns></returns>
        [HttpGet("tokens")]
        [CompanyAuthorize]
        public Task<string> GetToken()
        {
            return _authService.GetTokenAsync();
        }

        /// <summary>
        /// Register
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("register")]
        [ProducesResponseType(typeof(Response<string>), 400)]
        [ProducesResponseType(204)]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            await _authService.RegisterAsync(request);

            return NoContent();
        }
    }
}