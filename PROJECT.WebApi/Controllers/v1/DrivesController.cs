﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.Features.Drives.Commands.Copy;
using PROJECT.Application.Features.Drives.Commands.CreateFiles;
using PROJECT.Application.Features.Drives.Commands.CreateFolder;
using PROJECT.Application.Features.Drives.Commands.Delete;
using PROJECT.Application.Features.Drives.Commands.DownloadFiles;
using PROJECT.Application.Features.Drives.Commands.DownloadFolder;
using PROJECT.Application.Features.Drives.Commands.Move;
using PROJECT.Application.Features.Drives.Commands.Share;
using PROJECT.Application.Features.Drives.Queries.GetFolders;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Enums;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Hubs;
using PROJECT.WebApi.Hubs.Implements;
using PROJECT.WebApi.Hubs.Interfaces;
using PROJECT.WebApi.Hubs.Models;

namespace PROJECT.WebApi.Controllers.v1
{
    [CompanyAuthorize(CompanyRequired = false)]
    public class DrivesController : BaseController
    {
        private readonly INotificationWrapper _notificationWrapper;
        private readonly IUserRepository _userRepository;
        private readonly IAuthUserService _authUserService;

        public DrivesController(IHubContext<NotificationHub, INotifications> hubContext,
            INotificationWrapper notificationWrapper, IUserRepository userRepository, IAuthUserService authUserService)
        {
            _notificationWrapper = notificationWrapper;
            _notificationWrapper.SetHubClient(hubContext.Clients);
            _userRepository = userRepository;
            _authUserService = authUserService;
        }

        [HttpGet]
        public async Task<IActionResult> GetByFolderId([FromQuery] GetFoldersQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost("folders")]
        public async Task<IActionResult> Create([FromBody] CreateFolderCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("files")]
        public async Task<IActionResult> UploadFiles([FromForm] CreateFilesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("files/{id}")]
        public async Task<FileStreamResult> DownloadFile(int id)
        {
            var response = await Mediator.Send(new DownloadFileCommand {FileId = id});

            return new FileStreamResult(response.Data.Content, "application/octet-stream")
            {
                FileDownloadName = response.Data.FileName
            };
        }

        [HttpGet("folders/{id}")]
        public async Task<FileStreamResult> DownloadFolder(int id)
        {
            var response = await Mediator.Send(new DownloadFolderCommand {FolderId = id});

            return new FileStreamResult(response.Data.Content, "application/octet-stream")
            {
                FileDownloadName = response.Data.FileName
            };
        }

        [HttpPost("copy")]
        public async Task<IActionResult> Copy([FromBody] CopyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("move")]
        public async Task<IActionResult> Move([FromBody] MoveCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteCommand {DriveId = id}));
        }

        [HttpPost("shared")]
        public async Task<IActionResult> Share([FromBody] ShareCommand command)
        {
            var user = await _userRepository.GetByIdAsync(_authUserService.UserId);
            await _notificationWrapper.SendNotification(command.SharedUserRequests.Select(e => e.UserId), new NotificationModel
            {
                Category = NotificationCaterogy.CompanyDrives,
                Title = "New Shared Drives",
                Description = $"{user.FullName} shared the new file with you",
                Url = "/drives_company"
            });
            return Ok(await Mediator.Send(command));
        }
    }
}