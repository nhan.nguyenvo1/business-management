﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.Features.Employees.Commands.ChangeDepartment;
using PROJECT.Application.Features.Employees.Commands.CreateEmployee;
using PROJECT.Application.Features.Employees.Commands.DeleteEmployeeById;
using PROJECT.Application.Features.Employees.Commands.Invite;
using PROJECT.Application.Features.Employees.Queries.GetAllEmployees;
using PROJECT.Application.Features.Employees.Queries.GetEmployeeById;
using PROJECT.Application.Features.Employees.Queries.GetEmployeesByDepartmentId;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.ViewModels;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Attributes.Enums;
using PROJECT.WebApi.Hubs;
using PROJECT.WebApi.Hubs.Implements;
using PROJECT.WebApi.Hubs.Interfaces;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// Employees Api
    /// </summary>
    public class EmployeesController : BaseController
    {
        private readonly INotificationWrapper _notificationWrapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hubContext"></param>
        /// <param name="notificationWrapper"></param>
        public EmployeesController(IHubContext<NotificationHub, INotifications> hubContext,
            INotificationWrapper notificationWrapper)
        {
            _notificationWrapper = notificationWrapper;
            _notificationWrapper.SetHubClient(hubContext.Clients);
        }

        /// <summary>
        /// Get Employee Paging
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet]
        [CompanyAuthorize(Policy = EmployeeAuthority.View)]
        [ProducesResponseType(typeof(PagedResponse<EmployeeVm>), 200)]
        public async Task<IActionResult> Get([FromQuery] GetAllEmployeesQuery filter)
        {
            return Ok(await Mediator.Send(filter));
        }

        /// <summary>
        /// Get all employee for dropdown
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [CompanyAuthorize]
        [ProducesResponseType(typeof(Response<IReadOnlyCollection<EmployeeVm>>), 200)]
        [Cache(600, DataType = ReferenceDataType.Employees)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllEmployeesQuery { Limit = -1 }));
        }

        /// <summary>
        /// Get Employee By Department Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpGet("departments/{id}")]
        [CompanyAuthorize(Policy = EmployeeAuthority.View)]
        [ProducesResponseType(typeof(PagedResponse<EmployeeVm>), 200)]
        public async Task<IActionResult> GetByDepartmentId(int id,
            [FromQuery] GetEmployeesByDepartmentIdParameter filter)
        {
            var query = new GetEmployeesByDepartmentIdQuery
            {
                DepartmentId = id,
                Parameter = filter
            };
            return Ok(await Mediator.Send(query));
        }

        /// <summary>
        /// Get Employee By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [CompanyAuthorize(Policy = EmployeeAuthority.Detail)]
        [ProducesResponseType(typeof(Response<EmployeeVm>), 200)]
        public async Task<IActionResult> GetById(string id)
        {
            return Ok(await Mediator.Send(new GetEmployeeByIdQuery(id)));
        }

        /// <summary>
        /// Delete Employee By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [CompanyAuthorize(Policy = EmployeeAuthority.Delete)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        [InvalidateCached(ReferenceDataType.Employees)]
        public async Task<IActionResult> Delete(string id)
        {
            return Ok(await Mediator.Send(new DeleteEmployeeCommand(id)));
        }

        /// <summary>
        /// Create New Employee
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [CompanyAuthorize(Policy = EmployeeAuthority.Create)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        public async Task<IActionResult> Post([FromBody] CreateEmployeeCommand command)
        {
            var response = await Mediator.Send(command);

            return Ok(response);
        }

        /// <summary>
        /// Invite Employee
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("invite")]
        [CompanyAuthorize(Policy = EmployeeAuthority.Create)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        public async Task<IActionResult> Invite([FromBody] InviteCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Change Department
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPatch]
        [CompanyAuthorize(Policy = EmployeeAuthority.ChangeDepartment)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        [InvalidateCached(ReferenceDataType.Employees)]
        public async Task<IActionResult> ChangeDepartment([FromBody] ChangeDepartmentCommand command)
        {
            var response = await Mediator.Send(command);
            return Ok(response);
        }
    }
}