﻿using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Tasks.Commands.MarkCheckListCompleted;
using PROJECT.Application.Wrappers;
using PROJECT.WebApi.Attributes;
using System.Threading.Tasks;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// TaskCheckList Api
    /// </summary>
    [Route("api/v{version:apiVersion}/task-check-lists")]
    public class TaskCheckListsController : BaseController
    {
        /// <summary>
        /// Mark checklist complete
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("mark-as-completed")]
        [CompanyAuthorize]
        [ProducesResponseType(typeof(Response<int>), 200)]
        public async Task<IActionResult> MarkAsCompleted([FromBody] MarkCheckListCompletedCommand request)
        {
            return Ok(await Mediator.Send(request));
        }
    }
}