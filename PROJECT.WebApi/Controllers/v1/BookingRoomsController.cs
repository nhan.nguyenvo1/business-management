﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.Features.BookingRooms.Commands.BookingRooms;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Hubs;
using PROJECT.WebApi.Hubs.Implements;
using PROJECT.WebApi.Hubs.Interfaces;
using PROJECT.WebApi.Hubs.Models;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// Booking Rooms Controller
    /// </summary>
    [Route("api/v{version:apiVersion}/booking-rooms")]
    public class BookingRoomsController : BaseController
    {
        private readonly INotificationWrapper _notificationWrapper;
        private readonly IUserRepository _userRepository;
        private readonly IAuthUserService _authUserService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="hubContext"></param>
        /// <param name="notificationWrapper"></param>
        public BookingRoomsController(IHubContext<NotificationHub, INotifications> hubContext,
            INotificationWrapper notificationWrapper, IUserRepository userRepository, IAuthUserService authUserService)
        {
            _notificationWrapper = notificationWrapper;
            _notificationWrapper.SetHubClient(hubContext.Clients);
            _userRepository = userRepository;
            _authUserService = authUserService;
        }

        /// <summary>
        /// Book Room
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CompanyAuthorize(BookingRoomAuthority.Book)]
        [HttpPost("book")]
        [ProducesResponseType(typeof(Response<string>), 400)]
        [ProducesResponseType(typeof(Response<int>), 200)]
        public async Task<IActionResult> Book([FromBody] BookingRoomCommand command)
        {
            var response = await Mediator.Send(command);
            var user = await _userRepository.GetByIdAsync(_authUserService.UserId);

            await _notificationWrapper.SendNotification(command.UserIds, new NotificationModel
            {
                Category = NotificationCaterogy.BookingRoom,
                Title = "New room has booked",
                Description = $"You are invited to this meeting by ${user.FullName}.",
                Url = "/calendar"
            });

            return Ok(response);
        }
    }
}