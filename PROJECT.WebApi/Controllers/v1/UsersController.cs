﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Users.Commands.AssignRole;
using PROJECT.Application.Features.Users.Commands.CancelRoles;
using PROJECT.Application.Features.Users.Commands.UpdateAvatar;
using PROJECT.Application.Features.Users.Commands.UpdateProfile;
using PROJECT.Application.Wrappers;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Attributes.Enums;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// Users Api
    /// </summary>
    public class UsersController : BaseController
    {
        /// <summary>
        /// Update Avatar
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("update-avatar")]
        [CompanyAuthorize]
        [InvalidateCached(ReferenceDataType.Employees)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        public async Task<IActionResult> UpdateAvatar([FromForm] UpdateAvatarCommand command)
        {
            var response = await Mediator.Send(command);

            return Ok(response);
        }

        /// <summary>
        /// Update Profile
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut("update-profile")]
        [CompanyAuthorize]
        [InvalidateCached(ReferenceDataType.Employees)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        public async Task<IActionResult> UpdateProfile([FromBody] UpdateProfileCommand command)
        {
            var response = await Mediator.Send(command);

            return Ok(response);
        }

        /// <summary>
        /// Assign Role
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("assign-roles")]
        [CompanyAuthorize(Policy = UserRoleAuthority.Assign)]
        [InvalidateCached(ReferenceDataType.Employees)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        public async Task<IActionResult> AssignRole([FromBody] AssignRoleCommand command)
        {
            var response = await Mediator.Send(command);
            return Ok(response);
        }

        /// <summary>
        /// Cancel Role
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("cancel-roles")]
        [CompanyAuthorize(Policy = UserRoleAuthority.Assign)]
        [InvalidateCached(ReferenceDataType.Employees)]
        [ProducesResponseType(typeof(Response<string>), 200)]
        public async Task<IActionResult> CancelRole([FromBody] CancelRolesCommand command)
        {
            var response = await Mediator.Send(command);
            return Ok(response);
        }
    }
}