﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Roles.Commands.AssignPermissions;
using PROJECT.Application.Features.Roles.Commands.CancelPermissions;
using PROJECT.Application.Features.Roles.Commands.CreateRole;
using PROJECT.Application.Features.Roles.Commands.DeleteRoleById;
using PROJECT.Application.Features.Roles.Commands.UpdateRoleById;
using PROJECT.Application.Features.Roles.Queries.GetRoleById;
using PROJECT.Application.Features.Roles.Queries.GetRolesByUserId;
using PROJECT.Application.Features.Roles.Queries.GetRolesPaging;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    public class RolesController : BaseController
    {
        [HttpGet]
        [CompanyAuthorize(Policy = RoleAuthority.View)]
        public async Task<IActionResult> Get([FromQuery] GetRolesPagingQuery filter)
        {
            return Ok(await Mediator.Send(filter));
        }

        [HttpGet("{id}")]
        [CompanyAuthorize(Policy = RoleAuthority.Detail)]
        public async Task<IActionResult> GetById(string id)
        {
            return Ok(await Mediator.Send(new GetRoleByIdQuery(id)));
        }

        [HttpPut("{id}")]
        [CompanyAuthorize(Policy = RoleAuthority.Edit)]
        public async Task<IActionResult> Update(string id, [FromBody] UpdateRoleByIdCommand command)
        {
            if (!id.Equals(command.Id)) return BadRequest();

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [CompanyAuthorize(Policy = RoleAuthority.Delete)]
        public async Task<IActionResult> Delete(string id)
        {
            return Ok(await Mediator.Send(new DeleteRoleByIdCommand(id)));
        }

        [HttpPost]
        [CompanyAuthorize(Policy = RoleAuthority.Create)]
        public async Task<IActionResult> Post([FromBody] CreateRoleCommand command)
        {
            var response = await Mediator.Send(command);
            return CreatedAtAction(nameof(GetById), new {id = response.Data}, response);
        }

        [HttpPost("permissions")]
        [CompanyAuthorize(Policy = RoleAuthority.AssignPermissions)]
        public async Task<IActionResult> AssignPermissions([FromBody] AssignPermissionsCommand command)
        {
            var response = await Mediator.Send(command);
            return Ok(response);
        }

        [HttpDelete("permissions")]
        [CompanyAuthorize(Policy = RoleAuthority.CancelPermissions)]
        public async Task<IActionResult> CancelPermissions([FromBody] CancelPermissionsCommand command)
        {
            var response = await Mediator.Send(command);
            return Ok(response);
        }

        [HttpGet("users/{userId}")]
        public async Task<IActionResult> GetByUserId(string userId) 
        {
            return Ok(await Mediator.Send(new GetRolesByUserIdQuery { UserId = userId }));
        }
    }
}