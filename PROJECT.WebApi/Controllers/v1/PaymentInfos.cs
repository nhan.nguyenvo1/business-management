﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.PaymentInfos.Queries.GetPaymentInfo;
using PROJECT.Domain.Entities;
using System.Threading.Tasks;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// PaymentInfo Api
    /// </summary>
    public class PaymentInfosController : BaseController
    {
        /// <summary>
        /// Get Payment Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(PaymentInfo), 200)]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetPaymentInfoQuery()));
        }
    }
}
