﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Companies.Commands.AcceptInvitation;
using PROJECT.Application.Features.Companies.Commands.CreateCompany;
using PROJECT.Application.Features.Companies.Queries.GetCompanies;
using PROJECT.WebApi.Attributes;

namespace PROJECT.WebApi.Controllers.v1
{
    public class CompaniesController : BaseController
    {
        [CompanyAuthorize(CompanyRequired = false)]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateCompanyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        // /api/v1/companies?page=1&limit=10
        [CompanyAuthorize(CompanyRequired = false)]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetCompaniesQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        [CompanyAuthorize(CompanyRequired = false)]
        [AllowAnonymous]
        [HttpPost("accept-invitation")]
        public async Task<IActionResult> AcceptInvitation([FromBody] AcceptInvitationCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}