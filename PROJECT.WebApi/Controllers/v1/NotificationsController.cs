﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Notifications.Queries.GetNotifications;
using PROJECT.WebApi.Attributes;

namespace PROJECT.WebApi.Controllers.v1
{
    public class NotificationsController : BaseController
    {
        [CompanyAuthorize]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetNotificationsQuery()));
        }
    }
}