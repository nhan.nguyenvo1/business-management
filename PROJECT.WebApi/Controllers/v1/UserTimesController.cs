﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.UserTimes.Commands.AddSummary;
using PROJECT.Application.Features.UserTimes.Commands.CheckIn;
using PROJECT.Application.Features.UserTimes.Commands.CheckOut;
using PROJECT.Application.Features.UserTimes.Queries.GetUserTimeForDate;
using PROJECT.Application.Features.UserTimes.Queries.GetUserTimes;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    [Route("api/v{version:apiVersion}/user-times")]
    public class UserTimesController : BaseController
    {
        [HttpPost("check-in")]
        [CompanyAuthorize]
        public async Task<IActionResult> CheckInAsync([FromBody] CheckInCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("check-out")]
        [CompanyAuthorize]
        public async Task<IActionResult> CheckOutAsync([FromBody] CheckOutCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPatch("summary")]
        [CompanyAuthorize]
        public async Task<IActionResult> AddSummary([FromBody] AddSummaryCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet]
        [CompanyAuthorize(Policy = UserTimeAuthority.View)]
        public async Task<IActionResult> GetAsync([FromQuery] GetUserTimesQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        [HttpGet("for-date")]
        [CompanyAuthorize]
        public async Task<IActionResult> GetForDate([FromQuery] GetUserTimeForDateQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }
    }
}