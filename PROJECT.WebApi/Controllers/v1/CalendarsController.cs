﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Calendars.Queries.GetHomeCalendars;
using PROJECT.WebApi.Attributes;

namespace PROJECT.WebApi.Controllers.v1
{
    public class CalendarsController : BaseController
    {
        [HttpGet]
        [CompanyAuthorize]
        public async Task<IActionResult> GetAsync([FromQuery] GetHomeCalendarsQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }
    }
}