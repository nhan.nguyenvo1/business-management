﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Permissions.Authorities;
using PROJECT.WebApi.Permissions.Services;

namespace PROJECT.WebApi.Controllers.v1
{
    public class PermissionsController : BaseController
    {
        private readonly IPermissionService _permissionService;

        public PermissionsController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }

        [CompanyAuthorize(Policy = PermissionAuthority.View)]
        [HttpGet("roles/{roleId}/checked")]
        public async Task<IActionResult> GetCheckedByRoleId(string roleId)
        {
            return Ok(await _permissionService.GetCheckedByRoleId(roleId));
        }

        [CompanyAuthorize(Policy = PermissionAuthority.View)]
        [HttpGet("roles/{roleId}/un-checked")]
        public async Task<IActionResult> GetUnCheckedByRoleId(string roleId)
        {
            return Ok(await _permissionService.GetUnCheckedByRoleId(roleId));
        }
    }
}