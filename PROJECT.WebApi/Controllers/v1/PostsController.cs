﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.Features.Feeds.Commands.AddAttachment;
using PROJECT.Application.Features.Feeds.Commands.ApprovePost;
using PROJECT.Application.Features.Feeds.Commands.CreatePost;
using PROJECT.Application.Features.Feeds.Commands.DeleteFeedById;
using PROJECT.Application.Features.Feeds.Commands.EditFeedById;
using PROJECT.Application.Features.Feeds.Commands.RejectPost;
using PROJECT.Application.Features.Feeds.Commands.RemoveAttachment;
using PROJECT.Application.Features.Feeds.Queries.GetApprovedPostsPaging;
using PROJECT.Application.Features.Feeds.Queries.GetMyPostsPaging;
using PROJECT.Application.Features.Feeds.Queries.GetPostById;
using PROJECT.Application.Features.Feeds.Queries.GetPostsPaging;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Domain.Enums;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Hubs;
using PROJECT.WebApi.Hubs.Implements;
using PROJECT.WebApi.Hubs.Interfaces;
using PROJECT.WebApi.Hubs.Models;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    public class PostsController : BaseController
    {
        private readonly INotificationWrapper _notificationWrapper;
        private readonly IUserRepository _userRepository;
        private readonly IAuthUserService _authUserService;

        public PostsController(IHubContext<NotificationHub, INotifications> hubContext,
            INotificationWrapper notificationWrapper, IAuthUserService authUserService, IUserRepository userRepository)
        {
            _notificationWrapper = notificationWrapper;
            _notificationWrapper.SetHubClient(hubContext.Clients);
            _authUserService = authUserService;
            _userRepository = userRepository;
        }

        [HttpGet]
        [CompanyAuthorize(FeedAuthority.View)]
        public async Task<IActionResult> GetPaging([FromQuery] GetPostsPagingQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("my-posts")]
        [CompanyAuthorize]
        public async Task<IActionResult> GetPaging([FromQuery] GetMyPostsPagingQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("approved")]
        [CompanyAuthorize]
        public async Task<IActionResult> GetApprovedPaging([FromQuery] GetApprovedPostsPagingQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("{id}")]
        [CompanyAuthorize]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetPostByIdQuery(id, false)));
        }

        [HttpGet("{id}/raw")]
        [CompanyAuthorize]
        public async Task<IActionResult> RawById(int id)
        {
            return Ok(await Mediator.Send(new GetPostByIdQuery(id, true)));
        }

        [HttpPost("approve")]
        [CompanyAuthorize(FeedAuthority.Approve)]
        public async Task<IActionResult> Approve([FromBody] ApprovePostCommand command)
        {
            var response = await Mediator.Send(command);
            var user = await _userRepository.GetByIdAsync(_authUserService.UserId);
            await _notificationWrapper.SendNotification(response.Message, new NotificationModel
            {
                Category = NotificationCaterogy.MyFeeds,
                Title = "New Feeds",
                Description = $"Your feed has been approved by {user.FullName}",
                Url = $"/feeds/{response.Data}/raw"
            });
            return CreatedAtAction(nameof(GetById), new {Id = response.Data}, response);
        }


        [HttpPost("reject")]
        [CompanyAuthorize(FeedAuthority.Approve)]
        public async Task<IActionResult> Reject([FromBody] RejectPostCommand command)
        {
            var response = await Mediator.Send(command);
            var user = await _userRepository.GetByIdAsync(_authUserService.UserId);
            await _notificationWrapper.SendNotification(response.Message, new NotificationModel
            {
                Category = NotificationCaterogy.MyFeeds,
                Title = "New Feeds",
                Description = $"Your feed has been rejected by {user.FullName}",
                Url = $"/feeds/{response.Data}/raw"
            });
            return CreatedAtAction(nameof(GetById), new {Id = response.Data}, response);
        }

        [HttpPost]
        [CompanyAuthorize]
        public async Task<IActionResult> Create([FromForm] CreatePostCommand command)
        {
            var response = await Mediator.Send(command);
            return CreatedAtAction(nameof(RawById), new {Id = response.Data}, response);
        }

        [HttpDelete("{id}")]
        [CompanyAuthorize]
        public async Task<IActionResult> Delete(int id)
        {
            var response = await Mediator.Send(new DeleteFeedByIdCommand(id));

            return Ok(response);
        }

        [HttpPut("{id}")]
        [CompanyAuthorize]
        public async Task<IActionResult> Edit(int id, EditFeedByIdRequest request)
        {
            var response = await Mediator.Send(new EditFeedByIdCommand
            {
                Id = id,
                Title = request.Title,
                Description = request.Description,
                BriefDescription = request.BriefDescription
            });

            return Ok(response);
        }

        [HttpPatch("{id}/add-attachment")]
        [CompanyAuthorize]
        public async Task<IActionResult> AddAttachment(int id, [FromForm] AddAttachmentRequest request)
        {
            var response = await Mediator.Send(new AddAttachmentCommand
            {
                PostId = id,
                Attachment = request.Attachment
            });

            return Ok(response);
        }

        [HttpPatch("{postId}/remove-attachment/{attachmentId}")]
        [CompanyAuthorize]
        public async Task<IActionResult> RemoveAttachment(int postId, int attachmentId)
        {
            var response = await Mediator.Send(new RemoveAttachmentCommand
            {
                AttachmentId = attachmentId,
                PostId = postId
            });

            return Ok(response);
        }
    }
}