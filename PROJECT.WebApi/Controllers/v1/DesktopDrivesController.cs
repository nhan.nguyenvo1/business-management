﻿using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.DesktopDrives.Commands.CreateDrive;
using PROJECT.Application.Features.DesktopDrives.Commands.CreateFolder;
using PROJECT.Application.Features.DesktopDrives.Commands.DeleteDrive;
using PROJECT.Application.Features.DesktopDrives.Commands.DownloadDrive;
using PROJECT.Application.Features.DesktopDrives.Commands.MoveDrive;
using PROJECT.Application.Features.DesktopDrives.Commands.RenameDrive;
using PROJECT.Application.Features.DesktopDrives.Queries.GetRecursiveDrives;
using PROJECT.WebApi.Attributes;
using System.Threading.Tasks;

namespace PROJECT.WebApi.Controllers.v1
{
    /// <summary>
    /// Desktop Drives Controller
    /// </summary>
    [Route("api/v{version:apiVersion}/desktop-drive")]
    public class DesktopDrivesController : BaseController
    {
        /// <summary>
        /// Create File
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("files")]
        [CompanyAuthorize]
        public async Task<IActionResult> CreateFile([FromForm] CreateDriveCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Create Folder
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("folder")]
        [CompanyAuthorize]
        public async Task<IActionResult> CreateFolder([FromBody] CreateDesktopFolderCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Delete Drive
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        [CompanyAuthorize]
        public async Task<IActionResult> DeleteDrive([FromBody] DeleteDriveCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Move Drive
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("move")]
        [CompanyAuthorize]
        public async Task<IActionResult> MoveDrive([FromBody] MoveDriveCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Rename Drive
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("rename")]
        [CompanyAuthorize]
        public async Task<IActionResult> RenameDrive([FromBody] RenameDriveCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Get All Drive
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [CompanyAuthorize(CompanyRequired = false)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetRecursiveDrivesQuery()));
        }

        /// <summary>
        /// Download
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [CompanyAuthorize]
        public async Task<IActionResult> Download(int id)
        {
            return Ok(await Mediator.Send(new DownloadDriveCommand { FileId = id }));
        }
    }
}
