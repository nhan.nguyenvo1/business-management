﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using PROJECT.Application.Features.CompanyDrives.Commands.Copy;
using PROJECT.Application.Features.CompanyDrives.Commands.CreateFiles;
using PROJECT.Application.Features.CompanyDrives.Commands.CreateFolder;
using PROJECT.Application.Features.CompanyDrives.Commands.Delete;
using PROJECT.Application.Features.CompanyDrives.Commands.DownloadFile;
using PROJECT.Application.Features.CompanyDrives.Commands.DownloadFolder;
using PROJECT.Application.Features.CompanyDrives.Commands.Move;
using PROJECT.Application.Features.CompanyDrives.Queries.GetFolders;
using PROJECT.Domain.Enums;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Hubs;
using PROJECT.WebApi.Hubs.Implements;
using PROJECT.WebApi.Hubs.Interfaces;
using PROJECT.WebApi.Hubs.Models;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    [Route("api/v{version:apiVersion}/company-drives")]
    public class CompanyDrivesController : BaseController
    {
        [HttpGet]
        [CompanyAuthorize(Policy = CompanyDriveAuthority.View)]
        public async Task<IActionResult> GetByFolderId([FromQuery] GetCompanyFoldersQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost("copy")]
        [CompanyAuthorize(Policy = CompanyDriveAuthority.Copy)]
        public async Task<IActionResult> Copy([FromBody] CompanyDriveCopyCommand command)
        {
            var response = await Mediator.Send(command);

            return Ok(response);
        }

        [HttpPost("files")]
        [CompanyAuthorize(Policy = CompanyDriveAuthority.Upload)]
        public async Task<IActionResult> UploadFiles([FromForm] CreateCompanyFilesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("folders")]
        [CompanyAuthorize(Policy = CompanyDriveAuthority.CreateFolder)]
        public async Task<IActionResult> Create([FromBody] CreateCompanyFolderCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [CompanyAuthorize(Policy = CompanyDriveAuthority.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteCompanyDriveCommand {DriveId = id}));
        }

        [HttpGet("files/{id}")]
        [CompanyAuthorize(Policy = CompanyDriveAuthority.Download)]
        public async Task<FileStreamResult> DownloadFile(int id)
        {
            var response = await Mediator.Send(new DownloadCompanyFileCommand {FileId = id});

            return new FileStreamResult(response.Data.Content, "application/octet-stream")
            {
                FileDownloadName = response.Data.FileName
            };
        }

        [HttpGet("folders/{id}")]
        [CompanyAuthorize(Policy = CompanyDriveAuthority.Download)]
        public async Task<FileStreamResult> DownloadFolder(int id)
        {
            var response = await Mediator.Send(new DownloadCompanyFolderCommand {FolderId = id});

            return new FileStreamResult(response.Data.Content, "application/octet-stream")
            {
                FileDownloadName = response.Data.FileName
            };
        }

        [HttpPost("move")]
        [CompanyAuthorize(Policy = CompanyDriveAuthority.Move)]
        public async Task<IActionResult> Move([FromBody] CompanyDriveMoveCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}