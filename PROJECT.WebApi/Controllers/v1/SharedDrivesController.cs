﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.SharedDrives.Commands.Download;
using PROJECT.Application.Features.SharedDrives.Queries.GetFolders;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    [Route("api/v{version:apiVersion}/shared-drives")]
    public class SharedDrivesController : BaseController
    {
        [HttpGet]
        [CompanyAuthorize(Policy = SharedDriveAuthority.View)]
        public async Task<IActionResult> Get([FromQuery] GetSharedFoldersQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        [HttpGet("{id}/download")]
        [CompanyAuthorize(Policy = SharedDriveAuthority.Download)]
        public async Task<FileStreamResult> DownloadFile(int id)
        {
            var response = await Mediator.Send(new DownloadSharedDriveCommand {DriveId = id});

            return new FileStreamResult(response.Data.Content, "application/octet-stream")
            {
                FileDownloadName = response.Data.FileName
            };
        }
    }
}