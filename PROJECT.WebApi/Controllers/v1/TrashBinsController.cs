﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.TrashBins.Commands.Delete;
using PROJECT.Application.Features.TrashBins.Commands.Restore;
using PROJECT.Application.Features.TrashBins.Queries.GetDeletedDrives;
using PROJECT.WebApi.Attributes;

namespace PROJECT.WebApi.Controllers.v1
{
    [Route("api/v{version:apiVersion}/trash-bins")]
    public class TrashBinsController : BaseController
    {
        [HttpGet]
        [CompanyAuthorize]
        public async Task<IActionResult> Get([FromQuery] GetDeletedDrivesQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        [HttpDelete]
        [CompanyAuthorize]
        public async Task<IActionResult> Delete([FromBody] PermanentlyDeleteDrivesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("restore")]
        [CompanyAuthorize]
        public async Task<IActionResult> Restore([FromBody] RestoreDeletedDrivesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}