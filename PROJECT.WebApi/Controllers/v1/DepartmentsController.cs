using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Departments.Commands.CreateDepartment;
using PROJECT.Application.Features.Departments.Commands.DeleteDepartmentById;
using PROJECT.Application.Features.Departments.Commands.UpdateDepartment;
using PROJECT.Application.Features.Departments.Queries.GetAllDepartments;
using PROJECT.Application.Features.Departments.Queries.GetDepartmentById;
using PROJECT.Application.Features.Departments.Queries.GetDepartments;
using PROJECT.WebApi.Attributes;
using PROJECT.WebApi.Permissions.Authorities;

namespace PROJECT.WebApi.Controllers.v1
{
    public class DepartmentsController : BaseController
    {
        // GET: api/<controller>
        [Cache(600)]
        [HttpGet]
        [CompanyAuthorize(Policy = DepartmentAuthority.View)]
        public async Task<IActionResult> Get([FromQuery] GetAllDepartmentsQuery filter)
        {
            return Ok(await Mediator.Send(filter));
        }

        // GET: api/<controller>
        [Cache(600)]
        [HttpGet("all")]
        [CompanyAuthorize]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetDepartmentsQuery()));
        }

        [HttpGet("{id}")]
        [CompanyAuthorize(Policy = DepartmentAuthority.Detail)]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetDepartmentByIdQuery(id)));
        }

        [HttpPut("{id}")]
        [CompanyAuthorize(Policy = DepartmentAuthority.Edit)]
        public async Task<IActionResult> Update(int id, [FromBody] UpdateDepartmentCommand command)
        {
            if (id != command.Id) return BadRequest();

            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        [CompanyAuthorize(Policy = DepartmentAuthority.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteDepartmentCommand(id)));
        }

        [HttpPost]
        [CompanyAuthorize(Policy = DepartmentAuthority.Create)]
        public async Task<IActionResult> Post([FromBody] CreateDepartmentCommand command)
        {
            var response = await Mediator.Send(command);
            return CreatedAtAction(nameof(GetById), new {id = response.Data}, response);
        }
    }
}