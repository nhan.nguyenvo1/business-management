﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PROJECT.Application.Features.Plans.Commands.Upgrade;
using PROJECT.Application.Features.Plans.Queries.GetPlans;
using PROJECT.WebApi.Attributes;

namespace PROJECT.WebApi.Controllers.v1
{
    public class PlansController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromQuery] GetPlansQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        [CompanyAuthorize(CompanyRequired = false)]
        [HttpPost("upgrade")]
        public async Task<IActionResult> CreateRequest([FromBody] UpgradePlanCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}