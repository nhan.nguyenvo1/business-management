﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using PROJECT.Application.Wrappers;

namespace PROJECT.WebApi.Extensions
{
    /// <summary>
    /// Model state extension
    /// </summary>
    public static class ModelStateExtension
    {
        /// <summary>
        /// Get Model Error
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public static Response<string> GetModelError(this ModelStateDictionary modelState)
        {
            var error = modelState.Values.SelectMany(m => m.Errors)
                .Select(m => m.ErrorMessage).First();
            return new Response<string>(error);
        }
    }
}