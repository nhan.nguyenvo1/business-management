﻿using Microsoft.AspNetCore.Builder;
using PROJECT.WebApi.Middlewares;

namespace PROJECT.WebApi.Extensions
{
    /// <summary>
    /// Application Builder Extension
    /// </summary>
    public static class AppExtension
    {
        /// <summary>
        /// Swagger extension
        /// </summary>
        /// <param name="app"></param>
        public static void UseSwaggerExtension(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "BusinessManagement.WebApi"); });
        }

        /// <summary>
        /// Error Middleware Extension
        /// </summary>
        /// <param name="app"></param>
        public static void UseErrorHandlingMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ErrorHandlerMiddleware>();
        }
    }
}