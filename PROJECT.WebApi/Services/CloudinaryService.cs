﻿using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using PROJECT.Application.Dtos.Cloudinary;
using PROJECT.Application.Interfaces;

namespace PROJECT.WebApi.Services
{
    public class CloudinaryService : ICloudinaryService
    {
        private readonly Cloudinary _cloudinary;
        private readonly IMapper _mapper;

        public CloudinaryService(Cloudinary cloudinary, IMapper mapper)
        {
            _cloudinary = cloudinary;
            _mapper = mapper;
        }

        public async Task RemoveAsync(string path)
        {
            var deletionParams = new DeletionParams(path);
            await _cloudinary.DestroyAsync(deletionParams);
        }

        public string GetImage(string path, string extension = "jpg")
        {
            return _cloudinary
                .Api
                .UrlImgUp
                .Transform(new Transformation().Width(36).Crop("scale"))
                .BuildImageTag($"{path}.{extension}");
        }

        public async Task<CloudinaryResponse> UploadAsync(string name, Stream stream, bool useFileName,
            string folder = "")
        {
            var uploadParams = new RawUploadParams
            {
                File = new FileDescription(name, stream),
                Folder = folder,
                UseFilename = useFileName
            };

            var uploadResult = await _cloudinary.UploadAsync(uploadParams);
            return _mapper.Map<CloudinaryResponse>(uploadResult);
        }
    }
}