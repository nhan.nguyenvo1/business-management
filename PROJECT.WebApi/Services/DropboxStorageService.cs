﻿using System;
using System.IO;
using System.Threading.Tasks;
using Dropbox.Api;
using Dropbox.Api.Files;
using PROJECT.Application.Dtos.Drive;
using PROJECT.Application.Interfaces;

namespace PROJECT.WebApi.Services
{
    public class DropboxStorageService : IDriveStorageService
    {
        private readonly DropboxClient _dropboxClient;

        public DropboxStorageService(DropboxClient dropboxClient)
        {
            _dropboxClient = dropboxClient;
        }

        public Task ChangeName(string path, string newName)
        {
            throw new NotImplementedException();
        }

        public async Task<DriveMetadata> CopyAsync(string src, string des)
        {
            var result = await _dropboxClient.Files.CopyV2Async(src, des, autorename: true);

            return new DriveMetadata
            {
                Name = result.Metadata.Name,
                Path = result.Metadata.PathLower
            };
        }

        public async Task<DriveCreateFolderResult> CreateFolderAsync(string path)
        {
            try
            {
                var folder = await _dropboxClient.Files.GetMetadataAsync(path);
                return new DriveCreateFolderResult
                {
                    Name = folder.Name,
                    Path = folder.PathLower
                };
            }
            catch (Exception)
            {
                var result = await _dropboxClient.Files.CreateFolderV2Async(path);

                return new DriveCreateFolderResult
                {
                    Path = result.Metadata.PathLower,
                    Name = result.Metadata.Name
                };
            }
        }

        public async Task DeleteAsync(string path)
        {
            await _dropboxClient.Files.DeleteV2Async(path);
        }

        public async Task<DriveDownloadResponse> DownloadAsync(string path)
        {
            var response = await _dropboxClient.Files.DownloadAsync(path);

            return new DriveDownloadResponse
            {
                FileName = response.Response.Name,
                Content = await response.GetContentAsStreamAsync()
            };
        }

        public async Task<DriveDownloadResponse> DownloadZipAsync(string path)
        {
            var response = await _dropboxClient.Files.DownloadZipAsync(path);

            var fileName = response.Response.Metadata.Name.Contains(".zip")
                ? response.Response.Metadata.Name
                : $"{response.Response.Metadata.Name}.zip";

            return new DriveDownloadResponse
            {
                FileName = fileName,
                Content = await response.GetContentAsStreamAsync()
            };
        }

        public async Task<DriveMetadata> MoveAsync(string src, string des)
        {
            var result = await _dropboxClient.Files.MoveV2Async(src, des, autorename: true);

            return new DriveMetadata
            {
                Name = result.Metadata.Name,
                Path = result.Metadata.PathLower
            };
        }

        public async Task<bool> PathExists(string path)
        {
            try
            {
                await _dropboxClient.Files.GetMetadataAsync(path);
                return true;
            }
            catch (ApiException<GetMetadataError>)
            {
                return false;
            }
        }

        public async Task<DriveUploadFileResult> UploadFileAsync(string path, Stream body, bool replaced = false)
        {
            var result = replaced switch
            {
                true => await _dropboxClient.Files.UploadAsync(path, WriteMode.Overwrite.Instance, body: body),
                _ => await _dropboxClient.Files.UploadAsync(path, WriteMode.Add.Instance, body: body, autorename: true,
                    strictConflict: true)
            };

            return new DriveUploadFileResult
            {
                FileName = result.Name,
                Path = result.PathLower,
                FileSize = result.Size
            };
        }

        public Task RestoreAsync(string path)
        {
            throw new NotImplementedException();
        }
    }
}