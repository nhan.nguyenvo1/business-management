﻿using Microsoft.AspNetCore.Http;
using PROJECT.Application.Interfaces;
using System;
using System.Linq;

namespace PROJECT.WebApi.Services
{
    /// <summary>
    /// Hub service
    /// </summary>
    public class HubService : IHubService
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        /// <inheritdoc />
        public string ConnectionId
        {
            get
            {
                var httpContext = httpContextAccessor.HttpContext;
                if (httpContext == null)
                {
                    return string.Empty;
                }

                try
                {
                    var companyHeader = httpContext.Request.Headers["X-Connection-Id"];
                    return companyHeader.First();
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Create a new instance of <see cref="HubService"/>
        /// </summary>
        public HubService(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }
    }
}
