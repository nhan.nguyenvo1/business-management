﻿using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Net.Http.Headers;
using PROJECT.Application.Interfaces;

namespace PROJECT.WebApi.Services
{
    /// <summary>
    /// Auth user service
    /// </summary>
    public class AuthUserService : IAuthUserService
    {
        private readonly ClaimsIdentityOptions _options = new();

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public AuthUserService(IHttpContextAccessor httpContextAccessor)
        {
            var httpContext = httpContextAccessor.HttpContext;
            if (httpContext == null)
            {
                UserId = null;
                return;
            }

            if (httpContext.User?.Identity != null && httpContext.User.Identity.IsAuthenticated)
            {
                UserId = httpContext.User?.FindFirstValue(_options.UserIdClaimType);
            }
            else
            {
                var accessToken = httpContext.Request.Headers[HeaderNames.Authorization].ToString()
                    .Replace("Bearer ", "");
                var jwtTokenHandler = new JwtSecurityTokenHandler();
                if (!jwtTokenHandler.CanReadToken(accessToken)) return;

                var token = jwtTokenHandler.ReadJwtToken(accessToken);
                UserId = token.Claims.FirstOrDefault(claim => claim.Type == _options.UserIdClaimType)?.Value;
            }
        }

        /// <summary>
        /// User id
        /// </summary>
        public string UserId { get; }
    }
}