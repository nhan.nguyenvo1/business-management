﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using PROJECT.Application.Interfaces;

namespace PROJECT.WebApi.Services
{
    public class CompanyService : ICompanyService
    {
        public CompanyService(IHttpContextAccessor httpContextAccessor)
        {
            var httpContext = httpContextAccessor.HttpContext;
            if (httpContext == null)
            {
                CompanyId = null;
                return;
            }

            try
            {
                var companyHeader = httpContext.Request.Headers["X-Company-Id"];
                CompanyId = int.Parse(companyHeader.First());
            }
            catch (Exception)
            {
                CompanyId = null;
            }
        }

        public int? CompanyId { get; }
    }
}