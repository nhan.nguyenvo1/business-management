﻿using System;
using System.Threading.Tasks;

namespace PROJECT.WebApi.Services
{
    public interface IResponseCacheService
    {
        Task CacheResponseAsync(string key, object response, TimeSpan timeToLive);

        Task<string> GetCachedResponseAsync(string key);

        Task InvalidateCachedAsync(string key);
    }
}