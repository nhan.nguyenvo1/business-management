﻿using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using PROJECT.Application.Dtos.Company;
using PROJECT.Application.Interfaces;
using PROJECT.Application.Interfaces.Repositories;
using PROJECT.Application.Wrappers;
using PROJECT.Domain.Enums;

namespace PROJECT.WebApi.Attributes
{
    /// <summary>
    /// Company Authorize
    /// </summary>
    public class CompanyAuthorizeAttribute : AuthorizeAttribute, IAsyncAuthorizationFilter
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CompanyAuthorizeAttribute()
        {
        }

        /// <inheritdoc />
        public CompanyAuthorizeAttribute(string policy) : base(policy)
        {
        }

        /// <summary>
        /// Company Required
        /// </summary>
        public bool CompanyRequired { get; set; } = true;

        /// <inheritdoc />
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var user = context.HttpContext.User;

            if (!user.Identity.IsAuthenticated) return;

            if (CompanyRequired)
            {
                var companyService =
                    context.HttpContext.RequestServices.GetService(typeof(ICompanyService)) as ICompanyService;
                var userRepository =
                    context.HttpContext.RequestServices.GetService(typeof(IUserRepository)) as IUserRepository;
                var companyRepository =
                    context.HttpContext.RequestServices.GetService(typeof(ICompanyRepository)) as ICompanyRepository;

                if (!companyService.CompanyId.HasValue)
                {
                    context.Result = new ContentResult
                    {
                        Content = JsonSerializer.Serialize(new Response<string>("Missing company"),
                            new JsonSerializerOptions
                            {
                                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                            }),
                        StatusCode = (int) HttpStatusCode.NotFound,
                        ContentType = "application"
                    };
                    return;
                }

                var company = await companyRepository.GetByIdAsync<CompanyStatusVm>(companyService.CompanyId.Value);

                if (company == null)
                {
                    context.Result = new ContentResult
                    {
                        Content = JsonSerializer.Serialize(new Response<string>("Company does not exists"),
                            new JsonSerializerOptions
                            {
                                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                            }),
                        StatusCode = (int) HttpStatusCode.NotFound,
                        ContentType = "application"
                    };
                    return;
                }

                if (company.Status == CompanyStatus.Deactivated)
                {
                    context.Result = new ContentResult
                    {
                        Content = JsonSerializer.Serialize(
                            new Response<string>($"Company is deleted with reason {company.Reason}"),
                            new JsonSerializerOptions
                            {
                                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                            }),
                        StatusCode = (int) HttpStatusCode.NotFound,
                        ContentType = "application"
                    };
                    return;
                }

                var isInCompany = await userRepository.IsInCompanyAsync(companyService.CompanyId.Value);

                if (!isInCompany)
                {
                    context.Result = new ContentResult
                    {
                        Content = JsonSerializer.Serialize(new Response<string>("Company does not exists"),
                            new JsonSerializerOptions
                            {
                                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
                            }),
                        StatusCode = (int) HttpStatusCode.NotFound,
                        ContentType = "application"
                    };
                }
            }
        }
    }
}