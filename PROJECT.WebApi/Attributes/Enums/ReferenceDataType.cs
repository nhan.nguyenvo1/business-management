﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROJECT.WebApi.Attributes.Enums
{
    /// <summary>
    /// Reference Data Type
    /// </summary>
    public enum ReferenceDataType
    {
        /// <summary>
        /// Employees
        /// </summary>
        Employees,
        /// <summary>
        /// Departments
        /// </summary>
        Departments
    }
}
