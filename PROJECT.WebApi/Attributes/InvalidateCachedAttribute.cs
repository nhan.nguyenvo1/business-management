﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Domain.Settings;
using PROJECT.WebApi.Attributes.Enums;
using PROJECT.WebApi.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROJECT.WebApi.Attributes
{
    /// <summary>
    /// Invalidate Cached
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class InvalidateCachedAttribute : Attribute, IAsyncActionFilter
    {
        private readonly ReferenceDataType _referenceDataType;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="referenceDataType"></param>
        public InvalidateCachedAttribute(ReferenceDataType referenceDataType)
        {
            _referenceDataType = referenceDataType;
        }

        /// <inheritdoc />
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var cacheSetting = context.HttpContext.RequestServices.GetRequiredService<RedisCacheSetting>();

            if (!cacheSetting.Enabled)
            {
                await next();
                return;
            }

            var cacheService = context.HttpContext.RequestServices.GetRequiredService<IResponseCacheService>();
            await cacheService.InvalidateCachedAsync(_referenceDataType.ToString());

            await next();
            return;
        }
    }
}
