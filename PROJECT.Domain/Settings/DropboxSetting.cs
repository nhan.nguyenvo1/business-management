﻿namespace PROJECT.Domain.Settings
{
    public class DropboxSetting
    {
        public string AppKey { get; set; }

        public string AppSecret { get; set; }

        public string AccessToken { get; set; }
    }
}