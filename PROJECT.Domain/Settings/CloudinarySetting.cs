﻿namespace PROJECT.Domain.Settings
{
    public class CloudinarySetting
    {
        public string CloudName { get; set; }

        public string ApiKey { get; set; }

        public string ApiSecret { get; set; }

        public string ApiEnvironmentVariable { get; set; }
    }
}