﻿namespace PROJECT.Domain.Entities
{
    public class UserCompany
    {
        public string UserId { get; set; }

        public int CompanyId { get; set; }

        public virtual AppUser User { get; set; }

        public virtual Company Company { get; set; }
    }
}