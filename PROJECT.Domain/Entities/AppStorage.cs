﻿using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class AppStorage : BaseEntity, IHasCompany
    {
        public AppStorage()
        {
            Files = new List<AppFile>();
            CompanyDrives = new HashSet<CompanyDrive>();
        }

        public StorageType StorageType { get; set; }

        public ulong Limit { get; set; }

        public string UserId { get; set; }

        public string Path { get; set; }


        public virtual ICollection<AppFile> Files { get; set; }

        public virtual ICollection<CompanyDrive> CompanyDrives { get; set; }

        public virtual AppUser User { get; set; }

        public int CompanyId { get; set; }
    }
}