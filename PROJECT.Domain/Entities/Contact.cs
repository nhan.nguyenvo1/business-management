﻿using PROJECT.Domain.Common;

namespace PROJECT.Domain.Entities
{
    public class Contact : BaseEntity
    {
        public string Address { get; set; }

        public string Email { get; set; }

        public string MoreDescription { get; set; }

        public string PhoneNumber { get; set; }

        public bool IsDefault { get; set; }
    }
}
