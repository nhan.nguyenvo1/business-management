﻿namespace PROJECT.Domain.Entities
{
    public class UserTask
    {
        public int TaskId { get; set; }

        public string UserId { get; set; }

        public virtual AppTask AppTask { get; set; }

        public virtual AppUser AppUser { get; set; }
    }
}
