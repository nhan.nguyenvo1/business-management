﻿using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using System;

namespace PROJECT.Domain.Entities
{
    public class Activity : BaseEntity
    {
        public DateTime Date { get; set; }

        public string UserId { get; set; }

        public ActivityType ActivityType { get; set; }
    }
}
