﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class Department : BaseEntity, IDateTimeTracking, IHasOwner<AppUser, string>, IHasCompany
    {
        #region Constructor

        public Department()
        {
            Users = new List<AppUser>();

            Children = new List<Department>();
        }

        #endregion

        #region Properties

        public string Name { get; set; }

        public string Url { get; set; }

        public int? DepartmentId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public string SupervisorId { get; set; }

        public int CompanyId { get; set; }

        #endregion

        #region Relation Ship

        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }

        public virtual AppUser Supervisor { get; set; }

        public virtual Department Parent { get; set; }

        public virtual ICollection<Department> Children { get; set; }

        public virtual ICollection<AppUser> Users { get; set; }

        #endregion
    }
}