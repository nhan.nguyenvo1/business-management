﻿using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class Package : BaseEntity, ISoftDelete
    {
        public Package()
        {
            Users = new HashSet<AppUser>();
        }

        public string Name { get; set; }

        public int NumOfEmployee { get; set; }

        public int SizeOfDrive { get; set; }

        public float Cost { get; set; }

        public bool IsDefault { get; set; }

        public float Sale { get; set; }

        public virtual ICollection<AppUser> Users { get; set; }

        public bool Deleted { get; set; }
    }
}