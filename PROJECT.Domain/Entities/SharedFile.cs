﻿namespace PROJECT.Domain.Entities
{
    public class SharedFile
    {
        public string UserId { get; set; }

        public int FileId { get; set; }

        public bool CanRead { get; set; }

        public bool CanWrite { get; set; }

        public bool CanAdd { get; set; }

        public bool Root { get; set; }


        public virtual AppUser User { get; set; }

        public virtual AppFile File { get; set; }
    }
}