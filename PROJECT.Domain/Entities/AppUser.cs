﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class AppUser : IdentityUser, IDateTimeTracking, IHasOwner<AppUser, string>
    {
        public AppUser()
        {
            Histories = new List<History>();
            UserWorks = new List<UserWork>();
            UserTimes = new List<UserTime>();
            WorkSchedules = new List<WorkSchedule>();
            Notifications = new List<Notification>();
            Departments = new List<Department>();
            ReadRooms = new List<AppRoom>();
            UserRooms = new List<AppRoom>();
            BannedRooms = new List<AppRoom>();
            ChatNotifications = new List<ChatNotification>();
            AppFiles = new List<AppFile>();
            UserTasks = new List<AppTask>();
            Companies = new List<Company>();
            Storages = new List<AppStorage>();
            SharedFiles = new HashSet<SharedFile>();
            BookingRooms = new HashSet<BookingRoom>();
        }

        #region Properties

        public string AvatarUrl { get; set; }

        public string AvatarPublicId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserGender Gender { get; set; }

        public DateTime Dob { get; set; }

        public bool? FirstLogin { get; set; }

        public string IpAddress { get; set; }

        public string RefreshToken { get; set; }

        public DateTime CreatedDate { get; set; }

        public UserStatus Status { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public int? PackageId { get; set; }

        #endregion

        #region Relation Ship

        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }

        public virtual Package Package { get; set; }

        public virtual ICollection<Notification> Notifications { get; set; }

        public virtual ICollection<History> Histories { get; set; }

        public virtual ICollection<UserTime> UserTimes { get; set; }

        public virtual ICollection<UserWork> UserWorks { get; set; }

        [NotMapped] public virtual ICollection<WorkSchedule> WorkSchedules { get; set; }

        public virtual ICollection<Department> Departments { get; set; }

        public virtual ICollection<AppTask> UserTasks { get; set; }

        public virtual ICollection<AppRoom> ReadRooms { get; set; }

        public virtual ICollection<AppRoom> UserRooms { get; set; }

        public virtual ICollection<AppRoom> BannedRooms { get; set; }

        public virtual ICollection<ChatNotification> ChatNotifications { get; set; }

        public virtual ICollection<AppFile> AppFiles { get; set; }

        public virtual ICollection<Company> Companies { get; set; }

        public virtual ICollection<AppStorage> Storages { get; set; }

        public virtual ICollection<SharedFile> SharedFiles { get; set; }

        public virtual ICollection<BookingRoom> BookingRooms { get; set; }

        #endregion
    }
}