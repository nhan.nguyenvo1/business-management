﻿namespace PROJECT.Domain.Entities
{
    public class UserNotification
    {
        public string UserId { get; set; }

        public int NotificationId { get; set; }

        public bool Read { get; set; }

        public virtual AppUser User { get; set; }

        public virtual Notification Notification { get; set; }
    }
}