﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class WorkSchedule : BaseEntity, IDateTimeTracking, IHasOwner<AppUser, string>, IHasCompany
    {
        #region Constructor

        public WorkSchedule()
        {
            Users = new List<AppUser>();
            UserWorks = new List<UserWork>();
        }

        #endregion

        #region Properties

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime? DueDate { get; set; }

        public string Attachment { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public int CompanyId { get; set; }

        #endregion

        #region Relation Ship

        [NotMapped] public ICollection<AppUser> Users { get; set; }

        public virtual ICollection<UserWork> UserWorks { get; set; }

        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }

        #endregion
    }
}