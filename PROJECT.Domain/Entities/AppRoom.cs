﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class AppRoom : BaseEntity, IHasOwner<AppUser, string>, IDateTimeTracking, IHasCompany
    {
        public AppRoom()
        {
            AppChats = new List<AppChat>();
            RoomUsers = new List<AppUser>();
            BannedUsers = new List<AppUser>();
            ReadUsers = new List<AppUser>();
        }

        public string Name { get; set; }

        public string Password { get; set; }

        public RoomType RoomType { get; set; }

        public string AvatarUrl { get; set; }

        public string AvatarPublicId { get; set; }

        public virtual ICollection<AppChat> AppChats { get; set; }

        public virtual ICollection<AppUser> RoomUsers { get; set; }

        public virtual ICollection<AppUser> BannedUsers { get; set; }

        public virtual ICollection<AppUser> ReadUsers { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int CompanyId { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }


        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }
    }
}