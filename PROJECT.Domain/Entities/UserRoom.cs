﻿namespace PROJECT.Domain.Entities
{
    public class UserRoom
    {
        public string UserId { get; set; }

        public int RoomId { get; set; }

        public virtual AppUser AppUser { get; set; }

        public virtual BookingRoom AppRoom { get; set; }
    }
}
