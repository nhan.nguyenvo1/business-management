﻿using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;

namespace PROJECT.Domain.Entities
{
    public class PaymentInfo : BaseEntity
    {
        public string CardNumber { get; set; }

        public string CardBank { get; set; }

        public string CardHolder { get; set; }

        public bool IsDefault { get; set; }

        public PaymentType Type { get; set; }
    }
}
