﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class Company : BaseEntity, IDateTimeTracking, IHasOwner<AppUser, string>, ISoftDelete
    {
        public Company()
        {
            Users = new List<AppUser>();
        }

        public string Name { get; set; }

        public CompanyStatus Status { get; set; }

        public string ResonForPending { get; set; }

        public virtual ICollection<AppUser> Users { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }


        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }

        public bool Deleted { get; set; }
    }
}