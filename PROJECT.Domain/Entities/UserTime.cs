﻿using System;
using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class UserTime : BaseEntity, IHasCompany
    {
        #region Relation Ship

        public virtual AppUser AppUser { get; set; }

        #endregion

        #region Properties

        public string Summary { get; set; }

        public string UserId { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan? CheckIn { get; set; }

        public TimeSpan? CheckOut { get; set; }

        public int CompanyId { get; set; }

        #endregion
    }
}