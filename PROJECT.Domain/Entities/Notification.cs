﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class Notification : BaseEntity, IHasCompany, IDateTimeTracking
    {
        public Notification()
        {
            Users = new HashSet<AppUser>();
        }

        public NotificationCaterogy Category { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }

        public virtual ICollection<AppUser> Users { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int CompanyId { get; set; }
    }
}