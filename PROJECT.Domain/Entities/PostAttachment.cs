﻿using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class PostAttachment : BaseEntity, IHasCompany
    {
        #region Relation ship

        public virtual Post Post { get; set; }

        #endregion

        #region Properties

        public string Url { get; set; }

        public string FileName { get; set; }

        public string PublicId { get; set; }

        public int PostId { get; set; }

        public int CompanyId { get; set; }

        #endregion
    }
}