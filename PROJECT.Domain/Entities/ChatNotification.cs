﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class ChatNotification : BaseEntity, IDateTimeTracking, IHasCompany
    {
        public ChatNotification()
        {
            RelatedUser = new List<AppUser>();
        }

        public string Content { get; set; }

        public string Url { get; set; }


        public virtual ICollection<AppUser> RelatedUser { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int CompanyId { get; set; }
    }
}