﻿using System;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class AppChat : BaseEntity, IHasCompany
    {
        public int RoomId { get; set; }

        public string SenderId { get; set; }

        public string Content { get; set; }

        public DateTime When { get; set; }

        public string Attachment { get; set; }

        public string PublicId { get; set; }

        public MessageType MessageType { get; set; }


        public virtual AppUser Sender { get; set; }

        public virtual AppRoom Room { get; set; }

        public int CompanyId { get; set; }
    }
}