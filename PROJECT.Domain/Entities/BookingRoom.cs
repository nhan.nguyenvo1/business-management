﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class BookingRoom : BaseEntity, IDateTimeTracking, IHasOwner<AppUser, string>, IHasCompany
    {
        public BookingRoom()
        {
            AppUsers = new HashSet<AppUser>();
        }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan From { get; set; }

        public TimeSpan? To { get; set; }

        public string Room { get; set; }

        public string Url { get; set; }

        public BookingType Type { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int CompanyId { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public virtual ICollection<AppUser> AppUsers { get; set; }

        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }
    }
}