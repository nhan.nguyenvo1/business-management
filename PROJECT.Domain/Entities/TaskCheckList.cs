﻿using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class TaskCheckList : BaseEntity, IHasCompany
    {
        #region Relationship

        public virtual AppTask Task { get; set; }

        #endregion

        #region Properties

        public string Title { get; set; }

        public bool Checked { get; set; }

        public int TaskId { get; set; }

        public int CompanyId { get; set; }

        #endregion
    }
}