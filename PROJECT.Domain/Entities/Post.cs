﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class Post : BaseEntity, IDateTimeTracking, IHasOwner<AppUser, string>, IHasCompany
    {
        public Post()
        {
            PostAttachments = new List<PostAttachment>();
        }

        #region Properties

        public string Title { get; set; }

        public string BriefDescription { get; set; }

        public string Description { get; set; }

        public PostStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public string ApprovedBy { get; set; }

        public int CompanyId { get; set; }

        #endregion

        #region Relation Ship

        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }

        public virtual AppUser ApprovedUser { get; set; }

        public virtual ICollection<PostAttachment> PostAttachments { get; set; }

        #endregion
    }
}