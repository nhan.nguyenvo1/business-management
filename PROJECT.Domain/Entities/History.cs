﻿using System;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class History : BaseEntity, IHasCompany
    {
        #region Relation Ship

        public virtual AppUser AppUser { get; set; }

        #endregion

        #region Properties

        public DateTime CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string Content { get; set; }

        public HistoryAction Action { get; set; }

        public int CompanyId { get; set; }

        #endregion
    }
}