﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class AppTask : BaseEntity, IHasOwner<AppUser, string>, IDateTimeTracking, IHasCompany
    {
        public AppTask()
        {
            TaskUsers = new List<AppUser>();
            Attachments = new List<TaskAttachment>();
            CheckLists = new List<TaskCheckList>();
        }

        #region Properties

        public bool Completed { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime? DueDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int CompanyId { get; set; }

        #endregion

        #region Relationship

        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }

        public virtual ICollection<AppUser> TaskUsers { get; set; }

        public virtual ICollection<TaskAttachment> Attachments { get; set; }

        public virtual ICollection<TaskCheckList> CheckLists { get; set; }

        #endregion
    }
}