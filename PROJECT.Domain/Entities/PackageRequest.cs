﻿using System;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;

namespace PROJECT.Domain.Entities
{
    public class PackageRequest : BaseEntity
    {
        public string UserId { get; set; }

        public int PackageId { get; set; }

        public RequestStatus Status { get; set; }

        public DateTime? ClosedDate { get; set; }

        public float Cost { get; set; }

        public string Reason { get; set; }

        public virtual AppUser User { get; set; }

        public virtual Package Package { get; set; }
    }
}