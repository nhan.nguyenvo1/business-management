﻿using System;
using System.Collections.Generic;
using PROJECT.Domain.Common;
using PROJECT.Domain.Enums;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class AppFile : BaseEntity, IDateTimeTracking, IHasOwner<AppUser, string>, ISoftDelete, IHasCompany
    {
        public AppFile()
        {
            Children = new List<AppFile>();
            SharedFiles = new HashSet<SharedFile>();
        }

        public int StorageId { get; set; }

        public bool Shared { get; set; }

        public string SharedLink { get; set; }

        public string Name { get; set; }

        public FileType FileType { get; set; }

        public ulong? FileSize { get; set; }

        public int? FileId { get; set; }

        public string Path { get; set; }

        public virtual AppFile Parent { get; set; }

        public virtual AppStorage AppStorage { get; set; }

        public virtual ICollection<AppFile> Children { get; set; }

        public virtual ICollection<AppUser> AppUsers { get; set; }

        public virtual ICollection<SharedFile> SharedFiles { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public int CompanyId { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }


        public virtual AppUser Created { get; set; }

        public virtual AppUser Updated { get; set; }

        public bool Deleted { get; set; }
    }
}