﻿using PROJECT.Domain.Common;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class TaskAttachment : BaseEntity, IHasCompany
    {
        #region Relationship

        public virtual AppTask Task { get; set; }

        #endregion

        #region Properties

        public string Url { get; set; }

        public string PublicId { get; set; }

        public string FileName { get; set; }

        public int TaskId { get; set; }

        public int CompanyId { get; set; }

        #endregion
    }
}