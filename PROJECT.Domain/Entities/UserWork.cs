﻿using System;
using PROJECT.Domain.Enums;

namespace PROJECT.Domain.Entities
{
    public class UserWork
    {
        #region Properties

        public string UserId { get; set; }

        public int WorkId { get; set; }

        public DateTime LastModified { get; set; }

        public UserWorkStatus Status { get; set; }

        public string Attachment { get; set; }

        #endregion

        #region Relation Ship

        public virtual AppUser AppUser { get; set; }

        public virtual WorkSchedule WorkSchedule { get; set; }

        #endregion
    }
}