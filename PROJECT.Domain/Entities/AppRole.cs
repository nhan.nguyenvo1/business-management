﻿using Microsoft.AspNetCore.Identity;
using PROJECT.Domain.Interfaces;

namespace PROJECT.Domain.Entities
{
    public class AppRole : IdentityRole, IHasCompany
    {
        public AppRole()
        {
        }

        public AppRole(string role) : base(role)
        {
            Description = role;
        }

        public string Description { get; set; }

        public int CompanyId { get; set; }
    }
}