﻿namespace PROJECT.Domain.Constants
{
    public static class SystemConstant
    {
        public const string DefaultAvatar =
            "https://res.cloudinary.com/dddzstbl8/image/upload/v1615203221/AppUser/default.jpg";

        public const string DefaultRoomChatAvatar =
            "https://res.cloudinary.com/dddzstbl8/image/upload/v1621102897/Room/57fb3190d0cc1726d782c4e25e8561e9_vj6neb.png";

        public const string DefaultDepartment = "Easy Business";
    }
}