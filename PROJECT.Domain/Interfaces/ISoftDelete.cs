﻿namespace PROJECT.Domain.Interfaces
{
    public interface ISoftDelete
    {
        public bool Deleted { get; set; }
    }
}