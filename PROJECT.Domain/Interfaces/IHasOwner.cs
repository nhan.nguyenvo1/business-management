﻿namespace PROJECT.Domain.Interfaces
{
    public interface IHasOwner<TUser, TKey> where TUser : class
    {
        TKey CreatedBy { get; set; }

        TKey UpdatedBy { get; set; }

        TUser Created { get; set; }

        TUser Updated { get; set; }
    }
}