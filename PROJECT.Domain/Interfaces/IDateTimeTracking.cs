﻿using System;

namespace PROJECT.Domain.Interfaces
{
    public interface IDateTimeTracking
    {
        DateTime CreatedDate { get; set; }

        DateTime UpdatedDate { get; set; }
    }
}