﻿namespace PROJECT.Domain.Interfaces
{
    public interface IHasCompany
    {
        int CompanyId { get; set; }
    }
}