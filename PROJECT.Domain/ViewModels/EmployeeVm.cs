﻿namespace PROJECT.Domain.ViewModels
{
    public class EmployeeVm
    {
        public string Id { get; set; }

        public string Avatar { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public string Dob { get; set; }

        public string Status { get; set; }

        public string CreatedDate { get; set; }

        public string UpdatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public DepartmentVm Department { get; set; }
    }
}