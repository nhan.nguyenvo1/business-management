﻿using System.Collections.Generic;

namespace PROJECT.Domain.ViewModels
{
    public class DepartmentVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public DepartmentVm Parent { get; set; }

        public ICollection<DepartmentVm> Children { get; set; }

        public string CreatedBy { get; set; }

        public string UpdatedBy { get; set; }

        public string CreatedDate { get; set; }

        public string UpdatedDate { get; set; }
    }
}