﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum RoomType
    {
        Public = 1,
        Private = 2,
        PeerToPeer = 3
    }
}