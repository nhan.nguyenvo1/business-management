﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum HistoryAction
    {
        Select = 1,
        Update = 2,
        Delete = 3,
        Insert = 4
    }
}