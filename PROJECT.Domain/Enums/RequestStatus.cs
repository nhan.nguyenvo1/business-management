﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum RequestStatus
    {
        WaitingForPayment = 1,
        ReadyToApprove = 2,
        Closed = 3,
        Rejected = 4
    }
}