﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum UserWorkStatus
    {
        Submitted = 1,
        NoAttempt = 2
    }
}