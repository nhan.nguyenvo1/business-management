﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum NotificationCaterogy
    {
        NewFeed = 1,
        Feeds = 2,
        MyFeeds = 3,
        Employees = 4,
        Departments = 5,
        Roles = 6,
        Tasks = 7,
        MyTasks = 8,
        Calendar = 9,
        WorkTimes = 10,
        MyDrives = 11,
        CompanyDrives = 12,
        ShareDrives = 13,
        TrashDrives = 14,
        BookingRoom = 15
    }
}