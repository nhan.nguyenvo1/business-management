﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum FileType
    {
        Folder = 1,
        Document = 2, // Word, Google Docs
        Presentation = 3, // PowerPoint, Google Slides
        Spreadsheet = 4, // Excel, Google Sheets
        Image = 5,
        Other = 6
    }
}