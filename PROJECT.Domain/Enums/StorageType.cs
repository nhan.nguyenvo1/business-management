﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum StorageType
    {
        CompanyStorage = 1,
        PersonalStorage = 2
    }
}