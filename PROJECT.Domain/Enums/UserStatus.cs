﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum UserStatus
    {
        Online = 1,
        Offline = 2,
        Deleted = 3
    }
}