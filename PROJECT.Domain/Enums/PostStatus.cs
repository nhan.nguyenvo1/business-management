﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum PostStatus
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3
    }
}