﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum MessageType
    {
        Content = 1,
        Image = 2,
        File = 3
    }
}