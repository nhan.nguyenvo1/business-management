﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum UserGender
    {
        Male = 1,
        Female = 2,
        Other = 3
    }
}