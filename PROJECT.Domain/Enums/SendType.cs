﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum SendType
    {
        PeerToPeer = 1,
        Group = 2
    }
}