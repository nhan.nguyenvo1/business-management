﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum ActivityType
    {
        Login = 1,
        Register = 2
    }
}
