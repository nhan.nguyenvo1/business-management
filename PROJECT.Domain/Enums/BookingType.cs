﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum BookingType
    {
        PeerToPeer,
        Online
    }
}