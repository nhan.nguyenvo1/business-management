﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum CompanyStatus
    {
        Activated = 1,
        Deactivated = 2
    }
}