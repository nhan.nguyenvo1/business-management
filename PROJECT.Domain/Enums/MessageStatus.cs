﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum MessageStatus
    {
        NotSeen = 1,
        Seen = 2
    }
}