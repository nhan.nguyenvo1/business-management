﻿using System;

namespace PROJECT.Domain.Enums
{
    [Flags]
    public enum PaymentType
    {
        Banking = 1,
        Visa = 2,
        MasterCard = 3
    }
}
