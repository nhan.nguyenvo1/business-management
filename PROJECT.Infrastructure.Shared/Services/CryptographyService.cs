﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PROJECT.Application.Exceptions;
using PROJECT.Application.Interfaces;
using PROJECT.Domain.Settings;

namespace PROJECT.Infrastructure.Shared.Services
{
    public class CryptographyService : ICryptographyService
    {
        private readonly CryptoSetting _cryptoSetting;

        public CryptographyService(IOptions<CryptoSetting> cryptoSetting)
        {
            _cryptoSetting = cryptoSetting.Value;
        }

        public async Task<T> Decrypt<T>(string cipherText)
        {
            var iv = new byte[16];
            var buffer = Convert.FromBase64String(cipherText);

            using var aes = Aes.Create();
            aes.Key = Encoding.UTF8.GetBytes(_cryptoSetting.Key);
            aes.IV = iv;
            var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

            using var memoryStream = new MemoryStream(buffer);
            using var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            using var streamReader = new StreamReader(cryptoStream);

            try
            {
                return JsonConvert.DeserializeObject<T>(await streamReader.ReadToEndAsync());
            }
            catch (Exception)
            {
                throw new ApiException("Invalid Token");
            }
        }

        public async Task<string> Encrypt<T>(T o)
        {
            var plainText = JsonConvert.SerializeObject(o);

            var iv = new byte[16];
            byte[] array;

            using (var aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(_cryptoSetting.Key);
                aes.IV = iv;

                var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using var memoryStream = new MemoryStream();
                using var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
                using (var streamWriter = new StreamWriter(cryptoStream))
                {
                    await streamWriter.WriteAsync(plainText);
                }

                array = memoryStream.ToArray();
            }

            return Convert.ToBase64String(array);
        }
    }
}