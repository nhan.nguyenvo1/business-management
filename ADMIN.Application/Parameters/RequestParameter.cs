﻿namespace ADMIN.Application.Parameters
{
    public class RequestParameter
    {
        public RequestParameter()
        {
            Page = 1;
            Limit = 10;
        }

        public RequestParameter(int pageNumber, int pageSize)
        {
            Page = pageNumber < 1 ? 1 : pageNumber;
            Limit = pageSize > 10 ? 10 : pageSize;
        }

        public int Page { get; set; }

        public int Limit { get; set; }
    }
}