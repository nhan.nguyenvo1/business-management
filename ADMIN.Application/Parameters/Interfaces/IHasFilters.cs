﻿using System.Collections.Generic;

namespace ADMIN.Application.Parameters.Interfaces
{
    public interface IHasFilters
    {
        Dictionary<string, string> Filters { get; set; }
    }
}