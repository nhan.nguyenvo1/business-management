﻿namespace ADMIN.Application.Parameters.Interfaces
{
    public interface IHasSearch
    {
        string Search { get; set; }
    }
}