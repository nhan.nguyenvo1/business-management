﻿namespace ADMIN.Application.Parameters.Interfaces
{
    public interface IHasSort
    {
        string Sort { get; set; }

        string Order { get; set; }
    }
}