﻿using System.Reflection;
using ADMIN.Application.Behaviors;
using ADMIN.Application.Implements;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Mappings;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace ADMIN.Application
{
    public static class ServiceExtension
    {
        public static void AddApplicationLayer(this IServiceCollection services)
        {
            services.AddSingleton(provider =>
                new MapperConfiguration(cfg => { cfg.AddProfile(new GeneralProfile()); }).CreateMapper());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddTransient<IAuthorityService, AuthorityService>();
        }
    }
}