﻿using System.ComponentModel.DataAnnotations;

namespace ADMIN.Application.Dtos.Account
{
    public class VerifyEmailRequest
    {
        [Required] [EmailAddress] public string Email { get; set; }

        [Required] public string Token { get; set; }

        [Required] public string CurrentPassword { get; set; }

        [Required] public string NewPassword { get; set; }

        [Required] [Compare("NewPassword")] public string ConfirmPassword { get; set; }
    }
}