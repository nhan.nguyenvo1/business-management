﻿using System.ComponentModel.DataAnnotations;

namespace ADMIN.Application.Dtos.Account
{
    public class RefreshTokenRequest
    {
        [Required(ErrorMessage = "Refresh token cannot empty")]
        public string RefreshToken { get; set; }
    }
}