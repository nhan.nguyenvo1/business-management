﻿using System.ComponentModel.DataAnnotations;

namespace ADMIN.Application.Dtos.Account
{
    public class AuthRequest
    {
        [Required(ErrorMessage = "Email or username cannot empty")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password cannot empty")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Remember me cannot empty")]
        public bool RememberMe { get; set; }
    }
}