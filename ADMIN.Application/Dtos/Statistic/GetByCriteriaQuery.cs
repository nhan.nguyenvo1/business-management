﻿using ADMIN.Application.Enums;

namespace ADMIN.Application.Dtos.Statistic
{
    public class GetByCriteriaQuery
    {
        public Criteria Criteria { get; set; }

        public int Top { get; set; } = 10;
    }
}
