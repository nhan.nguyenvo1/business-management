﻿namespace ADMIN.Application.Dtos.Authority
{
    public class AuthorityVm
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public bool Checked { get; set; }
    }
}
