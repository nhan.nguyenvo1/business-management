﻿using System;
using AutoMapper;

namespace ADMIN.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            #region TimeSpan

            CreateMap<TimeSpan?, string>().ConvertUsing(ts => ts.HasValue ? ts.Value.ToString(@"hh\:mm\:ss") : "");

            #endregion

            #region Date time

            CreateMap<DateTime, string>().ConvertUsing(dt => dt.ToString("yyyy-MM-dd"));

            #endregion
        }
    }
}