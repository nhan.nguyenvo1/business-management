﻿namespace ADMIN.Application.Enums
{
    public enum Role
    {
        SupperAdmin,
        Admin,
        Manager,
        Staff
    }
}