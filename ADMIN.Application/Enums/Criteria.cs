﻿using System;

namespace ADMIN.Application.Enums
{
    [Flags]
    public enum Criteria
    {
        ActivityUsers = 1,
        DrivesBelongsToCompany = 2,
        RegisteredUsers = 3,
        TotalDrives = 4,
        TotalUsers = 5,
        TotalCompany = 6
    }
}
