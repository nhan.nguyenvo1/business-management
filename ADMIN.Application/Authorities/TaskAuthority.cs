﻿namespace ADMIN.Application.Authorities
{
    public static class TaskAuthority
    {
        public const string Assign = "Permission.Tasks.Assign";

        public const string View = "Permission.Tasks.View";

        public const string Edit = "Permission.Tasks.Edit";

        public const string Delete = "Permission.Tasks.Delete";
    }
}