﻿namespace ADMIN.Application.Authorities
{
    /// <summary>
    /// Employee Authority
    /// </summary>
    public static class EmployeeAuthority
    {
        public const string View = "Permission.Employees.View";

        public const string Create = "Permission.Employees.Create";

        public const string Delete = "Permission.Employees.Delete";

        public const string Detail = "Permission.Employees.Detail";

        public const string ChangeDepartment = "Permission.Employees.ChangeDepartment";
    }
}