﻿namespace ADMIN.Application.Authorities
{
    /// <summary>
    /// Booking Room Authority
    /// </summary>
    public static class BookingRoomAuthority
    {
        public const string Book = "Permission.BookingRooms.Book";
    }
}