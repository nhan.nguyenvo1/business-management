﻿using System.Collections.Generic;

namespace ADMIN.Application.Authorities.Display
{
    /// <summary>
    /// Authority Display
    /// </summary>
    public static class AuthorityDisplay
    {
        /// <summary>
        /// Display
        /// </summary>
        public static readonly IReadOnlyDictionary<string, string> Display = new Dictionary<string, string>
        {
            { BookingRoomAuthority.Book, "Book Room" },

            { CompanyDriveAuthority.Download, "Download Company Drive" },
            { CompanyDriveAuthority.Copy, "Copy Company Drive" },
            { CompanyDriveAuthority.CreateFolder, "Create Company Folder" },
            { CompanyDriveAuthority.Delete, "Delete Company Folder" },
            { CompanyDriveAuthority.Move, "Move Company Folder" },
            { CompanyDriveAuthority.Upload, "Upload Company Files" },
            { CompanyDriveAuthority.View, "View Company Drives" },

            { DepartmentAuthority.Create, "Create Department" },
            { DepartmentAuthority.Delete, "Delete Department" },
            { DepartmentAuthority.Detail, "View Detail Department" },
            { DepartmentAuthority.Edit, "Edit Department" },
            { DepartmentAuthority.View, "View Department" },

            { EmployeeAuthority.View, "View Employees" },
            { EmployeeAuthority.ChangeDepartment, "Change Employee Department" },
            { EmployeeAuthority.Create, "Invite Employees" },
            { EmployeeAuthority.Delete, "Delete Employee" },
            { EmployeeAuthority.Detail, "View Detail Of Employee" },

            { FeedAuthority.View, "View Approve/Reject Feeds" },
            { FeedAuthority.Approve, "Approve/Reject Feed" },

            { PermissionAuthority.View, "View List Permissions" },

            { RoleAuthority.AssignPermissions, "Assign Permission To Role" },
            { RoleAuthority.CancelPermissions, "Cancel Permission From Role" },
            { RoleAuthority.Create, "Create Role" },
            { RoleAuthority.Delete, "Delete Role" },
            { RoleAuthority.Detail, "View Detail Role" },
            { RoleAuthority.Edit, "Edit Role" },
            { RoleAuthority.View, "View List Roles" },

            { SharedDriveAuthority.View, "View Shared Drives" },
            { SharedDriveAuthority.Download, "Download Shared Drives" },

            { TaskAuthority.View, "View List Tasks" },
            { TaskAuthority.Assign, "Assign Task For Employees" },
            { TaskAuthority.Delete, "Delete Task" },
            { TaskAuthority.Edit, "Edit Task" },

            { UserRoleAuthority.Assign, "Assign Role To User" },
            { UserRoleAuthority.View, "View Roles Of User" },

            { UserTimeAuthority.View, "View Check In/Check Out Time" }
        };
    }
}
