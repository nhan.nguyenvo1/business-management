﻿namespace ADMIN.Application.Authorities
{
    /// <summary>
    /// Role Authority
    /// </summary>
    public static class RoleAuthority
    {
        public const string View = "Permission.Roles.View";

        public const string Detail = "Permission.Roles.Detail";

        public const string Create = "Permission.Roles.Create";

        public const string Edit = "Permission.Roles.Edit";

        public const string Delete = "Permission.Roles.Delete";

        public const string AssignPermissions = "Permission.Roles.AssignPermissions";

        public const string CancelPermissions = "Permission.Roles.CancelPermissions";
    }
}