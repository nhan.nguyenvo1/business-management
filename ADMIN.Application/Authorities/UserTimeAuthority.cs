﻿namespace ADMIN.Application.Authorities
{
    public static class UserTimeAuthority
    {
        public const string View = "Permission.WorkTimes.View";
    }
}