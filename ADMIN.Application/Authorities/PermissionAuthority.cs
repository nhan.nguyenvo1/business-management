﻿namespace ADMIN.Application.Authorities
{
    /// <summary>
    /// Permission Authority
    /// </summary>
    public static class PermissionAuthority
    {
        public const string View = "Permission.Permissions.View";
    }
}