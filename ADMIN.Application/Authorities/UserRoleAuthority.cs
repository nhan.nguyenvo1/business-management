﻿namespace ADMIN.Application.Authorities
{
    public static class UserRoleAuthority
    {
        public const string View = "Permission.UserRoles.View";

        public const string Assign = "Permission.UserRoles.Assign";
    }
}