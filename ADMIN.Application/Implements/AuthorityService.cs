﻿using ADMIN.Application.Authorities.Display;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Dtos.Authority;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ADMIN.Application.Implements
{
    public class AuthorityService : IAuthorityService
    {
        private readonly IQueryExecutorAsync _queryExecutorAsync;

        public AuthorityService(IQueryExecutorAsync queryExecutorAsync)
        {
            _queryExecutorAsync = queryExecutorAsync;
        }

        public async Task<Response<IReadOnlyCollection<AuthorityVm>>> GetByRoleIdAsync(string roleId, int companyId)
        {
            var permissions = GetPermissions();

            var permissionsInRole = await _queryExecutorAsync.Fetch<string>(RolesQueries.GetByRoleId, new { roleId, companyId, ClaimType = "permission" });

            return new Response<IReadOnlyCollection<AuthorityVm>>(permissions.Select(p => new AuthorityVm
            {
                Key = p,
                Value = AuthorityDisplay.Display[p],
                Checked = permissionsInRole.Contains(p)
            }).ToList());
        }

        private static List<string> GetPermissions()
        {
            var policies = Assembly.GetExecutingAssembly().GetExportedTypes().Where(t => t.IsClass && t.Namespace == "ADMIN.Application.Authorities").ToList();
            var permissions = new List<string>();
            policies.ForEach(policy =>
            {
                var info = policy
                        .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                        .Where(fi => fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(string))
                        .Select(x => (string)x.GetRawConstantValue())
                        .ToList();

                permissions.AddRange(info);
            });

            return permissions.ToList();
        }
    }
}
