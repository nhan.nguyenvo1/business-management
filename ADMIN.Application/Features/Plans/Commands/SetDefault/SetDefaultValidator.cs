﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Plans.Commands.SetDefault
{
    public class SetDefaultValidator : AbstractValidator<SetDefaultCommand>
    {
        public SetDefaultValidator()
        {
            RuleFor(p => p.PlanId)
                .NotEmpty();
        }
    }
}
