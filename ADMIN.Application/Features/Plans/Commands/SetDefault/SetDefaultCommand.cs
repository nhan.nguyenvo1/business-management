﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Plans.Commands.SetDefault
{
    public class SetDefaultCommand : IRequest<Response<int>>
    {
        public int PlanId { get; set; }

        public class SetDefaultCommandHandler : IRequestHandler<SetDefaultCommand, Response<int>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public SetDefaultCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<int>> Handle(SetDefaultCommand request, CancellationToken cancellationToken)
            {
                await _queryExecutorAsync.Execute(PlansQueries.SetDefault, new { request.PlanId });
                return new Response<int>(request.PlanId);
            }
        }
    }
}
