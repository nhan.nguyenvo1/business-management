﻿using FluentValidation;

namespace ADMIN.Application.Features.Plans.Commands.CreatePlan
{
    public class CreatePlanValidator : AbstractValidator<CreatePlanCommand>
    {
        public CreatePlanValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .NotNull();

            RuleFor(p => p.NumOfEmployee)
                .GreaterThan(0)
                .NotNull();

            RuleFor(p => p.SizeOfDrive)
                .GreaterThan(0)
                .NotNull();

            RuleFor(p => p.Cost)
                .GreaterThanOrEqualTo(0)
                .NotNull();
        }
    }
}