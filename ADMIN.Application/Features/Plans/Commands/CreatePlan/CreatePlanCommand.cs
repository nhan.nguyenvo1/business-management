﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.Plans.Commands.CreatePlan
{
    public class CreatePlanCommand : IRequest<Response<int>>
    {
        public string Name { get; set; }

        public int NumOfEmployee { get; set; }

        public int SizeOfDrive { get; set; }

        public double Cost { get; set; }

        public class CreatePlanCommandHandler : IRequestHandler<CreatePlanCommand, Response<int>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public CreatePlanCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<int>> Handle(CreatePlanCommand request, CancellationToken cancellationToken)
            {
                var command = new SimpleDbCommand(PlansQueries.Create, request);
                return new Response<int>(await _queryExecutorAsync.FetchOne<int>(command));
            }
        }
    }
}