﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.Plans.Commands.DeletePlans
{
    public class DeletePlansCommand : IRequest<Response<bool>>
    {
        public ICollection<int> Ids { get; set; }

        public class DeletePlansCommandHandler : IRequestHandler<DeletePlansCommand, Response<bool>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public DeletePlansCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<bool>> Handle(DeletePlansCommand request, CancellationToken cancellationToken)
            {
                var command = new SimpleDbCommand(PlansQueries.Delete, new
                {
                    request.Ids
                });
                await _queryExecutorAsync.Execute(command);

                return new Response<bool>(true);
            }
        }
    }
}