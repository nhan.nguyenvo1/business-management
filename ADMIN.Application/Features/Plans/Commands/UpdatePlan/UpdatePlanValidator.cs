﻿using FluentValidation;

namespace ADMIN.Application.Features.Plans.Commands.UpdatePlan
{
    public class UpdatePlanValidator : AbstractValidator<UpdatePlanCommand>
    {
        public UpdatePlanValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .NotNull();

            RuleFor(p => p.NumOfEmployee)
                .GreaterThan(0)
                .NotNull();

            RuleFor(p => p.SizeOfDrive)
                .GreaterThan(0)
                .NotNull();

            RuleFor(p => p.Cost)
                .GreaterThan(0)
                .NotNull();
        }
    }
}