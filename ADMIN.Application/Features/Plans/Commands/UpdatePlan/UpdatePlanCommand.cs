﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.Plans.Commands.UpdatePlan
{
    public class UpdatePlanCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int NumOfEmployee { get; set; }

        public int SizeOfDrive { get; set; }

        public double Cost { get; set; }

        public class UpdatePlanCommandHandler : IRequestHandler<UpdatePlanCommand, Response<int>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public UpdatePlanCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<int>> Handle(UpdatePlanCommand request, CancellationToken cancellationToken)
            {
                var command = new SimpleDbCommand(PlansQueries.Update, request);
                return new Response<int>(await _queryExecutorAsync.FetchOne<int>(command));
            }
        }
    }
}