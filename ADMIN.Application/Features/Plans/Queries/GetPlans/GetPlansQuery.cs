﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.Plans.Queries.GetPlans
{
    public class GetPlansQuery : IRequest<Response<IReadOnlyCollection<GetPlansVm>>>
    {
        public class GetPlansQueryHandler : IRequestHandler<GetPlansQuery, Response<IReadOnlyCollection<GetPlansVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetPlansQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetPlansVm>>> Handle(GetPlansQuery request,
                CancellationToken cancellationToken)
            {
                return new(
                    await _queryExecutorAsync.Fetch<GetPlansVm>(PlansQueries.Get, null));
            }
        }
    }
}