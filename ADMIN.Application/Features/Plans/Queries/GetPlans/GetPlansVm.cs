﻿namespace ADMIN.Application.Features.Plans.Queries.GetPlans
{
    public class GetPlansVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int NumOfEmployee { get; set; }

        public int SizeOfDrive { get; set; }

        public double Cost { get; set; }

        public bool IsDefault { get; set; }

        public int UsedUser { get; set; }
    }
}