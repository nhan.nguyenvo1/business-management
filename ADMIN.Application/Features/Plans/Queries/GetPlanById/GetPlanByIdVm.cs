﻿namespace ADMIN.Application.Features.Plans.Queries.GetPlanById
{
    public class GetPlanByIdVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int NumOfEmployee { get; set; }

        public int SizeOfDrive { get; set; }

        public double Cost { get; set; }

        public int UsedUser { get; set; }
    }
}