﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.Plans.Queries.GetPlanById
{
    public class GetPlanByIdQuery : IRequest<Response<GetPlanByIdVm>>
    {
        public int Id { get; set; }

        public class GetPlanByIdQueryHandler : IRequestHandler<GetPlanByIdQuery, Response<GetPlanByIdVm>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetPlanByIdQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<GetPlanByIdVm>> Handle(GetPlanByIdQuery request,
                CancellationToken cancellationToken)
            {
                return new(
                    await _queryExecutorAsync.FetchOne<GetPlanByIdVm>(PlansQueries.GetById, new {request.Id}));
            }
        }
    }
}