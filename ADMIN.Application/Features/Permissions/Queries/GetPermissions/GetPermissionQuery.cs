﻿using ADMIN.Application.Dtos.Authority;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Permissions.Queries.GetPermissions
{
    public class GetPermissionQuery : IRequest<Response<IReadOnlyCollection<AuthorityVm>>>
    {
        public int CompanyId { get; set; }

        public string RoleId { get; set; }

        public class GetPermissionQueryHandler : IRequestHandler<GetPermissionQuery, Response<IReadOnlyCollection<AuthorityVm>>>
        {
            private readonly IAuthorityService _authorityService;

            public GetPermissionQueryHandler(IAuthorityService authorityService)
            {
                _authorityService = authorityService;
            }

            public Task<Response<IReadOnlyCollection<AuthorityVm>>> Handle(GetPermissionQuery request, CancellationToken cancellationToken)
            {
                return _authorityService.GetByRoleIdAsync(request.RoleId, request.CompanyId);
            }
        }
    }
}
