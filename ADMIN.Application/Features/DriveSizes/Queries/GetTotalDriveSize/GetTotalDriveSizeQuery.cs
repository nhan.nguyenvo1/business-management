﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.DriveSizes.Queries.GetTotalDriveSize
{
    public class GetTotalDriveSizeQuery : IRequest<Response<long>>
    {
        public int? CompanyId { get; set; }

        public class GetTotalDriveSizeQueryHandler : IRequestHandler<GetTotalDriveSizeQuery, Response<long>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetTotalDriveSizeQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<long>> Handle(GetTotalDriveSizeQuery request,
                CancellationToken cancellationToken)
            {
                return new(await _queryExecutorAsync.FetchOne<long>(DriveQueries.GetSize, new
                {
                    request.CompanyId
                }));
            }
        }
    }
}