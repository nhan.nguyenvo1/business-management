﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Contacts.Commands.CreateContact
{
    public class CreateContactCommand : IRequest<Response<int>>
    {
        public string Address { get; set; }

        public string Email { get; set; }

        public string MoreDescription { get; set; }

        public string PhoneNumber { get; set; }

        public class CreateContactCommandHandler : IRequestHandler<CreateContactCommand, Response<int>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public CreateContactCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<int>> Handle(CreateContactCommand request, CancellationToken cancellationToken)
            {
                return new Response<int>(await _queryExecutorAsync.FetchOne<int>(ContactsQueries.Create, request));
            }
        }
    }
}
