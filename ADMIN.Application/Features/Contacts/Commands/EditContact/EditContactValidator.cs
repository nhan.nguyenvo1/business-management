﻿using FluentValidation;
using System.Text.RegularExpressions;

namespace ADMIN.Application.Features.Contacts.Commands.EditContact
{
    public class EditContactValidator : AbstractValidator<EditContactCommand>
    {
        public EditContactValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty();

            RuleFor(p => p.Email)
                .EmailAddress()
                .NotEmpty();

            RuleFor(p => p.Address)
                .MaximumLength(200)
                .NotEmpty();

            RuleFor(p => p.MoreDescription)
                .MaximumLength(600)
                .NotNull();

            RuleFor(p => p.PhoneNumber)
                .Length(9, 11)
                .Must(p => Regex.IsMatch(p, @"\d"))
                .NotEmpty();
        }
    }
}
