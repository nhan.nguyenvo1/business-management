﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Contacts.Commands.EditContact
{
    public class EditContactCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string MoreDescription { get; set; }

        public string PhoneNumber { get; set; }

        public class EditContactCommandHandler : IRequestHandler<EditContactCommand, Response<int>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public EditContactCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<int>> Handle(EditContactCommand request, CancellationToken cancellationToken)
            {
                return new Response<int>(await _queryExecutorAsync.FetchOne<int>(ContactsQueries.Edit, request));
            }
        }
    }
}
