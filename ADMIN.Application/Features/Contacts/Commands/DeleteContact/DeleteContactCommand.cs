﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Contacts.Commands.DeleteContact
{
    public class DeleteContactCommand : IRequest<Response<bool>>
    {
        public int Id { get; set; }

        public class DeleteContactCommandHandler : IRequestHandler<DeleteContactCommand, Response<bool>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public DeleteContactCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<bool>> Handle(DeleteContactCommand request, CancellationToken cancellationToken)
            {
                await _queryExecutorAsync.Execute(ContactsQueries.Delete, request);
                return new Response<bool>(true);
            }
        }
    }
}
