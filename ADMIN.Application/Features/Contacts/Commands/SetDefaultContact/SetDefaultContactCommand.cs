﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Contacts.Commands.SetDefaultContact
{
    public class SetDefaultContactCommand : IRequest<Response<bool>>
    {
        public int Id { get; set; }

        public class SetDefaultContactCommandHandler : IRequestHandler<SetDefaultContactCommand, Response<bool>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public SetDefaultContactCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<bool>> Handle(SetDefaultContactCommand request, CancellationToken cancellationToken)
            {
                await _queryExecutorAsync.Execute(ContactsQueries.SetDefault, request);
                return new Response<bool>(true);
            }
        }
    }
}
