﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Contacts.Queries.GetContactById
{
    public class GetContactByIdQuery : IRequest<Response<Contact>>
    {
        public int Id { get; set; }

        public class GetContactByIdQueryHandler : IRequestHandler<GetContactByIdQuery, Response<Contact>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetContactByIdQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<Contact>> Handle(GetContactByIdQuery request, CancellationToken cancellationToken)
            {
                var contact = await _queryExecutorAsync.FetchOne<Contact>(ContactsQueries.GetById, request);

                if (contact == null)
                {
                    throw new KeyNotFoundException();
                }

                return new Response<Contact>(contact);
            }
        }
    }
}
