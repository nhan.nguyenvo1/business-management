﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Contacts.Queries.GetContacts
{
    public class GetContactsQuery : IRequest<Response<IReadOnlyCollection<Contact>>>
    {
        public class GetContactsQueryHandler : IRequestHandler<GetContactsQuery, Response<IReadOnlyCollection<Contact>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetContactsQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<Contact>>> Handle(GetContactsQuery request, CancellationToken cancellationToken)
            {
                return new Response<IReadOnlyCollection<Contact>>(await _queryExecutorAsync.Fetch<Contact>(ContactsQueries.Get, null));
            }
        }
    }
}
