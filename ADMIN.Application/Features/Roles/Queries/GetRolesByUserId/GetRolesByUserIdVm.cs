﻿namespace ADMIN.Application.Features.Roles.Queries.GetRolesByUserId
{
    public class GetRolesByUserIdVm
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool Checked { get; set; }
    }
}
