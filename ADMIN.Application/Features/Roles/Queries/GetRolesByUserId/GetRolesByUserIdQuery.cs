﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Roles.Queries.GetRolesByUserId
{
    public class GetRolesByUserIdQuery : IRequest<Response<IReadOnlyCollection<GetRolesByUserIdVm>>>
    {
        public string UserId { get; set; }

        public int CompanyId { get; set; }

        public class GetRolesByUserIdQueryHandler : IRequestHandler<GetRolesByUserIdQuery, Response<IReadOnlyCollection<GetRolesByUserIdVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetRolesByUserIdQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetRolesByUserIdVm>>> Handle(GetRolesByUserIdQuery request, CancellationToken cancellationToken)
            {
                return 
                    new Response<IReadOnlyCollection<GetRolesByUserIdVm>>
                    (await _queryExecutorAsync.Fetch<GetRolesByUserIdVm>(RolesQueries.GetByUserId, new { request.UserId, request.CompanyId }));
            }
        }
    }
}
