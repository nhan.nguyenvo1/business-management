﻿namespace ADMIN.Application.Features.Roles.Queries.GetRolesByCompanyId
{
    public class GetRolesByCompanyIdVm
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
