﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Roles.Queries.GetRolesByCompanyId
{
    public class GetRolesByCompanyIdQuery : IRequest<Response<IReadOnlyCollection<GetRolesByCompanyIdVm>>>
    {
        public int CompanyId { get; set; }

        public class GetRolesByCompanyIdQueryHandler : IRequestHandler<GetRolesByCompanyIdQuery, Response<IReadOnlyCollection<GetRolesByCompanyIdVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetRolesByCompanyIdQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetRolesByCompanyIdVm>>> Handle(GetRolesByCompanyIdQuery request, CancellationToken cancellationToken)
            {
                return 
                    new Response<IReadOnlyCollection<GetRolesByCompanyIdVm>>
                    (await _queryExecutorAsync.Fetch<GetRolesByCompanyIdVm>(RolesQueries.GetByCompanyId, new { request.CompanyId }));
            }
        }
    }
}
