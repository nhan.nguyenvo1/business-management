﻿namespace ADMIN.Application.Features.Roles.Commands.UpdateRolePermissions
{
    public class UpdatePermissionRequest
    {
        public string Permission { get; set; }

        public bool Checked { get; set; }
    }
}
