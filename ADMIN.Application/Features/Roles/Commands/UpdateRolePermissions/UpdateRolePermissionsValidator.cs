﻿using FluentValidation;

namespace ADMIN.Application.Features.Roles.Commands.UpdateRolePermissions
{
    public class UpdateRolePermissionsValidator : AbstractValidator<UpdateRolePermissionsCommand>
    {
        public UpdateRolePermissionsValidator()
        {
            RuleFor(p => p.RoleId)
                .NotEmpty();

            RuleForEach(p => p.Permissions)
                .SetValidator(new UpdatePermissionRequestValidator());
        }
    }
}
