﻿using FluentValidation;

namespace ADMIN.Application.Features.Roles.Commands.UpdateRolePermissions
{
    public class UpdatePermissionRequestValidator : AbstractValidator<UpdatePermissionRequest>
    {
        public UpdatePermissionRequestValidator()
        {
            RuleFor(p => p.Permission)
                .NotEmpty();
        }
    }
}
