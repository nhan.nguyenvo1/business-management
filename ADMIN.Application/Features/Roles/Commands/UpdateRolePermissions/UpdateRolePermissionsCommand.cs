﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Roles.Commands.UpdateRolePermissions
{
    public class UpdateRolePermissionsCommand : IRequest<Response<string>>
    {
        public string RoleId { get; set; }

        public List<UpdatePermissionRequest> Permissions { get; set; }

        public class UpdateRolePermissionsCommandHandler : IRequestHandler<UpdateRolePermissionsCommand, Response<string>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public UpdateRolePermissionsCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<string>> Handle(UpdateRolePermissionsCommand request, CancellationToken cancellationToken)
            {
                var response = await _queryExecutorAsync.ExecuteInTransactionAsync(async trans =>
                {
                    foreach (var permission in request.Permissions)
                    {
                        if (permission.Checked)
                        {
                            await _queryExecutorAsync.Execute(RolesQueries.AddPermission, new { request.RoleId, ClaimType = "permission", ClaimValue = permission.Permission }, trans);
                        }
                        else
                        {
                            await _queryExecutorAsync.Execute(RolesQueries.RemovePermission, new { request.RoleId, ClaimType = "permission", ClaimValue = permission.Permission }, trans);
                        }
                    }
                    return request.RoleId;
                });

                return new Response<string>(response);
            }
        }
    }
}
