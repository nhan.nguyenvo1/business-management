﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Statistics.Queries.GetTotalUsers
{
    public class GetTotalUsersQuery : IRequest<Response<long>>
    {
        public class GetTotalUsersQueryHandler : IRequestHandler<GetTotalUsersQuery, Response<long>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetTotalUsersQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<long>> Handle(GetTotalUsersQuery request, CancellationToken cancellationToken)
            {
                return new Response<long>(await _queryExecutorAsync.FetchOne<long>(StatisticsQueries.GetTotalUser, null));
            }
        }
    }
}
