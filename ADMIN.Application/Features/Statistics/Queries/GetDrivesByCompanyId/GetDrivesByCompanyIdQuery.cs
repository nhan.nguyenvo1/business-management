﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Statistics.Queries.GetDrivesByCompanyId
{
    public class GetDrivesByCompanyIdQuery : IRequest<Response<IReadOnlyCollection<GetDrivesByCompanyIdVm>>>
    {
        public int Top { get; set; }

        public class GetDrivesByCompanyIdQueryHandler : IRequestHandler<GetDrivesByCompanyIdQuery, Response<IReadOnlyCollection<GetDrivesByCompanyIdVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetDrivesByCompanyIdQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetDrivesByCompanyIdVm>>> Handle(GetDrivesByCompanyIdQuery request, CancellationToken cancellationToken)
            {
                return new Response<IReadOnlyCollection<GetDrivesByCompanyIdVm>>(
                    await _queryExecutorAsync.Fetch<GetDrivesByCompanyIdVm>(StatisticsQueries.GetDrivesBelongToCompany, new { request.Top })
                );
            }
        }
    }
}
