﻿namespace ADMIN.Application.Features.Statistics.Queries.GetDrivesByCompanyId
{
    public class GetDrivesByCompanyIdVm
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }

        public double TotalSize { get; set; }
    }
}
