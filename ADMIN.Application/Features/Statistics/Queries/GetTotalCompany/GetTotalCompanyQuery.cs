﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Statistics.Queries.GetTotalCompany
{
    public class GetTotalCompanyQuery : IRequest<Response<long>>
    {
        public class GetTotalCompanyQueryHandler : IRequestHandler<GetTotalCompanyQuery, Response<long>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetTotalCompanyQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<long>> Handle(GetTotalCompanyQuery request, CancellationToken cancellationToken)
            {
                return new Response<long>(await _queryExecutorAsync.FetchOne<long>(StatisticsQueries.GetTotalCompany, null));
            }
        }
    }
}
