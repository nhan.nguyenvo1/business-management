﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Statistics.Queries.GetActivityUsers
{
    public class GetActivityUsersQuery : IRequest<Response<IReadOnlyCollection<GetActivityUsersVm>>>
    {
        public class GetActivityUsersQueryHandler : IRequestHandler<GetActivityUsersQuery, Response<IReadOnlyCollection<GetActivityUsersVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetActivityUsersQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetActivityUsersVm>>> Handle(GetActivityUsersQuery request, CancellationToken cancellationToken)
            {
                return new Response<IReadOnlyCollection<GetActivityUsersVm>>(await _queryExecutorAsync.Fetch<GetActivityUsersVm>(StatisticsQueries.GetActivityUser, null));
            }
        }
    }
}
