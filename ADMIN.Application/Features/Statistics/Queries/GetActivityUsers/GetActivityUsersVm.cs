﻿using System;

namespace ADMIN.Application.Features.Statistics.Queries.GetActivityUsers
{
    public class GetActivityUsersVm
    {
        public DateTime Date { get; set; }

        public long TotalAccess { get; set; }
    }
}
