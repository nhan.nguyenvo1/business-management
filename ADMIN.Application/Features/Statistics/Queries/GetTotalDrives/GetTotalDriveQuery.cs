﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Statistics.Queries.GetTotalDrives
{
    public class GetTotalDriveQuery : IRequest<Response<double>>
    {
        public class GetTotalDriveQueryHandler : IRequestHandler<GetTotalDriveQuery, Response<double>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetTotalDriveQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<double>> Handle(GetTotalDriveQuery request, CancellationToken cancellationToken)
            {
                return new Response<double>(await _queryExecutorAsync.FetchOne<double>(StatisticsQueries.GetTotalDrives, null));
            }
        }
    }
}
