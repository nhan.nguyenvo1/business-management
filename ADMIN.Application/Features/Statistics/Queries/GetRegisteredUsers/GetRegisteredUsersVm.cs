﻿using System;

namespace ADMIN.Application.Features.Statistics.Queries.GetRegisteredUsers
{
    public class GetRegisteredUsersVm
    {
        public DateTime Date { get; set; }

        public long TotalRegisteredUser { get; set; }
    }
}
