﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Statistics.Queries.GetRegisteredUsers
{
    public class GetRegisteredUsersQuery : IRequest<Response<IReadOnlyCollection<GetRegisteredUsersVm>>>
    {
        public class GetRegisteredUsersQueryHandler : IRequestHandler<GetRegisteredUsersQuery, Response<IReadOnlyCollection<GetRegisteredUsersVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;
            private readonly IDateTimeService _dateTimeService;

            public GetRegisteredUsersQueryHandler(IQueryExecutorAsync queryExecutorAsync, IDateTimeService dateTimeService)
            {
                _queryExecutorAsync = queryExecutorAsync;
                _dateTimeService = dateTimeService;
            }

            public async Task<Response<IReadOnlyCollection<GetRegisteredUsersVm>>> Handle(GetRegisteredUsersQuery request, CancellationToken cancellationToken)
            {
                return new Response<IReadOnlyCollection<GetRegisteredUsersVm>>(await _queryExecutorAsync.Fetch<GetRegisteredUsersVm>(StatisticsQueries.GetRegisteredUser, new { Date = _dateTimeService.UtcNow }));
            }
        }
    }
}
