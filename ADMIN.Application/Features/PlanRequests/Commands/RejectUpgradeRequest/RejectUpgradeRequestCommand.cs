﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Dtos.Email;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Enums;

namespace ADMIN.Application.Features.PlanRequests.Commands.RejectUpgradeRequest
{
    public class RejectUpgradeRequestCommand : IRequest<Response<bool>>
    {
        public int RequestPackageId { get; set; }

        public string Reason { get; set; }

        public class RejectUpgradeRequestCommandHandler : IRequestHandler<RejectUpgradeRequestCommand, Response<bool>>
        {
            private readonly IEmailService _emailService;
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public RejectUpgradeRequestCommandHandler(IQueryExecutorAsync queryExecutorAsync,
                IEmailService emailService)
            {
                _queryExecutorAsync = queryExecutorAsync;
                _emailService = emailService;
            }

            public async Task<Response<bool>> Handle(RejectUpgradeRequestCommand request,
                CancellationToken cancellationToken)
            {
                await _queryExecutorAsync.ExecuteInTransactionAsync(async transaction =>
                {
                    var userId = await _queryExecutorAsync.FetchOne<string>(PackageRequestQueries.Reject, new
                    {
                        Status = RequestStatus.Rejected,
                        request.Reason,
                        request.RequestPackageId
                    });

                    var email = await _queryExecutorAsync.FetchOne<string>(UserQueries.GetEmailById, new
                    {
                        UserId = userId
                    });

                    await _emailService.SendAsync(new EmailRequest
                    {
                        Subject = "Reject Upgrade Plan Request",
                        Body = $"Reason: {request.Reason}",
                        To = email
                    });
                });

                return new Response<bool>(true);
            }
        }
    }
}