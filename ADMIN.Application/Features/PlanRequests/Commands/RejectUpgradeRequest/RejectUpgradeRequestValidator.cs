﻿using FluentValidation;

namespace ADMIN.Application.Features.PlanRequests.Commands.RejectUpgradeRequest
{
    public class RejectUpgradeRequestValidator : AbstractValidator<RejectUpgradeRequestCommand>
    {
        public RejectUpgradeRequestValidator()
        {
            RuleFor(p => p.RequestPackageId)
                .NotNull()
                .WithMessage("Ambiguous Upgrade plan Request");

            RuleFor(p => p.Reason)
                .NotEmpty()
                .WithMessage("Reason cannot be empty")
                .NotNull()
                .WithMessage("Reason cannot be empty");
        }
    }
}