﻿using FluentValidation;

namespace ADMIN.Application.Features.PlanRequests.Commands.SetAsPaymented
{
    public class SetAsPaymentedValidator : AbstractValidator<SetAsPaymentedCommand>
    {
        public SetAsPaymentedValidator()
        {
            RuleFor(p => p.RequestPackageId)
                .NotNull()
                .WithMessage("Ambiguous Upgrade plan Request");
        }
    }
}