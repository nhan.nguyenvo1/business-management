﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Dtos.Email;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Enums;

namespace ADMIN.Application.Features.PlanRequests.Commands.SetAsPaymented
{
    public class SetAsPaymentedCommand : IRequest<Response<bool>>
    {
        public int RequestPackageId { get; set; }

        public class SetAsPaymentedCommandHandler : IRequestHandler<SetAsPaymentedCommand, Response<bool>>
        {
            private readonly IEmailService _emailService;
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public SetAsPaymentedCommandHandler(IQueryExecutorAsync queryExecutorAsync, IEmailService emailService)
            {
                _queryExecutorAsync = queryExecutorAsync;
                _emailService = emailService;
            }

            public async Task<Response<bool>> Handle(SetAsPaymentedCommand request, CancellationToken cancellationToken)
            {
                await _queryExecutorAsync.ExecuteInTransactionAsync(async transaction =>
                {
                    var userId = await _queryExecutorAsync.FetchOne<string>(PackageRequestQueries.SetAsPaymented, new
                    {
                        request.RequestPackageId,
                        Status = RequestStatus.ReadyToApprove
                    });

                    var email = await _queryExecutorAsync.FetchOne<string>(UserQueries.GetEmailById, new
                    {
                        UserId = userId
                    });

                    await _emailService.SendAsync(new EmailRequest
                    {
                        Subject = "Paymented",
                        Body = "Your request has been update status to paymented",
                        To = email
                    });
                });

                return new Response<bool>(true);
            }
        }
    }
}