﻿using FluentValidation;

namespace ADMIN.Application.Features.PlanRequests.Commands.ApproveUpgradeRequest
{
    public class ApproveUpgradeRequestValidator : AbstractValidator<ApproveUpgradeRequestCommand>
    {
        public ApproveUpgradeRequestValidator()
        {
            RuleFor(p => p.RequestPackageId)
                .NotNull()
                .WithMessage("Ambiguous Upgrade Plan Request");
        }
    }
}