﻿namespace ADMIN.Application.Features.PlanRequests.Commands.ApproveUpgradeRequest
{
    public class PackageRequestVm
    {
        public string UserId { get; set; }

        public string PackageId { get; set; }
    }
}