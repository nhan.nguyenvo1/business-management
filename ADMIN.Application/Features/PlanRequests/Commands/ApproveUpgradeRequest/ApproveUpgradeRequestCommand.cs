﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Dtos.Email;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Enums;

namespace ADMIN.Application.Features.PlanRequests.Commands.ApproveUpgradeRequest
{
    public class ApproveUpgradeRequestCommand : IRequest<Response<bool>>
    {
        public int RequestPackageId { get; set; }

        public class ApproveUpgradeRequestCommandHandler : IRequestHandler<ApproveUpgradeRequestCommand, Response<bool>>
        {
            private readonly IDateTimeService _dateTimeService;
            private readonly IEmailService _emailService;
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public ApproveUpgradeRequestCommandHandler(IQueryExecutorAsync queryExecutorAsync,
                IDateTimeService dateTimeService, IEmailService emailService)
            {
                _queryExecutorAsync = queryExecutorAsync;
                _dateTimeService = dateTimeService;
                _emailService = emailService;
            }

            public async Task<Response<bool>> Handle(ApproveUpgradeRequestCommand request,
                CancellationToken cancellationToken)
            {
                await _queryExecutorAsync.ExecuteInTransactionAsync(async transaction =>
                {
                    var packageRequest = await _queryExecutorAsync.FetchOne<PackageRequestVm>(
                        PackageRequestQueries.Approve, new
                        {
                            Status = RequestStatus.Closed,
                            ClosedDate = _dateTimeService.UtcNow,
                            request.RequestPackageId
                        });

                    if (packageRequest == null) throw new KeyNotFoundException("Request does not exists");

                    await _queryExecutorAsync.Execute(UserQueries.UpgradePlan, new
                    {
                        packageRequest.UserId,
                        packageRequest.PackageId
                    });

                    var email = await _queryExecutorAsync.FetchOne<string>(UserQueries.GetEmailById, new
                    {
                        packageRequest.UserId
                    });

                    await _emailService.SendAsync(new EmailRequest
                    {
                        Subject = "Reject Upgrade Plan Request",
                        Body = "Thanks for trust us",
                        To = email
                    });
                });

                return new Response<bool>(true);
            }
        }
    }
}