﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.PlanRequests.Queries.GetUpgradePlanRequests
{
    public class GetUpgradePlanRequestsQuery : IRequest<Response<IReadOnlyCollection<GetUpgradePlanRequestsVm>>>
    {
        public class GetUpgradePlanRequestsQueryHandler : IRequestHandler<GetUpgradePlanRequestsQuery,
            Response<IReadOnlyCollection<GetUpgradePlanRequestsVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetUpgradePlanRequestsQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetUpgradePlanRequestsVm>>> Handle(
                GetUpgradePlanRequestsQuery request, CancellationToken cancellationToken)
            {
                var command = new SimpleDbCommand(PackageRequestQueries.Get, null);

                return new Response<IReadOnlyCollection<GetUpgradePlanRequestsVm>>(
                    await _queryExecutorAsync.Fetch<GetUpgradePlanRequestsVm>(command));
            }
        }
    }
}