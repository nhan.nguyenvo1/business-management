﻿using System;

namespace ADMIN.Application.Features.PlanRequests.Queries.GetUpgradePlanRequests
{
    public class GetUpgradePlanRequestsVm
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public int PackageId { get; set; }

        public string PackageName { get; set; }

        public string Status { get; set; }

        public DateTime? ClosedDate { get; set; }

        public int RequestPackageId { get; set; }
    }
}