﻿using FluentValidation;

namespace ADMIN.Application.Features.PaymentInfos.Commands.CreatePaymentInfo
{
    public class CreatePaymentInfoValidator : AbstractValidator<CreatePaymentInfoCommand>
    {
        public CreatePaymentInfoValidator()
        {
            RuleFor(p => p.CardBank)
                .NotEmpty();

            RuleFor(p => p.CardHolder)
                .NotEmpty();

            RuleFor(p => p.CardNumber)
                .NotEmpty();

            RuleFor(p => p.Type)
                .NotEmpty();
        }
    }
}
