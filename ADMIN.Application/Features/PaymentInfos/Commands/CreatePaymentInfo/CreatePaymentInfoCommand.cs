﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Enums;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.PaymentInfos.Commands.CreatePaymentInfo
{
    public class CreatePaymentInfoCommand : IRequest<Response<int>>
    {
        public string CardNumber { get; set; }

        public string CardBank { get; set; }

        public string CardHolder { get; set; }

        public PaymentType Type { get; set; }

        public class CreatePaymentInfoCommandHandler : IRequestHandler<CreatePaymentInfoCommand, Response<int>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public CreatePaymentInfoCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<int>> Handle(CreatePaymentInfoCommand request, CancellationToken cancellationToken)
            {
                return new Response<int>(await _queryExecutorAsync.FetchOne<int>(PaymentInfosQueries.Create, request));
            }
        }
    }
}
