﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.PaymentInfos.Commands.DeletePaymentInfo
{
    public class DeletePaymentInfoCommand : IRequest<Response<bool>>
    {
        public int Id { get; set; }

        public class DeletePaymentInfoCommandHandler : IRequestHandler<DeletePaymentInfoCommand, Response<bool>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public DeletePaymentInfoCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<bool>> Handle(DeletePaymentInfoCommand request, CancellationToken cancellationToken)
            {
                await _queryExecutorAsync.Execute(PaymentInfosQueries.Delete, request);

                return new Response<bool>(true);
            }
        }
    }
}
