﻿using FluentValidation;

namespace ADMIN.Application.Features.PaymentInfos.Commands.EditPaymentInfo
{
    public class EditPaymentInfoValidator : AbstractValidator<EditPaymentInfoCommand>
    {
        public EditPaymentInfoValidator()
        {
            RuleFor(p => p.Id)
                .NotEmpty();

            RuleFor(p => p.CardBank)
                .NotEmpty();

            RuleFor(p => p.CardHolder)
                .NotEmpty();

            RuleFor(p => p.CardNumber)
                .NotEmpty();

            RuleFor(p => p.Type)
                .NotEmpty();
        }
    }
}
