﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.PaymentInfos.Commands.SetPaymentInfoDefault
{
    public class SetPaymentInfoDefaultCommand : IRequest<Response<int>>
    {
        public int Id { get; set; }

        public class SetPaymentInfoDefaultCommandHandler : IRequestHandler<SetPaymentInfoDefaultCommand, Response<int>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public SetPaymentInfoDefaultCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<int>> Handle(SetPaymentInfoDefaultCommand request, CancellationToken cancellationToken)
            {
                return new Response<int>(await _queryExecutorAsync.FetchOne<int>(PaymentInfosQueries.SetDefault, request));
            }
        }
    }
}
