﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.PaymentInfos.Queries.GetPaymentInfoById
{
    public class GetPaymentInfoByIdQuery : IRequest<Response<PaymentInfo>>
    {
        public int Id { get; set; }

        public class GetPaymentInfoByIdQueryHandler : IRequestHandler<GetPaymentInfoByIdQuery, Response<PaymentInfo>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetPaymentInfoByIdQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<PaymentInfo>> Handle(GetPaymentInfoByIdQuery request, CancellationToken cancellationToken)
            {
                var paymentInfo = await _queryExecutorAsync.FetchOne<PaymentInfo>(PaymentInfosQueries.GetById, request);

                if (paymentInfo == null)
                {
                    throw new KeyNotFoundException();
                }

                return new Response<PaymentInfo>(paymentInfo);
            }
        }
    }
}
