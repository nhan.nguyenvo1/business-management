﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.PaymentInfos.Queries.GetPaymentInfos
{
    public class GetPaymentInfosQuery : IRequest<Response<IReadOnlyCollection<PaymentInfo>>>
    {
        public class GetPaymentInfosQueryHandler : IRequestHandler<GetPaymentInfosQuery, Response<IReadOnlyCollection<PaymentInfo>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetPaymentInfosQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<PaymentInfo>>> Handle(GetPaymentInfosQuery request, CancellationToken cancellationToken)
            {
                return new Response<IReadOnlyCollection<PaymentInfo>>(await _queryExecutorAsync.Fetch<PaymentInfo>(PaymentInfosQueries.Get, null));
            }
        }
    }
}
