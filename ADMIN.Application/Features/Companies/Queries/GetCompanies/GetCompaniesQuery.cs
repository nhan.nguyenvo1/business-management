﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.Companies.Queries.GetCompanies
{
    public class GetCompaniesQuery : IRequest<Response<IReadOnlyCollection<GetCompaniesVm>>>
    {
        public class
            GetCompaniesQueryHandler : IRequestHandler<GetCompaniesQuery, Response<IReadOnlyCollection<GetCompaniesVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetCompaniesQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetCompaniesVm>>> Handle(GetCompaniesQuery request,
                CancellationToken cancellationToken)
            {
                var command = new SimpleDbCommand(CompanyQueries.SelectCompanies, null);
                return new Response<IReadOnlyCollection<GetCompaniesVm>>(
                    await _queryExecutorAsync.Fetch<GetCompaniesVm>(command));
            }
        }
    }
}