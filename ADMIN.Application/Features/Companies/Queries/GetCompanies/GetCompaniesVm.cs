﻿using System;

namespace ADMIN.Application.Features.Companies.Queries.GetCompanies
{
    public class GetCompaniesVm
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public long NumOfEmployees { get; set; }

        public long NumOfDepartments { get; set; }

        public long TotalTasks { get; set; }
    }
}