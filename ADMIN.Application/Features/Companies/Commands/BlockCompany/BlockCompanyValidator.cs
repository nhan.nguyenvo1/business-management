﻿using FluentValidation;

namespace ADMIN.Application.Features.Companies.Commands.BlockCompany
{
    public class BlockCompanyValidator : AbstractValidator<BlockCompanyCommand>
    {
        public BlockCompanyValidator()
        {
            RuleFor(p => p.CompanyId)
                .NotEmpty()
                .WithMessage("Ambiguous Company");

            RuleFor(p => p.Reason)
                .NotEmpty()
                .WithMessage("Reason cannot be empty");
        }
    }
}