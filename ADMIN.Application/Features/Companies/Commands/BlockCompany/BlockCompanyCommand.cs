﻿using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Dtos.Email;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Enums;

namespace ADMIN.Application.Features.Companies.Commands.BlockCompany
{
    public class BlockCompanyCommand : IRequest<Response<bool>>
    {
        public int CompanyId { get; set; }

        public string Reason { get; set; }

        public class BlockCompanyCommandHandler : IRequestHandler<BlockCompanyCommand, Response<bool>>
        {
            private readonly IEmailService _emailService;
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public BlockCompanyCommandHandler(IQueryExecutorAsync queryExecutorAsync, IEmailService emailService)
            {
                _queryExecutorAsync = queryExecutorAsync;
                _emailService = emailService;
            }

            public async Task<Response<bool>> Handle(BlockCompanyCommand request, CancellationToken cancellationToken)
            {
                var command = new SimpleDbCommand(CompanyQueries.BlockCompany, new
                {
                    request.CompanyId,
                    request.Reason,
                    Status = CompanyStatus.Deactivated
                });

                await _queryExecutorAsync.ExecuteInTransactionAsync(async transaction =>
                {
                    var userId = await _queryExecutorAsync.FetchOne<string>(command, transaction);

                    var email = await _queryExecutorAsync.FetchOne<string>(UserQueries.GetEmailById, new
                    {
                        UserId = userId
                    }, transaction);

                    await _emailService.SendAsync(new EmailRequest
                    {
                        To = email,
                        Subject = "Block Your Company",
                        Body = $"Your company has been blocked because {request.Reason}"
                    });
                });

                return new Response<bool>(true);
            }
        }
    }
}