﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Dtos.Email;
using ADMIN.Application.Interfaces;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using PROJECT.Domain.Enums;

namespace ADMIN.Application.Features.Companies.Commands.UnblockCompany
{
    public class UnblockCompanyCommand : IRequest<Response<bool>>
    {
        public int CompanyId { get; set; }

        public class UnblockCompanyCommandHandler : IRequestHandler<UnblockCompanyCommand, Response<bool>>
        {
            private readonly IEmailService _emailService;
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public UnblockCompanyCommandHandler(IQueryExecutorAsync queryExecutorAsync, IEmailService emailService)
            {
                _queryExecutorAsync = queryExecutorAsync;
                _emailService = emailService;
            }

            public async Task<Response<bool>> Handle(UnblockCompanyCommand request, CancellationToken cancellationToken)
            {
                var command = new SimpleDbCommand(CompanyQueries.UnblockCompany, new
                {
                    request.CompanyId,
                    Status = CompanyStatus.Activated
                });

                await _queryExecutorAsync.ExecuteInTransactionAsync(async transaction =>
                {
                    var userId = await _queryExecutorAsync.FetchOne<string>(command);

                    var email = await _queryExecutorAsync.FetchOne<string>(UserQueries.GetEmailById, new
                    {
                        UserId = userId
                    });

                    try
                    {
                        await _emailService.SendAsync(new EmailRequest
                        {
                            To = email,
                            Body = "Your company has been unblock. Enjoy your life.",
                            Subject = "Unblock Your Company"
                        });
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                });

                return new Response<bool>(true);
            }
        }
    }
}