﻿using FluentValidation;

namespace ADMIN.Application.Features.Companies.Commands.UnblockCompany
{
    public class UnblockCompanyValidator : AbstractValidator<UnblockCompanyCommand>
    {
        public UnblockCompanyValidator()
        {
            RuleFor(p => p.CompanyId)
                .NotNull();
        }
    }
}