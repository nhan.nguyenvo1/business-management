﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.Users.Queries.GetUsersByCompanyId
{
    public class GetUsersByCompanyIdQuery : IRequest<Response<IReadOnlyCollection<GetUsersByCompanyIdVm>>>
    {
        public int CompanyId { get; set; }

        public class GetUsersByCompanyIdQueryHandler : IRequestHandler<GetUsersByCompanyIdQuery,
            Response<IReadOnlyCollection<GetUsersByCompanyIdVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetUsersByCompanyIdQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetUsersByCompanyIdVm>>> Handle(
                GetUsersByCompanyIdQuery request, CancellationToken cancellationToken)
            {
                if (!(await _queryExecutorAsync.FetchOne<int>(CompanyQueries.Exists, new {request.CompanyId}) > 0))
                    throw new KeyNotFoundException("Company does not exists");

                var command = new SimpleDbCommand(UserQueries.GetUsersByCompanyId, new
                {
                    request.CompanyId
                });

                return new Response<IReadOnlyCollection<GetUsersByCompanyIdVm>>(
                    await _queryExecutorAsync.Fetch<GetUsersByCompanyIdVm>(command));
            }
        }
    }
}