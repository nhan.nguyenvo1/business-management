﻿using System;

namespace ADMIN.Application.Features.Users.Queries.GetUsersByCompanyId
{
    public class GetUsersByCompanyIdVm
    {
        public string Id { get; set; }

        public string Avatar { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string UserName { get; set; }

        public string Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public DateTime CreatedDate { get; set; }

        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        public string SupervisorId { get; set; }

        public string SupervisorName { get; set; }
    }
}