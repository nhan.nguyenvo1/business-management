﻿using System;

namespace ADMIN.Application.Features.Users.Queries.GetUsers
{
    public class GetUsersVm
    {
        public string Id { get; set; }

        public string Avatar { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string UserName { get; set; }

        public string Gender { get; set; }

        public DateTime DateOfBirth { get; set; }

        public DateTime CreatedDate { get; set; }

        public int PackageId { get; set; }

        public string PackageName { get; set; }
    }
}