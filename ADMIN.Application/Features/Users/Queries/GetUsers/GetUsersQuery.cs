﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;

namespace ADMIN.Application.Features.Users.Queries.GetUsers
{
    public class GetUsersQuery : IRequest<Response<IReadOnlyCollection<GetUsersVm>>>
    {
        public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, Response<IReadOnlyCollection<GetUsersVm>>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public GetUsersQueryHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<IReadOnlyCollection<GetUsersVm>>> Handle(GetUsersQuery request,
                CancellationToken cancellationToken)
            {
                var command = new SimpleDbCommand(UserQueries.GetUsers, null);

                return new Response<IReadOnlyCollection<GetUsersVm>>(
                    await _queryExecutorAsync.Fetch<GetUsersVm>(command));
            }
        }
    }
}