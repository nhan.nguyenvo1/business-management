﻿using FluentValidation;

namespace ADMIN.Application.Features.Users.Commands.UpdateUserRoles
{
    public class UpdateUserRolesRequestValidator : AbstractValidator<UpdateUserRolesRequest>
    {
        public UpdateUserRolesRequestValidator()
        {
            RuleFor(p => p.RoleId)
                .NotEmpty();
        }
    }
}
