﻿using FluentValidation;

namespace ADMIN.Application.Features.Users.Commands.UpdateUserRoles
{
    public class UpdateUserRolesCommandValidator : AbstractValidator<UpdateUserRolesCommand>
    {
        public UpdateUserRolesCommandValidator()
        {
            RuleFor(p => p.CompanyId).NotEmpty();

            RuleFor(p => p.UserId).NotEmpty();

            RuleForEach(p => p.Roles).SetValidator(new UpdateUserRolesRequestValidator());
        }
    }
}
