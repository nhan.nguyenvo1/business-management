﻿using ADMIN.Application.Constants.SqlQueries;
using ADMIN.Application.Wrappers;
using ADMIN.Infrastructure.DataAccess;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ADMIN.Application.Features.Users.Commands.UpdateUserRoles
{
    public class UpdateUserRolesCommand : IRequest<Response<string>>
    {
        public string UserId { get; set; }

        public int CompanyId { get; set; }

        public List<UpdateUserRolesRequest> Roles { get; set; }

        public class UpdateUserRolesCommandHandler : IRequestHandler<UpdateUserRolesCommand, Response<string>>
        {
            private readonly IQueryExecutorAsync _queryExecutorAsync;

            public UpdateUserRolesCommandHandler(IQueryExecutorAsync queryExecutorAsync)
            {
                _queryExecutorAsync = queryExecutorAsync;
            }

            public async Task<Response<string>> Handle(UpdateUserRolesCommand request, CancellationToken cancellationToken)
            {
                var response = await _queryExecutorAsync.ExecuteInTransactionAsync(async trans =>
                {
                    foreach (var role in request.Roles)
                    {
                        if (role.Checked)
                        {
                            await _queryExecutorAsync.Execute(UserQueries.AssignRole, new { request.UserId, role.RoleId }, trans);
                        }
                        else
                        {
                            await _queryExecutorAsync.Execute(UserQueries.CancelRole, new { request.UserId, role.RoleId }, trans);
                        }
                    }
                    return request.UserId;
                });

                return new Response<string>(response);
            }
        }
    }
}
