﻿namespace ADMIN.Application.Features.Users.Commands.UpdateUserRoles
{
    public class UpdateUserRolesRequest
    {
        public string RoleId { get; set; }

        public bool Checked { get; set; }
    }
}
