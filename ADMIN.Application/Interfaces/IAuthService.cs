﻿using System.Threading.Tasks;
using ADMIN.Application.Dtos.Account;
using ADMIN.Application.Wrappers;

namespace ADMIN.Application.Interfaces
{
    public interface IAuthService
    {
        Task<Response<AuthResponse>> SignInAsync(AuthRequest request);

        Task<Response<AuthResponse>> RefreshTokenAsync(RefreshTokenRequest request);

        Task<Response<string>> RevokeTokenAsync(RevokeTokenRequest request);

        Task<Response<string>> ChangePasswordAsync(ChangePasswordRequest request);

        Task<Response<string>> ForgotPasswordAsync(ForgotPasswordRequest request);

        Task<Response<string>> VerifyEmailAsync(VerifyEmailRequest request);

        Task<Response<string>> ResetPasswordAsync(ResetPasswordRequest request);

        Task<Response<AuthVm>> GetMeAsync();
    }
}