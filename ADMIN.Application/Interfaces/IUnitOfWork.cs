﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace ADMIN.Application.Interfaces
{
    public interface IUnitOfWork
    {
        Task CommitAsync(CancellationToken cancellation = default);

        Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellation = default);
    }
}