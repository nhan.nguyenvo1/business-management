﻿using System.Threading.Tasks;
using ADMIN.Application.Dtos.Email;

namespace ADMIN.Application.Interfaces
{
    public interface IEmailService
    {
        Task<bool> SendAsync(EmailRequest request);
    }
}