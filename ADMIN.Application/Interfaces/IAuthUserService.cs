﻿namespace ADMIN.Application.Interfaces
{
    public interface IAuthUserService
    {
        string UserId { get; }
    }
}