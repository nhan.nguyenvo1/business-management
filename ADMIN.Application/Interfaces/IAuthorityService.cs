﻿using ADMIN.Application.Dtos.Authority;
using ADMIN.Application.Wrappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ADMIN.Application.Interfaces
{
    public interface IAuthorityService
    {
        public Task<Response<IReadOnlyCollection<AuthorityVm>>> GetByRoleIdAsync(string roleId, int companyId);
    }
}
