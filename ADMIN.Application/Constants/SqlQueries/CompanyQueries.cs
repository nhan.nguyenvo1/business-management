﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public abstract class CompanyQueries
    {
        public const string SelectCompanies = @"
            SELECT company.Id
				, company.Name
				, CASE
					WHEN company.Status = 1 THEN 'Activating'
					ELSE 'Deactivated'
				  END AS Status
				, u.FirstName + ' ' + u.LastName AS CreatedBy
				, u.CreatedDate
				, u.UpdatedDate
				, COUNT(DISTINCT cu.UserId) AS NumOfEmployees
				, COUNT(DISTINCT d.Id) AS NumOfDepartments
				, COUNT(DISTINCT t.Id) AS TotalTasks
			FROM [dbo].[Company] company
			JOIN [dbo].[User] u ON company.CreatedBy = u.Id
			JOIN [dbo].[CompanyUser] cu ON cu.CompanyId = company.Id
			JOIN [dbo].[Department] d ON d.CompanyId = company.Id
			LEFT JOIN [dbo].[Task] t ON t.CompanyId = company.Id
			GROUP BY company.Id
				, company.Name
				, CASE
					WHEN company.Status = 1 THEN 'Activating'
					ELSE 'Deactivated'
				  END
				, u.FirstName + ' ' + u.LastName
				, u.CreatedDate
				, u.UpdatedDate
		";

        public const string BlockCompany = @"
			UPDATE [dbo].[Company]
			SET	
				Status = @Status
				, ResonForPending = @Reason
			OUTPUT inserted.CreatedBy
			WHERE Id = @CompanyId
		";

        public const string UnblockCompany = @"
			UPDATE [dbo].[Company]
			SET	
				Status = @Status
			OUTPUT inserted.CreatedBy
			WHERE Id = @CompanyId
		";

        public const string Exists = @"
			SELECT COUNT(Id)
			FROM [dbo].[Company]
			WHERE Id = @CompanyId
		";
    }
}