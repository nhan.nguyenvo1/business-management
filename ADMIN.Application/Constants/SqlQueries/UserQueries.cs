﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public abstract class UserQueries
    {
        public const string GetUsers = @"
            SELECT u.Id
                , u.AvatarUrl AS Avatar
                , u.FirstName + ' ' + u.LastName AS FullName
                , u.Email
                , u.PhoneNumber
                , u.UserName
                , CASE
                    WHEN u.Gender = 1 THEN 'Male'
                    WHEN u.Gender = 2 THEN 'Female'
                    ELSE 'Other'
                  END AS Gender
                , u.Dob AS DateOfBirth
                , u.CreatedDate
                , package.Id AS PackageId
                , package.Name AS PackageName
            FROM [dbo].[User] u
            LEFT JOIN [dbo].[Package] package ON u.PackageId = package.Id
        ";

        public const string GetUsersByCompanyId = @"
            SELECT DISTINCT u.Id
                , u.AvatarUrl AS Avatar
                , u.FirstName + ' ' + u.LastName AS FullName
                , u.Email
                , u.PhoneNumber
                , u.UserName
                , CASE
                    WHEN u.Gender = 1 THEN 'Male'
                    WHEN u.Gender = 2 THEN 'Female'
                    ELSE 'Other'
                  END AS Gender
                , u.Dob AS DateOfBirth
                , u.CreatedDate
				, department.Id AS DepartmentId
				, department.Name AS DepartmentName
				, supervisor.Id AS SupervisorId
				, supervisor.FirstName + ' ' + supervisor.LastName AS SupervisorName
            FROM [dbo].[User] u
            JOIN [dbo].[CompanyUser] companyUser ON u.Id = companyUser.UserId AND companyUser.CompanyId = @CompanyId
			JOIN [dbo].[DepartmentUsers] departmentUsers ON departmentUsers.UsersId = u.Id
			JOIN [dbo].[Department] department ON department.Id = departmentUsers.DepartmentsId AND department.CompanyId = @CompanyId
			LEFT JOIN [dbo].[User] supervisor ON supervisor.Id = department.SupervisorId
        ";

        public const string UpgradePlan = @"
            UPDATE [dbo].[User]
            SET
	            PackageId = @PackageId
            WHERE Id = @UserId
        ";

        public const string GetEmailById = @"
            SELECT Email
            FROM [dbo].[user]
            WHERE Id = @UserId
        ";

        public const string AssignRole = @"
            IF NOT EXISTS (SELECT 1 FROM UserRole WHERE RoleId = @RoleId AND UserId = @UserId)
            BEGIN
                INSERT INTO UserRole (RoleId, UserId)
                VALUES (@RoleId, @UserId)
            END;
        ";

        public const string CancelRole = @"
            IF EXISTS (SELECT 1 FROM UserRole WHERE RoleId = @RoleId AND UserId = @UserId)
            BEGIN
                DELETE UserRole
                WHERE RoleId = @RoleId AND UserId = @UserId
            END;
        ";
    }
}