﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public class ContactsQueries
    {
        public const string Get =
            @"
                SELECT Address
                    , Email
                    , MoreDescription
                    , PhoneNumber
                    , IsDefault
                    , Id
                FROM Contact
                ORDER BY IsDefault DESC, Id ASC
            ";

        public const string GetById =
            @"
                SELECT Address
                    , Email
                    , MoreDescription
                    , PhoneNumber
                    , IsDefault
                    , Id
                FROM Contact
                WHERE Id = @Id
            ";

        public const string Create =
            @"
                INSERT INTO Contact (Address, Email, MoreDescription, PhoneNumber)
                OUTPUT inserted.Id
                VALUES (@Address, @Email, @MoreDescription, @PhoneNumber);
            ";

        public const string Delete =
            @"
                DELETE FROM Contact WHERE Id = @Id;
            ";

        public const string Edit =
            @"
                UPDATE Contact
                SET Address = @Address
                  , Email = @Email
                  , MoreDescription = @MoreDescription
                  , PhoneNumber = @PhoneNumber
                OUTPUT inserted.Id
                WHERE Id = @Id;
            ";

        public const string SetDefault =
            @"
                UPDATE Contact
                SET IsDefault = 0
                WHERE IsDefault = 1;

                UPDATE Contact
                SET IsDefault = 1
                WHERE Id = @Id;
            ";
    }
}
