﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public class StatisticsQueries
    {
        public const string GetActivityUser =
            @"
                SELECT TOP 10 [Date]
                    , COUNT([Date]) AS TotalAccess
                FROM Activity
                WHERE ActivityType = 1
                GROUP BY [Date]
                ORDER BY [Date] DESC;
            ";

        public const string GetDrivesBelongToCompany =
            @"
                SELECT TOP (@Top)
	                c.Id AS CompanyId
                    , c.Name AS CompanyName
                    , ISNULL(SUM(cd.FileSize), 0) AS TotalSize
                FROM Company c
                LEFT JOIN CompanyDrive cd ON c.Id = cd.CompanyId
                GROUP BY c.Id, c.Name
            ";

        public const string GetRegisteredUser =
            @"
                SELECT TOP 10 [Date]
                    , COUNT([Date]) AS TotalRegisteredUser
                FROM Activity
                WHERE ActivityType = 2
                GROUP BY [Date]
                ORDER BY [Date] DESC;
            ";

        public const string GetTotalDrives =
            @"
                SELECT SUM(f.FileSize) + SUM(cd.FileSize)
                FROM CompanyDrive cd, AppFile f
            ";

        public const string GetTotalUser =
            @"
                SELECT COUNT(Id)
                FROM [User]
            ";


        public const string GetTotalCompany =
            @"
                SELECT COUNT(Id)
                FROM [Company]
            ";
    }
}
