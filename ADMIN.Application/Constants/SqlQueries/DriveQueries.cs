﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public abstract class DriveQueries
    {
        public const string GetSize = @"
            SELECT SUM(FileSize)
            FROM [dbo].[AppFile]
            WHERE @Total IS NULL OR CompanyId = @CompanyId
        ";
    }
}