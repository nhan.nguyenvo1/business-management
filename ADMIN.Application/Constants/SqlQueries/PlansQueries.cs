﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public class PlansQueries
    {
        public const string Get =
            @"
                SELECT p.Id
	                , p.Name
                    , p.NumOfEmployee
                    , p.SizeOfDrive
                    , p.Cost
                    , p.IsDefault
                    , COUNT(u.Id) AS UsedUser
                FROM [dbo].[Package] p
                LEFT JOIN [dbo].[User] u ON u.PackageId = p.Id
                WHERE p.[Deleted] = 0
			    GROUP BY p.Id
	                , p.Name
                    , p.NumOfEmployee
                    , p.SizeOfDrive
                    , p.Cost
                    , p.IsDefault
			    ORDER BY p.IsDefault DESC
                        , COUNT(u.Id) DESC
                        , p.Name ASC
            ";

        public const string GetById =
            @"
                SELECT p.Id
	                , p.Name
                    , p.NumOfEmployee
                    , p.SizeOfDrive
                    , p.Cost
                    , COUNT(u.Id) AS UsedUser
                FROM [dbo].[Package] p
                LEFT JOIN [dbo].[User] u ON u.PackageId = p.Id
                WHERE p.[Deleted] = 0
			    GROUP BY p.Id
	                , p.Name
                    , p.NumOfEmployee
                    , p.SizeOfDrive
                    , p.Cost
                HAVING p.Id = @Id
			    ORDER BY COUNT(u.Id) DESC
                        , p.Name ASC
            ";

        public const string Create =
            @"
                INSERT INTO [dbo].[Package] ([Name], [NumOfEmployee], [SizeOfDrive], [Cost])
                OUTPUT inserted.Id
                VALUES (@Name, @NumOfEmployee, @SizeOfDrive, @Cost)
            ";

        public const string Delete =
            @"
                UPDATE [dbo].[Package]
                SET Deleted = 1
                WHERE Id IN @Ids
            ";

        public const string Update =
            @"
                UPDATE [dbo].[Package]
                SET Name = @Name
                    , NumOfEmployee = @NumOfEmployee
                    , SizeOfDrive = @SizeOfDrive
                    , Cost = @Cost
                OUTPUT inserted.Id
                WHERE Id = @Id AND Deleted = 0
            ";

        public const string SetDefault =
            @"
                UPDATE [dbo].[Package]
                SET IsDefault = 0
                WHERE IsDefault = 1;
                
                UPDATE [dbo].[Package]
                SET IsDefault = 1
                WHERE Id = @PlanId AND Deleted = 0;
            ";
    }
}