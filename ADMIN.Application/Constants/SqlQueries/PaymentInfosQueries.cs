﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public class PaymentInfosQueries
    {
        public const string GetById =
            @"
                SELECT Id
                    , CardNumber
                    , CardBank
                    , CardHolder
                    , IsDefault
                    , Type
                FROM PaymentInfo
                WHERE Id = @Id;
            ";

        public const string Get =
            @"
                SELECT Id
                    , CardNumber
                    , CardBank
                    , CardHolder
                    , IsDefault
                    , Type
                FROM PaymentInfo
                ORDER BY IsDefault DESC, Id ASC;
            ";

        public const string Create =
            @"
                INSERT INTO PaymentInfo (CardNumber, CardBank, CardHolder, IsDefault, Type)
                OUTPUT inserted.id
                VAlUES (@CardNumber, @CardBank, @CardHolder, 0, @Type);
            ";

        public const string Delete =
            @"
                DELETE FROM PaymentInfo WHERE Id = @Id;
            ";

        public const string Edit =
            @"
                UPDATE PaymentInfo
                SET CardNumber = @CardNumber
                    , CardBank = @CardBank
                    , CardHolder = @CardHolder
                    , Type = @Type
                OUTPUT inserted.Id
                WHERE Id = @Id;
            ";

        public const string SetDefault =
            @"
                UPDATE PaymentInfo
                SET IsDefault = 0
                WHERE IsDefault = 1;

                UPDATE PaymentInfo
                SET IsDefault = 1
                OUTPUT inserted.Id
                WHERE Id = @Id;
            ";
    }
}
