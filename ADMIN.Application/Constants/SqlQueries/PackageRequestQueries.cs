﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public abstract class PackageRequestQueries
    {
        public const string Get = @"
            SELECT u.Id
				, u.FirstName + ' ' + u.LastName AS FullName
				, u.UserName
				, u.PhoneNumber
				, u.Email
				, p.Id AS PackageId
				, p.Name AS PackageName
				, CASE
					WHEN pr.Status = 1 THEN 'WaitingForPayment'
					WHEN pr.Status = 2 THEN 'ReadyToApprove'
					WHEN pr.Status = 4 THEN 'Rejected'
					ELSE 'Closed'
				  END AS Status
				, pr.ClosedDate
				, pr.Id AS RequestPackageId
			FROM [dbo].[PackageRequest] pr
			JOIN [dbo].[Package] p ON pr.PackageId = p.Id
			JOIN [dbo].[User] u ON pr.UserId = u.Id
        ";

        public const string Approve = @"
			UPDATE [dbo].[PackageRequest]
			SET
				Status = @Status,
				ClosedDate = @ClosedDate
			OUTPUT inserted.UserId, inserted.PackageId
			WHERE Id = @RequestPackageId
		";

        public const string Reject = @"
			UPDATE [dbo].[PackageRequest]
			SET
				Status = @Status,
				Reason = @Reason
			OUTPUT inserted.UserId
			WHERE Id = @RequestPackageId
		";

        public const string SetAsPaymented = @"
			UPDATE [dbo].[PackageRequest]
			SET
				Status = @Status
			OUTPUT inserted.UserId
			WHERE Id = @RequestPackageId
		";
    }
}