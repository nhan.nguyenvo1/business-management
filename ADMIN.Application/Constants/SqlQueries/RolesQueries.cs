﻿namespace ADMIN.Application.Constants.SqlQueries
{
    public static class RolesQueries
    {
        public const string GetByRoleId =
            @"
                SELECT rc.ClaimValue
                FROM RoleClaim rc
                JOIN [Role] r ON r.Id = rc.RoleId
                WHERE RoleId = @roleId AND r.CompanyId = @companyId AND rc.ClaimType = @ClaimType
            ";

        public const string GetByUserId =
            @"
                SELECT  
	                r.Id
	                , r.[Name]
	                , CASE
		                WHEN ur.UserId IS NOT NULL THEN 1
		                ELSE 0
	                  END AS Checked
                FROM [Role] r 
                LEFT JOIN [UserRole] ur ON ur.RoleId = r.Id AND ur.UserId = @UserId
                WHERE r.CompanyId = @CompanyId
            ";

        public const string GetByCompanyId =
            @"
                SELECT r.Id, r.[Name], r.Description
                FROM [Role] r
                WHERE r.CompanyId = @CompanyId
            ";

        public const string AddPermission =
            @"
                IF NOT EXISTS (SELECT 1 FROM RoleClaim WHERE RoleId = @RoleId AND ClaimType = @ClaimType AND ClaimValue = @ClaimValue)
                BEGIN
	                INSERT INTO [RoleClaim] (RoleId, ClaimType, ClaimValue)
					                VALUES (@RoleId, @ClaimType, @ClaimValue)
                END
            ";

        public const string RemovePermission =
            @"
                IF EXISTS (SELECT 1 FROM RoleClaim WHERE RoleId = @RoleId AND ClaimType = @ClaimType AND ClaimValue = @ClaimValue)
                BEGIN
                    DELETE [RoleClaim]
                    WHERE RoleId = @RoleId AND ClaimType = @ClaimType AND ClaimValue = @ClaimValue
                END
            ";
    }
}
