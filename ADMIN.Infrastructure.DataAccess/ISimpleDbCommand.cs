﻿using System.Data;

namespace ADMIN.Infrastructure.DataAccess
{
    public interface ISimpleDbCommand<out T>
    {
        /// <summary>
        ///     Gets the command text.
        /// </summary>
        /// <value>
        ///     The command text. If this is just text, then the CommandType should be CommandType.Text; if this is a stored
        ///     procedure name, then use CommandType.StoredProcedure.
        ///     I think you can also use CommandType.TableDirect and use a table name, but I've actually yet to try that.
        /// </value>
        string CommandText { get; }

        /// <summary>
        ///     Gets the command timeout.
        /// </summary>
        /// <value>
        ///     The command timeout. This is optional. If no value is set, then the default is used (though I'm not sure where this
        ///     comes from).
        /// </value>
        int? CommandTimeout { get; }

        /// <summary>
        ///     Gets the type of the command.
        /// </summary>
        /// <value>
        ///     The type of the command. This is identical to IDbCommand.CommandType: Text, StoredProcedure (and TableDirect).
        /// </value>
        CommandType CommandType { get; }

        /// <summary>
        ///     Gets the parameters.
        /// </summary>
        /// <value>
        ///     The parameters. See Dapper. The basic idea is that this is a class or, more likely, a dynamic object
        ///     where the property names match the @parameters in the query.
        /// </value>
        T Parameters { get; }
    }
}