﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ADMIN.Infrastructure.DataAccess
{
    public static class QueryExecutorAsyncExtensions
    {
        /// <summary>
        ///     Fetches a single row. Use this as a convenience when you know your query should not return more than 1 row.
        ///     Returns null if no rows are found. This will thrown an exception if more than 1 row is returned.
        /// </summary>
        /// <typeparam name="T">The class or value for data set being returned. Can be 'dynamic'.</typeparam>
        /// <param name="executor">The executor.</param>
        /// <param name="command">The command.</param>
        /// <param name="transaction">Optional transaction; if this is not set, then a new connection will be created and used.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        ///     executor
        ///     or
        ///     command
        /// </exception>
        public static async Task<T> FetchOne<T>(this IQueryExecutorAsync executor, ISimpleDbCommand<object> command,
            IDbTransaction transaction = null)
        {
            var results = await executor
                .Fetch<T>(command, transaction);
            return results.SingleOrDefault();
        }

        /// <summary>
        ///     Fetches a collection of the given type using the command.
        /// </summary>
        /// <typeparam name="T">The class or value for data set being returned. Can be 'dynamic'.</typeparam>
        /// <param name="executor">The executor.</param>
        /// <param name="commandText">The command text.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="transaction">Optional transaction; if this is not set, then a new connection will be created and used.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        ///     executor
        ///     or
        ///     commandText
        /// </exception>
        public static Task<IReadOnlyCollection<T>> Fetch<T>(
            this IQueryExecutorAsync executor,
            string commandText,
            object parameters,
            IDbTransaction transaction = null,
            CommandType commandType = CommandType.Text,
            int? commandTimeout = null
        )
        {
            var command = new SimpleDbCommand
            {
                CommandText = commandText,
                Parameters = parameters,
                CommandType = commandType,
                CommandTimeout = commandTimeout
            };
            return executor.Fetch<T>(command, transaction);
        }

        /// <summary>
        ///     Executes the command with no return value.
        /// </summary>
        /// <param name="executor">The executor.</param>
        /// <param name="commandText">The command text.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="transaction">Optional transaction; if this is not set, then a new connection will be created and used.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        ///     executor
        ///     or
        ///     commandText
        /// </exception>
        public static Task Execute(
            this IQueryExecutorAsync executor,
            string commandText,
            object parameters,
            IDbTransaction transaction = null,
            CommandType commandType = CommandType.Text,
            int? commandTimeout = null
        )
        {
            var command = new SimpleDbCommand
            {
                CommandText = commandText,
                Parameters = parameters,
                CommandType = commandType,
                CommandTimeout = commandTimeout
            };
            return executor.Execute(command, transaction);
        }

        /// <summary>
        ///     Fetches a single row. Use this as a convenience when you know your query should not return more than 1 row.
        ///     Returns null if no rows are found. This will thrown an exception if more than 1 row is returned.
        /// </summary>
        /// <typeparam name="T">The class or value for data set being returned. Can be 'dynamic'.</typeparam>
        /// <param name="executor">The executor.</param>
        /// <param name="commandText">The command text.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="transaction">Optional transaction; if this is not set, then a new connection will be created and used.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        ///     executor
        ///     or
        ///     commandText
        /// </exception>
        public static Task<T> FetchOne<T>(
            this IQueryExecutorAsync executor,
            string commandText,
            object parameters,
            IDbTransaction transaction = null,
            CommandType commandType = CommandType.Text,
            int? commandTimeout = null
        )
        {
            var command = new SimpleDbCommand
            {
                CommandText = commandText,
                Parameters = parameters,
                CommandType = commandType,
                CommandTimeout = commandTimeout
            };
            return executor.FetchOne<T>(command, transaction);
        }

        /// <summary>
        ///     Fetches the collections of the given types using a single command.
        ///     Caller is responsible for ensuring that parameters match the function.
        ///     T1 and T2 can be dynamic if you do not want to use a pre-defined Data class.
        /// </summary>
        /// <typeparam name="T1">The class or value for the first data set being returned. Can be 'dynamic'.</typeparam>
        /// <typeparam name="T2">The class or value for the second data set being returned. Can be 'dynamic'.</typeparam>
        /// <param name="executor">The executor.</param>
        /// <param name="commandText">The command text.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="transaction">Optional transaction; if this is not set, then a new connection will be created and used.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <returns></returns>
        public static Task<Tuple<IReadOnlyCollection<T1>, IReadOnlyCollection<T2>>> FetchMultiple<T1, T2>(
            this IQueryExecutorAsync executor,
            string commandText,
            object parameters,
            IDbTransaction transaction = null,
            CommandType commandType = CommandType.Text,
            int? commandTimeout = null
        )
        {
            var command = new SimpleDbCommand
            {
                CommandText = commandText,
                Parameters = parameters,
                CommandType = commandType,
                CommandTimeout = commandTimeout
            };
            return executor.FetchMultiple<T1, T2>(command, transaction);
        }

        public static Task<T> ExecuteScalar<T>(
            this IQueryExecutorAsync executor,
            string commandText,
            object parameters,
            IDbTransaction transaction = null,
            CommandType commandType = CommandType.Text,
            int? commandTimeout = null
        )
        {
            var command = new SimpleDbCommand
            {
                CommandText = commandText,
                Parameters = parameters,
                CommandType = commandType,
                CommandTimeout = commandTimeout
            };
            return executor.ExecuteScalar<T>(command, transaction);
        }
    }
}