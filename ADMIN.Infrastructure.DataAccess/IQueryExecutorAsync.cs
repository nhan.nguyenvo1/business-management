﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;

namespace ADMIN.Infrastructure.DataAccess
{
    public interface IQueryExecutorAsync
    {
        /// <summary>
        ///     Fetches a collection of the given type using the command.
        ///     Caller is responsible for ensuring that parameters match the command (see Dapper documentation for more details).
        /// </summary>
        /// <typeparam name="T">The class or value for data set being returned. Can be 'dynamic'.</typeparam>
        /// <param name="command">The command.</param>
        /// <param name="transaction">Optional transaction; if this is not set, then a new connection will be created and used.</param>
        /// <returns></returns>
        Task<IReadOnlyCollection<T>> Fetch<T>(ISimpleDbCommand<object> command, IDbTransaction transaction = null);

        /// <summary>
        ///     Executes the command with no return value.
        ///     Caller is responsible for ensuring that parameters match the command (see Dapper documentation for more details).
        /// </summary>
        /// <typeparam name="TParameters">
        ///     The type of the Parameters property for the command. For now, this should be either
        ///     object or DynamicParameters.
        /// </typeparam>
        /// <param name="command">The command.</param>
        /// <param name="transaction">Optional transaction; if this is not set, then a new connection will be created and used.</param>
        /// <returns></returns>
        Task Execute<TParameters>(ISimpleDbCommand<TParameters> command, IDbTransaction transaction = null);

        /// <summary>
        ///     Fetches the collections of the given types using a single command.
        ///     Caller is responsible for ensuring that parameters match the function.
        ///     T1 and T2 can be dynamic if you do not want to use a pre-defined Data class.
        /// </summary>
        /// <typeparam name="T1">The class or value for the first data set being returned. Can be 'dynamic'.</typeparam>
        /// <typeparam name="T2">The class or value for the second data set being returned. Can be 'dynamic'.</typeparam>
        /// <param name="command">The command.</param>
        /// <param name="transaction">Optional transaction; if this is not set, then a new connection will be created and used.</param>
        /// <returns></returns>
        Task<Tuple<IReadOnlyCollection<T1>, IReadOnlyCollection<T2>>> FetchMultiple<T1, T2>(
            ISimpleDbCommand<object> command, IDbTransaction transaction = null);

        /// <summary>
        ///     Executes the multiple commands sequentially wrapped inside a transaction.
        ///     Note that is designed to run a set of 'self-contained' queries inside a single transaction;
        ///     it is not designed to handle nested transactions. If we find that we need this someone should
        ///     update this probably by allowing the parent transaction to be passed in.
        /// </summary>
        /// <param name="commands">The list of commands with parameters.</param>
        /// <returns></returns>
        Task ExecuteSequentiallyInTransaction(params ISimpleDbCommand<object>[] commands);

        Task<T> ExecuteScalar<T>(ISimpleDbCommand<object> command, IDbTransaction transaction);

        Task<T> FetchMultipleResults<T>(ISimpleDbCommand<object> command, Func<SqlMapper.GridReader, Task<T>> func,
            IDbTransaction transaction = null);

        Task<T> ExecuteInTransactionAsync<T>(Func<IDbTransaction, Task<T>> executionContext,
            IDbTransaction transaction = null);

        Task ExecuteInTransactionAsync(Func<IDbTransaction, Task> executionContext, IDbTransaction transaction = null);

        Task ExecuteSequentiallyInTransaction(IDbTransaction dbTransaction, params ISimpleDbCommand<object>[] commands);
    }
}