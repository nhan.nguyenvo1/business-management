﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ADMIN.Infrastructure.DataAccess
{
    public static class ServiceExtension
    {
        public static void AddInfrastructureDataAccessLayer(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddSingleton<IConnectionFactory, DefaultOperationalDbConnectionFactory>();
            services.AddSingleton<ISimpleDbCommand<object>, SimpleDbCommand>();
            services.AddTransient<IQueryExecutorAsync, QueryExecutorAsync>();
        }
    }
}