﻿using System.Data;

namespace ADMIN.Infrastructure.DataAccess
{
    public interface IConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}