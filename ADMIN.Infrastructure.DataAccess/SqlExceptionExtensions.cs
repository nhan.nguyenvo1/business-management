﻿using System.Data.SqlClient;

namespace ADMIN.Infrastructure.DataAccess
{
    public static class SqlExceptionExtensions
    {
        private const int UniqueIndexViolationNumber = 2601;
        private const int UniqueConstraintViolationNumber = 2627;

        public static bool IsUniqueViolationException(this SqlException ex)
        {
            return ex?.Number == UniqueIndexViolationNumber || ex?.Number == UniqueConstraintViolationNumber;
        }
    }
}