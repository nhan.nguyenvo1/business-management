﻿using System.Data;

namespace ADMIN.Infrastructure.DataAccess
{
    public class SimpleDbCommand : ISimpleDbCommand<object>
    {
        public SimpleDbCommand()
        {
            CommandType = CommandType.Text;
        }

        public SimpleDbCommand(string commandText, object parameters) : this()
        {
            CommandText = commandText;
            Parameters = parameters;
        }

        /// <inheritdoc />
        public string CommandText { get; set; }

        /// <inheritdoc />
        public int? CommandTimeout { get; set; }

        /// <inheritdoc />
        public CommandType CommandType { get; set; }

        /// <inheritdoc />
        public object Parameters { get; set; }
    }
}