﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace ADMIN.Infrastructure.DataAccess
{
    public class DefaultOperationalDbConnectionFactory : IConnectionFactory
    {
        private readonly string _connectionString;

        public DefaultOperationalDbConnectionFactory(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("ProjectDbConnectionString");
        }

        public IDbConnection CreateConnection()
        {
            if (string.IsNullOrEmpty(_connectionString))
                throw new ArgumentNullException("Missing ProjectDbConnectionString Argument");

            return new SqlConnection(_connectionString);
        }
    }
}