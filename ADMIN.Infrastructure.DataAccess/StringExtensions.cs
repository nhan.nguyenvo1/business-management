﻿using Dapper;

namespace ADMIN.Infrastructure.DataAccess
{
    public static class StringExtensions
    {
        /// <summary>
        ///     Return an ANSI DbString parameter for Dapper queries against a varchar column.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static DbString ToVarCharString(this string parameter)
        {
            return new() {Value = parameter, IsAnsi = true};
        }

        public static DbString ToVarCharString(this string parameter, int length)
        {
            return new() {Value = parameter, IsAnsi = true, Length = length};
        }

        /// <summary>
        ///     Return an non-ANSI DbString parameter for Dapper queries against a nvarchar column.
        ///     Note: This is the default so usage of this is purely ornamental.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public static DbString ToNVarCharString(this string parameter)
        {
            return new() {Value = parameter, IsAnsi = false};
        }
    }
}