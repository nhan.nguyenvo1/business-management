﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;

namespace ADMIN.Infrastructure.DataAccess
{
    public class QueryExecutorAsync : IQueryExecutorAsync
    {
        private readonly IConnectionFactory connectionFactory;

        /// <summary>
        ///     Constructor.
        /// </summary>
        /// <param name="connectionFactory"></param>
        public QueryExecutorAsync(IConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }

        // <inheritdoc />
        public async Task<T> FetchMultipleResults<T>(ISimpleDbCommand<object> command,
            Func<SqlMapper.GridReader, Task<T>> func, IDbTransaction transaction)
        {
            using var localConnection = transaction == null ? connectionFactory.CreateConnection() : null;
            var connection = transaction?.Connection ?? localConnection;
            using var results = await connection.QueryMultipleAsync(command.CommandText, command.Parameters,
                transaction, command.CommandTimeout,
                command.CommandType);
            return await func(results);
        }

        /// <inheritdoc />
        public async Task<IReadOnlyCollection<T>> Fetch<T>(ISimpleDbCommand<object> command, IDbTransaction transaction)
        {
            using var localConnection = transaction == null ? connectionFactory.CreateConnection() : null;
            var connection = transaction?.Connection ?? localConnection;
            var results = await connection.QueryAsync<T>(command.CommandText, command.Parameters, transaction,
                command.CommandTimeout,
                command.CommandType);

            return results.AsList();
        }

        /// <inheritdoc />
        public async Task<Tuple<IReadOnlyCollection<T1>, IReadOnlyCollection<T2>>> FetchMultiple<T1, T2>(
            ISimpleDbCommand<object> command,
            IDbTransaction transaction)
        {
            using var localConnection = transaction == null ? connectionFactory.CreateConnection() : null;
            var connection = transaction?.Connection ?? localConnection;
            var dataSet = await connection.QueryMultipleAsync(command.CommandText, command.Parameters, transaction,
                command.CommandTimeout, command.CommandType);
            var items1 = await dataSet.ReadAsync<T1>();
            var items2 = await dataSet.ReadAsync<T2>();
            return new Tuple<IReadOnlyCollection<T1>, IReadOnlyCollection<T2>>(items1.AsList(), items2.AsList());
        }

        /// <inheritdoc />
        public async Task Execute<TParameters>(ISimpleDbCommand<TParameters> command, IDbTransaction transaction)
        {
            using var localConnection = transaction == null ? connectionFactory.CreateConnection() : null;
            var connection = transaction?.Connection ?? localConnection;
            await connection.ExecuteAsync(command.CommandText, command.Parameters, transaction, command.CommandTimeout,
                command.CommandType);
        }

        public async Task ExecuteSequentiallyInTransaction(IDbTransaction dbTransaction,
            params ISimpleDbCommand<object>[] commands)
        {
            await ExecuteInTransactionAsync(async transaction =>
            {
                foreach (var command in commands) await Execute(command, transaction);
            }, dbTransaction);
        }

        /// <inheritdoc />
        public Task ExecuteSequentiallyInTransaction(params ISimpleDbCommand<object>[] commands)
        {
            return ExecuteSequentiallyInTransaction(null, commands);
        }

        /// <inheritdoc />
        public async Task<T> ExecuteScalar<T>(ISimpleDbCommand<object> command, IDbTransaction transaction)
        {
            using var localConnection = transaction == null ? connectionFactory.CreateConnection() : null;
            var connection = transaction?.Connection ?? localConnection;
            return await connection.ExecuteScalarAsync<T>(command.CommandText, command.Parameters, transaction,
                command.CommandTimeout, command.CommandType);
        }

        public async Task<T> ExecuteInTransactionAsync<T>(Func<IDbTransaction, Task<T>> executionContext,
            IDbTransaction transaction)
        {
            using var localConnection = transaction == null ? connectionFactory.CreateConnection() : null;
            if (transaction == null) localConnection.Open();

            using var localTransaction = transaction == null ? localConnection.BeginTransaction() : null;
            try
            {
                var result = await executionContext(transaction ?? localTransaction);

                localTransaction?.Commit();
                return result;
            }
            catch
            {
                Rollback(localTransaction);
                throw;
            }
        }

        public async Task ExecuteInTransactionAsync(Func<IDbTransaction, Task> executionContext,
            IDbTransaction transaction)
        {
            using var localConnection = transaction == null ? connectionFactory.CreateConnection() : null;
            if (transaction == null) localConnection.Open();

            using var localTransaction = transaction == null ? localConnection.BeginTransaction() : null;
            try
            {
                await executionContext(transaction ?? localTransaction);
                localTransaction?.Commit();
            }
            catch
            {
                Rollback(localTransaction);
                throw;
            }
        }

        private static void Rollback(IDbTransaction localTransaction)
        {
            try
            {
                localTransaction?.Rollback();
            }
            catch (InvalidOperationException e)
            {
                //do not throw here because we've rolled back more than once and it will cause real errors to be hidden
                if (e.HResult != -2146233079) throw;
            }

            localTransaction?.Dispose();
        }
    }
}