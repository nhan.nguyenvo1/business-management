﻿using ADMIN.Application.Interfaces;
using ADMIN.Infrastructure.Shared.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Domain.Settings;
using PROJECT.Infrastructure.Shared.Services;

namespace PROJECT.Infrastructure.Shared
{
    public static class ServiceExtension
    {
        public static void AddSharedInfrastructure(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<MailSetting>(config.GetSection("MailSetting"));
            services.Configure<JwtSetting>(config.GetSection("JwtSetting"));
            services.Configure<CloudinarySetting>(config.GetSection("CloudinarySetting"));
            services.Configure<ClientSetting>(config.GetSection("ClientSetting"));
            services.Configure<TimeZoneSetting>(config.GetSection("TimeZoneSetting"));

            services.AddTransient<IDateTimeService, DateTimeService>();
            services.AddTransient<IEmailService, EmailService>();
        }
    }
}