﻿using System;
using ADMIN.Application.Interfaces;
using Microsoft.Extensions.Options;
using PROJECT.Domain.Settings;

namespace ADMIN.Infrastructure.Shared.Services
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime UtcNow { get; private set; }

        public DateTimeService(IOptions<TimeZoneSetting> timeZoneSetting)
        {
            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneSetting.Value.Id);

            UtcNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);
        }
    }
}