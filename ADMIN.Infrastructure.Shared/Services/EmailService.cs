﻿using System;
using System.Threading.Tasks;
using ADMIN.Application.Dtos.Email;
using ADMIN.Application.Interfaces;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using PROJECT.Domain.Settings;

namespace PROJECT.Infrastructure.Shared.Services
{
    public class EmailService : IEmailService
    {
        public EmailService(IOptions<MailSetting> mailSettings, ILogger<EmailService> logger)
        {
            MailSettings = mailSettings.Value;
            Logger = logger;
        }

        private MailSetting MailSettings { get; }
        private ILogger<EmailService> Logger { get; }

        public async Task<bool> SendAsync(EmailRequest request)
        {
            try
            {
                // create message
                var email = new MimeMessage {Sender = MailboxAddress.Parse(MailSettings.EmailFrom)};
                email.To.Add(MailboxAddress.Parse(request.To));
                email.Subject = request.Subject;
                var builder = new BodyBuilder {HtmlBody = request.Body};
                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                await smtp.ConnectAsync(MailSettings.SmtpHost, MailSettings.SmtpPort, SecureSocketOptions.StartTls);
                await smtp.AuthenticateAsync(MailSettings.SmtpUser, MailSettings.SmtpPass);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);

                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError("Error: {Ex}", ex);
                return false;
            }
        }
    }
}