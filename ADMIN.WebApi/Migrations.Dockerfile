FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src
COPY ["ADMIN.WebApi/ADMIN.WebApi.csproj", "ADMIN.WebApi/"]
COPY ["ADMIN.WebApi/Setup.sh", "ADMIN.WebApi/"]
COPY ["ADMIN.Infrastructure.Shared/ADMIN.Infrastructure.Shared.csproj", "ADMIN.Infrastructure.Shared/"]
COPY ["ADMIN.Application/ADMIN.Application.csproj", "ADMIN.Application/"]
COPY ["PROJECT.Domain/PROJECT.Domain.csproj", "PROJECT.Domain/"]
COPY ["ADMIN.Infrastructure.DataAccess/ADMIN.Infrastructure.DataAccess.csproj", "ADMIN.Infrastructure.DataAccess/"]
COPY ["ADMIN.Infrastructure.Identity/ADMIN.Infrastructure.Identity.csproj", "ADMIN.Infrastructure.Identity/"]
RUN dotnet restore "ADMIN.WebApi/ADMIN.WebApi.csproj"

RUN dotnet tool install --global dotnet-ef

COPY . .
WORKDIR "/src/ADMIN.Infrastructure.Identity"

RUN /root/.dotnet/tools/dotnet-ef migrations add InitialMigrations --startup-project ../ADMIN.WebApi

WORKDIR "/src/ADMIN.WebApi"
RUN chmod +x ./Setup.sh
CMD /bin/bash ./Setup.sh