﻿using ADMIN.Application.Interfaces;
using ADMIN.WebApi.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ADMIN.WebApi.Installers
{
    public class DIInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IAuthUserService, AuthUserService>();
        }
    }
}