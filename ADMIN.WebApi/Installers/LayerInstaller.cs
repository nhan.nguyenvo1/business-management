﻿using ADMIN.Application;
using ADMIN.Infrastructure.DataAccess;
using ADMIN.Infrastructure.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PROJECT.Infrastructure.Shared;

namespace ADMIN.WebApi.Installers
{
    public class LayerInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSharedInfrastructure(configuration);
            services.AddAdminIdentityInfracstructure(configuration);
            services.AddInfrastructureDataAccessLayer(configuration);
            services.AddApplicationLayer();
        }
    }
}