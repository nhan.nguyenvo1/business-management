﻿using ADMIN.Application.Features.PaymentInfos.Commands.CreatePaymentInfo;
using ADMIN.Application.Features.PaymentInfos.Commands.DeletePaymentInfo;
using ADMIN.Application.Features.PaymentInfos.Commands.EditPaymentInfo;
using ADMIN.Application.Features.PaymentInfos.Commands.SetPaymentInfoDefault;
using ADMIN.Application.Features.PaymentInfos.Queries.GetPaymentInfoById;
using ADMIN.Application.Features.PaymentInfos.Queries.GetPaymentInfos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ADMIN.WebApi.Controllers.v1
{
    [Authorize]
    public class PaymentInfosController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetPaymentInfosQuery()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetPaymentInfoByIdQuery { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreatePaymentInfoCommand command)
        {
            var response = await Mediator.Send(command);
            return CreatedAtAction(nameof(GetById), new { Id = response.Data }, command);
        }

        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] EditPaymentInfoCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeletePaymentInfoCommand { Id = id }));
        }

        [HttpPut("{id}/set-default")]
        public async Task<IActionResult> SetDefault(int id)
        {
            return Ok(await Mediator.Send(new SetPaymentInfoDefaultCommand { Id = id }));
        }
    }
}
