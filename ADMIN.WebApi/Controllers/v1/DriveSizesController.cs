﻿using System.Threading.Tasks;
using ADMIN.Application.Features.DriveSizes.Queries.GetTotalDriveSize;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ADMIN.WebApi.Controllers.v1
{
    public class DriveSizesController : BaseController
    {
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetTotalSize()
        {
            return Ok(await Mediator.Send(new GetTotalDriveSizeQuery {CompanyId = null}));
        }

        [HttpGet("companies/{id}")]
        [Authorize]
        public async Task<IActionResult> GetSizeOfCompany(int id)
        {
            return Ok(await Mediator.Send(new GetTotalDriveSizeQuery {CompanyId = id}));
        }
    }
}