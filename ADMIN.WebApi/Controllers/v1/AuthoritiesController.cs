﻿using ADMIN.Application.Features.Permissions.Queries.GetPermissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ADMIN.WebApi.Controllers.v1
{
    [Authorize]
    public class AuthoritiesController : BaseController
    {
        [HttpGet("roles/{roleId}/companies/{companyId}")]
        public async Task<IActionResult> Get(string roleId, int companyId)
        {
            return Ok(await Mediator.Send(new GetPermissionQuery
            {
                CompanyId = companyId,
                RoleId = roleId
            }));
        }
    }
}
