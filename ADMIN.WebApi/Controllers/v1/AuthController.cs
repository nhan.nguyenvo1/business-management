﻿using System.Threading.Tasks;
using ADMIN.Application.Dtos.Account;
using ADMIN.Application.Interfaces;
using ADMIN.WebApi.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ADMIN.WebApi.Controllers.v1
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("sign-in")]
        public async Task<IActionResult> SignInAsync([FromBody] AuthRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.SignInAsync(request);

            return Ok(response);
        }

        [HttpPost("refresh-token")]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshTokenAsync([FromBody] RefreshTokenRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.RefreshTokenAsync(request);

            return Ok(response);
        }

        [HttpPost("revoke-token")]
        [Authorize]
        public async Task<IActionResult> RevokeTokenAsync([FromBody] RevokeTokenRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.RevokeTokenAsync(request);

            return Ok(response);
        }

        [HttpPost("change-password")]
        [Authorize]
        public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePasswordRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.ChangePasswordAsync(request);

            return Ok(response);
        }

        [HttpPost("forgot-password")]
        public async Task<IActionResult> ForgotPasswordAsync([FromBody] ForgotPasswordRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.ForgotPasswordAsync(request);

            return Ok(response);
        }

        [HttpPost("verify-email")]
        public async Task<IActionResult> VerifyEmailAsync([FromBody] VerifyEmailRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.VerifyEmailAsync(request);

            return Ok(response);
        }

        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordRequest request)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState.GetModelError());

            var response = await _authService.ResetPasswordAsync(request);

            return Ok(response);
        }

        [HttpGet("me")]
        [Authorize]
        public async Task<IActionResult> GetMeAsync()
        {
            var response = await _authService.GetMeAsync();

            return Ok(response);
        }
    }
}