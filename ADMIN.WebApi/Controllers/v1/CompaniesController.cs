﻿using System.Threading.Tasks;
using ADMIN.Application.Features.Companies.Commands.BlockCompany;
using ADMIN.Application.Features.Companies.Commands.UnblockCompany;
using ADMIN.Application.Features.Companies.Queries.GetCompanies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ADMIN.WebApi.Controllers.v1
{
    public class CompaniesController : BaseController
    {
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get([FromQuery] GetCompaniesQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        [HttpPost("block")]
        [Authorize]
        public async Task<IActionResult> Block([FromBody] BlockCompanyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("unblock")]
        [Authorize]
        public async Task<IActionResult> Block([FromBody] UnblockCompanyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}