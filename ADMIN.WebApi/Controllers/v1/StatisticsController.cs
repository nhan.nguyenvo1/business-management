﻿using ADMIN.Application.Dtos.Statistic;
using ADMIN.Application.Enums;
using ADMIN.Application.Features.Statistics.Queries.GetActivityUsers;
using ADMIN.Application.Features.Statistics.Queries.GetDrivesByCompanyId;
using ADMIN.Application.Features.Statistics.Queries.GetRegisteredUsers;
using ADMIN.Application.Features.Statistics.Queries.GetTotalCompany;
using ADMIN.Application.Features.Statistics.Queries.GetTotalDrives;
using ADMIN.Application.Features.Statistics.Queries.GetTotalUsers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ADMIN.WebApi.Controllers.v1
{
    [Authorize]
    public class StatisticsController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> GetByCriteria([FromQuery] GetByCriteriaQuery query)
        {
            return query.Criteria switch
            {
                Criteria.ActivityUsers => Ok(await Mediator.Send(new GetActivityUsersQuery())),
                Criteria.DrivesBelongsToCompany => Ok(await Mediator.Send(new GetDrivesByCompanyIdQuery { Top = query.Top })),
                Criteria.RegisteredUsers => Ok(await Mediator.Send(new GetRegisteredUsersQuery())),
                Criteria.TotalDrives => Ok(await Mediator.Send(new GetTotalDriveQuery())),
                Criteria.TotalUsers => Ok(await Mediator.Send(new GetTotalUsersQuery())),
                Criteria.TotalCompany => Ok(await Mediator.Send(new GetTotalCompanyQuery())),
                _ => Ok(await Mediator.Send(new GetActivityUsersQuery())),
            };
        }
    }
}
