using System.Threading.Tasks;
using ADMIN.Application.Features.Plans.Commands.CreatePlan;
using ADMIN.Application.Features.Plans.Commands.DeletePlans;
using ADMIN.Application.Features.Plans.Commands.SetDefault;
using ADMIN.Application.Features.Plans.Commands.UpdatePlan;
using ADMIN.Application.Features.Plans.Queries.GetPlanById;
using ADMIN.Application.Features.Plans.Queries.GetPlans;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ADMIN.WebApi.Controllers.v1
{
    [Authorize]
    public class PlansController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetPlansQuery()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetPlanByIdQuery
            {
                Id = id
            }));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreatePlanCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] UpdatePlanCommand command)
        {
            command.Id = id;
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("delete")]
        public async Task<IActionResult> Delete([FromBody] DeletePlansCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("{id}/set-default")]
        public async Task<IActionResult> SetDefault(int id)
        {
            return Ok(await Mediator.Send(new SetDefaultCommand
            {
                PlanId = id
            }));
        }
    }
}