﻿using ADMIN.Application.Features.Roles.Commands.UpdateRolePermissions;
using ADMIN.Application.Features.Roles.Queries.GetRolesByCompanyId;
using ADMIN.Application.Features.Roles.Queries.GetRolesByUserId;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ADMIN.WebApi.Controllers.v1
{
    [Authorize]
    public class RolesController : BaseController
    {
        [HttpGet("companies/{companyId}")]
        public async Task<IActionResult> Get(int companyId)
        {
            return Ok(await Mediator.Send(new GetRolesByCompanyIdQuery
            {
                CompanyId = companyId
            }));
        }

        [HttpGet("companies/{companyId}/users/{userId}")]
        public async Task<IActionResult> GetByUserId(int companyId, string userId)
        {
            return Ok(await Mediator.Send(new GetRolesByUserIdQuery
            {
                CompanyId = companyId,
                UserId = userId
            }));
        }

        [HttpPost("permissions")]
        public async Task<IActionResult> UpdatePermissions([FromBody] UpdateRolePermissionsCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
