﻿using ADMIN.Application.Features.Contacts.Commands.CreateContact;
using ADMIN.Application.Features.Contacts.Commands.DeleteContact;
using ADMIN.Application.Features.Contacts.Commands.EditContact;
using ADMIN.Application.Features.Contacts.Commands.SetDefaultContact;
using ADMIN.Application.Features.Contacts.Queries.GetContactById;
using ADMIN.Application.Features.Contacts.Queries.GetContacts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ADMIN.WebApi.Controllers.v1
{
    [Authorize]
    public class ContactsController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await Mediator.Send(new GetContactsQuery()));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetContactByIdQuery { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateContactCommand command)
        {
            var response = await Mediator.Send(command);
            return CreatedAtAction(nameof(GetById), new { Id = response.Data }, command);
        }

        [HttpPut]
        public async Task<IActionResult> Edit([FromBody] EditContactCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteContactCommand { Id = id }));
        }

        [HttpPut("{id}/set-default")]
        public async Task<IActionResult> SetDefault(int id)
        {
            return Ok(await Mediator.Send(new SetDefaultContactCommand { Id = id }));
        }
    }
}
