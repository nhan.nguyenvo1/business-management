﻿using System.Threading.Tasks;
using ADMIN.Application.Features.Users.Commands.UpdateUserRoles;
using ADMIN.Application.Features.Users.Queries.GetUsers;
using ADMIN.Application.Features.Users.Queries.GetUsersByCompanyId;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ADMIN.WebApi.Controllers.v1
{
    [Authorize]
    public class UsersController : BaseController
    {
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetUsersQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        [HttpGet("companies/{id}")]
        public async Task<IActionResult> GetByCompanyId(int id)
        {
            return Ok(await Mediator.Send(new GetUsersByCompanyIdQuery {CompanyId = id}));
        }

        [HttpPost("roles")]
        public async Task<IActionResult> UpdateRoles([FromBody] UpdateUserRolesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}