﻿using ADMIN.Application.Dtos.Account;
using ADMIN.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ADMIN.WebApi.Controllers.v1
{
    public class IntranetsController : BaseController
    {
        private readonly IAuthService _authService;

        public IntranetsController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("users")]
        public async Task<IActionResult> Create([FromBody] RegisterRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
