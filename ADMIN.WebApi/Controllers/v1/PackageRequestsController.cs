﻿using System.Threading.Tasks;
using ADMIN.Application.Features.PlanRequests.Commands.ApproveUpgradeRequest;
using ADMIN.Application.Features.PlanRequests.Commands.RejectUpgradeRequest;
using ADMIN.Application.Features.PlanRequests.Commands.SetAsPaymented;
using ADMIN.Application.Features.PlanRequests.Queries.GetUpgradePlanRequests;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ADMIN.WebApi.Controllers.v1
{
    public class PackageRequestsController : BaseController
    {
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get([FromQuery] GetUpgradePlanRequestsQuery filters)
        {
            return Ok(await Mediator.Send(filters));
        }

        [HttpPost("approve")]
        [Authorize]
        public async Task<IActionResult> Approve([FromBody] ApproveUpgradeRequestCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("reject")]
        [Authorize]
        public async Task<IActionResult> Reject([FromBody] RejectUpgradeRequestCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("set-as-paymented")]
        [Authorize]
        public async Task<IActionResult> SetAsPaymented([FromBody] SetAsPaymentedCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}