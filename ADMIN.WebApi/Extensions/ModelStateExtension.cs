﻿using System.Linq;
using ADMIN.Application.Wrappers;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ADMIN.WebApi.Extensions
{
    public static class ModelStateExtension
    {
        public static Response<string> GetModelError(this ModelStateDictionary modelState)
        {
            var error = modelState.Values.SelectMany(m => m.Errors)
                .Select(m => m.ErrorMessage).First();
            return new Response<string>(error);
        }
    }
}